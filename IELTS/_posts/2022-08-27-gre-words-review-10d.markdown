---
layout: post
title:  "GRE Words 10 Day Review"
date:   2022-08-27 13:52:01 +0800
category: IELTSPosts
---

# New Words (10-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English       | Chinese         | Synonyms                                                      | Helpful Memory Ideas       | Usage |
|---------------|-----------------|---------------------------------------------------------------|----------------------------|-------|
| 1            | 1               | 1                                                                  | 1                           | 1                   |
| fickleness   | 浮躁；变化无常         | inconstancy                                                        |                             |                     |
| deplore      | 悲悼，哀叹；谴责        | bemoan, bewail, grieve, lament, moan                               |                             |                     |
| congenital   | 先天的，天生的         | innate                                                             | vestigial 退化的               |                     |
| defamation   | 诽谤，中伤           | aspersion, calumniation, denigration, vilification                 |                             |                     |
| supplant     | 排挤，取代           |                                                                    | sub + plant                 |                     |
| succinct     | 简明的，简洁的         | compact, precise, concise, compendiary, compendious, curt, laconic | suc + cinct 隐秘性             |                     |
| indigenous   | 土产的，本地的；生来的，固有的 | native, innate                                                     | ingenious 聪明的；善于创造发明的；心灵手巧的 |                     |
| deviant      | 越出常规的           |                                                                    | deviate 偏离；aberrant 越轨的；异常的 |                     |
| sobriety     | 节制；庄重           | moderation, gravity                                                |                             |                     |
| understated  | 轻描淡写的，低调的       | unostentatious, unpretentious                                      |                             |                     |
| repudiate    | 拒绝接受，回绝，抛弃      | dismiss, reprobate, spurn                                          | re + pud + iate             |                     |
| inherent     | 固有的，内在的         |                                                                    | in + her + ent              | be inherent in sth. |
| contemptible | 令人轻视的           |                                                                    | contempt is the n. form     |                     |
| finicky      | 苛求的，过分讲究的       | dainty, fastidious, finicking, too particular or exacting, fussy   |                             |                     |
| acquiescent  | 默认的             |                                                                    | acquiesce is the n. form    |                     |
| agoraphobic  | 患荒野恐惧症的（人）      |                                                                    |                             |                     |
| sedate       | 镇静的             | sober, staid                                                       |                             |                     |
| 2            | 2                       | 2                                                                    | 2                               | 2                          |
| scruple      | 顾忌，迟疑                   | boggle, stickle, an ethical consideration, hesitate                  |                                 |                            |
| resilient    | 有弹性的；能恢复活力的，适应力强的       |                                                                      |                                 |                            |
| remunerative | 报酬高的，有利润的               | gainful, lucrative                                                   |                                 |                            |
| fluffy       | 有绒毛的；无聊的，琐碎的            |                                                                      | fluffy n.                       |                            |
| profligacy   | 放荡；肆意挥霍                 |                                                                      | profligate v.                   |                            |
| instinctive  | 本能的                     |                                                                      | instinct n.. Distinct 清楚的，明显的   |                            |
| allusion     | 暗指，间接提到                 |                                                                      | allude v.                       |                            |
| defiant      | 反抗的，挑衅的                 | bold, impudent                                                       | deviant 越出常规的                   |                            |
| admonish     | 训诫；警告                   | to warn, advise, to reprove mildly                                   |                                 |                            |
| deferential  | 顺从的，恭顺的                 | duteous, dutiful                                                     | deference n.                    |                            |
| denigrate    | 污蔑，诽谤                   | defame, blacken, to disparage the character or reputation of         |                                 |                            |
| fickle       | （尤指在感情方面）易变的，变化无常的，不坚定的 |                                                                      | tickle 痒痒                       |                            |
| reminiscent  | 回忆的；使人联想的               | tending to remind                                                    | reminiscence n.                 | reminiscent of sb. or sth. |
| defer        | 遵从，听从；延期                | yield with courtesy, delay                                           |                                 |                            |
| stint        | 节制，限量，节省                | to restrict                                                          | stint on sth. 吝惜…               |                            |
| proprietary  | 私有的                     | privately owned and managed                                          |                                 |                            |
| repellent    | 令人厌恶的                   | loathsome, odious, repugnant, revolting, arousing disgust, repulsive |                                 |                            |
| trite        | 陈腐的，陈词滥调的               | bathetic, commonplace, hackneyed or boring                           |                                 |                            |
| deign        | 惠允（做某事）；施惠于人            | stoop, to condescend to do sth.                                      | reigning 统治的，起支配作用的；feign 假装，装作 |                            |
| tendentious  | 有偏见的                    |                                                                      | tend 倾向                         |                            |
| conjecture   | 推测，臆测                   | presume, suppose, surmise                                            | juncture 关键时刻，危急关头；结合处，接合点      |                            |
| ingenious    | 聪明的；善于创造发明的；心灵手巧的       | clever, inventive                                                    | ingenuous 纯真的，淳朴的；坦率的           |                            |
| subterranean | 地下的，地表下的                |                                                                      | sub + terranean                 |                            |
| stratify     | （使）层化                   | to divide or arrange into classes, castes, or social strata          |                                 |                            |
| 3            | 3                   | 3                                                                                     | 3                                         | 3                   |
| counteract   | 消除，抵消               | neutralize                                                                            | counter + act                             |                     |
| trickle      | 细流；细细的流             | dribble, drip, filter                                                                 | "fickle （尤指在感情方面）易变的，变化无常的，不坚定的；tickle 痒痒 | "                   |
| anticipatory | 预想的，预期的             |                                                                                       | anticipate v.                             |                     |
| enamored     | 倾心的，被迷住             | bewitched, captivated, charmed, enchanted, infatuated, inflamed with love, fascinated | armored 披甲的，装甲的                           | be enamored of sth. |
| rigorous     | 严格的，严峻的             | austere, rigid, severe, stern, manifesting, exercising, favoring rigor                | vigorous 精力充沛的；有力的                        |                     |
| incredulity  | 怀疑，不相信              | disbelief                                                                             | in + credit                               |                     |
| repeal       | 废除（法律）              |                                                                                       | re + peal                                 |                     |
| stagger      | 蹒跚，摇晃               | lurch, reel, sway, waver, wobble, move on unsteadily                                  |                                           |                     |
| agitation    | 焦虑，不安；公开辩论，鼓动宣传     | anxiety, public argument or action for social or political change                     |                                           |                     |
| charter      | 特权或豁免权              | special privilege or immunity                                                         |                                           |                     |
| articulate   | 清楚的说话；（用关节）连接       | enunciate                                                                             |                                           |                     |
| preclude     | 预防，排除；阻止            | deter, forestall, forfend, obviate, ward, prevent, rule out, exclude                  |                                           |                     |
| frisky       | 活泼的，快活的             | impish, mischievous, sportive, waggish, playful, merry, frolicsome                    |                                           |                     |
| disputable   | 有争议的                | arguable, debatable                                                                   |                                           |                     |
| aromatic     | 芬芳的，芳香的             |                                                                                       | aroma 芳香；incense 香味，激怒                    |                     |
| overblown    | 盛期已过的；残败的；夸张的       | past the prime or bloom, inflated                                                     | overdraw 透支；夸大                            |                     |
| pristine     | 原始的；质朴的，纯洁的；新鲜的，干净的 | prime                                                                                 |                                           |                     |
| voyeur       | 窥淫癖者                |                                                                                       |                                           |                     |
| impecunious  | 不名一文的；没钱的           | having very little or no money                                                        |                                           |                     |
| consign      | 托运，托人看管             | con + sign                                                                            |                                           |                     |
| epitome      | 典型；梗概               | showing all the typical qualities of sth., abstract, summary, abridgment              |                                           |                     |
| disparage    | 贬低，轻蔑               | belittle, derogate, to speak slightingly of, depreciate, decry                        | separate 分离                               |                     |
| pitiful      | 值得同情的，可怜的           | deserving pity                                                                        |                                           |                     |
| tributary    | 支流，进贡国；支流的，辅助的，进贡的  | making additions or yielding supplies, contributory                                   |                                           |                     |
| tawdry       | 华而不实的，俗丽的           | cheap but showy                                                                       |                                           | tawdry clothing     |
| overwrought  | 紧张过度的；兴奋过度的         | very nervous or excited                                                               |                                           |                     |
| susceptible  | 易受影响的，敏感的           | unresistant to some stimulus, influence, or agency                                    |                                           |                     |
| gluttonous   | 贪吃的，暴食的             | edacious, hoggish, ravenous, voracious, very greedy for food                          |                                           |                     |
| abet         | 教唆；鼓励，帮助            | to incite, encourage, urge and help on                                                | abut 邻接，毗邻                                |                     |
| 4            | 4                  | 4                                                                           | 4                              | 4                     |
| surly        | 脾气暴躁的；阴沉的          | bad tempered, sullen                                                        |                                |                       |
| calamity     | 大灾祸，不幸之事           | an extreme misfortune                                                       |                                |                       |
| hypothetical | 假设的                | conjectural                                                                 |                                |                       |
| incarnate    | 具有人体的；化身的，拟人化的     | given in a bodily form, personified                                         |                                |                       |
| sluggish     | 缓慢的，行动迟缓的；反应慢的     | markedly slow in movement, flow, or growth, slow to respond, torpid         |                                |                       |
| retract      | 撤回，取消；缩回，拉回        | to take back or withdraw, to draw or pull back                              | re + tract                     |                       |
| autograph    | 亲笔稿；手迹；在…上亲笔签名；亲笔的 |                                                                             |                                |                       |
| choppy       | 波浪起伏的；（风）不断改变方向的   | rough with small waves, changeable, variable                                |                                |                       |
| redundant    | 累赘的，多余的            | diffuse, prolix, verbose, wordly, superfluous                               | despondent 失望的；意气消沉的           |                       |
| commitment   | 承诺，许诺              |                                                                             |                                |                       |
| iridescent   | 色彩斑斓的              |                                                                             | iris 光圈，鸢尾花；irradiate 照射；照耀，照亮 |                       |
| plaintive    | 悲伤的，哀伤的            | expressive of woe, melancholy                                               | plaint n. 哀诉                   |                       |
| penitent     | 悔过的，忏悔的            | expressing regretful pain, repentant                                        |                                |                       |
| arable       | 适于耕种的              | suitable for plowing and planting                                           |                                | arable field          |
| perpetrate   | 犯罪；负责              | to commit, to be responsible for                                            |                                |                       |
| arcane       | 神秘的，秘密的            | cabalistic, impenetrable, inscrutable, mystic, mysterious, hidden or secret |                                |                       |
| proofread    | 校正，校对              | to read and mark corrections                                                |                                |                       |
| impound      | 限制；依法没收，扣押         | to confine, to seize and hold in the custody of the law, take possession of |                                |                       |
| amenable     | 顺从的，愿接受的           | willing, submissive                                                         |                                | be amenable to        |
| cavil        | 调毛病；吹毛求疵           | quibble                                                                     |                                | cavil at sth.         |
| compliment   | 恭维，称赞              | praise, flattery, congratulate, felicitate, laud                            |                                |                       |
| cautionary   | 劝人谨慎的，警戒的          | admonishing, admonitory, cautioning, monitory, givig advise or a warning    |                                |                       |
| humane       | 人道的，慈悲的；人文主义的      |                                                                             |                                |                       |
| superannuate | 使退休领养老金            | to retire and pension because of age or infirmity                           |                                |                       |
| artisan      | 技工                 | skilled workman or craftsman                                                |                                |                       |
| rave         | 热切赞扬；胡言乱语，说疯话      |                                                                             |                                |                       |
| flaunty      | 炫耀的，虚化的            | ostentatious                                                                | flaunt v.                      |                       |
| procurement  | 取得，获得；采购           |                                                                             | procure v.                     |                       |
| sinuous      | 蜿蜒的，迂回的            | characterized by many curves and twists, winding                            |                                |                       |
| sprawling    | 植物蔓生的；（城市）无法计划开展的  | spreading out ungracefully                                                  |                                | sprawling handwriting |
| exasperate   | 激怒，使恼怒             | to make angry, vex, huff, irritate, peeve, pique, rile, roil                |                                |                       |
| vacillate    | 游移不定，踌躇            | to waver in mind, will or feeling                                           |                                |                       |
| graft        | 嫁接，结合；贪污           |                                                                             | go + raft 木筏；draft 草稿          |                       |
| parasitic    | 寄生的                |                                                                             |                                |                       |
| benevolent   | 善心的，仁心的            | altruistic, humane, philanthropic, kindly, charitable                       |                                |                       |
| castigate    | 惩治；严责              | chasten, chastise, discipline, to punish or rebuke severely                 |                                |                       |
| relieved     | 宽慰的，如释重负的          |                                                                             |                                |                       |
| hierarchy    | 阶层，等级制度            | a system of ranks                                                           |                                |                       |
| controversy  | 公开辩论，论战            | altercation, bickering, contention, lurrah, dispute, quarrel, strife        |                                |                       |
| portend      | 预兆，预示              | to give an omen                                                             |                                |                       |
| rustic       | 乡村的，乡土气的           | of or relating to, or suitable for the country                              |                                |                       |
| veracity     | 真实，诚实              | devotion to the truth, truthfulness                                         |                                |                       |
| facetious    | 滑稽的，好开玩笑的          | joking or jesting often inappropriately                                     |                                |                       |
| topple       | 倾覆，推倒              | to overthrow                                                                |                                |                       |
| perishable   | 易腐烂的（东西），易变质的      | likely to decay or go bad quickly                                           |                                |                       |
| cessation    | 中止；（短暂的）停止         | a short pause or a stop                                                     |                                |                       |
| contagious   | 传染的；有感染力的          | easily passed from person to person, communicable                           |                                |                       |
| prodigious   | 巨大的；惊人的，奇异的        | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童                     |                       |
| 6              | 6                         | 6                                                                         | 6                                  | 6                        |
| atheist        | 无神论者                      | no deity                                                                  |                                    |                          |
| unspotted      | 清白的，无污点的                  | without spot, flawless                                                    |                                    |                          |
| aspiring       | 有抱负的，有理想的                 |                                                                           | aspire v.                          |                          |
| paltry         | 无价值的，微不足道的                | trashy, trivial, petty, negligible                                        |                                    |                          |
| reprisal       | 报复；报复行动                   | practice in retaliation                                                   |                                    |                          |
| inordinate     | 过度的；过分的                   | immoderate, excessive                                                     |                                    |                          |
| treatise       | 论文                        | a long writing work dealing systemctically with one subject               |                                    |                          |
| congenial      | 意气相投的；性情好的；适意的            | companionable, amiable, agreeable                                         |                                    |                          |
| flatter        | 恭维，奉承                     | adulate, blandish, honey, slaver                                          |                                    |                          |
| submission     | 从属，服从                     |                                                                           | sub + mission                      |                          |
| scour          | 冲刷；擦掉；四处搜索                | to clear, dig, remove, rub, to range over in a search                     |                                    |                          |
| perfunctory    | 例行公事般的；敷衍的                | characterized by routine or superficiality                                |                                    |                          |
| spatial        | 有关空间的，在空间的                |                                                                           | space n.                           |                          |
| vehement       | 猛烈的，热烈的                   | exquisite, fierce, furious, intense, violent, marked by forceful energy   |                                    |                          |
| promulgate     | 颁布（法令）；宣传，传播              | announce, annunciate, declear, disseminate, proclaim                      |                                    |                          |
| dearth         | 缺乏；短缺                     | scarcity                                                                  | dear + th                          |                          |
| tardy          | 延迟；迟到的；缓慢的                | sluggish                                                                  | gaudy, tawdry 俗丽的                  |                          |
| exodus         | 大批离去，成群外出                 | a mass departure or emigration                                            |                                    |                          |
| suppliant      | 恳求的，哀求的；恳求者，哀求者           |                                                                           |                                    |                          |
| bygone         | 过去的                       | gone by                                                                   |                                    | a bygone age             |
| canny          | 精明仔细的                     | shrewd and careful                                                        |                                    |                          |
| reiterate      | 重申，反复地说                   | iterate, ingeminate                                                       | illiterate 文盲的                      |                          |
| scavenge       | 清除，从废物中提取有用物质             | to cleanse, to salvage from discarded or refuse material                  |                                    |                          |
| transmit       | 传送，传播                     |                                                                           |                                    |                          |
| aristocratic   | 贵族（化）的                    |                                                                           | hypocrisy 伪善；虚伪                    |                          |
| rebuke         | 指责，谴责                     | admonish, chide, reproach                                                 | provoke 挑衅；revoke 撤销               |                          |
| potent         | 强有力的，有影响力的，有权力的；有效力的，有效能的 |                                                                           |                                    |                          |
| falsify        | 篡改；说谎                     | to alter a record, etc. fraudulently, to tell sb. falsehoods, lie         |                                    |                          |
| distent        | 膨胀的；扩张的                   | swollen, expanded                                                         | potent 强有力的，有影响力的，有权力的；有效力的，有效能的   |                          |
| complementary  | 互补的                       | combining well to form a whole                                            | complement n. 补充物                  | be complementary to sth. |
| rancor         | 深仇；怨恨                     | bitter deep-seated ill will, enmity                                       |                                    |                          |
| pounce         | （猛禽的）爪；猛扑；突然袭击            |                                                                           |                                    | pounce on 突然袭击           |
| flagrant       | 罪恶昭著的；公然的                 | conspicuously offensive                                                   |                                    |                          |
| brassy         | 厚脸皮的；无礼的                  | brazen, insolent                                                          |                                    |                          |
| impassive      | 无动于衷的，冷漠的                 | stolid, phlegmatic                                                        | passively 被动的，顺从的                  |                          |
| accede         | 同意                        | to give assent, consent                                                   |                                    | accede to 答应             |
| ethnic         | 民族的，种族的                   | of a national, racial or tribal group that has a common culture tradition | ethos （个人、团体、民族）风貌、气质；zenith 天顶，极点 |                          |
| repugnant      | 令人厌恶的                     | strong distaste or aversion                                               |                                    | be repugnant to sb.      |
| notorious      | 臭名昭著的                     | widely and unfavorably known                                              | not + orious；nutritious 有营养的       |                          |
| claustrophobic | （患，导致）幽闭恐惧症的              |                                                                           | claustrophobia 幽闭恐惧症               |                          |
| solvent        | 有偿债能力的；溶剂                 | capable of meeting financial obligations                                  | solve v.                           |                          |
| 7                | 7                | 7                                                                                                                                                                      | 7                                | 7                                   |
| sporadic         | 不定时发生的           | occurring occasionally                                                                                                                                                 |                                  |                                     |
| impart           | 传授，赋予；传递；告知，透露   | bestow, transmit, to make known                                                                                                                                        |                                  |                                     |
| acoustic         | 听觉的；声音的          |                                                                                                                                                                        | caustic 讽刺的                      |                                     |
| diverge          | 分歧，分开            | deviate                                                                                                                                                                |                                  |                                     |
| escalate         | （战争等）升级；扩大，上升，攀升 | to make a conflict more serious, to grow or increase rapidly                                                                                                           |                                  |                                     |
| apportion        | （按比例或计划）分配       |                                                                                                                                                                        |                                  | apportion sth. among/between/to sb. |
| idiomatic        | 符合语言习惯的；惯用的      |                                                                                                                                                                        | idiom 习惯用语                       |                                     |
| hypochondriac    | 忧郁症患者；忧郁症的       |                                                                                                                                                                        | hypochondria 忧郁症                 |                                     |
| ablaze           | 着火的；燃烧的；闪耀的      | being on fire, radiant with light or emotion                                                                                                                           | glaze 上釉，装玻璃                     | ablaze with sth.                    |
| didactic         | 教诲的；说教的          | morally instructive, boringly pedantic or moralistic                                                                                                                   |                                  |                                     |
| celibate         | 独身者；不结婚的         | an unmarried person                                                                                                                                                    | ceiling 天花板                      |                                     |
| voluble          | 健谈的；易旋转的         | talkative, rotating                                                                                                                                                    |                                  |                                     |
| reprimand        | 训诫，谴责            | admonish, chide, monish, reproach, decry, accuse, reprove, rebuke, disapprove, blame, arraign, charge, criminate, incriminate, inculpate, a severe or official reproof |                                  |                                     |
| bluff            | 虚张声势；悬崖峭壁        | pretense of strength, high cliff                                                                                                                                       |                                  |                                     |
| pantomime        | 哑剧               |                                                                                                                                                                        |                                  |                                     |
| transitory       | 短暂的              | transient                                                                                                                                                              |                                  |                                     |
| akin             | 同族的；类似的          | related by blood, essentially similar, related, or compatible                                                                                                          |                                  |                                     |
| assorted         | 各式各样的；混杂的        | consisting of various kinds, mixed                                                                                                                                     | as + sorted                      |                                     |
| craft            | 行业，手艺            | occupation, especially one that needs skills                                                                                                                           |                                  | arts and crafts                     |
| bashfulness      | 羞怯               | shyness, timidity                                                                                                                                                      | bashful adj.                     |                                     |
| substantive      | 根本的；独立存在的        | dealing with essentials, being in a totally independently entity                                                                                                       |                                  |                                     |
| irreverent       | 不尊敬的             | disrespectful                                                                                                                                                          | revere v.                        |                                     |
| corroborate      | 支持或证实；强化         | to bolster, make more certain, to strengthen                                                                                                                           | collaborate 协作                   |                                     |
| apathetic        | 无感情的；无兴趣的        |                                                                                                                                                                        |                                  | be apathetic about ath.             |
| infer            | 推断，推论，推理         | to reach in an opinion from reasoning                                                                                                                                  |                                  |                                     |
| subsist          | 生存下去；继续存在；维持生活   | to exist                                                                                                                                                               |                                  |                                     |
| repute           | 认为，以为；名誉         |                                                                                                                                                                        |                                  |                                     |
| indiscriminately | 随意的，任意的          | in a random manner, promiscuously, arbitrary                                                                                                                           |                                  |                                     |
| conjure          | 召唤，想起；变魔术，变戏法    | to call or bring to mind, invoke, to practise magic or legerdemain                                                                                                     | juncture 关键时刻，危急关头；结合处，接合点       | conjure for 召唤                      |
| trenchant        | 犀利的，尖锐的          | sharply perceptive, penetrating                                                                                                                                        | propensity 嗜好，习性                 |                                     |
| fleeting         | 短暂的；飞逝的          | transient, passing swiftly                                                                                                                                             | fleet v.                         |                                     |
| retain           | 保留，保持；留住，止住      | to keep possession of, to hold in place                                                                                                                                | maintain                         |                                     |
| vertebrate       | 脊椎动物（的）          | an animal that has a spine                                                                                                                                             | vertebra 脊椎骨；reverberate 发出回声，回响 |                                     |
| derogatory       | 不敬的；贬损的          | disparaging, belittling                                                                                                                                                |                                  |                                     |
| extract          | 拔出；强索            | to take sth, out with effort or by force, to forccefully obtain money or information                                                                                   |                                  |                                     |
| crumple          | 把…弄皱；起皱；破裂       | crush together into creases or wrinkles, to fall apart                                                                                                                 |                                  |                                     |
| demur            | 表示抗议，反对          |                                                                                                                                                                        | de + mur                         |                                     |
| solitary         | 孤独的；隐士           | without companions, recluse, forsaken, lonesome, lorn                                                                                                                  | solit 孤独；solid 固体                |                                     |
| adumbrate        | 预示               | to foreshadow in a vague way                                                                                                                                           |                                  |                                     |
| 8           | 8                   | 8                                                                        | 8                            | 8               |
| disseminate  | 传播，宣传                | to spread abroad, promulgate widely                                       |                               |                  |
| imperious    | 傲慢的；专横的              | overbearing, arrogant, magisterial, peremptory                            |                               | imperious manner |
| brute        | 野兽（的）；残忍的（人）         | beast, bestial, beastly, brutal                                           |                               |                  |
| trivial      | 琐碎的，没有价值的            | paltry, picayune, picayunish, trifling                                    |                               |                  |
| trivial      | 琐碎的，没有价值的            | paltry, picayune, picayunish, trifling                                    |                               |                  |
| volatile     | 反复无常的；易挥发的           | fickle, inconstant, mercuial, capricious                                  |                               |                  |
| illiterate    | 文盲的                  | ignorant, uneducated                                                      | reiterate 重申，反复地说             |                  |
| denounce     | 指责                   | to accuse publicly                                                        |                               |                  |
| suspend      | 暂停，中止；吊，悬            |                                                                           | sus + pend                    |                  |
| garble       | 曲解，篡改                | to alter or distort as to create a wrong impression or change the meaning |                               |                  |
| allude       | 暗指，影射，间接提到           |                                                                           |                               |                  |
| serendipity  | 偶然性；偶然的事物            |                                                                           |                               |                  |
| bilateral    | 两边的，双边的              |                                                                           | bi + lateral                  |                  |
| elusive      | 难懂的，难以描述的；不易被抓获的     |                                                                           |                               |                  |
| flout        | 蔑视，违抗                | fleer, gibe, jeer, jest, sneer                                            |                               |                  |
| cavity       | （牙齿等的）洞，腔            | a hollow place in a tooth                                                 | carve                         |                  |
| quixotic     | 不切实际的，空想的            | foolishly impractical                                                     |                               |                  |
| antithetical | 相反的；对立的              | opposite, contradictory, contrary, converse, counter, reverse             | antithesis n.；antipathy 反感，厌恶 |                  |
| intrepid     | 勇敢的；刚毅的              | characterized by fearlessness and fortitude                               |                               |                  |
| extremity    | 极度；绝境                |                                                                           | extreme adj.                  |                  |
| inhibit      | 阻止；抑制                | bridle, constrain, withhold                                               |                               |                  |
| obfuscate    | 使困惑，使迷惑              | to muddle, confuse, bewilder                                              |                               |                  |
| compensatory | 补偿性的；报酬的             | compensating                                                              |                               |                  |
| exquisite    | 精致的；近乎完美的            | elaborately made, delicate, consummate, perfected                         |                               |                  |
| dilapidated  | 破旧的；毁坏的              | broken down                                                               |                               |                  |
| moratorium   | 延缓偿付；活动中止            |                                                                           |                               |                  |
| aseptic      | 净化的；无菌的              | not septic                                                                | a + septic；sepsis 脓毒症         |                  |
| impersonate  | 模仿；扮演                |                                                                           |                               |                  |
| avid         | 渴望的，热心的              |                                                                           |                               | be avid for sth. |
| concentrate  | 聚集，浓缩                | compress, condense, converge, concenter                                   |                               |                  |
| dissident    | 唱反调者                 | a person who disagrees, dissenter                                         |                               |                  |
| decorous     | 合宜的；高雅的              | marked by propriety and good state                                        |                               |                  |
| stalk        | 隐伏跟踪（猎物）             | to pursue quarry or prey stealthily                                       |                               |                  |
| perfervid    | 过于热心的                | excessively fervent                                                       |                               |                  |
| deter        | 威慑，吓住；阻止             | to discourage, inhibit                                                    |                               |                  |
| herald       | 宣布…的消息；预示…的来临；传令官，信使 |                                                                           |                               |                  |
| penal        | 惩罚的；刑罚的              |                                                                           | penalty n.                    |                  |
| sycophant    | 马屁精                  | a servile self-seeking flatterer                                          |                               |                  |
| contemporary | 同时代的；当代的             |                                                                           | con + temporary               | contemporary art |
| affront      | 侮辱，冒犯                | to offend, confront, encounter                                            |                               |                  |
| pragmatic    | 实际的；务实的；注重生效的；实用主义的  |                                                                           |                               |                  |
| drowsy       | 昏昏欲睡的                | ready to fall asleep                                                      | drowse v. 打瞌睡                 |                  |
| critique     | 批评性的分析               | critical analysis                                                         |                               |                  |
| speculate    | 沉思，思索；投机             | to meditate on or ponder                                                  |                               |                  |
| 9               | 9                           | 9                                                                          | 9                                          | 9                   |
| banal           | 乏味的；陈腐的                     | dull or stale, commonplace, insipid, bland, flat, sapless, vapid           |                                            |                     |
| inimical        | 敌意的，不友善的                    | hostile, unfriendly                                                        |                                            |                     |
| innocuous       | （行为，言论等）无害的                 | harmless, innocent, innoxious, inoffensive, unoffensive                    |                                            |                     |
| stately         | 庄严的；宏伟的                     | august, dignified, majestic                                                |                                            |                     |
| tenuous         | 纤细的；稀薄的；脆弱的，无力的             | not dense, rare, flimsy, weak                                              |                                            |                     |
| perquisite      | 额外收入；津贴；小费                  | cumshaw, lagniappe, largess                                                | per + quisite；prerequisite 先决条件            |                     |
| haggle          | 讨价还价                        | bargain, dicker, wrangle                                                   |                                            |                     |
| falter          | 蹒跚，支支吾吾地说                   | flounder, stagger, tumble, stammer                                         |                                            |                     |
| affable         | 和蔼的；友善的                     | gentle, amiable, pleasant and easy to approach or talk to                  |                                            |                     |
| solemn          | 严肃的，庄严的；隆重的；黑色的             | made with great seriousness                                                |                                            |                     |
| flustered       | 慌张的，激动不安的                   | nervous and upset, perturbed, rattled                                      |                                            |                     |
| peripatetic     | 巡游的，游动的                     | itinerant                                                                  |                                            |                     |
| composed        | 镇定的，沉着的；由…组成的               | tranquil, self-possessed                                                   |                                            | be composed of sth. |
| preoccupation   | 全神贯注，专注；使人专注的东西             |                                                                            |                                            |                     |
| apparition      | 幽灵；特异景象                     | phantasm, phantom, revenant, specter                                       | appear + tion                              |                     |
| emulate         | 与…竞争，努力赶上                   | to strive to equal or excel                                                |                                            |                     |
| collusion       | 共谋，勾结                       |                                                                            |                                            | collusion with      |
| smug            | 自满的，自命不凡的                   | highly self-satisfied                                                      |                                            |                     |
| peevish         | 不满的，抱怨的；暴躁的；易怒的             | huffy, irritable, pettish, discontented, querulous, fractious, fretful     | peeve v.                                   |                     |
| imbue           | 浸染；浸透；使充满，灌输，激发             | to permeate or influence as if by dyeing, to endow                         |                                            |                     |
| astray          | 迷路的；误入歧途的                   | off the right path                                                         |                                            |                     |
| restive         | 不安静的；不安宁的                   | impatient                                                                  | strive 奋斗，努力                               |                     |
| frantic         | 疯狂的；狂乱的                     | wild with anger, frenzied                                                  | fry + ant + tic                            |                     |
| synergic        | 协同作用的                       | of combined action or coorperation                                         | synergy n.                                 |                     |
| subdued         | （光、声）柔和的，缓和的；（人）温和的         | unnaturally or unusually quiet in behavior                                 |                                            |                     |
| recessive       | 隐性遗传病的，后退的                  | tending to recede, withdraw                                               |                                            |                     |
| reconnaissance  | 侦查，预先勘探                     | a preliminary survey to gain information                                   | renaissance 复兴                             |                     |
| fraught         | 充满的                         | filled, charged, loaded                                                    | freight n. 货物                              |                     |
| recluse         | 隐士；隐居的                      | cloistered, seclusive, sequestered                                         | reclusive adj.；clause 从句，（法律）条款            |                     |
| resort          | 求助，诉诸；度假胜地                  |                                                                            | report                                     |                     |
| self-procession | 自我列队                        |                                                                            |                                            |                     |
| spiny           | 针状的；多刺的，棘手的                 | thorny                                                                     | spine n.                                   |                     |
| ingrate         | 忘恩负义的人                      | an ungrateful person                                                       | in + grate                                 |                     |
| catalyze        | 促使，激励                       | to bring about, to inspire                                                 | tantalize 挑逗                               |                     |
| intrigue        | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of         |                                            |                     |
| burgeon         | 迅速成长，发展                     | to grow rapidly, proliferate, blossom, bloom, flower, outbloom, effloresce |                                            |                     |
| equilibrium     | 平衡                          |                                                                            | equi + …；egalitarian, equalitarian 主张人人平等的 |                     |
| consort         | 陪伴；结交；配偶                    |                                                                            | con + sort                                 |                     |
| revise          | 校订，修订                       | redraft, redraw, revamp                                                    |                                            |                     |
| aloof           | 远离的；冷漠的                     |                                                                            |                                            |                     |
| blatant         | 厚颜无耻的；显眼的；炫耀的               | brazen, brassy, completely obvious, conspicuous, showy                     |                                            |                     |
| braid           | 编织；麦穗；辫子                    | plait, twine, weave                                                        |                                            |                     |
| fraudulent      | 欺骗的；不诚实的                    | acting with fraud, deceitful                                               |                                            |                     |
| arid            | 干旱的；枯燥的                     | dry, dull, uninteresting                                                   |                                            | an arid discussion  |
| clamber         | 吃力的爬上；攀登                    | to climb awkwardly                                                         |                                            |                     |
| surrogate       | 代替品；代理人                     | substitution                                                               |                                            |                     |
| narcotic        | 麻醉剂；催眠的                     | soporific                                                                  |                                            |                     |
| emphatic        | 重视的，强调的                     | showing emphasis                                                           | emphasize v.                               |                     |
| reciprocally    | 相互的；相反的                     | mutually                                                                   |                                            |                     |
| phlegmatic      | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                        |                                            |                     |
| penetrating     | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                          |                                            |                     |
| deploy          | 部署；拉长（战线），展开                |                                                                            |                                            |                     |
| paean           | 赞美歌，诗歌                      | a song of joy, praise, triumph, chorale                                    |                                            |                     |
| foible          | 小缺点，小毛病                     | a small weakness, fault                                                    |                                            |                     |
| exploit         | 剥削；充分利用                     |                                                                            |                                            |                     |
| schism          | 分裂，教会分裂                     | division, separation, fissure, fracture, rift                              |                                            |                     |
| duplicity       | 欺骗，口是心非                     | deceit, dissemblance, dissimluation, guile                                 |                                            |                     |
| 10            | 10                    | 10                                                                    | 10                   | 10                      |
| collateral    | 平行的；附属的；旁系的；担保品       | parallel, subordinate                                                 | col + later + al     |                         |
| incapacitate  | 使无能力，使不适合             | disable                                                               |                      |                         |
| revert        | 恢复；重新考虑               | to go back, to consider again                                         | re + vert            |                         |
| annex         | 兼并；附加                 | to obtain or take for oneself, to attach                              |                      |                         |
| inexorable    | 不为所动的；无法改变的           | incapable of being moved or influenced, cannot be altered             |                      |                         |
| slimy         | 黏滑的                   | of, relating to, or resembling slime, glutinous, miry, muddy, viscous | flimsy 轻而薄的；易损坏的     |                         |
| coarse        | 粗糙的，低劣的；粗俗的           | of low quality, not refined                                           |                      |                         |
| tacit         | 心照不宣的                 | understood without being put into words                               |                      |                         |
| affirmation   | 肯定，断言                 | the state of being affirmed, assertion                                |                      |                         |
| banish        | 放逐                    | to send sb. out of the country as a punishment                        |                      |                         |
| equestrian    | 骑马的；骑士阶层的             | of horse riding, one who rides on horseback                           | equ 古意：马             |                         |
| shiftless     | 没出息的，懒惰的              | lacking in ambition or incentive, lazy, torpid, inefficient           |                      |                         |
| acidic        | 酸的                    | acid                                                                  |                      |                         |
| improvise     | 即席创作                  | impromptu, extemporize                                                |                      |                         |
| benefactor    | 行善者，捐助者               | a person who has given financial help, patron                         |                      |                         |
| stoic         | 坚忍克己之人                | a person who firmly retraining response to pain or stress             | a person named Stoic |                         |
| episodic      | 偶然发生的；分散性的            | occuring irregularly                                                  | episode n. 片段        |                         |
| segment       | 部分                    | bit, fragment                                                         | section              |                         |
| withstand     | 反抗，经受                 | to oppose with force or resolution, to endure successfully            |                      |                         |
| desecrate     | 玷辱，亵渎                 | treat as not sacred, profane                                          | de + secra (sacred)  |                         |
| dichotomy     | 两分法；矛盾对立，分歧；具有两分特征的事物 | bifurcation, sth, with seemingly contradictory qualities              |                      |                         |
| outgrowth     | 结果；副产品                | consequence, by-product                                               |                      |                         |
| constituent   | 成分；选区内的选民             | component, element, a member of a constituency                        |                      |                         |
| ribaldry      | 下流的语言；粗鄙的幽默           | ribald language or humor, obscenity                                   |                      |                         |
| grating       | （声音）刺耳的；恼人的           | hoarse, raucous, irritating, annoying, harsh and rasping              |                      |                         |
| perspicacious | 独具慧眼的；敏锐的             | of acute mental vision or discernment                                 |                      |                         |
| pitfall       | 陷阱；隐患                 | a hidden or not easily recognized danger or difficulty                |                      |                         |
| atrophy       | 萎缩，衰退                 | decadence, declination, degeneracy, degeneration, deterioration       |                      |                         |
| omit          | 疏忽，遗漏；不做，未能做          | to leave out, to leave undone, disregard, ignore, neglect             |                      |                         |
| speculative   | 推理的；思索的；投机的           | conjectured, guessed, supposed, surmised, risky                       | speculation n.       |                         |
| elapse        | 消逝，过去                 | pass, go by                                                           |                      |                         |
| allegiance    | 忠诚，拥护                 | loyalty or devotion to a cause or a person                            |                      |                         |
| whittle       | 削（木头）；削减              | to pare or cut off chips, to reduce, pare                             | whistle 口哨           |                         |
| drone         | 嗡嗡的响；单调的说；单调的低音       |                                                                       |                      |                         |
| divisive      | 引起分歧的；导致分裂的           | creating disunity or dissension                                       |                      |                         |
| crooked       | 不诚实的；弯曲的              | dishonest, not straight                                               | crook v.             |                         |
| bombastic     | 夸夸其谈的                 |                                                                       |                      |                         |
| creed         | 教义，信条                 | belief, tenet                                                         |                      |                         |
| armored       | 披甲的，装甲的               | equipped or protected with an armor                                   |                      | an armored car          |
| underhanded   | 秘密的；狡诈的               | marked by secrecy and deception, sly                                  |                      |                         |
| shrivel       | （使）枯萎                 | mummify, wilt, wither                                                 |                      |                         |
| containment   | 阻止，遏制                 | keeping sth, within limits                                            |                      | a policy of containment |
| attrition     | 摩擦，磨损                 | by friction                                                           |                      |                         |
| accentuate    | 强调；重读                 | emphasis, pronounce with an stress                                    | accent + uate        |                         |
| reversible    | 可逆的，可反转的              |                                                                       |                      |                         |
| tension       | 紧张，焦虑；张力              | anxiety, stretching force                                             |                      |                         |
| consternation | 大为吃惊；惊骇               | great fear or shock                                                   |                      |                         |
| preservative  | 保护的；防腐的；防腐剂           |                                                                       | preserve v.          |                         |
| genial        | 友好的；和蔼的               | cheerful, friendly, amiable                                           |                      |                         |
| deride        | 嘲笑，愚弄                 | to laugh at, ridicule                                                 |                      |                         |
| ignominious   | 可耻的；耻辱的               | disgraceful, humiliating, despicable, dishonorable                    |                      |                         |
| synchronous   | 同时发生的                 | concurrent, coexistent, happening at precisely the same time          |                      |                         |
| furtive       | 偷偷的，秘密的               | catlike, clandestine, covert, secret, surrepitious                    |                      |                         |
| 11             | 11             | 11                                                                               | 11                                        | 11                       |
| drastic        | 猛烈的；激烈的        | violent and severe                                                               | dram                                      | a drastic change         |
| averse         | 反对的；不愿意的       | not willing or inclined, opposed                                                 |                                           |                          |
| impervious     | 不能渗透的；不为所动的    |                                                                                  | previous 先前的 pervious 透水的                 |                          |
| ravage         | 摧毁；使荒废         | desolate, devastate, havoc, ruin and destroy                                     |                                           |                          |
| arrogant       | 自负的；自大的        | overbearing, haughty, proud, pompous, imperious                                  |                                           |                          |
| coax           | 哄诱；巧言哄骗        | induce, persuade, wheedle                                                        |                                           | coax sth out of/from sb. |
| forfeit        | 丧失；被罚没收；丧失的东西  |                                                                                  |                                           |                          |
| exposition     | 阐释，博览会         |                                                                                  |                                           |                          |
| stunning       | 极富魅力的          | amazing, dazzling, marvellous, strikingly impressive                             |                                           |                          |
| penalize       | 置…与不利地位，处罚     |                                                                                  | penal, penalty                            |                          |
| obliterate     | 涂掉，擦掉          | efface, erase                                                                    | ob 去掉 liter 文字 ate                        |                          |
| extrapolate    | 预测，推测          | speculate                                                                        |                                           |                          |
| callous        | 结硬块的；无情的       | thick and hardened, lacking pity, unfeeling                                      | callus 老茧                                 |                          |
| unpremeditated | 无预谋的，非故意的      |                                                                                  |                                           |                          |
| trample        | 踩坏，践踏；蹂躏       | tread, override                                                                  |                                           |                          |
| euphoric       | 欢欣的            | feel happy and excited                                                           |                                           |                          |
| obsequiousness | 谄媚             | servility, subservience, abject submissiveness                                   |                                           |                          |
| immaculate     | 洁净的；无暇的        | perfectly clean, unsoiled, impeccable                                            |                                           |                          |
| imposing       | 给人深刻印象的；壮丽雄伟的  | impressive, grand                                                                |                                           |                          |
| subside        | （建筑物等）下陷；平息，减退 | tend downward, descend, become quiet or less, ebb, lull, moderate, slacken, wane |                                           |                          |
| satirize       | 讽刺             | lampoon, mock, quip, scorch                                                      | sarcastic, sneering, caustic, ironic adj. |                          |
| abut           | 邻接，毗邻          | to border upon                                                                   | abet 教唆                                   |                          |
| shatter        | 使落下；使散开；粉碎；破坏  |                                                                                  |                                           |                          |
| disciple       | 信徒，弟子          | convinced adherent                                                               | discipline                                |                          |
| introvent      | 性格内向的人         | one whose thoughts and feelings are directed toward oneself                      |                                           |                          |
| pedantry       | 迂腐             |                                                                                  | pedant n.                                 |                          |
| banter         | 打趣，玩笑          | playful, good-humored joking                                                     |                                           |                          |
| thwart         | 阻挠；挫败          | baffle, balk, foil, furstrate                                                    | throat 嗓子                                 |                          |
| superfluous    | 多余的，累赘的        | exceeding what is needed                                                         | "supercilious 目中无人的                       | "                        |   |
| disparate      | 迥然不同的          |                                                                                  |                                           |                          |
| dorsal         | 背部的；脊背的        |                                                                                  |                                           |                          |
| imminent       | 即将发生的；逼近的      | close, impending, proximate                                                      |                                           |                          |
| negligibly     | 无足轻重的；不值一提的    |                                                                                  | negligible adj.                           |                          |
| saturate       | 浸湿；浸透；使大量吸收或充满 |                                                                                  |                                           |                          |
| brink          | （峭壁）边缘         | verge, border, edge                                                              | blink 眨眼                                  |                          |
| atrocity       | 邪恶；暴行          | evil, iniquitousness, ferocity, barbarousness, truculence,                       | atrocious adj.                            |                          |
| unpalatable    | 不好吃的；令人不快的     |                                                                                  |                                           |                          |
| mortify        | 使丢脸；侮辱         | affront, insult, humiliate                                                       |                                           |                          |
| hale           | 健壮的；矍铄的        | healthy                                                                          |                                           |                          |
| deliberation   | 细想；考虑          | consideration, speculation, congitation                                          |                                           |                          |
| renounce       | 声明放弃；拒绝，否认     |                                                                                  |                                           |                          |
| reciprocation  | 互换；报答；往复运动     | mutual exchange, return, alternating motion                                      |                                           | reciprocation of sth.    |
| crimson        | 绯红色（的）         | deep purplish red                                                                |                                           |                          |
| reimburse      | 偿还             | pay back, repay                                                                  | re + im + burse                           |                          |
| surmise        | 推测，猜疑          | conjecture, speculate                                                            |                                           |                          |
| jumble         | 使混乱，混杂；杂乱      | to mix in disorder, a disorderly mixture                                         |                                           |                          |
| ascetic        | 禁欲的；苦行者        | astringent, austere, mortified, severe, stern                                    |                                           |                          |
| revitalize     | 使重新充满活力        | to give new life or vigor to, rejuvenate                                         |                                           |                          |
| 12              | 12                    | 12                                                                              | 12                   | 12                 |
| terrorize       | 恐吓                    | to fill with terror or anxiety, intimidate, threaten, menace                    |                      |                    |
| tempting        | 诱惑人的                  | having an appeal, alluring, attractive, enticing, inviting, seductive           |                      |                    |
| inadvertent     | 疏忽的；不经意的              | unintentional                                                                   | in + advertent       |                    |
| recount         | 叙述；描写                 | narrate                                                                         |                      |                    |
| populous        | 人口稠密的                 | densely populated                                                               |                      |                    |
| skew            | 不直的，歪斜的               | crooked, slanting, running obliquely                                            |                      |                    |
| pliant          | 易受影响的；易弯的             | easily influenced, pliable, malleable                                           |                      |                    |
| rancid          | 不新鲜的，变味的              | rank, stinking                                                                  |                      |                    |
| temporal        | 时间的，世俗的               |                                                                                 |                      |                    |
| antiquated      | 陈旧的，过时的；年老的           | antique, archaic, dated                                                         |                      | antiquated ideas   |
| covet           | 贪求，妄想                 | want ardently                                                                   | cover - r            |                    |
| fortuitous      | 偶然发生的；偶然的             | accidental, lucky, casual, contingent, incidental                               | fortitude 坚毅，坚忍不拔    |                    |
| ill-will        | 敌意，仇视，恶感              | enmity, malice, antagonism, animosity, animus, antipathy, opposition, rancor    |                      |                    |
| obsessed        | 着迷的；沉迷的               |                                                                                 | obsess v.            |                    |
| gouge           | 挖出，骗钱；半圆凿             | scoop out, cheat out of money, a semicircular chisel                            | gauge 准则，规范          |                    |
| ascent          | 上升，攀登；上坡路，提高，提升       |                                                                                 |                      |                    |
| discernible     | 可识别的，可辩别的             | being recognized or identified                                                  | discern + able       |                    |
| unwieldy        | 难控制的；笨重的              | cumbersome, not easily managed                                                  |                      |                    |
| coterie         | （有共同兴趣的）小团体           |                                                                                 | cote 小屋              |                    |
| decimate        | 毁掉大部分，大量杀死            | to destroy or kill a large part                                                 |                      |                    |
| fascinate       | 迷惑，迷住                 | charm, captivate, attract                                                       |                      |                    |
| strip           | 剥去；狭长的一片              | denude, disrobe, a long narrow piece                                            |                      |                    |
| apropos         | 适当的；有关                |                                                                                 |                      |                    |
| perfidious      | 不忠的；背信弃义的             | faithless                                                                       |                      |                    |
| compelling      | 引起兴趣的                 | captivating, keenly interesting                                                 | a compelling subject |                    |
| contemplation   | 注视，凝视；意图，期望           |                                                                                 |                      |                    |
| exhale          | 呼出，呼气，散发              |                                                                                 |                      |                    |
| ascribe         | 归因于；归咎于               |                                                                                 |                      |                    |
| thrive          | 茁壮成长；繁荣，兴旺            | flourish, prosper                                                               |                      |                    |
| strenuous       | 奋发的；热烈的               | vigorously active, fervent, zealous, earnest, energetic, spirited               |                      |                    |
| nocturnal       | 夜晚的，夜间发生的             | happening in the night                                                          |                      |                    |
| synoptic        | 摘要的                   | affording a general view of the whole                                           | hypnotic 催眠的；催眠药     |                    |
| infiltrate      | 渗透，渗入                 | to pass through                                                                 |                      |                    |
| misapprehension | 误会，误解                 | misunderstanding, misconception, misinterpretation                              |                      |                    |
| prescient       | 有远见的，预知的              |                                                                                 |                      |                    |
| incoherent      | 不连贯的；语无伦次的            | lacking coherence                                                               |                      |                    |
| imperturbable   | 冷静的；沉着的               | cool, disimpassionated, unflappable, unruffled                                  |                      |                    |
| cacophonous     | 发音不和谐的，不协调的           | marked by cacophony                                                             |                      |                    |
| shoal           | 浅滩，浅水处；浅的             | shallow                                                                         |                      |                    |
| reinstate       | 回复                    |                                                                                 | re + in + state      |                    |
| quaint          | 离奇有趣的；古色古香的           | unusual and attractive                                                          |                      |                    |
| vogue           | 时尚，流行                 | popular acceptation or favor                                                    |                      |                    |
| somber          | 忧郁的；阴暗的               | melancholy, dark and gloomy, dim, grave, shadowy, shady, overcast, disconsolate |                      |                    |
| bastion         | 堡垒，防御工事               |                                                                                 |                      |                    |
| flaunt          | 炫耀，夸耀                 | flash, parade, show off                                                         |                      |                    |
| hover           | 翱翔，徘徊                 |                                                                                 |                      |                    |
| defy            | 违抗，藐视                 |                                                                                 |                      |                    |
| sanguine        | 乐观的；红润的               | optimistic, cheerful and confident                                              |                      |                    |
| absurd          | 荒谬的，可笑的               |                                                                                 |                      | absurd assumptions |
| criss-cross     | 交叉往来                  |                                                                                 |                      |                    |
| morose          | 郁闷的                   | sullen, gloomy, disconsolate                                                    |                      |                    |
| persuasive      | 易使人信服的；有说服力的          |                                                                                 |                      |                    |
| spurious        | 假的；伪造的                | forged, false, falsified                                                        |                      |                    |
| empiricism      | 经验主义                  |                                                                                 | empiric 经验主义者        |                    |
| pronounced      | 显著的；明确的               | strongly marked                                                                 |                      |                    |
| sanctimonious   | 假装虔诚的                 | hypocritically pious or devout                                                  |                      |                    |
| bland           | 情绪平稳的；无味的             | pleasantly smooth, insipid                                                      |                      |                    |
| span            | 跨度，两个界限的距离            | extent, length, reach, spread, a stretch between two limits                     |                      |                    |
| plethora        | 过多，过剩                 | excess, superfluity, overabundance, overflow, overmuch, overplus                |                      |                    |
| petulant        | 暴躁的，易怒的               | insolent, peevish                                                               |                      |                    |
| spinal          | 脊骨的，脊髓的               |                                                                                 |                      |                    |
| intent          | 专心的，专注的，热切的，渴望的，目的，意向 |                                                                                 | intend v.            |                    |
| tangential      | 切线的；离题的               | discursive, excursive, rambling, impertinent                                    |                      |                    |
| ulterior        | 较远的，将来的；隐秘的，别有用心的     |                                                                                 |                      |                    |
| resurge         | 复活                    |                                                                                 |                      |                    |
| antiseptic      | 杀菌剂，防腐的               |                                                                                 | anti + septic        |                    |
| 13            | 13                       | 13                                                                      | 13                   | 13               |
| inflamed      | 发炎的，红肿的                  |                                                                         | infection            |                  |
| smother       | 熄灭，覆盖，使窒息                | to cover thickly                                                        |                      |                  |
| segregate     | 分离，隔离                    |                                                                         |                      |                  |
| igneous       | 火的，似火的                   | fiery, having the nature of fire                                        |                      |                  |
| nonentity     | 无足轻重的人或事                 |                                                                         |                      |                  |
| perpendicular | 垂直的，竖直的                  | exactly upright, vertical                                               |                      |                  |
| prodigal      | 挥霍的，挥霍者                  | lavish                                                                  |                      |                  |
| cynical       | 愤世嫉俗的                    | captious, peevish                                                       | cynic 愤世嫉俗者          |                  |
| arboreal      | 树木的                      |                                                                         | arbor 凉亭             |                  |
| resignation   | 听从，顺从；辞职                 |                                                                         | resign v.            |                  |
| mischievous   | 淘气的；有害的                  |                                                                         |                      |                  |
| recital       | 独奏会，演唱会；吟诵               |                                                                         | recite v. 背诵         |                  |
| quiescence    | 静止，沉寂                    | quietness, stillness, tranquillity                                      | quiescent adj.       |                  |
| dormant       | 冬眠的；静止的                  | torpid in winter, quiet, still                                          |                      |                  |
| prerogative   | 特权                       | privilege, birthright, perquisite                                       |                      |                  |
| plumb         | 测探物，铅锤；垂直的；精确的；深入了解；测量深度 |                                                                         | plump 丰满的，使丰满        |                  |
| farce         | 闹剧，滑稽剧；可笑的行为             |                                                                         |                      |                  |
| arbitrary     | 专横的，武断的                  | discretionary, despotic, dictatorial, monocratic, autarchic, autocratic |                      |                  |
| disjunctive   | 分离的，转折的，反意的              |                                                                         | dis + junctive       |                  |
| condescending | 谦逊的；故意屈尊的                |                                                                         |                      |                  |
| devastate     | 摧毁，破坏                    | ravage, destroy, depredate, wreck                                       |                      |                  |
| inviolate     | 不受侵犯的；未受损害的              | not violated or profaned                                                |                      |                  |
| jubilant      | 欢呼的，喜气洋洋的                | elated, exultant                                                        |                      |                  |
| intricacy     | 错综复杂                     |                                                                         | intricate adj.       |                  |
| untenable     | 难以防守的；不能租赁的              |                                                                         | tend 租赁              |                  |
| dissect       | 解剖；剖析                    | dichotomize, disjoin, disjoint, dissever, separate                      | dis + section        |                  |
| ranching      | 大牧场                      | a large farm                                                            |                      |                  |
| exaggerate    | 夸大，夸张；过分强调               | overstate, overemphasize, intensify, magnify                            |                      |                  |
| poignant      | 令人痛苦的；伤心的；尖锐的；尖刻的        | painfully affecting the feelings, cutting                               |                      |                  |
| potable       | 适于饮用的                    |                                                                         |                      |                  |
| florid        | 华丽的；（脸）红润的               | highly decorated, showy, rosy, ruddy                                    |                      |                  |
| disprove      | 证明…有误                    |                                                                         |                      |                  |
| veer          | 转向，改变（话题等）               | curve, sheer, slew, slue, swever                                        |                      |                  |
| exasperation  | 激怒，恼怒                    |                                                                         | exasperate v.        |                  |
| salutary      | 有益的，有益健康的                |                                                                         |                      |                  |
| distinctive   | 出众的，有特色的                 | distinguish, characteristic, typical, eminent, prominent, conspicuous   |                      |                  |
| skimp         | 节省花费                     | barely sufficient funds                                                 |                      | be skimp on sth. |
| enthral       | 迷惑                       |                                                                         | thrall 王座            |                  |
| religion      | 宗教信仰                     |                                                                         |                      |                  |
| reclaim       | 纠正，改造；开垦                 |                                                                         |                      |                  |
| dialect       | 方言                       |                                                                         |                      |                  |
| demobilize    | 遣散，使复员                   | disband                                                                 |                      |                  |
| rhapsodic     | 狂热的，狂喜的；狂想曲的             |                                                                         |                      |                  |
| vindictive    | 报复的                      | vengeful                                                                |                      |                  |
| equitable     | 公正的，合理的                  | dispassionate, impartial, impersonal, nondiscriminatory, objective      |                      |                  |
| reluctance    | 勉强，不情愿                   |                                                                         | reluctant adj.       |                  |
| glossy        | 有光泽的；光滑的                 | glassy, glistening, lustrous, polished                                  |                      |                  |
| transcendent  | 超越的，卓越的，出众的              | ultimate, unsurpassable, utmost, uttermost, supreme                     |                      |                  |
| recondite     | 深奥的，晦涩的                  |                                                                         | reconcile 和解，调和      |                  |
| culpable      | 有罪的，该受谴责的                | blameworthy, deserving blame                                            |                      |                  |
| tactile       | 有触觉的                     |                                                                         |                      |                  |
| pomposity     | 自大的行为或言论                 |                                                                         | pompous adj. pomp n. |                  |
| 14              | 14                  | 14                                                                                           | 14                                              | 14                                 |
| viscous         | 粘滞的，粘性的             | glutinous                                                                                    |                                                 |                                    |
| venerate        | 崇敬，敬仰               | adore, revere, worship, regard with reverential respect                                      |                                                 |                                    |
| slash           | 大量削减                | reduce sharply                                                                               |                                                 |                                    |
| capricious      | 变化无常的，任性的           | fickleness, fickle, inconstant, lubricious, mercurial, whimsical, whimsied, erratic, flighty |                                                 |                                    |
| insolent        | 粗野的，无礼的             | impudent, boldly disrespectful                                                               | obsolescent 、逐渐荒废的                              |                                    |
| deplete         | 大量减少，使枯竭            | drain, impoverish, exhaust                                                                   |                                                 |                                    |
| fecund          | 肥沃的；多产的；创造力强的       | fertile, prolific, intellectually productive                                                 | defunct 死亡的                                     |                                    |
| recuperate      | 恢复（健康、体力）；复原        | regain, recover                                                                              |                                                 |                                    |
| renowned        | 有名的                 | having renown, celebrated, famous                                                            |                                                 |                                    |
| prudish         | 过分守礼的；假道学的          | marked by prudery, priggish                                                                  |                                                 |                                    |
| defecate        | 澄清，净化               | clarify                                                                                      |                                                 |                                    |
| raze            | 夷为平地；彻底破坏           | destroy to the ground, destroy completely                                                    |                                                 |                                    |
| novice          | 生手，新手               | beginner, apprentice                                                                         |                                                 |                                    |
| uninitiated     | 外行的，缺乏经验的           | inexperienced, not skilled or knowledgeable                                                  |                                                 |                                    |
| opprobrious     | 辱骂的；侮辱的             | expressing scorn, abusive                                                                    | abrasive 研磨的                                    |                                    |
| consummate      | 完全的；完善的；完成          | complete, perfect, accomplish                                                                |                                                 |                                    |
| flickering      | 闪烁的，摇曳的；忽隐忽现的       | glittery, twinkling                                                                          | flicker v.                                      |                                    |
| fuse            | 融化，融合               | combine                                                                                      |                                                 |                                    |
| rescind         | 废除，取消               | make void                                                                                    |                                                 |                                    |
| rivet           | 铆钉；吸引（注意）           | attract                                                                                      |                                                 | be riveted to the spot/ground 呆若木鸡 |
| turmoil         | 混乱，骚乱               | tumult, turbulence                                                                           |                                                 |                                    |
| impunity        | 免受惩罚                | exemption from punishment                                                                    |                                                 |                                    |
| unimpassionated | 没有激情的               | without passion or zeal                                                                      |                                                 |                                    |
| insinuate       | 暗指，暗示               | imply                                                                                        |                                                 |                                    |
| grim            | 冷酷的，可怕的             | ghastly, forbidding, appearing stern                                                         |                                                 |                                    |
| unobtrusive     | 不引人注目的              | not + noticeable                                                                             | obtrusive 碍眼的                                   |                                    |
| momentous       | 极为重要的；重大的           | considerable, significant, substantial, consequential                                        | commemorate 纪念（伟人，伟大事件等），庆祝；monumental 极大的，纪念碑的 |                                    |
| unsound         | 不健康的，不健全的；不结实的；无根据的 |                                                                                              | save and sound                                  |                                    |
| reprehensible   | 应受谴责的               | deserving reprehension, culpable, blameworthy, blamable, blameful, censurable                | apprehensive 、害怕的；敏悟的                           |                                    |
| potted          | 盆栽的；瓶装的，罐装的         |                                                                                              | pot                                             |                                    |
| provident       | 深谋远虑的，节俭的           | prudent, frugal, thrifty                                                                     |                                                 |                                    |
| investigative   | 调查的                 |                                                                                              | investigate                                     |                                    |
| concoction      | （古怪或少见的）混合物         |                                                                                              |                                                 |                                    |
| countenance     | 支持，赞成；表情            | advocate, approbate, approve, encourage, favor, sanction                                     |                                                 |                                    |
| aspiration      | 抱负，渴望               | strong desire or ambition                                                                    |                                                 |                                    |
| invulnerable    | 不会受伤害的；刀枪不入的        | incapable of being injured or wounded                                                        |                                                 |                                    |
| impulse         | 动力，刺激               | impelling and motivating force                                                               |                                                 |                                    |
| asymmetric      | 不对称的                |                                                                                              | a + symmetric                                   |                                    |
| embroider       | 刺绣，镶边；装饰            | needlework, provide embellishment                                                            |                                                 |                                    |
| preempt         | 以优先权获得的；取代          |                                                                                              |                                                 |                                    |
| compel          | 强迫                  | coercive, concuss, oblige, force or constrain                                                |                                                 |                                    |
| veracious       | 诚实的；说真话的            | truthful, honest, faithful, veridical                                                        | valorous 勇敢的                                    |                                    |
| jaded           | 疲惫的；厌倦的             | wearied, fatigued, dull, satiated                                                            |                                                 | be jaded by sth.                   |
| recourse        | 求助，依靠               | turning sb, for help                                                                         |                                                 |                                    |
| reparation      | 补偿；赔偿               | repairing, restoration, compensation                                                         |                                                 |                                    |
| circumscribe    | 划界限；限制              | restrict, restrain, limit                                                                    | circum 圆 + scribe 画                             |                                    |
| impediment      | 妨碍，障碍物              | obstacle                                                                                     |                                                 | an impediment to reform            |
| slanderous      | 诽谤的                 | abusive, false and defamatory                                                                |                                                 |                                    |
| 15             | 15                      | 15                                                                      | 15                                       | 15                   |
| recalcitrant   | 顽抗的                     | unruly, tenacious, stubborn, hardly                                     |                                          |                      |
| succumb        | 屈从，屈服；因…死亡              |                                                                         | sus 下面 + sumb 躺                          |                      |
| noisome        | 有恶臭的；令人作呕的，令人讨厌的        | annoy, foul, smelly, fetid                                              |                                          |                      |
| consecutive    | 连续的                     | serial, sequential, successively, continuously, uninterruptedly         |                                          |                      |
| rhetorical     | 修辞（学）的；辞藻华丽的            |                                                                         | rhetoric n.                              |                      |
| propitiate     | 讨好；抚慰                   | assuage, conciliate, mollify, consolate, pacify, placate                |                                          |                      |
| painstakingly  | 细心的，专注的；辛苦的             | fastidiously, assiduously, exhaustively, meticulously                   |                                          |                      |
| conflagration  | （建筑、森林）大火               | big, destructive fire                                                   |                                          |                      |
| dwindle        | 变少，减少                   | diminish, shrink, decrease                                              |                                          |                      |
| indenture      | 契约；合同                   |                                                                         | indent 切割成锯齿状（古代分割成锯齿状的契约）               |                      |
| retribution    | 报应，惩罚                   |                                                                         |                                          | retribution for sth. |
| ungainly       | 笨拙的                     | clumsy, awkward, gawky, lumbering, lumpish, lacking smooth or dexterity |                                          |                      |
| futile         | 无效的，无用的；没出息的            | bootless, otiose, unavailing, useless, without advantage or benefit     | fertile, fertilizer, fertilize 多产、化肥、使受精 |                      |
| prostrate      | 俯卧的；沮丧的；使下跪鞠躬           | prone, powerless, helpless                                              | pro (prone) + strate                     |                      |
| graze          | （让动物）吃草；放牧              |                                                                         | grass 草；glaze 上釉，装玻璃                     |                      |
| threat         | 威胁，恐吓；凶兆                |                                                                         |                                          |                      |
| cone           | 松果；圆锥体                  |                                                                         |                                          |                      |
| renegade       | 叛徒，叛教者                  | apostate, defector                                                      | re + neg 反 + ade                         |                      |
| intruding      | 入侵（性质）的                 | intrusive                                                               | intrude v.                               |                      |
| untasted       | 为尝试过的，未体验过的             |                                                                         | un + stated                              |                      |
| presumptuous   | 放肆的；过分的                 | overweening, presuming, uppity                                          |                                          |                      |
| decorum        | 礼节，礼貌                   | etiquette, courtesy, politeness                                         | décor 装饰                                 |                      |
| philanthropic  | 慈善（事业）的；博爱的             | characterized by philanthropy, humanitarian                             |                                          |                      |
| efficacy       | 功效，有效性                  | capability, effectiveness, efficiency                                   |                                          |                      |
| informative    | 提供信息的；见多识广的             | instructive                                                             |                                          |                      |
| patriarchal    | 家长的，族长的；父权制的            |                                                                         | patriarchy, patriarch                    |                      |
| docile         | 驯服的；听话的                 | easy to control                                                         |                                          |                      |
| sedulous       | 聚精会神的；勤勉的               | diligent                                                                |                                          |                      |
| antiquarianism | 古物研究；好古癖                |                                                                         | antique 古董；古代的                           |                      |
| contravene     | 违背（法规、习俗等）              | violate, conflict with, breach, infract, infringe, offend, transgress   |                                          |                      |
| forebode       | 预感，预示；预兆                | foretell, predict, premonition, prenotion                               |                                          |                      |
| exclusive      | （人）孤僻的；（物）专用的           |                                                                         |                                          |                      |
| inexplicable   | 无法解释的                   |                                                                         |                                          |                      |
| dilate         | 使膨胀；使扩大                 | swell, expand, distent,                                                 |                                          |                      |
| grief          | 忧伤，悲伤                   | deep or violent sorrow                                                  | brief 简介，简短的                             |                      |
| commiserate    | 同情，怜悯                   | sorrow or pity for sb.                                                  |                                          |                      |
| oblivious      | 遗忘的；忘却的；疏忽的             | forgetful, unmindful                                                    |                                          |                      |
| ameliorate     | 改善，改良                   | improve                                                                 |                                          |                      |
| shunt          | 使（火车）转到另一个轨道；改变（某物的）方向的 |                                                                         |                                          |                      |
| 16           | 16              | 16                                                                 | 16                             | 16                        |
| inimitable   | 无法效仿的           |                                                                    | in + imitate + able            |                           |
| concomitant  | 伴随的             | accompanying, attendant                                            | con + come + itant             |                           |
| irritating   | 刺激的；使生气的        | irritative, annoying                                               |                                |                           |
| compendium   | 简要，概略           | digest, pandect, sketch, syllabus, summary, abstract               | recapitulate 扼要概述；synoptic 摘要的 |                           |
| boastfulness | 自吹自擂；浮夸         |                                                                    | boast v.                       |                           |
| suspension   | 暂停；悬浮           | abeyance, quiescence                                               |                                |                           |
| deprecatory  | 不赞成的，反对的        | disapproving                                                       |                                | be deprecatory about sth. |
| disperse     | 消散，驱散           | dispel, dissipate, scatter                                         |                                |                           |
| perennial    | 终年的，全年的；永久的，持久的 | continuing, eternal, lifelong, permanent, perpetual, enduring      |                                |                           |
| evasive      | 回避的，逃避的；推脱的     |                                                                    | evade v.                       |                           |
| predisposed  | 预设的；有倾向的；使易受感染  | dispose in advance, make susceptible                               |                                |                           |
| monotony     | 单调，千篇一律         | tedious sameness, humdrum, monotone                                |                                |                           |
| overt        | 公开的，非秘密的        | apparent, manifest                                                 |                                |                           |
| huddle       | 挤成一团；一推人        | crowd, nestle close                                                | huddle together to handle sth. |                           |
| thatch       | 以茅草覆盖；茅草屋顶      |                                                                    |                                |                           |
| infirm       | 虚弱的             | physically weak                                                    |                                |                           |
| unstinting   | 慷慨的，大方的         | very generous                                                      | skinflint 吝啬鬼                  |                           |
| abysmal      | 极深的；糟透的         | bottomless, unfathomable, wretched, immeasurably bad               |                                |                           |
| postulate    | 要求，假定           | demand, claim, assume, presume                                     |                                |                           |
| nonviable    | 无法生存的，不能存活的     |                                                                    | non + viable                   |                           |
| calummy      | 诽谤，中伤           | defamation, aspersion, calumniation, denigration, vilification     | slanderous 诽谤的；denigrate 诽谤    |                           |
| commodious   | 宽敞的             | spacious, roomy                                                    |                                |                           |
| parable      | 寓言              |                                                                    | allegory 寓言                    |                           |
| sterile      | 贫瘠且没有植被的；无细菌的   |                                                                    |                                |                           |
| clutter      | 弄乱；凌乱           |                                                                    | scatter 四散逃窜                   |                           |
| auspices     | 支持，赞助           | approval and support                                               |                                |                           |
| respite      | 休息，暂缓           |                                                                    | despite, in spite of 总是        |                           |
| invigorate   | 鼓舞，使精力充沛        |                                                                    | vigor 活力                       |                           |
| probity      | 刚直，正直           | uprightness, honesty                                               |                                |                           |
| impetuous    | 冲动的，鲁莽的         | impulsive, sudden                                                  | an impetuous decision          |                           |
| abeyance     | 中止，搁置           | temporary suspension                                               | “又被压死”                         | in abeyance               |
| noncommittal | 态度暧昧的；不承担责任的    |                                                                    | non + commit + tal             |                           |
| temptation   | 诱惑，诱惑物          | sth. tempting, allurement, decoy, enticement, lure, inveiglement   |                                |                           |
| succinctness | 简洁，简明           | conciseness, concision                                             |                                |                           |
| turbulent    | 混乱的，骚乱的         | causing unrest, violence, disturbance, tempestuous                 |                                |                           |
| mordant      | 讥讽的，尖酸的         | sarcastic, sneering, caustic, ironic                               |                                |                           |
| tribulation  | 苦难，忧患           | distress or suffering resulting from oppression or persecution     |                                |                           |
| angular      | 生硬的，笨拙的         | lacking grace or soomthness, awkward, having angles, thin and bony |                                |                           |
| precede      | 在…之前，早于         | be earlier than                                                    |                                |                           |
| purport      | 意义，涵义，主旨        | meaning conveyed, implied, gist                                    |                                |                           |
| 17              | 17                   | 17                                                                                                 | 17                           | 17                    |
| impeccable      | 无瑕疵的                 | faultless, flawless, fleckless, immaculate, indefectible, irreproachable, perfect                  |                              |                       |
| gospel          | 教义，信条                | doctrine, belief, tenet                                                                            |                              |                       |
| rationale       | 理由；基本原理              | fundamental reasons, maxim, an established principle, axiom                                        |                              |                       |
| ominous         | 恶兆的，不详的              | portentous, of an evil omen                                                                        | omen n.                      |                       |
| viral           | 病毒性的                 |                                                                                                    | virus n.                     |                       |
| particulate     | 微粒的；微粒               | minute separate particle                                                                           |                              |                       |
| urbane          | 温文尔雅的                | notably polite or polished in manner, refined, svelte                                              |                              |                       |
| designate       | 指定，任命；指明，指出          | indicate and set apart for a specific purpose                                                      |                              |                       |
| illustrative    | 解说性的；用作说明的           | serving, tending, designed to illustrate                                                           | for illustrative purpose     |                       |
| potshot         | 盲目射击；肆意抨击            |                                                                                                    |                              |                       |
| delude          | 欺骗，哄骗                | mislead, deceive, trick, beguile, bluff                                                            |                              |                       |
| cogent          | 有说服力的                | compelling, convincing, valid                                                                      |                              | cogent arguments      |
| connoisseur     | 鉴赏家，行家               |                                                                                                    | con + noissse (know) + ur    |                       |
| mite            | 极小量，小虫子              |                                                                                                    | mite 原指螨虫                    |                       |
| veil            | 面纱，遮蔽物；以面纱掩盖         |                                                                                                    |                              |                       |
| pithy           | （讲话或文章）简练有力的；言简意赅的   | concise, tersely cogent                                                                            |                              |                       |
| jolting         | 令人震惊的                | astonishing, astounding, startling, surprising                                                     |                              |                       |
| impute          | 归咎于，归于               | attribute                                                                                          |                              |                       |
| incipient       | 初期的，起初的              | inceptive, initial, initiatory, introductory, nascent, beginning                                   |                              |                       |
| exert           | 运用，行使，施加             | apply with great energy or straining effort                                                        |                              |                       |
| impudent        | 鲁莽的，无礼的              | contemptuous, impulsive, brassy, insolent, flip                                                    |                              |                       |
| guile           | 欺骗，欺诈，狡猾             | deceit, cunning, underhanded, trickery, impostor, cheat, dissemblance, dissimluation               |                              |                       |
| homogeneity     | 同种，同质                |                                                                                                    | homogeneous                  |                       |
| effervescence   | 冒泡；活泼                | bubbling, hissing, foaming, liveliness, exhilaration, buoyancy, ebullience, exuberance, exuberancy |                              |                       |
| heinous         | 十恶不赦的，可憎的            | reprehensible, abominable                                                                          | hate                         |                       |
| economy         | 节约；经济；经济的            | frugality                                                                                          |                              |                       |
| abhor           | 憎恨，厌恶                | detest, abominate, loathe, execrate, hate, antipathy,                                              | repellent, repugnant 令人厌恶的   |                       |
| underlie        | 位于…之下；构成…的基础；（经济）优先于 |                                                                                                    |                              |                       |
| ostracize       | 排斥；放逐                | banish, expatriate, expulse, oust, exile by ostracism                                              |                              |                       |
| anatomical      | 解剖学的                 |                                                                                                    | anatomy 解剖学                  |                       |
| conscientious   | 尽责的；小心谨慎的            | careful, scrupulous                                                                                |                              | be conscientious of … |
| ossify          | 骨化，僵化                | into bone                                                                                          | oss （骨头） + ify               |                       |
| proselytize     | （使）皈依                | recruit or convert to a new faith                                                                  |                              |                       |
| despicable      | 可鄙的，卑劣的              | contemptible, deserving to be despired                                                             |                              |                       |
| adamant         | 坚决的，固执的；强硬的          | obdurate, rigid, unbending, unyielding, inflexible, unbreakable                                    | remain adamant in …          |                       |
| untreated       | 未治疗的，未经处理的           |                                                                                                    |                              |                       |
| exposure        | 暴露，显露，曝光             |                                                                                                    | expose v.                    |                       |
| dictate         | 口述；命令                |                                                                                                    | speak, read aloud, prescribe |                       |
| secular         | 世俗的，尘世的              | worldly rather than spiritual                                                                      |                              |                       |
| scarcity        | 不足的，缺乏的              |                                                                                                    | scarce adj.                  | scarcity of …         |
| vestige         | 痕迹，遗迹                | relic, trace                                                                                       |                              |                       |
| explicitly      | 明白的；明确的              | definitely, distinctly, expressly                                                                  |                              |                       |
| intensification | 增强，加剧，激烈化            |                                                                                                    | intensify v.                 |                       |
| antedate        | 填写比实际日期早的日期；早于       | antecede, foreun, pace, precede, predate                                                           |                              |                       |
| ratify          | 批准                   | confirm, approve formally                                                                          |                              |                       |
| pensive         | 沉思的；忧心忡忡的            | reflective, meditative, anxious                                                                    | pension 养老金                  |                       |
| drizzly         | 毛毛细雨的                |                                                                                                    |                              |                       |
| barren          | 不育的，贫瘠的，不结果实的        | sterile, bare                                                                                      | “拔了”                         |                       |
| glossary        | 词汇表，难词表，             |                                                                                                    |                              |                       |
| salient         | 显著的，突出的              | noticeable, conspicuous, prominent, outstanding                                                    |                              |                       |
| regale          | 款待，宴请；使…快乐           | feast with delicacies (delicacy)                                                                   |                              |                       |
| emblematic      | 作为象征的                | symbolic, representative                                                                           | emblem n.                    |                       |
| intuition       | 直觉，直觉知识              |                                                                                                    | intuit v.                    |                       |
| eloquent        | 雄辩的，流利的              | articulate, forceful and fluent expression                                                         |                              |                       |
| fused           | 熔化的                  |                                                                                                    | fuse v.                      |                       |
| unadorned       | 未装饰的，朴素的             | austere, homespun, plain                                                                           |                              |                       |
| discomfort      | 使难堪，使困惑              | to make uneasy, disconcert, embarrass                                                              |                              |                       |
| 18            | 18                      | 18                                                                                                      | 18                                                | 18            |
| avaricious    | 贪婪的，贪心的                 | greedy, insatiably, insatiable                                                                          | avarice n.                                        |               |
| irrevocable   | 不可撤销的                   |                                                                                                         | ir + revoke + able                                |               |
| debilitate    | 使衰弱                     | weaken, make weak or feeble, depress, enervate                                                          |                                                   |               |
| intractable   | 倔强的；难管理的；难加工的           | difficult to manipulate, manage, stubborn, unruly                                                       |                                                   |               |
| serenity      | 平静                      | placidity, repose, tranquility                                                                          | serene adj.                                       |               |
| debauch       | 使堕落，败坏；堕落               | corrupt, deprave, pervert                                                                               |                                                   |               |
| render        | 呈递，提供；给予，归还             |                                                                                                         | lender 借出者，render 归还                              |               |
| repel         | 击退；使反感                  | fight against, resist, cause aversion                                                                   |                                                   |               |
| thematic      | 主题的                     |                                                                                                         | theme                                             |               |
| stringy       | 吝啬的，小气的                 | not generous or liberal, misery, chinchy, niggard, parsimonious, penurious, pinchpenny                  |                                                   |               |
| exhaust       | 耗尽，使筋疲力尽；（机器）废气         |                                                                                                         |                                                   |               |
| umbrage       | 不快，愤怒                   | pique, resentment, insult                                                                               | umbra 影子                                          |               |
| scramble      | 攀登；搅乱，使混乱；争夺            |                                                                                                         | scale 攀登 + amble 行走                               |               |
| nonplussed    | 不知所措的，陷于困境的             |                                                                                                         | nonplus v.                                        |               |
| belie         | 掩饰，证明为假                 | disguise, misrepresent, prove false, distort, falsify, garble, misstate                                 |                                                   |               |
| exacerbate    | 使加重，使恶化                 | aggravate disease, pain, annoyance, etc                                                                 |                                                   |               |
| propitious    | 吉利的，顺利的；有利的             | auspicious, favorable, advantageous                                                                     |                                                   |               |
| irrigate      | 灌溉；冲洗（伤口）               |                                                                                                         |                                                   |               |
| squalid       | 污秽的，肮脏的                 | dirty, seedy, slummy, sordid, unclean                                                                   |                                                   |               |
| assimilate    | 同化，吸收                   | to absorb and incorporate                                                                               | a + simil 相同 + ate；dissemblance, dissimluation 欺骗 |               |
| antithesis    | 对立，相对                   | contract or opposition                                                                                  |                                                   |               |
| endow         | 捐赠；赋予                   | contribute, bequeath                                                                                    |                                                   |               |
| impugn        | 提出异议；对…表示怀疑             | to challenge as false or questionable                                                                   |                                                   |               |
| sequester     | （使）隐退；使隔离               | seclude, withdraw, set apart                                                                            | sequestrate 扣押                                    |               |
| capitulate    | （有条件的）投降                | surrender conditionally                                                                                 |                                                   |               |
| subjective    | 主观的，想象的                 |                                                                                                         | subject n.                                        |               |
| parsimony     | 过分节俭，吝啬                 | being stringy, thrift                                                                                   |                                                   |               |
| singularity   | 独特；奇点（天文上密度无限大，体积无限小的点） | peculiarity, unusual or distinctive manner                                                              | singular 单数的；非凡的                                  |               |
| paramount     | 最重要的；至高无上的              |                                                                                                         | para + mount 山；surmount 超过                        |               |
| bellicose     | 好战的，好斗的                 | warlike, belligerent                                                                                    | bell 战斗                                           |               |
| revile        | 辱骂；恶言相向                 | rail, use abusive language                                                                              |                                                   |               |
| sprawl        | 散乱的延伸；四肢摊开坐、卧、躺         |                                                                                                         | sprawling, rampant 植物蔓生的；（城市）无法计划开展的              |               |
| retire        | 撤退，后撤；退休，退役             | withdraw, move back                                                                                     |                                                   | retire from … |
| upstage       | 高傲的                     | haughty                                                                                                 | up + stage                                        |               |
| unscathed     | 未受损伤的；未受伤害的             | wholly unharmed                                                                                         | un + scathed                                      |               |
| defiance      | 挑战，违抗，反抗                | open disobedience, contempt, contumacy, recalcitrance, stubbornness                                     | defiant 藐视                                        |               |
| amble         | 漫步                      | ambul, saunter                                                                                          | 本身是词根                                             |               |
| defect        | 缺点，瑕疵；变节，脱党             | fault, flaw, forsake, blemish                                                                           | infect 感染                                         |               |
| stipulate     | 要求以…为条件；约定；规定           | to make an agreement                                                                                    |                                                   |               |
| proposition   | 看法，主张                   | proposal, opinion, statement, expresses, judgement                                                      |                                                   |               |
| trespass      | 侵犯；闯入私人空间               | make an unwarranted or uninvited incursion                                                              |                                                   |               |
| appall        | 使惊骇；使胆寒                 | shock, fill with horror or dismay                                                                       | a + pale 苍白                                       |               |
| reactionary   | 极端保守的；反动的               | ultraconservative                                                                                       | re 反 + actionary 动的                               |               |
| balmy         | 芳香的；温和的；止痛的             | soothing, mild, pleasant                                                                                | balm                                              |               |
| tepid         | 微温的；不热情的                | moderately warm, lacking in emotional warmth or enthusiasm, halfhearted, lukewarm, unenthusiastic       |                                                   |               |
| complicate    | 使复杂化                    | make sth, more difficult, entangle, muddle, perplex, ravel, snarl                                       |                                                   |               |
| unobstructed  | 没有阻碍的                   | free from obstructions                                                                                  |                                                   |               |
| presume       | 推测，假定；认定                |                                                                                                         | pre 预先 + sum 抓住 + e                               |               |
| obdurate      | 固执的，顽固的                 | stubbornly persistent, inflexible, adamant, dogged, obdurate, rigid, unbending, unyielding, unbreakable |                                                   |               |
| tortuous      | 曲折的，拐弯抹角的；弯弯曲曲的         | devious, indirect tactics, winding                                                                      |                                                   |               |
| fray          | 吵架，打斗；磨破                | noisy quarrel or fight, become worn, ragged, or reveled by rubbing                                      |                                                   |               |
| pestilential  | 引起瘟疫的；致命的；极讨厌的          | causing pestilence, deadly, irritating                                                                  | pestilence 瘟疫                                     |               |
| unfounded     | 无事实根据的                  | groundless, unwarranted                                                                                 |                                                   |               |
| ramshackle    | 摇摇欲坠的                   | rickety                                                                                                 | ram + shake                                       |               |
| truncate      | 截短，缩短                   | shorten by cutting off                                                                                  |                                                   |               |
| inflict       | 使遭受（痛苦、损伤等）             | cause sth, unpleasant or be endured                                                                     |                                                   |               |
| communal      | 全体共同的，共享的               | held in common                                                                                          |                                                   |               |
| unctuous      | 油质的；油腔滑调的               | fatty, oily, greasy, oleaginous                                                                         |                                                   |               |
| venerable     | 值得尊敬的，庄严的               | august, revered, respectable                                                                            | venerate v.                                       |               |
| contemplative | 沉思的（人）                  |                                                                                                         |                                                   |               |
| incorrigible  | 积习难改的，不可救药的             |                                                                                                         | in + correct + able                               |               |
| 19           | 19                   | 19                                                                                                                                             | 19                                                      | 19                   |
| sketch       | 草图；概略；画草图，写概略        | compendium, digest, syllabus, simple description or outline                                                                                    |                                                         |                      |
| schematic    | 纲要的，图解的              | of schema or diagram                                                                                                                           |                                                         |                      |
| folly        | 愚蠢；愚蠢的想法             | foolery, idiocy, insanity, foolish action, lack of wisdom                                                                                      |                                                         |                      |
| civility     | 彬彬有礼，斯文              | politeness, etiquette, courtesy, decorum                                                                                                       |                                                         |                      |
| eschew       | 避开，戒绝                | shun, avoid, abstain                                                                                                                           | es 出 + chew 咀嚼 吃一堑长一智                                   |                      |
| translucent  | （半）透明的               |                                                                                                                                                |                                                         |                      |
| endemic      | 地方性的                 | native, restricted in a locality or region                                                                                                     |                                                         |                      |
| ordeal       | 严峻的考验                | difficult and severe trial, tribulation, affliction                                                                                            |                                                         |                      |
| denote       | 指示，表示                | mark, indicate, signify                                                                                                                        |                                                         |                      |
| confide      | 吐露（心事）；倾诉            | tell confidentially, show confidence by imparting secrets                                                                                      |                                                         |                      |
| arresting    | 醒目的，引人注意的；断言         | catching the attention, gripping                                                                                                               | assert v.                                               |                      |
| obviate      | 排除，消除                | remove, get rid of, eliminate, exclude, preclude                                                                                               |                                                         |                      |
| precipitous  | 陡峭的                  | steep, perpendicular, overchanging in rise or fall                                                                                             |                                                         |                      |
| frenetic     | 狂乱的，发狂的              | frantic, frenzied, wild with anger                                                                                                             | fry + enetic                                            |                      |
| afflict      | 折磨，使痛苦               | cause persistent pain or suffering                                                                                                             |                                                         |                      |
| chagrin      | 失望，懊恼                | annoyance and disappointment                                                                                                                   | cha 茶 + grin 笑                                          |                      |
| chary        | 小心的，审慎的              | careful, cautious, calculating, circumspect, discreet, gingerly, wary                                                                          |                                                         | be chary of/about    |
| grandiose    | 宏伟的；浮夸的              |                                                                                                                                                | grand                                                   |                      |
| scrupulous   | 恪守道德规范的，一丝不苟的        | having moral integrity, punctiliously exact, conscientious, meticulous, punctilious                                                            |                                                         |                      |
| obsequious   | 逢迎的，谄媚的              | menial, servile, slavish, subservient                                                                                                          | ubiquitous 无处不在的；iniquitous 邪恶的，不公正的；quixotic 不切实际的，空想的 |                      |
| spongy       | 像海绵的，不坚实的            | resembling a sponge, not firm or solid                                                                                                         |                                                         | spongy topsoil 松软的表土 |
| vexation     | 恼怒，苦恼                | harassing, irritation                                                                                                                          | vex v.                                                  |                      |
| deluge       | 大洪水；暴雨               | great flood, heavy rainfall                                                                                                                    |                                                         |                      |
| prod         | 戳，刺；刺激，激励            | poke, stir up, urge                                                                                                                            | prod sb, into doing sth.                                |                      |
| extricate    | 可解救的，能脱险的            | capable of being freed from difficulty                                                                                                         |                                                         |                      |
| assuredness  | 确定，自信                |                                                                                                                                                | assure v.                                               |                      |
| pinpoint     | 准确的确定；使突出，引起注意；极其精神的 |                                                                                                                                                | pin 针 + point 针尖                                        |                      |
| ignominy     | 羞耻，耻辱                | shame or dishonor, infamy, disgrace, disrepute, opprobrium                                                                                     | ig 不 + nominy 名声                                        |                      |
| heretical    | 异端的，异教的              | dissident, heterodox, unorthodox                                                                                                               | heresy, heretic n.                                      |                      |
| agrarian     | 土地的                  | of land                                                                                                                                        |                                                         |                      |
| anomalous    | 反常的；不协调的             | inconsistent with what is usual, normal, or expected, marked by icongruity or contradiction, aberrant, abnormal, deviant, divergent, irregular |                                                         |                      |
| uneven       | 不平坦的，不一致的，不对等的       | not even, not uniform, unequal                                                                                                                 |                                                         |                      |
| acrimonious  | 尖酸刻薄的，激烈的            | caustic, biting, rancorous, indigent, irate, ireful, wrathful, wroth                                                                           |                                                         |                      |
| repulse      | 击退，回绝；拒绝             | repel, rebuff, rejection                                                                                                                       | re + pulse                                              |                      |
| prose        | 散文                   |                                                                                                                                                | p + rose                                                |                      |
| venal        | 腐败的，贪赃枉法的            | characterized by corrupt, bribery                                                                                                              |                                                         |                      |
| acute        | 灵敏的，敏锐的；剧烈的，急性的      | keen, shrewd, sensitive, sharpness, severity                                                                                                   |                                                         |                      |
| annotate     | 注解                   |                                                                                                                                                | an + note + ate                                         |                      |
| blockbuster  | 巨型炸弹；一鸣惊人的事物         | large high-explosive bomb                                                                                                                      |                                                         |                      |
| deprecation  | 反对                   | disapproval                                                                                                                                    | deprecate v.                                            |                      |
| rectify      | 改正，矫正；提纯             |                                                                                                                                                | rect 直的 + ify                                           |                      |
| unearth      | 挖出，发现                | dig up out of the earth                                                                                                                        |                                                         |                      |
| epoch        | 新纪元；重大的事件            |                                                                                                                                                |                                                         |                      |
| unnoteworthy | 不显著的，不值得注意的          |                                                                                                                                                | un + noteworthy                                         |                      |
| circumspect  | 慎重的                  | consider all circumstances                                                                                                                     |                                                         |                      |
| gloat        | 幸灾乐祸的看，心满意足的看        | America                                                                                                                                        |                                                         | gloat over sth.      |
| brevity      | 短暂                   | shortness or duraion                                                                                                                           |                                                         |                      |
| enduring     | 持久的，不朽的              | lasting                                                                                                                                        |                                                         |                      |
| pernicious   | 有害的，致命的              | noxious, deadly                                                                                                                                |                                                         |                      |
| decipher     | 破译；解开                | decode, make out the meaning, crack, decrypt                                                                                                   |                                                         |                      |
| phenomenal   | 显著的，非凡的              | extraordinary, remarkable                                                                                                                      |                                                         |                      |
| impenetrable | 不能穿透的；不可理解的          | incapable of being penetrated, unfathomable, inscrutable, impassable, impermeable, imperviable, impervious                                     |                                                         |                      |
| enervate     | 使虚弱，使无力              | lessen the vitality or strength of                                                                                                             |                                                         |                      |
| 20            | 20               | 20                                                                                                                   | 20                                                                 | 20                                            |
| balk          | 梁木；大梁；妨碍；畏缩不前    | a large squared wooden beam, baffle, bilk, foil, thwart                                                              |                                                                    |                                               |
| carnage       | 大屠杀，残杀           | NANJING CARNAGE, bloodbath, massacre                                                                                 | carnivorous 肉食动物的；carnivore 肉食动物                                   |                                               |
| spanking      | 强烈的，疾行的          | strong, fast                                                                                                         |                                                                    | a spanking pace/rate                          |
| steller       | 星的，星球的           | of or relating to stars                                                                                              |                                                                    |                                               |
| explicate     | 详细解说             | make clear or explicit, explain fully, elucidate, illustrate, interpret                                              |                                                                    |                                               |
| bleach        | 去色，漂白；漂白剂        |                                                                                                                      | b + leach 过滤                                                       |                                               |
| baffle        | 使困惑，难到           | confuse, puzzle, confound                                                                                            |                                                                    |                                               |
| pejorative    | 轻视的，贬低的          | tending to disparage, depreciatory, belittle, derogate, to speak slightingly of, depreciate, decry, denounce, debase |                                                                    |                                               |
| ponderous     | 笨重的，（因太重太大）不便搬运的 | unwieldy or clumsy because of weight or size                                                                         | pond 大的，沉的                                                         |                                               |
| particular    | 特别是；细节，详情        | fact, detail, item                                                                                                   |                                                                    |                                               |
| pedagogic     | 教育学的             | of or relating to education                                                                                          | Pedagogy 教育学；pedant 学究                                             |                                               |
| fluid         | 流体的，流动的；易变的，不固定的 | capable of flowing, subject to change or movement                                                                    |                                                                    |                                               |
| visible       | 可见的，明显的          |                                                                                                                      | vision + able                                                      |                                               |
| pungent       | 辛辣的，刺激的；尖锐的，尖刻的  | piquant, sharp and to the point                                                                                      | pung 尖刺 + ent                                                      |                                               |
| eligible      | 合格的，有资格的         | suitable, qualified                                                                                                  |                                                                    |                                               |
| superb        | 上乘的，出色的          | highest degree of excellence, brilliance, or competence, lofty, sublime                                              |                                                                    |                                               |
| supersede     | 淘汰；取代            | take the position, force out as inferior                                                                             |                                                                    |                                               |
| ambivalent    | （对人对物）有矛盾看法的     | having contradictory attitudes towards sth.                                                                          |                                                                    |                                               |
| compliant     | 服从的，顺从的          | complying, yielding, submissive                                                                                      | com + pliant 柔顺的；Complaint 抱怨；plaintive 伤的，哀伤的                     | 悲                                             |
| concentration | 专心，专注；集中         |                                                                                                                      | concentrate v                                                      |                                               |
| heed          | 注意，留心            | give careful attention to                                                                                            | need 需要                                                            |                                               |
| incentive     | 刺激，诱因，动机；刺激因素    | motive, motivate, goad, impetus, impulse, stimulus, incite                                                           |                                                                    |                                               |
| tapering      | 尖端细的；渐进式；圆锥      |                                                                                                                      | taper 逐渐变细                                                         |                                               |
| forsake       | 遗弃；放弃            | leave, abandon, give up, renounce                                                                                    | seek 寻觅                                                            |                                               |
| stout         | 肥胖的；强壮的          | bulky in body, vigorous, sturdy                                                                                      |                                                                    |                                               |
| incendiary    | 防火的，纵火的          |                                                                                                                      | in + cend 发（火）光+ iary                                              |                                               |
| incandescent  | 遇热发光的；发白热光的      | Incandescent lamps 白炽灯                                                                                               | candy 蜡烛，糖果                                                        |                                               |
| fussy         | 爱挑剔的，难取悦的        | dainty, exacting, fastidious, finicky, meticulous                                                                    | fuzzy 模糊的                                                          |                                               |
| imprudent     | 轻率的，不理智的         | indiscreet, not wise                                                                                                 |                                                                    |                                               |
| wearisome     | 令人厌倦、疲倦          |                                                                                                                      | weary v.                                                           |                                               |
| truce         | 停战，休战            | Moratorium                                                                                                           |                                                                    |                                               |
| carve         | 雕刻；切成片           | shape by cutting, shipping, hewing, slice, cleave, dissect, disserve, sever, split                                   |                                                                    |                                               |
| demean        | 贬抑；降低            | degrade, humble, belittle                                                                                            |                                                                    |                                               |
| nominally     | 名义上的，有名无实的       |                                                                                                                      | nominal 名义上的                                                       |                                               |
| proverb       | 谚语               | brief popular epigram or maxim                                                                                       | pro 早的 + verb 说                                                    |                                               |
| refrain       | 抑制；叠句            | curb, restrain, regular recurring phrase or verse                                                                    | re + frain 笼头                                                      |                                               |
| draft         | 草稿；草案；汇票         | preliminary written version of sth.                                                                                  |                                                                    |                                               |
| pack          | 包裹；兽群            | a number of wild animals                                                                                             |                                                                    |                                               |
| humdrum       | 单调的，乏味的          | dull, monotonous, boring                                                                                             | hum 嗡嗡 + drum 鼓声                                                   |                                               |
| nuance        | 细微差异             | subtle difference, nicety, shade, subtlety, refinement                                                               |                                                                    |                                               |
| philistine    | 庸俗的人；对文化艺术无知的人   | a smug, ignorant, especially middle-class person                                                                     | Philistia 腓力斯人；煤老板                                                 |                                               |
| sabotage      | 阴谋破坏，颠覆活动        | deliberate subversion                                                                                                |                                                                    | an act of military sabotage                   |
| anathema      | 被诅咒的人，（天主教的）革出教门 | one that is cursed, a formal ecclesiastical ban, curse                                                               |                                                                    |                                               |
| chastisement  | 惩罚               | punishment, castigation, penalty                                                                                     |                                                                    |                                               |
| indulgent     | 放纵的，纵容的          | characterized by indulgence                                                                                          | indulge, cosset, pamper, spoil 放纵，纵容；满足                            | indulgent parents                             |
| monetary      | 金钱的；货币的          | about money, nation's currency or coinage, financial, fiscal, pecuniary                                              | money n.                                                           |                                               |
| equivocator   | 说话模棱两可的人         |                                                                                                                      | equivocate 拐弯抹角 v.                                                 |                                               |
| humility      | 谦逊；谦恭            | being humble, modesty                                                                                                | humble adj.；humiliate 羞辱，使丢脸                                       |                                               |
| wane          | 减少，衰落            | decrease in size, extent, scope, or degree, dwindle, abate, ebb, shrink, slacken, subside                            | swan 天鹅 wane 衰退（天鹅湖被制裁）                                            |                                               |
| ingeniousness | 独创性              | ingenuity                                                                                                            |                                                                    |                                               |
| indignation   | 愤慨，义愤            | anger or scorn, righteous anger                                                                                      |                                                                    | be full of righteous indignation              |
| sonorous      | （声音）洪亮的          | full or loud in sound                                                                                                |                                                                    |                                               |
| woolly        | 羊毛的；模糊的          | lacking sharp detail or clarity                                                                                      | wool n.                                                            |                                               |
| seismic       | 地震的；由地震引起的       | of or caused by an earthquake                                                                                        | seism 地震 tsunami, quake                                            |                                               |
| sustain       | 承受（困难）；支撑（重量或压力） | undergo, withstand, bolster, prop, underprop                                                                         |                                                                    |                                               |
| actuate       | 开动，促使            | motivate, activate                                                                                                   | accurate 准确的                                                       |                                               |
| strait        | 海峡；狭窄的           | narrow                                                                                                               | isthmus 地峡；tacit 心照不宣的                                             |                                               |
| drab          | 黄褐色的；单调的，乏味的     | yellowish brown, not bright or lively, monotonous                                                                    | "daub 涂抹；乱画                                                        | "                                             |   |
| subdue        | 征服；压制；减轻         | crush, overpower, subjugate, conquer, vanquish, reduce                                                               |                                                                    |                                               |
| disinterested | 公正的，客观的          | impartial, unbiased, detached, dispassionate, neutral                                                                | uninsterested 不感兴趣的                                                | be disinterested in …, disinterested attitude |
| vitiate       | 削弱，损害            | make faulty or defective, impair                                                                                     | vice 恶的 + tate                                                     |                                               |
| dissipate     | （使）消失，消散；浪费      | break up and scatter or vanish, to waste or squander                                                                 |                                                                    |                                               |
| discretion    | 谨慎，审慎            | circumspection, prudence                                                                                             |                                                                    |                                               |
| instantaneous | 立即的，即刻的；瞬间的      | immediate, occuring                                                                                                  | instant 立即的，顷刻                                                     |                                               |
| sully         | 玷污，污染            | make soiled or tarnished, defile, stain, tarnish                                                                     | blemished, discolored, marked, spotted, tarnished, stained 污染的，玷污的 |                                               |
| ceramic       | 陶器的，陶瓷制品         |                                                                                                                      | ceram 陶瓷                                                           |                                               |
| unregulated   | 未受控制的，未受约束的      | not controlled                                                                                                       |                                                                    |                                               |
| unconfirmed   | 未经证实的            | not proved to be true, not confirmed                                                                                 |                                                                    |                                               |
| debatable     | 未决定的，有争论的        | open to dispute                                                                                                      | debate v.                                                          |                                               |
| specious      | 似是而非的；华而不实的      | tawdry                                                                                                               | spec 看 + ious                                                      |                                               |
| ballad        | 歌谣，小曲            |                                                                                                                      | “芭蕾的”                                                              |                                               |
| gall          | 胆汁；怨恨            | bile, hatred, bitter feeling                                                                                         | wall 墙                                                             |                                               |
| proliferate   | 激增；（迅速）繁殖，增生     | increase rapidly, multiply, grow by rapid production                                                                 |                                                                    |                                               |
