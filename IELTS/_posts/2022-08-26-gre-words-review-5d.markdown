---
layout: post
title:  "GRE Words 5 Day Review"
date:   2022-08-26 13:52:01 +0800
category: IELTSPosts
---

# New Words (5-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English       | Chinese         | Synonyms                                                      | Helpful Memory Ideas       | Usage |
|---------------|-----------------|---------------------------------------------------------------|----------------------------|-------|
| 1             | 1               | 1                                                             | 1                          | 1     |
| fickleness    | 浮躁；变化无常         | inconstancy                                                   |                            |       |
| deplore       | 悲悼，哀叹；谴责        | bemoan, bewail, grieve, lament, moan                          |                            |       |
| corroboration | 证实，支持           | corroborate                                                   | corroborate 支持，强化          |       |
| supplant      | 排挤，取代           |                                                               | sub + plant                |       |
| indigenous    | 土产的，本地的；生来的，固有的 | native, innate                                                | indi + gen + ous           |       |
| rigid         | 严格的；僵硬的，刚硬的     | stiff, incompliant, inflexible, unpliable, unyielding         |                            |       |
| sumptuous     | 豪华的，奢侈的         | deluxe, palatial, extremely costly, luxurious, or magnificent | sumpt 归纳 + uous            |       |
| obstinacy     | 固执，倔强，顽固        | stubbornness                                                  | obstinate is the adj. form |       |
| premeditate   | 预谋，预先考虑或安排      |                                                               | pre + mediate              |       |
| contemptible  | 令人轻视的           |                                                               | contempt is the n. form    |       |
| insipid       | 乏味的，枯燥的         | distasteful, savorless, unappetizing, unpalatable, unsavory   |                            |       |
| 2           | 2                | 2                                                                    | 2                    | 2     |
| demotic     | 民众的；通俗的          |                                                                      |                      |       |
| resemble    | 与…相似，像           | be similar to                                                        |                      |       |
| scruple     | 顾忌，迟疑            | boggle, stickle, an ethical consideration, hesitate                  |                      |       |
| sparing     | 节俭的              | frugal, thrifty                                                      | spare v.             |       |
| intimation  | 暗示               | clue, cue, hint, suggestion                                          |                      |       |
| defiant     | 反抗的，挑衅的          | bold, impudent                                                       |                      |       |
| deciduous   | 非永久的；短暂的；脱落的；落叶的 |                                                                      | de + ciduous         |       |
| murky       | 黑暗的，昏暗的；朦胧的      | drak, gloomy, vague                                                  | murk n.              |       |
| deferential | 顺从的，恭顺的          | duteous, dutiful                                                     | deference n.         |       |
| denigrate   | 污蔑，诽谤            | defame, blacken, to disparage the character or reputation of         |                      |       |
| bumper      | 特大的；保险杠          | unusually large, a device for absorbing shock or preventing damage   |                      |       |
| stint       | 节制，限量，节省         | to restrict                                                          | stint on sth. 吝惜…    |       |
| repellent   | 令人厌恶的            | loathsome, odious, repugnant, revolting, arousing disgust, repulsive |                      |       |
| trite       | 陈腐的，陈词滥调的        | bathetic, commonplace, hackneyed or boring                           |                      |       |
| stratify    | （使）层化            | to divide or arrange into classes, castes, or social strata          |                      |       |
| fluctuate   | 波动，变动            | to undulate as waves, to be continually changing                     |                      |       |
| engender    | 产生，引起            | cause, generate, induce, provoke, to produce, beget                  |                      |       |
| 3            | 3                  | 3                                                                                     | 3                    | 3                   |
| anticipatory | 预想的，预期的            |                                                                                       | anticipate v.        |                     |
| enamored     | 倾心的，被迷住            | bewitched, captivated, charmed, enchanted, infatuated, inflamed with love, fascinated |                      | be enamored of sth. |
| rehabilitate | 使恢复（健康、能力、地位等）     |                                                                                       | re + habilitate      |                     |
| entrenched   | （权力、传统）确立的，牢固的     | strongly established                                                                  |                      |                     |
| repeal       | 废除（法律）             |                                                                                       | re + peal            |                     |
| impecunious  | 不名一文的；没钱的          | having very little or no money                                                        |                      |                     |
| conspiracy   | 共谋，阴谋              |                                                                                       | conspire v.          | conspiracy against  |
| tributary    | 支流，进贡国；支流的，辅助的，进贡的 | making additions or yielding supplies, contributory                                   |                      |                     |
| abet         | 教唆；鼓励，帮助           | to incite, encourage, urge and help on                                                |                      |                     |
| 4             | 4                | 4                                                                           | 4                    | 4             |
| incarnate     | 具有人体的；化身的，拟人化的   | given in a bodily form, personified                                         |                      |               |
| surmount      | 克服，战胜；登上         | to prevail over, overcome, to get to the top of                             | sur + mount          |               |
| choppy        | 波浪起伏的；（风）不断改变方向的 | rough with small waves, changeable, variable                                |                      |               |
| tout          | 招徕顾客，极力赞扬        | to praise or publicize loudly                                               |                      |               |
| virtuous      | 有美德的             | showing virtue                                                              | virtue n.            |               |
| plaintive     | 悲伤的，哀伤的          | expressive of woe, melancholy                                               | plaint n. 哀诉         |               |
| penitent      | 悔过的，忏悔的          | expressing regretful pain, repentant                                        |                      |               |
| impound       | 限制；依法没收，扣押       | to confine, to seize and hold in the custody of the law, take possession of |                      |               |
| succor        | 救助，援助            | to go to the aid of                                                         |                      |               |
| cavil         | 调毛病；吹毛求疵         | quibble                                                                     |                      | cavil at sth. |
| torpor        | 死气沉沉             | dullness, languor, lassitude, lethargy, extreme sluggishness of function    |                      |               |
| superannuate  | 使退休领养老金          | to retire and pension because of age or infirmity                           |                      |               |
| rave          | 热切赞扬；胡言乱语，说疯话    |                                                                             |                      |               |
| divert        | 转移；（使）转向；使娱乐     |                                                                             |                      |               |
| procurement   | 取得，获得；采购         |                                                                             | procure v.           |               |
| arduous       | 费力的，艰难的          | marked by great labor or effort                                             |                      | arduous march |
| exasperate    | 激怒，使恼怒           | to make angry, vex, huff, irritate, peeve, pique, rile, roil                |                      |               |
| domesticated  | 驯养的，家养的          |                                                                             | dome + sticated      |               |
| vacillate     | 游移不定，踌躇          | to waver in mind, will or feeling                                           |                      |               |
| benevolent    | 善心的，仁心的          | altruistic, humane, philanthropic, kindly, charitable                       |                      |               |
| castigate     | 惩治；严责            | chasten, chastise, discipline, to punish or rebuke severely                 |                      |               |
| veracity      | 真实，诚实            | devotion to the truth, truthfulness                                         |                      |               |
| tenacious     | 坚韧的，顽强的          |                                                                             |                      |               |
| surreptitious | 鬼鬼祟祟的            | clandestinely                                                               |                      |               |
| prodigious    | 巨大的；惊人的，奇异的      | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童           |               |
| 5           | 5               | 5                                                                  | 5                    | 5     |
| inquisitive | 好奇的；爱打听的        | inclined to ask questions, prying                                  | inquire 询问           |       |
| disarray    | 混乱；无秩序          | untidy condition, disorder, confusion                              | dis + array          |       |
| chromatic   | 彩色的，五彩的         | having color or colors                                             |                      |       |
| scorn       | 轻蔑，瞧不起          | contemn, despise                                                   |                      |       |
| reigning    | 统治的，起支配作用的      | benign 良性的，温和的                                                     |                      |       |
| negligent   | 疏忽的，粗心大意的       | neglectful, regardless, remiss, slack                              |                      |       |
| treacherous | 背叛的；叛逆的         | showing great disloyalty and deceit                                |                      |       |
| evince      | 表明，表示           | indicate, make manifest, show plainly                              |                      |       |
| bemuse      | 使昏头昏脑；使迷惑       | bedaze, daze, paralyze, petrify, stun, to puzzle, distract, absorb |                      |       |
| insidious   | 暗中危害的；阴险的       | working or spreading harmfully in a subtle or stealthy manner      |                      |       |
| dilettante  | 一知半解者，业余爱好者     | dabbler, amateur                                                   |                      |       |
| feckless    | 无效的，效率低的；不负责任的  | inefficient, irresponsible                                         |                      |       |
| exotic      | 异国的，外来的；奇异的     | not native, foreign, strikingly unusual                            |                      |       |
| distortion  | 扭曲，曲解           |                                                                    |                      |       |
| forlorn     | 孤独的；凄凉的         | abandoned or deserted, wretched, miserable                         |                      |       |
| trepidation | 恐惧，惶恐           | timorousness, uncertainty, agitation                               |                      |       |
| propensity  | 嗜好，习性           | bent, leaning, tendency, penchant, proclivity                      |                      |       |
| visionary   | 有远见的；幻想的；空想家    | dreamy, idealistic, utopain                                        |                      |       |
| exempt      | 被免除的；被豁免的；免除，豁免 |                                                                    |                      |       |
| 6             | 6               | 6                                                                         | 6                    | 6                        |
| reprisal      | 报复；报复行动         | practice in retaliation                                                   |                      |                          |
| surrender     | 投降；放弃；归还        | give in, give up, give back                                               |                      |                          |
| congenial     | 意气相投的；性情好的；适意的  | companionable, amiable, agreeable                                         |                      |                          |
| irreversible  | 不能撤回的，不能取消的     | not reversible, irrevocable, unalterable                                  |                      |                          |
| bewilder      | 使迷惑，混乱          | to confuse, befog, confound, perplex                                      |                      |                          |
| ineptitude    | 无能；不适当          | the quality or state of being inept                                       | inept adj.           |                          |
| scour         | 冲刷；擦掉；四处搜索      | to clear, dig, remove, rub, to range over in a search                     | "scorn 轻蔑，瞧不起        | "                        |   |
| promulgate    | 颁布（法令）；宣传，传播    | announce, annunciate, declear, disseminate, proclaim                      |                      |                          |
| transitional  | 转变的，变迁的         |                                                                           | transition           |                          |
| diminish      | （使）减少，缩小        | to make less or cause to appear less, belittle, dwindle                   |                      |                          |
| falsify       | 篡改；说谎           | to alter a record, etc. fraudulently, to tell sb. falsehoods, lie         |                      |                          |
| distent       | 膨胀的；扩张的         | swollen, expanded                                                         |                      |                          |
| complementary | 互补的             | combining well to form a whole                                            | complement n. 补充物    | be complementary to sth. |
| rancor        | 深仇；怨恨           | bitter deep-seated ill will, enmity                                       |                      |                          |
| agitate       | 鼓动，煽动；使焦虑       |                                                                           | agitation n.         |                          |
| flagrant      | 罪恶昭著的；公然的       | conspicuously offensive                                                   |                      |                          |
| hypocrisy     | 伪善；虚伪           | cant, pecksniff, sanctimony, sham                                         |                      |                          |
| ethnic        | 民族的，种族的         | of a national, racial or tribal group that has a common culture tradition |                      |                          |
| repugnant     | 令人厌恶的           | strong distaste or aversion                                               |                      | be repugnant to sb.      |
| tread         | 踏，践踏；行走；步态；车轮胎面 | step                                                                      |                      |                          |
| 7                | 7                | 7                                                             | 7                    | 7                                   |
| escalate         | （战争等）升级；扩大，上升，攀升 | to make a conflict more serious, to grow or increase rapidly  |                      |                                     |
| apportion        | （按比例或计划）分配       |                                                               |                      | apportion sth. among/between/to sb. |
| frivolous        | 轻薄的，轻佻的          | marked by unbecoming levity                                   |                      |                                     |
| didactic         | 教诲的；说教的          | morally instructive, boringly pedantic or moralistic          |                      |                                     |
| fraud            | 欺诈，欺骗；骗子         | deceit, trickery, impostor, cheat                             |                      |                                     |
| voluble          | 健谈的；易旋转的         | talkative, rotating                                           |                      |                                     |
| reprimand        | 训诫，谴责            | a severe or official reproof                                  |                      |                                     |
| bluff            | 虚张声势；悬崖峭壁        | pretense of strength, high cliff                              |                      |                                     |
| flimsy           | 轻而薄的；易损坏的        | easily broken or damaged                                      | flim 胶卷              |                                     |
| akin             | 同族的；类似的          | related by blood, essentially similar, related, or compatible |                      |                                     |
| assorted         | 各式各样的；混杂的        | consisting of various kinds, mixed                            |                      |                                     |
| incompetent      | 无能力的；不胜任的        |                                                               | in + competent       |                                     |
| irreverent       | 不尊敬的             | disrespectful                                                 |                      |                                     |
| mutable          | 可变的；易变的          | changeable, fluid, mobile, protean                            |                      |                                     |
| infer            | 推断，推论，推理         | to reach in an opinion from reasoning                         | elicit 得出，引出         |                                     |
| iniquitous       | 邪恶的；不公正的         | wicked, unjust                                                |                      |                                     |
| repute           | 认为，以为；名誉         |                                                               |                      |                                     |
| contrite         | 悔罪的；痛悔的          | feeling contrition, repentant                                 | contrition n.        |                                     |
| self-deprecation | 自嘲的              | self-mockery                                                  |                      |                                     |
| indiscriminately | 随意的，任意的          | in a random manner, promiscuously, arbitrary                  |                      |                                     |
| ornamental       | 装饰性的             | of or relating to ornament                                    |                      |                                     |
| trenchant        | 犀利的，尖锐的          | sharply perceptive, penetrating                               |                      |                                     |
| derogatory       | 不敬的；贬损的          | disparaging, belittling                                       |                      |                                     |
| crumple          | 把…弄皱；起皱；破裂       | crush together into creases or wrinkles, to fall apart        |                      |                                     |
| demur            | 表示抗议，反对          |                                                               | de + mur             |                                     |
| solitary         | 孤独的；隐士           | without companions, recluse, forsaken, lonesome, lorn         | solit 孤独             |                                     |
| adumbrate        | 预示               | to foreshadow in a vague way                                  |                      |                                     |
| 8            | 8                    | 8                                                                         | 8                    | 8     |
| gratuitous   | 无缘无故的；免费的            | without cause or justification, free                                      | gratuity 小费          |       |
| brute        | 野兽（的）；残忍的（人）         | beast, bestial, beastly, brutal                                           |                      |       |
| volatile     | 反复无常的；易挥发的           | fickle, inconstant, mercuial, capricious                                  |                      |       |
| garble       | 曲解，篡改                | to alter or distort as to create a wrong impression or change the meaning |                      |       |
| venial       | （错误）轻微的，可原谅的         | forgivable, pardonable, excusable, remittable                             |                      |       |
| bilateral    | 两边的，双边的              |                                                                           | bi + lateral         |       |
| elusive      | 难懂的，难以描述的；不易被抓获的     |                                                                           |                      |       |
| flout        | 蔑视，违抗                | fleer, gibe, jeer, jest, sneer                                            |                      |       |
| quixotic     | 不切实际的，空想的            | foolishly impractical                                                     |                      |       |
| preferably   | 更可取的，更好的，宁可          |                                                                           | prefer v.            |       |
| antithetical | 相反的；对立的              | opposite, contradictory, contrary, converse, counter, reverse             | antithesis n.        |       |
| intrepid     | 勇敢的；刚毅的              | characterized by fearlessness and fortitude                               |                      |       |
| compensatory | 补偿性的；报酬的             | compensating                                                              |                      |       |
| exquisite    | 精致的；近乎完美的            | elaborately made, delicate, consummate, perfected                         |                      |       |
| dilapidated  | 破旧的；毁坏的              | broken down                                                               |                      |       |
| moratorium   | 延缓偿付；活动中止            |                                                                           |                      |       |
| decorous     | 合宜的；高雅的              | marked by propriety and good state                                        |                      |       |
| herald       | 宣布…的消息；预示…的来临；传令官，信使 |                                                                           |                      |       |
| penal        | 惩罚的；刑罚的              |                                                                           | penalty n.           |       |
| sycophant    | 马屁精                  | a servile self-seeking flatterer                                          |                      |       |
| pragmatic    | 实际的；务实的；注重生效的；实用主义的  |                                                                           | pregnant 怀孕          |       |
| 9             | 9                           | 9                                                                  | 9                                  | 9                      |
| banal         | 乏味的；陈腐的                     | dull or stale, commonplace, insipid, bland, flat, sapless, vapid   |                                    |                        |
| confrontation | 对抗                          | the clashing of forces or ideas                                    | confront v.                        |                        |
| contemplate   | 深思；凝视                       | excogitate, prepend, ponder, view, gaze, to think about intently   |                                    | contemplate doing sth. |
| underplay     | 弱化…的重要性；表演不充分               | to make sth. less important, to underact                           |                                    |                        |
| preservere    | 坚持不懈                        |                                                                    | preserve 保护；保存                     |                        |
| tenuous       | 纤细的；稀薄的；脆弱的，无力的             | not dense, rare, flimsy, weak                                      | "tenacious 坚韧的，顽强的                 | "                      |
| haggle        | 讨价还价                        | bargain, dicker, wrangle                                           |                                    |                        |
| flustered     | 慌张的，激动不安的                   | nervous and upset, perturbed, rattled                              |                                    |                        |
| emulate       | 与…竞争，努力赶上                   | to strive to equal or excel                                        |                                    |                        |
| cater         | 迎合，提供饮食与服务                  | to provide food and service                                        | crater 火山口；弹坑                      | cater to …             |
| cumbersome    | 笨重的，难处理的                    | clumsy, unwieldy                                                   | cumber + some                      |                        |
| impassionate  | 慷慨激昂的；充满激情的                 | filled with passion or zeal                                        |                                    |                        |
| ficitious     | 假的；虚构的                      | not real, imaginary, fabluous                                      | science fiction 科幻小说；fractious 易怒的 |                        |
| synergic      | 协同作用的                       | of combined action or coorperation                                 | synergy n.                         |                        |
| consititution | 宪法；体质                       |                                                                    |                                    |                        |
| fraught       | 充满的                         | filled, charged, loaded                                            | freight n. 货物                      |                        |
| tangible      | 可触摸的                        | touchable, palpable                                                |                                    |                        |
| reenact       | 再制定                         | to enact (as a law) again                                          | annex 兼并；附加                        |                        |
| spiny         | 针状的；多刺的，棘手的                 | thorny                                                             | spine n. 脊椎，刺                      |                        |
| intrigue      | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of |                                    |                        |
| decadence     | 衰落，颓废                       | the process of becoming decadent                                   |                                    |                        |
| revise        | 校订，修订                       | redraft, redraw, revamp                                            |                                    |                        |
| aloof         | 远离的；冷漠的                     |                                                                    |                                    |                        |
| blatant       | 厚颜无耻的；显眼的；炫耀的               | brazen, completely obvious, conspicuous, showy                     |                                    |                        |
| arid          | 干旱的；枯燥的                     | dry, dull, uninteresting                                           |                                    | an arid discussion     |
| surrogate     | 代替品；代理人                     | substitution                                                       | interrogation 审问，质问；疑问句            |                        |
| emphatic      | 重视的，强调的                     | showing emphasis                                                   | emphasize v.                       |                        |
| reciprocally  | 相互的；相反的                     | mutually                                                           |                                    |                        |
| notoriety     | 臭名昭著的（人）                    |                                                                    | notorious adj.                     |                        |
| decry         | 责难；贬低                       | to denounce, depreciate officially, disparage                      |                                    |                        |
| phlegmatic    | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                |                                    |                        |
| penetrating   | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                  |                                    |                        |
| deploy        | 部署；拉长（战线），展开                |                                                                    |                                    |                        |
| paean         | 赞美歌，诗歌                      | a song of joy, praise, triumph, chorale                            |                                    |                        |
| exploit       | 剥削；充分利用                     |                                                                    | exposit  说明                        |                        |
| 10            | 10               | 10                                                                                              | 10                               | 10                |
| revert        | 恢复；重新考虑          | to go back, to consider again                                                                   | re + vert                        |                   |
| hamper        | 妨碍；阻挠；有盖的大篮子     | hinder, impede, a large basket with a cover                                                     | bumper 特大的；保险杠                   |                   |
| aptitude      | 适宜；才能，资质         | a natural tendency, a natural ability to do sth.                                                |                                  | aptitude for sth. |
| revoke        | 撤销，废除；召回         | rescind, call back                                                                              | rebuke 训斥, provoke 挑衅            |                   |
| garish        | 俗丽的，过于艳丽的        | too bright or gaudy, tastelessly showy                                                          |                                  |                   |
| inexorable    | 不为所动的；无法改变的      | incapable of being moved or influenced, cannot be altered                                       |                                  |                   |
| redeem        | 弥补，赎回，偿还         | to atone for, expiate                                                                           |                                  |                   |
| multifaceted  | 多方面的             | protean, various, versatile                                                                     | multi + faced；munificent 慷慨的，丰厚的 |                   |
| coarse        | 粗糙的，低劣的；粗俗的      | of low quality, not refined                                                                     | coercive 强制性的，强迫性的 coerce n.     |                   |
| tacit         | 心照不宣的            | understood without being put into words                                                         |                                  |                   |
| improvise     | 即席创作             | impromptu, extemporize                                                                          | improverished 贫穷的                |                   |
| episodic      | 偶然发生的；分散性的       | occuring irregularly                                                                            | episode n. 片段                    |                   |
| contention    | 争论；论点            | the act of dispute, discord, a statement one argues for as valid                                | con + tention                    |                   |
| desecrate     | 玷辱，亵渎            | treat as not sacred, profane                                                                    | de + secra (sacred)              |                   |
| overbear      | 压倒；镇压；比…重要；超过    |                                                                                                 |                                  |                   |
| predatory     | 掠夺的；食肉的          | predatorial, rapacious, raptorial                                                               |                                  |                   |
| perspicacious | 独具慧眼的；敏锐的        | of acute mental vision or discernment                                                           |                                  |                   |
| specter       | 鬼怪，幽灵；缠绕心头的恐惧，凶兆 | eidolon, phantasm, phantom, spirit, a ghostly apparition, sth. that haunts or perturbs the mind | spectator 目击者；观众                 |                   |
| atrophy       | 萎缩，衰退            | decadence, declination, degeneracy, degeneration, deterioration                                 |                                  |                   |
| omit          | 疏忽，遗漏；不做，未能做     | to leave out, to leave undone, disregard, ignore, neglect                                       |                                  |                   |
| elapse        | 消逝，过去            | pass, go by                                                                                     |                                  |                   |
| whittle       | 削（木头）；削减         | to pare or cut off chips, to reduce, pare                                                       |                                  |                   |
| drone         | 嗡嗡的响；单调的说；单调的低音  |                                                                                                 | prone 俯卧的；倾向…的                   |                   |
| divisive      | 引起分歧的；导致分裂的      | creating disunity or dissension                                                                 | dividend 红利，股利                   |                   |
| crooked       | 不诚实的；弯曲的         | dishonest, not straight                                                                         | crook v.；brook 小河 a small stream |                   |
| indent        | 切割成锯齿状           | notch, cut serratedly                                                                           |                                  |                   |
| creed         | 教义，信条            | belief, tenet                                                                                   |                                  |                   |
| chubby        | 丰满的；圆胖的          | plump                                                                                           |                                  | chubby cheeks     |
| underhanded   | 秘密的；狡诈的          | marked by secrecy and deception, sly                                                            |                                  |                   |
| shrivel       | （使）枯萎            | mummify, wilt, wither                                                                           | strive 奋斗，努力                     |                   |
| premise       | 前提               |                                                                                                 |                                  |                   |
| attrition     | 摩擦，磨损            | by friction                                                                                     |                                  |                   |
| accentuate    | 强调；重读            | emphasis, pronounce with an stress                                                              | accent + uate                    |                   |
| tension       | 紧张，焦虑；张力         | anxiety, stretching force                                                                       |                                  |                   |
| delineate     | 勾画，描述            | to sketch out, draw, describe                                                                   |                                  |                   |
| portray       | 描述，描绘；描画         | depict, make a picture                                                                          | pottery 陶器                       |                   |
| pliable       | 易弯曲的；柔软的；易受影响的   | ductile, easily influenced, easily bend, supple                                                 |                                  |                   |
| deride        | 嘲笑，愚弄            | to laugh at, ridicule                                                                           |                                  |                   |
| ignominious   | 可耻的；耻辱的          | disgraceful, humiliating, despicable, dishonorable                                              |                                  |                   |
| 11             | 11             | 11                                                                               | 11                                        | 11                   |
| assiduous      | 勤勉的；专心的        | diligent, persevering, attentive                                                 |                                           | be assiduous in sth. |
| averse         | 反对的；不愿意的       | not willing or inclined, opposed                                                 | revere 尊敬                                 |                      |
| archaic        | 古老的            | earlier or more primitive time                                                   |                                           |                      |
| colloquial     | 口语的，口头的        | conversational                                                                   | loquacious 长话短说 loqu 说                    |                      |
| ravage         | 摧毁；使荒废         | desolate, devastate, havoc, ruin and destroy                                     |                                           |                      |
| forfeit        | 丧失；被罚没收；丧失的东西  |                                                                                  | flaw 瑕疵；裂缝，变得有缺陷                          |                      |
| antagonism     | 对抗，敌对          | animosity, animus, antipathy, opposition, rancor                                 |                                           |                      |
| extrapolate    | 预测，推测          | speculate                                                                        | interpolate 插入，（通过插入新语句）篡改                |                      |
| itinerant      | 巡回的            | peripatetic, nomadic                                                             | iteration                                 |                      |
| subordinate    | 次要的；下级的；下级     | inferior, submissive                                                             |                                           |                      |
| provocation    | 挑衅，激怒          |                                                                                  |                                           |                      |
| trample        | 踩坏，践踏；蹂躏       | tread, override                                                                  |                                           |                      |
| euphoric       | 欢欣的            | feel happy and excited                                                           |                                           |                      |
| packed         | 压紧的；充满人的；拥挤的   | compressed, crowded, crammed                                                     |                                           |                      |
| obsequiousness | 谄媚             | servility, subservience, abject submissiveness                                   |                                           |                      |
| immaculate     | 洁净的；无暇的        | perfectly clean, unsoiled, impeccable                                            |                                           |                      |
| imposing       | 给人深刻印象的；壮丽雄伟的  | impressive, grand                                                                |                                           |                      |
| subside        | （建筑物等）下陷；平息，减退 | tend downward, descend, become quiet or less, ebb, lull, moderate, slacken, wane |                                           |                      |
| satirize       | 讽刺             | lampoon, mock, quip, scorch                                                      | sarcastic, sneering, caustic, ironic adj. |                      |
| banter         | 打趣，玩笑          | playful, good-humored joking                                                     |                                           |                      |
| superfluous    | 多余的，累赘的        | exceeding what is needed                                                         |                                           |                      |
| disparate      | 迥然不同的          |                                                                                  |                                           |                      |
| dorsal         | 背部的；脊背的        |                                                                                  | drowse v. 打瞌睡                             |                      |
| saturate       | 浸湿；浸透；使大量吸收或充满 |                                                                                  |                                           |                      |
| brink          | （峭壁）边缘         | verge, border, edge                                                              | blink 眨眼                                  |                      |
| mortify        | 使丢脸；侮辱         | affront, insult, humiliate                                                       | motif （文艺作品等的）主题，主旨                       |                      |
| crimson        | 绯红色（的）         | deep purplish red                                                                |                                           |                      |
| reimburse      | 偿还             | pay back, repay                                                                  | re + im + burse                           |                      |
| squander       | 浪费，挥霍          | to spead extravagantly                                                           |                                           |                      |
| jumble         | 使混乱，混杂；杂乱      | to mix in disorder, a disorderly mixture                                         |                                           |                      |
| 12            | 12                    | 12                                                                              | 12                                                                | 12                        |
| distant       | 疏远的，冷淡的               | reserved or aloof in personal relationship, insociable, unsociable              |                                                                   |                           |
| tyrant        | 暴君                    | despot, autocrat                                                                |                                                                   |                           |
| tempting      | 诱惑人的                  | having an appeal, alluring, attractive, enticing, inviting, seductive           |                                                                   |                           |
| inadvertent   | 疏忽的；不经意的              | unintentional                                                                   | in + advertent                                                    |                           |
| recount       | 叙述；描写                 | narrate, depict, delineate, portray, describe                                   |                                                                   |                           |
| skew          | 不直的，歪斜的               | crooked, slanting, running obliquely                                            |                                                                   |                           |
| pliant        | 易受影响的；易弯的             | easily influenced, pliable, malleable                                           |                                                                   |                           |
| impertinent   | 不切题的；无礼的              | irrelevant, rude, reckless                                                      |                                                                   |                           |
| virtuoso      | 艺术大师                  |                                                                                 |                                                                   |                           |
| strive        | 奋斗，努力                 | struggle hard                                                                   |                                                                   |                           |
| rancid        | 不新鲜的，变味的              | rank, stinking                                                                  |                                                                   |                           |
| temporal      | 时间的，世俗的               |                                                                                 |                                                                   |                           |
| antiquated    | 陈旧的，过时的；年老的           | antique, archaic, dated                                                         |                                                                   | antiquated ideas          |
| fortuitous    | 偶然发生的；偶然的             | accidental, lucky, casual, contingent, incidental                               | fortitude 坚毅，坚忍不拔                                                 |                           |
| ill-will      | 敌意，仇视，恶感              | enmity, malice, antagonism, animosity, animus, antipathy, opposition, rancor    |                                                                   |                           |
| obsessed      | 着迷的；沉迷的               |                                                                                 | obsess v.                                                         |                           |
| gouge         | 挖出，骗钱；半圆凿             | scoop out, cheat out of money, a semicircular chisel                            | gauge 准则，规范                                                       |                           |
| jocular       | 滑稽的；幽默的；爱开玩笑的         | humorous                                                                        | joke                                                              |                           |
| reticent      | 沉默寡言的                 | inclined to be slient, reserved, taciturn, uncommounicative                     |                                                                   |                           |
| ascent        | 上升，攀登；上坡路，提高，提升       |                                                                                 |                                                                   |                           |
| discernible   | 可识别的，可辩别的             | being recognized or identified                                                  | discern + able                                                    |                           |
| unwieldy      | 难控制的；笨重的              | cumbersome, not easily managed                                                  |                                                                   |                           |
| formidable    | 令人畏惧的；可怕的；难以克服的       | causing fear or dread, hard to handle or overcome                               |                                                                   |                           |
| decimate      | 毁掉大部分，大量杀死            | to destroy or kill a large part                                                 |                                                                   |                           |
| strip         | 剥去；狭长的一片              | denude, disrobe, a long narrow piece                                            |                                                                   |                           |
| apropos       | 适当的；有关                |                                                                                 |                                                                   |                           |
| perfidious    | 不忠的；背信弃义的             | faithless                                                                       |                                                                   |                           |
| compelling    | 引起兴趣的                 | captivating, keenly interesting                                                 | a compelling subject                                              |                           |
| brusqueness   | 唐突，直率                 | candor, abruptness                                                              | brusque adj.                                                      |                           |
| commensurate  | 同样大小的，相称的             | equal in measure, proportionate                                                 |                                                                   | be commensurate with sth. |
| contemplation | 注视，凝视；意图，期望           |                                                                                 |                                                                   |                           |
| eccentric     | 古怪的，反常的；古怪的人；没有共同圆心的  | unconventional                                                                  |                                                                   |                           |
| imperative    | 紧急的                   | urgent, pressing, importunate, exigent                                          |                                                                   |                           |
| exhale        | 呼出，呼气，散发              |                                                                                 |                                                                   |                           |
| ascribe       | 归因于；归咎于               |                                                                                 |                                                                   |                           |
| inherently    | 固有的，天性的               | intrinsically                                                                   |                                                                   |                           |
| thrive        | 茁壮成长；繁荣，兴旺            | flourish, prosper                                                               |                                                                   |                           |
| strenuous     | 奋发的；热烈的               | vigorously active, fervent, zealous, earnest, energetic, spirited               |                                                                   |                           |
| iconoclastic  | 偶像派的；打破习俗的            |                                                                                 |                                                                   | iconoclastic ideas        |
| nocturnal     | 夜晚的，夜间发生的             | happening in the night                                                          |                                                                   |                           |
| buoyant       | 有浮力的；快乐的              | showing buoyancy, cheerful                                                      |                                                                   |                           |
| synoptic      | 摘要的                   | affording a general view of the whole                                           |                                                                   |                           |
| infiltrate    | 渗透，渗入                 | to pass through                                                                 |                                                                   |                           |
| imperturbable | 冷静的；沉着的               | cool, disimpassionated, unflappable, unruffled                                  |                                                                   |                           |
| evanescent    | 易消失的；短暂的              | vanishing, transient, ephemeral                                                 |                                                                   |                           |
| reinstate     | 回复                    |                                                                                 | re + in + state                                                   |                           |
| quaint        | 离奇有趣的；古色古香的           | unusual and attractive                                                          |                                                                   |                           |
| somber        | 忧郁的；阴暗的               | melancholy, dark and gloomy, dim, grave, shadowy, shady, overcast, disconsolate |                                                                   |                           |
| resign        | 委托；放弃，辞职              | abandon, cede, relinquish, surrender                                            |                                                                   |                           |
| rarefy        | 使稀少；使稀薄               | dilute, attenuate, make thin, rare, porous, or less dense                       |                                                                   |                           |
| flaunt        | 炫耀，夸耀                 | flash, parade, show off                                                         |                                                                   |                           |
| hover         | 翱翔，徘徊                 |                                                                                 |                                                                   |                           |
| defy          | 违抗，藐视                 |                                                                                 |                                                                   |                           |
| perception    | 感知，知觉；洞察力             |                                                                                 | percept n.                                                        |                           |
| absurd        | 荒谬的，可笑的               |                                                                                 |                                                                   | absurd assumptions        |
| persuasive    | 易使人信服的；有说服力的          |                                                                                 | pervasive 弥漫的，遍布的                                                 |                           |
| spurious      | 假的；伪造的                | forged, false, falsified                                                        |                                                                   |                           |
| prudent       | 审慎的；精明的；节俭的，精打细算的     | judicious, sage, sane                                                           | impudent, contemptuous, impulsive, brassy, insolent, flip 鲁莽的，无礼的 |                           |
| empiricism    | 经验主义                  |                                                                                 | empiric 经验主义者                                                     |                           |
| sedative      | （药物）镇静的；镇静剂           | calming, relaxing, soothing, tranquilizer                                       |                                                                   |                           |
| pronounced    | 显著的；明确的               | strongly marked                                                                 |                                                                   |                           |
| irrational    | 不合理的，不理智的；失去理性的       | fallacious, illogical, unreasoned                                               |                                                                   |                           |
| bland         | 情绪平稳的；无味的             | pleasantly smooth, insipid                                                      |                                                                   |                           |
| span          | 跨度，两个界限的距离            | extent, length, reach, spread, a stretch between two limits                     |                                                                   |                           |
| plethora      | 过多，过剩                 | excess, superfluity, overabundance, overflow, overmuch, overplus                |                                                                   |                           |
| prospect      | 勘探，期望；前景              |                                                                                 |                                                                   |                           |
| intent        | 专心的，专注的，热切的，渴望的，目的，意向 |                                                                                 | intend v.                                                         |                           |
| tangential    | 切线的；离题的               | discursive, excursive, rambling, impertinent                                    |                                                                   |                           |
| ulterior      | 较远的，将来的；隐秘的，别有用心的     |                                                                                 |                                                                   |                           |
| resurge       | 复活                    |                                                                                 |                                                                   |                           |
| 13            | 13                       | 13                                                                      | 13                                                                                                                            | 13               |
| improvised    | 临时准备，即席而作                | making offhand                                                          | improvident, squander, prodigal, lavish, extravagant, profligate  无远见的，不节俭的，挥霍的；provident, prudent, frugal, thrifty 深谋远虑的，节俭的 |                  |
| inflamed      | 发炎的，红肿的                  |                                                                         | infection                                                                                                                     |                  |
| smother       | 熄灭，覆盖，使窒息                | to cover thickly                                                        |                                                                                                                               |                  |
| segregate     | 分离，隔离                    |                                                                         | aggravated 加重，恶化；激怒，使恼火                                                                                                       |                  |
| igneous       | 火的，似火的                   | fiery, having the nature of fire                                        | ign 前缀                                                                                                                        |                  |
| nonentity     | 无足轻重的人或事                 |                                                                         |                                                                                                                               |                  |
| reportorial   | 记者的，报道的                  |                                                                         |                                                                                                                               |                  |
| perpendicular | 垂直的，竖直的                  | exactly upright, vertical                                               |                                                                                                                               |                  |
| prodigal      | 挥霍的，挥霍者                  | lavish                                                                  |                                                                                                                               |                  |
| cynical       | 愤世嫉俗的                    | captious, peevish                                                       | cynic 愤世嫉俗者                                                                                                                   |                  |
| arboreal      | 树木的                      |                                                                         | arbor 凉亭                                                                                                                      |                  |
| resignation   | 听从，顺从；辞职                 |                                                                         | resign v.；commission 委托，佣金                                                                                                    |                  |
| recital       | 独奏会，演唱会；吟诵               |                                                                         | recite v. 背诵                                                                                                                  |                  |
| dormant       | 冬眠的；静止的                  | torpid in winter, quiet, still                                          |                                                                                                                               |                  |
| prerogative   | 特权                       | privilege, birthright, perquisite                                       |                                                                                                                               |                  |
| plumb         | 测探物，铅锤；垂直的；精确的；深入了解；测量深度 |                                                                         |                                                                                                                               |                  |
| unwitting     | 无意的，不知不觉的                |                                                                         |                                                                                                                               |                  |
| arbitrary     | 专横的，武断的                  | discretionary, despotic, dictatorial, monocratic, autarchic, autocratic |                                                                                                                               |                  |
| fractious     | （脾气）易怒的，好争吵的             | peevish, irritable                                                      | friction 摩擦                                                                                                                   |                  |
| devastate     | 摧毁，破坏                    | ravage, destroy, depredate, wreck                                       |                                                                                                                               |                  |
| inexhaustible | 用不完的                     |                                                                         |                                                                                                                               |                  |
| assail        | 质问，攻击                    | aggress, beset, storm, strike                                           |                                                                                                                               |                  |
| periphery     | 次要部分，外围；外表面              |                                                                         | prophesy 预言                                                                                                                   |                  |
| irascible     | 易怒的                      | hot-tempered, peevish, petulant, touchy, querulous, fractious, fretful  |                                                                                                                               |                  |
| abrasive      | 研磨的                      |                                                                         | abrade v.                                                                                                                     |                  |
| harsh         | 严厉的；粗糙的；刺耳的              | stern, rough, sharp, rugged, scabrous                                   |                                                                                                                               |                  |
| exaggerate    | 夸大，夸张；过分强调               | overstate, overemphasize, intensify, magnify                            |                                                                                                                               |                  |
| disprove      | 证明…有误                    |                                                                         |                                                                                                                               |                  |
| supercilious  | 目中无人的                    | coolly or patronizingly haughty                                         |                                                                                                                               |                  |
| florid        | 华丽的；（脸）红润的               | highly decorated, showy, rosy, ruddy                                    |                                                                                                                               |                  |
| exasperation  | 激怒，恼怒                    |                                                                         | exasperate v.                                                                                                                 |                  |
| distinctive   | 出众的，有特色的                 | distinguish, characteristic, typical, eminent, prominent, conspicuous   |                                                                                                                               |                  |
| skimp         | 节省花费                     | barely sufficient funds                                                 |                                                                                                                               | be skimp on sth. |
| receptive     | 善于接受的，能接纳的               |                                                                         |                                                                                                                               |                  |
| dialect       | 方言                       |                                                                         |                                                                                                                               |                  |
| demobilize    | 遣散，使复员                   | disband                                                                 |                                                                                                                               |                  |
| rhapsodic     | 狂热的，狂喜的；狂想曲的             |                                                                         |                                                                                                                               |                  |
| vindictive    | 报复的                      | vengeful                                                                |                                                                                                                               |                  |
| reluctance    | 勉强，不情愿                   |                                                                         | reluctant adj.                                                                                                                |                  |
| transcendent  | 超越的，卓越的，出众的              | ultimate, unsurpassable, utmost, uttermost, supreme                     |                                                                                                                               |                  |
| recondite     | 深奥的，晦涩的                  |                                                                         |                                                                                                                               |                  |
| culpable      | 有罪的，该受谴责的                | blameworthy, deserving blame                                            |                                                                                                                               |                  |
| tactile       | 有触觉的                     |                                                                         |                                                                                                                               |                  |
| prig          | 自命清高者，道学先生               |                                                                         |                                                                                                                               |                  |
| 14             | 14            | 14                                                                                           | 14                   | 14                                 |
| viscous        | 粘滞的，粘性的       | glutinous                                                                                    |                      |                                    |
| slash          | 大量削减          | reduce sharply                                                                               | stash 藏匿             |                                    |
| capricious     | 变化无常的，任性的     | fickleness, fickle, inconstant, lubricious, mercurial, whimsical, whimsied, erratic, flighty |                      |                                    |
| tentative      | 试探性的；尝试性的     | not fully worked or developed                                                                | retentive 有记性的，记忆力强的 |                                    |
| insolent       | 粗野的，无礼的       | impudent, boldly disrespectful                                                               |                      |                                    |
| deplete        | 大量减少，使枯竭      | drain, impoverish, exhaust                                                                   |                      |                                    |
| recuperate     | 恢复（健康、体力）；复原  | regain, recover                                                                              |                      |                                    |
| vulnerable     | 易受伤的；脆弱的      |                                                                                              | vulnerability 脆弱性    |                                    |
| prudish        | 过分守礼的；假道学的    | marked by prudery, priggish                                                                  |                      |                                    |
| defecate       | 澄清，净化         |                                                                                              |                      |                                    |
| anarchy        | 无政府；政治混乱      | absence of government, political disorder                                                    |                      |                                    |
| raze           | 夷为平地；彻底破坏     | destroy to the ground, destroy completely                                                    |                      |                                    |
| belligerent    | 发动战争的；好斗的，挑衅的 | defiant, provoke, provocation, rebellious                                                    |                      |                                    |
| sentimentalize | （使）感伤         |                                                                                              |                      |                                    |
| consummate     | 完全的；完善的；完成    | complete, perfect, accomplish                                                                |                      |                                    |
| fuse           | 融化，融合         | combine                                                                                      |                      |                                    |
| rescind        | 废除，取消         | make void                                                                                    |                      |                                    |
| rivet          | 铆钉；吸引（注意）     | attract                                                                                      |                      | be riveted to the spot/ground 呆若木鸡 |
| facile         | 易做到的；肤浅的      | superficial, easily accomplished or attained                                                 |                      |                                    |
| impunity       | 免受惩罚          | exemption from punishment                                                                    |                      |                                    |
| insinuate      | 暗指，暗示         | imply                                                                                        |                      |                                    |
| grim           | 冷酷的，可怕的       | ghastly, forbidding, appearing stern                                                         |                      |                                    |
| momentous      | 极为重要的；重大的     | considerable, significant, substantial, consequential                                        |                      |                                    |
| provident      | 深谋远虑的，节俭的     | prudent, frugal, thrifty                                                                     |                      |                                    |
| countenance    | 支持，赞成；表情      | advocate, approbate, approve, encourage, favor, sanction                                     |                      |                                    |
| sanctum        | 圣所            | a sacred place                                                                               |                      | inner sanctum                      |
| choreography   | 舞蹈术的；舞台舞蹈的    |                                                                                              | choreography n.      |                                    |
| impulse        | 动力，刺激         | impelling and motivating force                                                               |                      |                                    |
| eclectic       | 折衷的，兼容并蓄的     |                                                                                              |                      |                                    |
| forte          | 长处，特长；强音的     | special accomplishment                                                                       |                      |                                    |
| embroider      | 刺绣，镶边；装饰      | needlework, provide embellishment                                                            |                      |                                    |
| compel         | 强迫            | coercive, concuss, oblige, force or constrain                                                |                      |                                    |
| optimal        | 最佳的，最理想的      | most desirable or satisfatory, optimum                                                       |                      |                                    |
| veracious      | 诚实的；说真话的      | truthful, honest, faithful, veridical                                                        | valorous 勇敢的         |                                    |
| poise          | 使平衡；泰然自若      | hold in equilibrium                                                                          |                      |                                    |
| jaded          | 疲惫的；厌倦的       | wearied, fatigued, dull, satiated                                                            |                      | be jaded by sth.                   |
| salubrious     | 有益健康的         | healthful, promoting health                                                                  | salutary 有益的，有益健康的   |                                    |
| slanderous     | 诽谤的           | abusive, false and defamatory                                                                |                      |                                    |
| 15             | 15                      | 15                                                                   | 15                                                       | 15    |
| succumb        | 屈从，屈服；因…死亡              |                                                                      | sus 下面 + sumb 躺                                          |       |
| autonomy       | 自治                      | self-government                                                      | Anatomy 解剖学                                              |       |
| propitiate     | 讨好；抚慰                   | assuage, conciliate, mollify, consolate, pacify, placate             |                                                          |       |
| extensive      | 广大的；多方面的；广泛的            |                                                                      |                                                          |       |
| futile         | 无效的，无用的；没出息的            | bootless, otiose, unavailing, useless, without advantage or benefit  | fertile, fertilizer, fertilize 多产、化肥、使受精；facile 易做到的，肤浅的 |       |
| prostrate      | 俯卧的；沮丧的；使下跪鞠躬           | prone, powerless, helpless                                           | pro (prone) + strate                                     |       |
| appropriate    | 适当的；拨款；盗用               | misappropriation                                                     |                                                          |       |
| presumptuous   | 放肆的；过分的                 | overweening, presuming, uppity                                       |                                                          |       |
| decorum        | 礼节，礼貌                   | etiquette, courtesy, politeness                                      | décor 装饰                                                 |       |
| pervade        | 弥漫，遍及                   | become diffused throughout                                           | pervasive adj.                                           |       |
| keen           | 锋利的，锐利的；敏锐的；热心的         | sensitive, enthusiastic                                              |                                                          |       |
| patriarchal    | 家长的，族长的；父权制的            |                                                                      | patriarchy, patriarch                                    |       |
| rowdy          | 吵闹的；粗暴的                 |                                                                      |                                                          |       |
| docile         | 驯服的；听话的                 | easy to control                                                      |                                                          |       |
| hitherto       | 到目前为止                   |                                                                      | a hitherto unknown fact                                  |       |
| sedulous       | 聚精会神的；勤勉的               | diligent                                                             |                                                          |       |
| antiquarianism | 古物研究；好古癖                |                                                                      | antique 古董；古代的                                           |       |
| discreet       | 小心的；言行谨慎的               | prudent, modest, calculating, cautious, chary, circumspect, gingerly |                                                          |       |
| dilate         | 使膨胀；使扩大                 | swell, expand, distent,                                              | "dilute 稀释，冲淡                                            | "     |   |
| commiserate    | 同情，怜悯                   | sorrow or pity for sb.                                               |                                                          |       |
| oblivious      | 遗忘的；忘却的；疏忽的             | forgetful, unmindful                                                 |                                                          |       |
| aberration     | 越轨                      | being aberrant                                                       |                                                          |       |
| shunt          | 使（火车）转到另一个轨道；改变（某物的）方向的 |                                                                      |                                                          |       |
| 16            | 16             | 16                                                             | 16                                                  | 16    |
| compendium    | 简要，概略          | digest, pandect, sketch, syllabus, summary, abstract           | recapitulate 扼要概述；synoptic 摘要的                      |       |
| faddish       | 流行一时的，时尚的      |                                                                | fad n.；vogue 时尚，流行                                  |       |
| predisposed   | 预设的；有倾向的；使易受感染 | dispose in advance, make susceptible                           |                                                     |       |
| copious       | 丰富的，多产的        | plentiful, abundant                                            |                                                     |       |
| convoluted    | 旋绕的；费解的        | coiled, spiraled, intricate, complicated, extremely involved   |                                                     |       |
| deprecate     | 反对，轻视          | disapprove, belittle                                           |                                                     |       |
| thatch        | 以茅草覆盖；茅草屋顶     |                                                                |                                                     |       |
| infirm        | 虚弱的            | physically weak                                                | prig 自命清高者，道学先生                                     |       |
| scarlet       | 猩红的，鲜红的        | bright red                                                     | scarlet fever 猩红热                                   |       |
| unstinting    | 慷慨的，大方的        | very generous                                                  | skinflint 吝啬鬼                                       |       |
| postulate     | 要求，假定          | demand, claim, assume, presume                                 |                                                     |       |
| commodious    | 宽敞的            | spacious, roomy                                                | Commodities 商品                                      |       |
| expeditiously | 迅速的，敏捷的        | promptly, efficiently, rapidly, speedily, swiftly, agile       |                                                     |       |
| clutter       | 弄乱；凌乱          |                                                                | scatter 四散逃窜                                        |       |
| probity       | 刚直，正直          | uprightness, honesty                                           |                                                     |       |
| impetuous     | 冲动的，鲁莽的        | impulsive, sudden                                              | an impetuous decision                               |       |
| caricature    | 讽刺画；滑稽模仿       | burlesque, mock, mockery, travesty                             | sarcastic, sarcastic, sneering, caustic, ironic 讽刺的 |       |
| narrow        | 狭窄的，狭隘的，变窄     | contract, slender width, prejudiced                            |                                                     |       |
| beset         | 镶嵌；困扰          | harass                                                         |                                                     |       |
| asceticism    | 禁欲主义           |                                                                | ascetic 禁欲的；苦行者                                     |       |
| turbulent     | 混乱的，骚乱的        | causing unrest, violence, disturbance, tempestuous             |                                                     |       |
| descry        | 看见，察觉          | discern, catch sight of                                        | decry 谴责；outcry 呐喊                                  |       |
| mordant       | 讥讽的，尖酸的        | sarcastic, sneering, caustic, ironic                           |                                                     |       |
| tribulation   | 苦难，忧患          | distress or suffering resulting from oppression or persecution |                                                     |       |
| purport       | 意义，涵义，主旨       | meaning conveyed, implied, gist                                |                                                     |       |
| curt          | （言辞，行为）简略而草率的  | brief, rude, terse                                             |                                                     |       |
| 17             | 17                 | 17                                                                                   | 17                                   | 17    |
| impeccable     | 无瑕疵的               | faultless, flawless, fleckless, immaculate, indefectible, irreproachable, perfect    |                                      |       |
| rationale      | 理由；基本原理            | fundamental reasons, maxim, an established principle, axiom                          |                                      |       |
| scenic         | 风景如画的              | natural scenery                                                                      | scenic lift                          |       |
| ominous        | 恶兆的，不详的            | portentous, of an evil omen                                                          | omen n.                              |       |
| particulate    | 微粒的；微粒             | minute separate particle                                                             |                                      |       |
| designate      | 指定，任命；指明，指出        | indicate and set apart for a specific purpose                                        |                                      |       |
| protrude       | 突出，伸出              | jut out                                                                              | """convex 凸的；concave 凹的"             | """"  |
| potshot        | 盲目射击；肆意抨击          |                                                                                      |                                      |       |
| spherical      | 球的，球状的             | globe-shaped, globular, rotund, round                                                |                                      |       |
| delude         | 欺骗，哄骗              | mislead, deceive, trick, beguile, bluff                                              |                                      |       |
| pithy          | （讲话或文章）简练有力的；言简意赅的 | concise, tersely cogent                                                              |                                      |       |
| jolting        | 令人震惊的              | astonishing, astounding, startling, surprising                                       |                                      |       |
| impute         | 归咎于，归于             | attribute                                                                            |                                      |       |
| incipient      | 初期的，起初的            | inceptive, initial, initiatory, introductory, nascent, beginning                     |                                      |       |
| platitude      | 陈词滥调               | banal, trite, stale remark, cliché                                                   |                                      |       |
| astigmatic     | 散光的，乱视的            |                                                                                      | a + stigma 污点 + tic 看不见污点的           |       |
| guile          | 欺骗，欺诈，狡猾           | deceit, cunning, underhanded, trickery, impostor, cheat, dissemblance, dissimluation |                                      |       |
| abhor          | 憎恨，厌恶              | detest, abominate, loathe, execrate, hate, antipathy,                                | repellent, repugnant 令人厌恶的           |       |
| ostracize      | 排斥；放逐              | banish, expatriate, expulse, oust, exile by ostracism                                |                                      |       |
| bravado        | 故作勇敢，虚张声势          | pretended courage                                                                    | brav 勇敢 + ado                        |       |
| repugnance     | 嫌恶，反感              | strong dislike, distaste, or antagnoism                                              |                                      |       |
| reconciliation | 和解，调节              |                                                                                      | conciliate v.                        |       |
| obliqueness    | 斜度，倾斜              |                                                                                      | oblique                              |       |
| fret           | （使）烦躁；焦虑；烦躁        | irritate, annoy, agitation, cark, pother, ruffle, vex, tension                       |                                      |       |
| ossify         | 骨化，僵化              | into bone                                                                            | oss （骨头） + ify                       |       |
| ridicule       | 嘲笑，奚落              | unkind expression of amusement                                                       | ridiculous 荒谬的，可笑的                   |       |
| proselytize    | （使）皈依              | recruit or convert to a new faith                                                    |                                      |       |
| despicable     | 可鄙的，卑劣的            | contemptible, deserving to be despired                                               |                                      |       |
| debase         | 贬低，贬损              |                                                                                      | de + base                            |       |
| irredeemable   | 无法挽回的              |                                                                                      | redeem 挽回，弥补                         |       |
| untreated      | 未治疗的，未经处理的         |                                                                                      |                                      |       |
| dictate        | 口述；命令              |                                                                                      | speak, read aloud, prescribe         |       |
| secular        | 世俗的，尘世的            | worldly rather than spiritual                                                        |                                      |       |
| egoist         | 自我主义者              |                                                                                      | egoism 自我主义；egoistic(al) 自我中心的，自私自利的 |       |
| vestige        | 痕迹，遗迹              | relic, trace                                                                         |                                      |       |
| glossary       | 词汇表，难词表，           |                                                                                      |                                      |       |
| evade          | 躲避，逃避              | duck, eschew, shun, avoid, escape, elude                                             | evasive adj.                         |       |
| emblematic     | 作为象征的              | symbolic, representative                                                             | emblem n.                            |       |
| fused          | 熔化的                |                                                                                      | fuse v.                              |       |
| fraternity     | 同行；友爱              |                                                                                      | camaraderie 同志之情；友情                  |       |
| unfertilized   | 未施肥的，未受精的          | unimpregnated                                                                        |                                      |       |
| unadorned      | 未装饰的，朴素的           | austere, homespun, plain                                                             |                                      |       |
| 18                | 18                | 18                                                                      | 18                                   | 18              |
| avaricious        | 贪婪的，贪心的           | greedy, insatiably, insatiable                                          | avarice n.                           |                 |
| irrevocable       | 不可撤销的             |                                                                         | ir + revoke + able                   |                 |
| consumption       | 消费，消耗             |                                                                         | consume v.                           |                 |
| corrosive         | 腐蚀性的，腐蚀的          | erosive, caustic                                                        | corrode v.                           |                 |
| exorcise          | 驱除妖魔；去除           | expel, get rid of                                                       |                                      |                 |
| iterate           | 重申，重做             | do or utter repeatedly                                                  |                                      |                 |
| debilitate        | 使衰弱               | weaken, make weak or feeble, depress, enervate                          |                                      |                 |
| intractable       | 倔强的；难管理的；难加工的     | difficult to manipulate, manage, stubborn, unruly                       |                                      |                 |
| debauch           | 使堕落，败坏；堕落         | corrupt, deprave, pervert                                               |                                      |                 |
| repel             | 击退；使反感            | fight against, resist, cause aversion                                   |                                      |                 |
| scramble          | 攀登；搅乱，使混乱；争夺      |                                                                         | scale 攀登 + amble 行走                  |                 |
| belie             | 掩饰，证明为假           | disguise, misrepresent, prove false, distort, falsify, garble, misstate |                                      |                 |
| propitious        | 吉利的，顺利的；有利的       | auspicious, favorable, advantageous                                     |                                      |                 |
| irrigate          | 灌溉；冲洗（伤口）         |                                                                         |                                      |                 |
| rekindle          | 再点火；使重新振作         |                                                                         |                                      | a rekindle hope |
| squalid           | 污秽的，肮脏的           | dirty, seedy, slummy, sordid, unclean                                   |                                      |                 |
| unfettered        | 自由的，不受约束的         | free, unrestrained                                                      |                                      |                 |
| antithesis        | 对立，相对             | contract or opposition                                                  |                                      |                 |
| impugn            | 提出异议；对…表示怀疑       | to challenge as false or questionable                                   |                                      |                 |
| sequester         | （使）隐退；使隔离         | seclude, withdraw, set apart                                            | sequestrate 扣押                       |                 |
| resurrect         | 使复活；复兴            |                                                                         |                                      |                 |
| capitulate        | （有条件的）投降          | surrender conditionally                                                 | recapitulate 扼要概述                    |                 |
| subjective        | 主观的，想象的           |                                                                         | subject n.                           |                 |
| parsimony         | 过分节俭，吝啬           | being stringy, thrift                                                   |                                      |                 |
| attune            | 使调和               |                                                                         |                                      |                 |
| retrieve          | 寻回，取回；挽回，弥补       | regain, redeem, remedy                                                  |                                      |                 |
| revile            | 辱骂；恶言相向           | rail, use abusive language                                              |                                      |                 |
| radiant           | 发光的；容光焕发的         |                                                                         | rad 光线                               |                 |
| sprawl            | 散乱的延伸；四肢摊开坐、卧、躺   |                                                                         | sprawling, rampant 植物蔓生的；（城市）无法计划开展的 |                 |
| upstage           | 高傲的               | haughty                                                                 | up + stage                           |                 |
| perceptive        | 感知的；知觉的；有洞察力的；敏锐的 |                                                                         |                                      |                 |
| defect            | 缺点，瑕疵；变节，脱党       | fault, flaw, forsake, blemish                                           | infect 感染                            |                 |
| ethereal          | 太空的；轻巧的；轻飘飘的      |                                                                         | ether 太空，苍天                          |                 |
| stipulate         | 要求以…为条件；约定；规定     | to make an agreement                                                    |                                      |                 |
| proposition       | 看法，主张             | proposal, opinion, statement, expresses, judgement                      |                                      |                 |
| figment           | 虚构的事情             | merely imagined                                                         | ficitious 假的；虚构的                     |                 |
| trespass          | 侵犯；闯入私人空间         | make an unwarranted or uninvited incursion                              |                                      |                 |
| extravagance      | 奢侈，挥霍             |                                                                         | extravagant adj.                     |                 |
| appall            | 使惊骇；使胆寒           | shock, fill with horror or dismay                                       | a + pale 苍白                          |                 |
| counterproductive | 事与愿违的             |                                                                         | counter + productive                 |                 |
| tortuous          | 曲折的，拐弯抹角的；弯弯曲曲的   | devious, indirect tactics, winding                                      |                                      |                 |
| fray              | 吵架，打斗；磨破          | noisy quarrel or fight, become worn, ragged, or reveled by rubbing      |                                      |                 |
| pestilential      | 引起瘟疫的；致命的；极讨厌的    | causing pestilence, deadly, irritating                                  | pestilence 瘟疫                        |                 |
| unctuous          | 油质的；油腔滑调的         | fatty, oily, greasy, oleaginous                                         |                                      |                 |
| physiological     | 生理（学）的            |                                                                         | physiology 生理学                       |                 |
| contemplative     | 沉思的（人）            |                                                                         |                                      |                 |
| incorrigible      | 积习难改的，不可救药的       |                                                                         | in + correct + able                  |                 |
| 19           | 19                     | 19                                                                                                                                             | 19                                                       | 19                     |
| sketch       | 草图；概略；画草图，写概略          | compendium, digest, syllabus, simple description or outline                                                                                    |                                                          |                        |
| schematic    | 纲要的，图解的                | of schema or diagram                                                                                                                           |                                                          |                        |
| compliance   | 顺从，遵从                  | obedience                                                                                                                                      | comply 顺从                                                |                        |
| recurring    | 反复的，再发生的               |                                                                                                                                                | recur v.                                                 |                        |
| fruitlessly  | 徒劳的，无益的                | unproductive, unprofitable                                                                                                                     | fruitful, fertile, fecund, productive, prolific 多产的，多果实的 |                        |
| folly        | 愚蠢；愚蠢的想法               | foolery, idiocy, insanity, foolish action, lack of wisdom                                                                                      |                                                          |                        |
| germinate    | 发芽，发展                  |                                                                                                                                                | germ 种子 + minate 开始发展、发芽                                 |                        |
| eschew       | 避开，戒绝                  | shun, avoid, abstain                                                                                                                           | es 出 + chew 咀嚼 吃一堑长一智                                    |                        |
| imposture    | 冒充                     | fraud, deceit, trickery, impostor, cheat, guile, mislead, deceive, trick, beguile, bluff, delude                                               | impostor 冒充者，骗子                                          |                        |
| ordeal       | 严峻的考验                  | difficult and severe trial, tribulation, affliction                                                                                            |                                                          |                        |
| denote       | 指示，表示                  | mark, indicate, signify                                                                                                                        |                                                          |                        |
| confide      | 吐露（心事）；倾诉              | tell confidentially, show confidence by imparting secrets                                                                                      |                                                          |                        |
| unyielding   | 坚定的；不屈的；不能弯曲的          | firmness, obduracy, inflexibility                                                                                                              |                                                          |                        |
| obviate      | 排除，消除                  | remove, get rid of, eliminate, exclude, preclude                                                                                               |                                                          |                        |
| rueful       | 抱憾的；后悔的，悔恨的            | showing sympathy or pity, mournful, regretful                                                                                                  |                                                          |                        |
| unskilled    | 不熟练的；无需技能的             |                                                                                                                                                |                                                          |                        |
| precipitous  | 陡峭的                    | steep, perpendicular, overchanging in rise or fall                                                                                             |                                                          |                        |
| afflict      | 折磨，使痛苦                 | cause persistent pain or suffering                                                                                                             |                                                          |                        |
| chagrin      | 失望，懊恼                  | annoyance and disappointment                                                                                                                   | cha 茶 + grin 笑                                           |                        |
| grandiose    | 宏伟的；浮夸的                |                                                                                                                                                | grand                                                    |                        |
| scrupulous   | 恪守道德规范的，一丝不苟的          | having moral integrity, punctiliously exact, conscientious, meticulous, punctilious                                                            |                                                          |                        |
| hypocritical | 虚伪的，伪善的                | characterized by hypocrisy                                                                                                                     |                                                          | hypocritical affection |
| obsequious   | 逢迎的，谄媚的                | menial, servile, slavish, subservient                                                                                                          | ubiquitous 无处不在的；iniquitous 邪恶的，不公正的；quixotic 不切实际的，空想的  |                        |
| spongy       | 像海绵的，不坚实的              | resembling a sponge, not firm or solid                                                                                                         |                                                          | spongy topsoil 松软的表土   |
| harass       | 侵扰，烦扰                  | annoy persistently                                                                                                                             |                                                          | harass the border area |
| radicalism   | 激进主义                   |                                                                                                                                                | radical adj.                                             |                        |
| deluge       | 大洪水；暴雨                 | great flood, heavy rainfall                                                                                                                    |                                                          |                        |
| insane       | 无意义的，空洞的，愚蠢的           | empty, lacking sense, void, silly, innocuous, insipid, jejune, sapless, vapid                                                                  |                                                          |                        |
| extricate    | 可解救的，能脱险的              | capable of being freed from difficulty                                                                                                         | intricate 错综复杂的                                          |                        |
| assuredness  | 确定，自信                  |                                                                                                                                                | assure v.                                                |                        |
| pinpoint     | 准确的确定；使突出，引起注意；极其精神的   |                                                                                                                                                | pin 针 + point 针尖                                         |                        |
| ignominy     | 羞耻，耻辱                  | shame or dishonor, infamy, disgrace, disrepute, opprobrium                                                                                     | ig 不 + nominy 名声                                         |                        |
| conclusive   | 最后的，结论的，决定性的；确凿的，消除怀疑的 | of a conclusion, convincing                                                                                                                    | conclude v.                                              |                        |
| anomalous    | 反常的；不协调的               | inconsistent with what is usual, normal, or expected, marked by icongruity or contradiction, aberrant, abnormal, deviant, divergent, irregular |                                                          |                        |
| uncanny      | 神秘的，离奇的                | wired, supernatural                                                                                                                            | un + canny 安静的，谨慎的                                       |                        |
| repulse      | 击退，回绝；拒绝               | repel, rebuff, rejection                                                                                                                       | re + pulse                                               |                        |
| ally         | 结盟，联合；同盟者，伙伴           | associate                                                                                                                                      |                                                          |                        |
| venal        | 腐败的，贪赃枉法的              | characterized by corrupt, bribery                                                                                                              |                                                          |                        |
| aquatic      | 水生的，水中的                | living in or upon water                                                                                                                        | aqua 水 + tic                                             |                        |
| blockbuster  | 巨型炸弹；一鸣惊人的事物           | large high-explosive bomb                                                                                                                      |                                                          |                        |
| deprecation  | 反对                     | disapproval                                                                                                                                    | deprecate v.                                             |                        |
| gloat        | 幸灾乐祸的看，心满意足的看          | America                                                                                                                                        |                                                          | gloat over sth.        |
| walrus       | 海象                     | a large gregarious marine mammal                                                                                                               |                                                          |                        |
| impenetrable | 不能穿透的；不可理解的            | incapable of being penetrated, unfathomable, inscrutable, impassable, impermeable, imperviable, impervious                                     |                                                          |                        |
| enervate     | 使虚弱，使无力                | lessen the vitality or strength of                                                                                                             |                                                          |                        |
| 20            | 20               | 20                                                                                                                   | 20                                             | 20                               |
| baffle        | 使困惑，难到           | confuse, puzzle, confound                                                                                            |                                                |                                  |
| heterodox     | 异端的，非正统的         | unorthodox, dissident, heretical                                                                                     | hetero 其他的，相异的 + dox 思想                        |                                  |
| pejorative    | 轻视的，贬低的          | tending to disparage, depreciatory, belittle, derogate, to speak slightingly of, depreciate, decry, denounce, debase |                                                |                                  |
| pedagogic     | 教育学的             | of or relating to education                                                                                          | Pedagogy 教育学；pedant 学究                         |                                  |
| fluid         | 流体的，流动的；易变的，不固定的 | capable of flowing, subject to change or movement                                                                    |                                                |                                  |
| eligible      | 合格的，有资格的         | suitable, qualified                                                                                                  |                                                |                                  |
| superb        | 上乘的，出色的          | highest degree of excellence, brilliance, or competence, lofty, sublime                                              |                                                |                                  |
| incentive     | 刺激，诱因，动机；刺激因素    | motive, motivate, goad, impetus, impulse, stimulus, incite                                                           |                                                |                                  |
| stout         | 肥胖的；强壮的          | bulky in body, vigorous, sturdy                                                                                      |                                                |                                  |
| flippant      | 轻率的，无礼的          | frivolous, disrespectful, impertinent, rude, reckless, insolent, impudent, impulsive, brassy                         | flip 用指轻弹；蹦蹦跳跳；无礼的，冒失的                         |                                  |
| incendiary    | 防火的，纵火的          |                                                                                                                      | in + cend 发（火）光+ iary                          |                                  |
| incandescent  | 遇热发光的；发白热光的      | Incandescent lamps 白炽灯                                                                                               | candy 蜡烛，糖果；conflagration （建筑、森林）大火            |                                  |
| fussy         | 爱挑剔的，难取悦的        | dainty, exacting, fastidious, finicky, meticulous                                                                    | fuzzy 模糊的                                      |                                  |
| imprudent     | 轻率的，不理智的         | indiscreet, not wise                                                                                                 | prudent 谨慎的                                    |                                  |
| wearisome     | 令人厌倦、疲倦          |                                                                                                                      | weary v.                                       |                                  |
| carve         | 雕刻；切成片           | shape by cutting, shipping, hewing, slice, cleave, dissect, disserve, sever, split                                   | engrave 雕刻，铭刻                                  |                                  |
| demean        | 贬抑；降低            | degrade, humble, belittle                                                                                            |                                                |                                  |
| elude         | 逃避，搞不清；理解不了      | avoid adroitly, escape the perception or understanding, eschew, evade, shun                                          |                                                |                                  |
| humdrum       | 单调的，乏味的          | dull, monotonous, boring                                                                                             | hum 嗡嗡 + drum 鼓声                               |                                  |
| anathema      | 被诅咒的人，（天主教的）革出教门 | one that is cursed, a formal ecclesiastical ban, curse                                                               |                                                |                                  |
| chastisement  | 惩罚               | punishment, castigation, penalty                                                                                     |                                                |                                  |
| enormous      | 极大的，巨量的          | shockingly large, colossal, gargantuan, immense, Titanic, tremendous                                                 |                                                |                                  |
| ingeniousness | 独创性              | ingenuity                                                                                                            |                                                |                                  |
| indignation   | 愤慨，义愤            | anger or scorn, righteous anger                                                                                      |                                                | be full of righteous indignation |
| sustain       | 承受（困难）；支撑（重量或压力） | undergo, withstand, bolster, prop, underprop                                                                         |                                                |                                  |
| actuate       | 开动，促使            | motivate, activate                                                                                                   | accurate 准确的                                   |                                  |
| strait        | 海峡；狭窄的           | narrow                                                                                                               | isthmus 地峡；tacit 心照不宣的                         |                                  |
| drab          | 黄褐色的；单调的，乏味的     | yellowish brown, not bright or lively, monotonous                                                                    |                                                |                                  |
| subdue        | 征服；压制；减轻         | crush, overpower, subjugate, conquer, vanquish, reduce                                                               |                                                |                                  |
| dissipate     | （使）消失，消散；浪费      | break up and scatter or vanish, to waste or squander                                                                 |                                                |                                  |
| excavate      | 开洞，凿洞；挖掘         | scoop, shovel, unearth                                                                                               | ex + cave 洞 + ate                              |                                  |
| extol         | 赞美               | laud, prise highly, acclaim, applaud, compliment, praise, recommend, commend, eulogize, tout, felicitate, rave       |                                                |                                  |
| contempt      | 轻视，鄙视            | defiance, disdain, disparagement                                                                                     | depreciate, belittle, disparage, disapprove v. | contempt for …                   |
| 21          | 21                  | 21                                                                                           | 21                                | 21                   |
| instigate   | 怂恿，鼓动，煽动            | urge on, foment, incite, agitate, abet, provoke, stir                                        | in 进入 + stig (sting 刺激) + ate     |                      |
| formidably  | 可怕的；难对付的，强大的        | strongly                                                                                     | formidable                        |                      |
| tact        | 机智，圆滑               | keen sense of what to do or say                                                              |                                   |                      |
| adverse     | 有害的，不利的，敌对的，相反的     | not favorable, hostile, contrary                                                             | ad + verse                        |                      |
| precipitate | 使突然降临，加速，促成；鲁莽的，轻率的 | bring about abruptly, hasten, impetuous                                                      | peripatetic 巡游的，游动的               | precipitate … into … |
| scheme      | 阴谋，（作品等）体系结构        | crafty or secret plan, systematic or organized framework, design                             | schema 图表                         |                      |
| incumbent   | 在职者，现任者；义不容辞的       | holder of an office or benefice, obligatory                                                  | in + cumbent (cumbersome 阻碍的，笨重的) |                      |
| euphemistic | 委婉的                 |                                                                                              | euphemism 婉言，委婉的说法                |                      |
| exorbitant  | 过分的，过度的             | excessive, extravagant, extreme, immoderate, inordinate                                      |                                   | exorbitant fees      |
| overstate   | 夸张，对…言过其实           | exaggerate, overemphasize, intensify, magnify                                                |                                   | over + state         |
| grave       | 严肃的，庄重的；墓穴          | serious                                                                                      | gravity n.                        |                      |
| transgress  | 冒犯，违背               | violate                                                                                      |                                   |                      |
| fumigate    | 用烟熏消毒               |                                                                                              | fume 烟 + gate 动作（消毒）              |                      |
| perspire    | 出汗                  | sweat                                                                                        | per + spire 呼吸                    |                      |
| impair      | 损害，削弱               | damage, reduce, injure, blemish, mar, prejudice, tarnish, vitiate                            |                                   |                      |
| adversity   | 逆境，不幸               |                                                                                              | adverse adj.                      |                      |
| plump       | 丰满的，使丰满             | well-rounded                                                                                 |                                   |                      |
| extraneous  | 外来的，无关的             | from the outside, not pertinent                                                              | extra + neous                     |                      |
| analogous   | 相似的，可比拟的            | akin, comparable, corresponding, parallel, undifferentiated                                  |                                   | be analogous to …    |
| animosity   | 憎恶，仇恨               | feeling of strong dislike or hatred, animus, antagonism, enmity, hostility, rancor           |                                   |                      |
| inflate     | 使充气，使膨胀             | fill with air                                                                                |                                   |                      |
| venomous    | 有毒的                 | full of venom, poisonous, deadly, vicious, virulent                                          |                                   |                      |
| alloy       | 合金                  | substance composed of two or more metals, admixture, amalgam, composite, fusion, interfusion |                                   |                      |
| pugnacious  | 好斗的                 | having a quarrelsome and combative nature                                                    |                                   |                      |
| spurn       | 拒绝，摒弃               | disdainful rejection                                                                         | squirt, spurt 喷出，溅                |                      |
| 22             | 22              | 22                                                             | 22                                       | 22                 |
| nomadic        | 游牧的；流浪的         | itinerant, peripatetic, vagabond, vagrant                      |                                          |                    |
| sanctuary      | 圣地，庇护所；庇护       | wildlife sanctuary                                             |                                          |                    |
| unbecoming     | 不合身的；不得体的       | improper, unsuitable to the wearer                             |                                          |                    |
| approbation    | 赞许；认可           | commendation, official approval                                | ap + prove + bation                      |                    |
| slavish        | 卑屈的；效仿的，无创造性的   |                                                                | slave + ish                              |                    |
| aghast         | 惊骇的，吓呆的         | terrified, frightened, scared, scary                           | ghost 鬼                                  |                    |
| intimate       | 亲密的；密友；暗示       | closely acquainted, an intimate friend, hint or imply, suggest |                                          |                    |
| paragon        | 模范，典范           | paradigm, exemplar, a model of excellence or perfection        |                                          |                    |
| rudimentary    | 初步的；未充分发展的      | fundamental, elementary                                        |                                          |                    |
| recklessness   | 鲁莽，轻率           |                                                                | reckless adj.                            |                    |
| digressive     | 离题的，枝节的         | characterized by digressions                                   |                                          | digressive remarks |
| blackmail      | 敲诈，勒索           | payment extorted by threatening                                | black (黑) + mail                         |                    |
| replicate      | 复制              | produce a replica                                              | re + plic 折叠 + ate                       |                    |
| snug           | 温暖舒适的           | warm and comfortable, cozy                                     |                                          | snug harbor        |
| odious         | 可憎的；令人作呕的       | disgusting, offensive                                          | “呕得要死”                                   |                    |
| divulge        | 泄漏，透露           | disclose, betray, reveal                                       |                                          |                    |
| repudiation    | 拒绝接受，否认         |                                                                | repudiate v.                             |                    |
| impressionable | 易受影响的           | easily affected by impressions                                 |                                          |                    |
| primordial     | 原始的，最初的         | primeval, primitive                                            |                                          |                    |
| voluptuous     | 撩人的，沉溺酒色的       |                                                                | volupt 享乐,挥霍 + uous                      |                    |
| inert          | 惰性的；呆滞的         | chemical inactive, dull                                        | in 不 + ert 动的                            |                    |
| untutored      | 未受教育的           |                                                                | un + tutored                             |                    |
| opportune      | 合适的，适当的         | right for a particular purpose                                 |                                          |                    |
| modulate       | 调整，调节；调音        | adjust, tune                                                   |                                          |                    |
| conversant     | 精通的，熟练的         | versed, familiar and acquainted                                | versed                                   |                    |
| disposable     | 一次性的；可自由使用的     |                                                                |                                          |                    |
| unbridled      | 放纵的；不受约束的       | unrestrained, unchecked, uncurbed, ungoverned, indulgent       |                                          |                    |
| intrinsic      | 固有的，内在的，本质的     | congenital, connate, elemental, inherent, innate               |                                          |                    |
| downfall       | 垮台              | sudden fall, overthrow, upset                                  |                                          |                    |
| warrantable    | 可保证的，可承认的       |                                                                | warrant v.                               |                    |
| resonant       | （声音）洪亮的；回响的，共鸣的 | enriched by resonance, echoing                                 |                                          |                    |
| sophomoric     | 一知半解的           |                                                                | dilettante, dabbler, amateur 一知半解者，业余爱好者 |                    |
| pilgrim        | 朝圣者；（出国）旅行者     | wayfarer                                                       |                                          |                    |
| unreserved     | 无限制的；未被预定的      |                                                                | un + reserved                            |                    |
| sordid         | 卑鄙的，肮脏的         | dirty, filthy, foul, mean, seedy                               |                                          |                    |
| 23            | 23              | 23                                                                            | 23                                        | 23              |
| levy          | 征税；征兵           |                                                                               | impose a tax, draft into military service |                 |
| pledge        | 誓言，保证；发誓        | solemn promise, vow to do sth., commitment, swear                             |                                           |                 |
| myopic        | 近视眼的；目光短浅的      | lack of foresight or discernment                                              |                                           | a myopic child  |
| serpentine    | 像蛇般蜷曲的；蜿蜒的      | winding or turning one way to another                                         | serpent 蛇                                 |                 |
| lump          | 块，肿块；形成块状       | become lumpy                                                                  |                                           | lump … together |
| labile        | 易变化的；不稳定的       | open to change, unstable, fickle, unsteady                                    |                                           |                 |
| retort        | 反驳              | counter argument                                                              |                                           |                 |
| abstinent     | 饮食有度的；有节制的，禁欲的  | constraining from indulgence                                                  |                                           |                 |
| despotic      | 专横的，暴虐的         | autocratic, tyrannical                                                        | despot 暴君                                 |                 |
| lank          | 细长的；长、直且柔软的     | long, thin, straight, thin, and limp, slender                                 |                                           |                 |
| outmaneuver   | 以策略制胜           |                                                                               | out + maneuver                            |                 |
| epidermis     | 表皮，外皮           | the outmost layer of the skin                                                 |                                           |                 |
| picayunish    | 微不足道的，不值钱的      | of little value, petty, small-minded                                          |                                           |                 |
| ventriloquist | 口技表演者，腹语表演者     |                                                                               | ventriloquism                             |                 |
| crimp         | 使皱起，使卷曲；抵制，束缚   |                                                                               |                                           |                 |
| demoralize    | 使士气低落           | dispirit                                                                      | de + moral 士气                             |                 |
| hoist         | 提升，升起；起重机       | raise or haul up, lift, elevate                                               |                                           |                 |
| podiatrist    | 足病医生            | chiropodist                                                                   | pod 脚 + iatr 治疗 + ist 人                   |                 |
| concord       | 一致；和睦           | agreement, harmony                                                            | con                                       |                 |
| swindle       | 诈骗              |                                                                               | wind                                      |                 |
| ordinance     | 法令，条例           | governmental statue of regulation                                             | ordin 命令 + ance                           |                 |
| sentiment     | 多愁善感；思想感情       | tender feeling or emotion                                                     | sent 感觉 + ment                            |                 |
| salve         | 药膏；减轻，缓和        |                                                                               | salv 救援 + e                               |                 |
| canyon        | 峡谷              | a long, narrow valley between cliffs                                          |                                           |                 |
| lurk          | 潜伏，埋伏           | stay hidden, lie in wait                                                      | lark 云雀                                   |                 |
| expunge       | 删除              | erase, delete, cancel                                                         |                                           |                 |
| bouffant      | 蓬松的，膨胀的         | puffed out, puffy                                                             |                                           |                 |
| waddle        | 摇摇摆摆的走          |                                                                               | “歪道“                                      |                 |
| defile        | 弄污，弄脏；峡谷，隘路     | make filthy or dirty, pollute, narrow valley or mountain pass                 | de + file (vile 卑鄙)                       |                 |
| defuse        | 拆除引信，使去除危险性；平息  |                                                                               | de + fuse 导火索                             |                 |
| quota         | 定额，配额           | someone's share                                                               |                                           |                 |
| potentiate    | 加强，强化           | make effective or active                                                      | potent                                    |                 |
| mien          | 风采，态度           | air, bearing, demeanor                                                        | “迷你”                                      |                 |
| obtuse        | 愚笨的；钝的          | dull or insensitive, blunt                                                    |                                           |                 |
| bumble        | 说话含糊；拙劣的做       | stumble, proceed clumsily, burr, buzz, hum                                    | humble 谦逊的                                |                 |
| expulsion     | 驱逐，逐出           | the act of expelling                                                          |                                           |                 |
| wretched      | 可怜的，不幸的，悲惨的     | in a very unhappy and unfortunate state                                       |                                           |                 |
| swoop         | 猛扑，攫取           | move in a sudden sweep, seize or snatch                                       |                                           |                 |
| plunder       | 抢劫，掠夺           | maraud, pillage                                                               | pl (place) + under                        |                 |
| epilogue      | 收场白；尾声          | a closing section                                                             |                                           |                 |
| ensue         | 接着发生            | happen afterwards                                                             | en 进入 + sue跟从                             |                 |
| soggy         | 湿透的             | clammy, dank, sopping, waterlogged, saturated or heavy with water or moisture |                                           |                 |
| convene       | 集合；召集           | come together, assemble, call or meet                                         | con 一起 + ven 来                            |                 |
| guzzle        | 大吃大喝；大量消耗       | drink and eat greedily and immoderately                                       |                                           |                 |
| pall          | 令人发腻；失去吸引力      |                                                                               |                                           |                 |
| riot          | 暴动，闹事           | to create or engage in a riot, frolic, revel, carouse                         |                                           |                 |
| artifice      | 巧妙方法；诡计         | skill and ingenuity, a sly trick                                              |                                           |                 |
| capacious     | 容量大的，宽敞的        | containing a great deal, spacious                                             |                                           |                 |
| dispatch      | 派遣；迅速处理；匆匆吃完；迅速 | send off promptly, dispose efficiently, promptness, haste                     |                                           |                 |
| knit          | 编织；密接，紧密相联      | threads & needles, connect closely                                            |                                           |                 |
| chirp         | （鸟虫）唧唧叫         | utter in a sharp, shrill tone                                                 |                                           |                 |
| inveigle      | 诱骗，诱使           | win with deception, lure                                                      |                                           |                 |
| scamper       | 奔跑，蹦蹦跳跳         | run nimbly and playfully                                                      | s + camper 露营者                            |                 |
| mumble        | 咕哝，含糊不清的说       | grumble, murmur, mutter, speak or say unclearly                               |                                           |                 |
| reproof       | 责备，斥责           | criticism for a fault, rebuke, admonishment, reprimand, reproach, scolding    |                                           |                 |
| trapeze       | 高空秋千，吊架         |                                                                               |                                           |                 |
| contaminate   | 弄脏，污染           | make impure, pollute, smudge                                                  | con + tamin 接触 + ate                      |                 |
| 24           | 24            | 24                                                                        | 24                               | 24                            |
| abate        | 减轻，减少         | wane                                                                      | a + bate 减少                      |                               |
| induct       | 使就职；使入伍       | install, enroll in the armed forces                                       |                                  |                               |
| snide        | 挖苦的，讽刺的       | slyly disparaging, insinuating, sarcastic, sneering, caustic, ironic      |                                  |                               |
| ingress      | 进入            | entering                                                                  |                                  |                               |
| linger       | 逗留，继续存留；徘徊    | continue to stay                                                          | linguistic 语言的                   |                               |
| ripple       | （使）泛起涟漪；波痕，涟漪 |                                                                           |                                  | a ripple of applause/laughter |
| disclaim     | 放弃权利；拒绝承认     | renounce, deny                                                            |                                  |                               |
| recline      | 斜倚，躺卧         | lie down                                                                  |                                  |                               |
| recess       | 休假；凹槽         | alcove, cleft, suspension of business                                     | re 反着 + cess 走                   |                               |
| enjoin       | 命令，吩咐         | command, direct or impose by authoritative order                          | en 使 + join                      |                               |
| earthy       | 粗俗的，土气的       | rough, plain in taste                                                     | earth                            |                               |
| boast        | 自夸            | brag, swank                                                               | roast 烤                          |                               |
| miserly      | 吝啬的           | penurious                                                                 | miser 吝啬鬼                        |                               |
| garnish      | 装饰            | decorate, embellish                                                       | gar 花 + nish                     |                               |
| distrait     | 心不在焉的         | absent-minded, distracted                                                 |                                  |                               |
| chic         | 漂亮的，时髦的       | modish, posh, swanky, cleverly stylish, currently fashionable             |                                  |                               |
| flay         | 剥皮；抢夺，掠夺；严厉指责 | strip off the skin, rob, pillage, criticize or scold mercilessly          | fray 吵架，打架                       |                               |
| retch        | 作呕，恶心         | vomit                                                                     |                                  |                               |
| occlude      | 使闭塞           | prevent the passage of                                                    | oc + clud 关闭                     |                               |
| traipse      | 漫步，闲荡         | wander, ramble, roam, jog                                                 |                                  |                               |
| inoculate    | 注射预防针         | inject a serum, vaccine to create immunity                                |                                  |                               |
| rendering    | 表演，翻译         | performance, translation                                                  |                                  |                               |
| cozen        | 欺骗，哄骗         | coax, deceive                                                             | dozen 一打                         |                               |
| saddle       | 鞍，马鞍          | a seat of one who rides on horseback                                      |                                  |                               |
| scotch       | 镇压，扑灭         | put an end to                                                             | Scotch 苏格兰                       |                               |
| drub         | 重击，打败         | beat severely, defeat decisively                                          |                                  |                               |
| obsolete     | 废弃的，过时的       | no longer in use, out of date, old-fashioned                              |                                  |                               |
| trim         | 修剪，井井有条的      | make neat by cutting and clipping                                         |                                  |                               |
| complaisance | 彬彬有礼；殷勤；柔顺    |                                                                           | com 一起 + plais 高兴 + ance         |                               |
| asinine      | 愚笨的           | of asses, stupid, silly                                                   | as (ass) + in + in + e           |                               |
| abash        | 使羞愧；使尴尬       | make embarrassed                                                          | a+bash                           |                               |
| douse        | 把…浸在水中的；熄灭    | plunge into water, extinguish                                             | do + use 又做又用                    |                               |
| dispense     | 分配，分法         | to distribute in portions                                                 |                                  |                               |
| incriminate  | 连累，牵连         | involve in                                                                | in + crim (crime) + in + ate     |                               |
| affected     | 不自然的；假装的      | behaving in an artificial way, assumed, factitious, fictitious, unnatural |                                  |                               |
| solitude     | 孤独            |                                                                           | solit - 孤独前缀；soliloquy 自言自语；戏剧独白 |                               |
| wince        | 畏缩，退缩         | flinch, cringe, quail, shrink back involuntarily                          |                                  |                               |
| dint         | 击打出凹痕；力       | make a dent in                                                            | by dint of … 凭借                  |                               |
| evacuate     | 撤离；疏散         | withdraw, remove inhabitants for protective purpose                       |                                  |                               |
| feline       | 猫的，猫科的        |                                                                           | fel 猫 + ine                      |                               |
| leach        | 过滤            | by percolation                                                            | bleach 去色，漂白                     |                               |
| infatuate    | 使迷恋；使糊涂       | inspire with a foolish or extravagant love or admiration                  |                                  |                               |
| execrable    | 可憎的，讨厌的       | deserving to be execrated, abominable, detestable                         |                                  | execrable poetry              |
| 25           | 25                  | 25                                                     | 25                   | 25    |
| mattress     | 床垫                  | sleep on                                               |                      |       |
| nifty        | 极好的，极妙的             | good and attractive, great                             |                      |       |
| persnickety  | 势利的；爱挑剔的            | snobbish, fuzzy, fastidious                            |                      |       |
| crass        | 愚钝的，粗糙的             | crude and unrefined                                    |                      |       |
| abrogate     | 废止，废除               | abolish, repeal by authority                           | ab + rog + ate       |       |
| dissimulate  | 隐藏，掩饰               | pretend, dissemble                                     |                      |       |
| duress       | 胁迫                  | force and threats, compulsion                          |                      |       |
| idle         | 无所事事的，无效的；懒散        |                                                        |                      |       |
| episode      | 一段情节；插曲，片段          | circumstance, development, happening, incident         |                      |       |
| jab          | 猛刺                  |                                                        |                      |       |
| compunction  | 懊悔，良心不安             | guilt, remorse, penitence                              |                      |       |
| blasé        | 厌倦享乐的，玩厌了的          | board with pleasure or dissipation                     |                      |       |
| enclosure    | 圈地，围场               |                                                        | enclose v.           |       |
| comatose     | 昏迷的                 | unconscious, torpid                                    | coma n.              |       |
| ravish       | 使着迷；强夺              | overcome, take away                                    |                      |       |
| spout        | 喷出；滔滔不绝的说           | eject in stream                                        |                      |       |
| exculpate    | 开脱；申明无罪，证明无罪        | free from blame, declare and prove guiltless           |                      |       |
| deputy       | 代表；副手               | representative                                         | duty                 |       |
| mellifluous  | 柔美流畅的               | sweetly and smoothly flowing                           |                      |       |
| depose       | 免职，宣誓作证             |                                                        | de + pose 放          |       |
| grotesque    | 怪诞的，古怪的；（艺术）风格怪异的   | bizarre, fantastic                                     |                      |       |
| jug          | 用陶罐等炖；水壶；关押         | jail, imprison, in an earthenware vessel               |                      |       |
| embolden     | 鼓励                  | to give confidence to                                  | em + bold + en       |       |
| testify      | 见证，证实               | bear witness to …                                      |                      |       |
| whoop        | 高喊，欢呼               | eagerness, exuberance, jubilation                      |                      |       |
| impasse      | 僵局，死路               | deadlock, blind alley                                  | im 无 + pass + e      |       |
| martyr       | 烈士，殉道者              |                                                        | 词根：目击者               |       |
| daub         | 涂抹；乱画               |                                                        |                      |       |
| ennoble      | 授予爵位，使高贵            | make noble                                             |                      |       |
| wiry         | 瘦而结实的               | lean, supple, and vigorous                             |                      |       |
| woo          | 求爱，求婚；恳求，争取         |                                                        |                      |       |
| sullen       | 忧郁的                 | gloomy, dismal, brooding, morose, sulky                |                      |       |
| manifesto    | 宣言，声明               | public declaration                                     | manifest v.          |       |
| enthralling  | 迷人的，吸引人的            |                                                        | en + thrall 奴隶 + ing |       |
| anneal       | 使（金属、玻璃等）退火；使变强；使变硬 | strengthen and harden                                  |                      |       |
| forfeiture   | （名誉等）丧失             |                                                        | forfeit v.           |       |
| malfeasance  | 不法行为，渎职             | misconduct by a public official                        |                      |       |
| scurvy       | 卑鄙的，下流的             | despicable                                             | scurry v. 疾行         |       |
| wheedle      | 哄骗，诱骗               | cajole, coax, seduce                                   |                      |       |
| careen       | （船）倾斜；使倾斜           | lean sideways                                          |                      |       |
| peculate     | 挪用（公款）              | embezzle                                               |                      |       |
| vandalism    | （对公物等）恶意破坏          | willful, malicious                                     |                      |       |
| effulgent    | 灿烂的，光辉的             | of great brightness                                    |                      |       |
| carouse      | 狂饮寻乐                | noisy, merry drinking party                            |                      |       |
| cephalic     | 头的，头部的              | of head of skull                                       | cephal 头             |       |
| wan          | 虚弱的，病态的             | feeble, sickly, pallid, ghastly, haggard, sallow       |                      |       |
| preponderate | 超过，胜过               | exceed                                                 | pre + ponderate      |       |
| snuggle      | 紧靠，依偎               | draw close                                             |                      |       |
| blackball    | 投票反对；排斥             | ostracize                                              | black + ball         |       |
| hubris       | 傲慢，目中无人             |                                                        |                      |       |
| shabby       | 破旧的，卑鄙的             | scruffy, shoddy, dilapidated, despicable, contemptible |                      |       |
| moat         | 壕沟，护城河              |                                                        |                      |       |
| compulsion   | 强迫；（难以抗拒的）冲动        |                                                        |                      |       |
| musket       | 旧式步枪；毛瑟枪            |                                                        |                      |       |
| snarl        | 纠缠，混乱；咆哮，怒骂；怒吼声     | knot                                                   |                      |       |
| grumpy       | 脾气暴躁的               | grouchy, peevish                                       | grump v.             |       |
| tangy        | 气味刺激的，扑鼻的           | pleasantly sharp flavor                                |                      |       |
| impeach      | 控告；怀疑；弹劾            | accuse, charge, indict                                 |                      |       |
| snappy       | 生气勃勃的；漂亮的，时髦的       | stylish                                                |                      |       |
| 26            | 26                | 26                                                              | 26                     | 26           |
| shattered     | 破碎的               |                                                                 | shatter v.             |              |
| perverse      | 刚愎自用的，固执的；反常的     | obstinate in opposing, wrongheaded                              |                        |              |
| maple         | 枫树                |                                                                 |                        |              |
| comeuppance   | 应得的惩罚；因果报应        | deserved rebuke or penalty                                      | come up + ance         |              |
| abject        | 极可怜的；卑下的          | miserable, wretched, degraded, base                             | ab + ject              |              |
| malaise       | 不适，不舒服            | illness                                                         | “没累死”                  |              |
| protuberant   | 突出的，隆起的           | prominent, thrusting out                                        |                        |              |
| lateral       | 侧面的               | side                                                            |                        |              |
| twee          | 矫揉造作的，故作多情的       | dainty, delicate, cute or quaint                                |                        |              |
| nettle        | 荨麻；烦扰，激怒          | irritate, provoke                                               |                        |              |
| inadvertently | 不小心的，疏忽的          | by accident                                                     |                        |              |
| remit         | 免除，宽恕；汇款          | release                                                         |                        |              |
| reminisce     | 追忆，回想             |                                                                 | re + mind + sce        |              |
| lumber        | 跌跌撞撞的走，笨拙的走；杂物；木材 | move with heavy clumsiness                                      |                        |              |
| stanza        | （诗歌的）节、段          |                                                                 | stan (stand) + za      |              |
| rumpus        | 喧闹，骚乱             | noisy commotion, clamor, tumult, uproar                         |                        |              |
| writ          | 令状；书面令            |                                                                 | write - e              |              |
| peremptory    | 不容反抗的；专横的         | masterful                                                       | per + empt + ory       |              |
| beget         | 产生，引起             | bring into being                                                |                        |              |
| scatter       | 散开，驱散             | dispel, disperse, dissipate                                     |                        |              |
| choleric      | 易怒的，暴躁的           | of irascible nature, irritable                                  | choler 胆汁 + ic         |              |
| jeer          | 嘲笑                | mock, taunt, scoff at                                           |                        | jeer at …    |
| spate         | 许多，大量；暴涨，发洪水      |                                                                 |                        | a spate of … |
| invoke        | 祈求，恳求；使生效         | implore, entreat                                                |                        |              |
| allowance     | 津贴，补助；允许，承认       | subsidize                                                       |                        |              |
| exiguous      | 太少的，不足的           | scanty, meager, scarce, skimpy, sparse                          |                        |              |
| acumen        | 敏锐，精明             |                                                                 | acu + men              |              |
| gourmand      | 贪食者               | glutton                                                         |                        |              |
| hurdle        | 跨栏，障碍；克服          | barrier, block, obstruction, snag, obstacle, surmount, overcome |                        |              |
| vie           | 竞争                | compete, contend, contest, emulate, rival                       |                        |              |
| hinge         | 铰链；关键             | joint, determining factor                                       |                        |              |
| gander        | 雄鹅；笨蛋，傻瓜；闲逛       |                                                                 | gender 性别              |              |
| putrid        | 腐臭的               | rotten, malodorous                                              |                        |              |
| gruff         | 粗鲁的，板着脸的；粗哑的      | rough, hoarse                                                   |                        |              |
| ensconce      | 安置，安坐             | shelter, establish, settle                                      | en + sconce 小堡垒        |              |
| vista         | 远景；展望             | distant view, prospect, extensive mental view                   |                        |              |
| cloy          | （甜食）生腻，吃腻         | satiate                                                         |                        |              |
| pitiless      | 无情的，冷酷的           | cruel, harsh                                                    |                        |              |
| discern       | 识别，看出             | recognize, distinguish                                          |                        |              |
| tusk          | （大象等）长牙           |                                                                 |                        |              |
| trounce       | 痛击；严惩             | thrash or punish severely                                       |                        |              |
| discriminate  | 区别，歧视             |                                                                 |                        |              |
| winkle        | 挑出，剔出；取出          |                                                                 |                        |              |
| beacon        | 烽火，灯塔             |                                                                 | beach；pecan 山核桃        |              |
| parturition   | 生产，分娩             |                                                                 |                        |              |
| shriek        | 尖叫                | utter a sharp, shrill, sound                                    |                        |              |
| accessory     | 附属的，次要的           | additional, supplementary, subsidiary                           |                        |              |
| gaffe         | 失礼，失态             | social or diplomatic blunder                                    |                        |              |
| wend          | 行，走，前进            | proceed on                                                      |                        |              |
| traduce       | 中伤，诽谤             | slander or defame                                               |                        |              |
| scud          | 疾行，飞奔             |                                                                 |                        |              |
| gutless       | 没有勇气的；怯懦的         | cowardly, spineless                                             |                        |              |
| magniloquent  | 夸张的               |                                                                 | magn 大 + loqu 说话 + ent |              |
| boding        | 凶兆，前兆；先兆的         | omen especially for coming evil                                 |                        |              |
| demarcate     | 划分，划界             |                                                                 |                        |              |
| opulence      | 富裕；丰富             | affluence, profusion                                            |                        |              |
| smudge        | 污迹；污点；弄脏          |                                                                 | s + mud 泥 + age        |              |
| consecrate    | 奉献；使神圣            | dedicate, sanctify                                              | con + sacre + ate      |              |
| flux          | 不断的变动；变迁，动荡不安     |                                                                 |                        |              |
