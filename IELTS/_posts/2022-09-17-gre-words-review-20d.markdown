---
layout: post
title:  "GRE Words 20 Day Review"
date:   2022-09-17 19:52:01 +0800
category: IELTSPosts
---

# New Words (10-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English       | Chinese         | Synonyms                                                      | Helpful Memory Ideas       | Usage |
|---------------|-----------------|---------------------------------------------------------------|----------------------------|-------|
| 1             | 1                | 1                                                                | 1                          | 1                     |
| congenital    | 先天的，天生的          | innate                                                           |                            |                       |
| irradiate     | 照射；照耀，照亮         | shnie, light up                                                  |                            |                       |
| platitudinous | 陈腐的              |                                                                  | platitude                  | platitudinous remarks |
| excise        | 切除，切去            | remove, cut off                                                  |                            |                       |
| refractory    | 倔强的，难管理的；（病）难治的  | stubborn, unmanageable, resistant                                | fract 断裂                   |                       |
| automotive    | 汽车的，自动的          |                                                                  | automation 自动化             |                       |
| buckle        | 皮带环扣；扣紧          |                                                                  |                            | buckle up 扣好安全带       |
| inhibition    | 阻止；抑制；抑制物        | forbid, debar, restrict                                          | inhibite is the v. form    |                       |
| disillusion   | 使梦想破灭，使醒悟        |                                                                  | dis + illusion             |                       |
| connive       | 默许；纵容；共谋         | feign ignorance, conspire                                        | con + nive 眨眼              |                       |
| rubbery       | 橡胶似的，有弹性的        |                                                                  | rubber 橡胶                  |                       |
| supplant      | 排挤，取代            |                                                                  | sub + plant                |                       |
| recede        | 后退，撤回            | withdraw                                                         | re + cede                  |                       |
| provoke       | 激怒；引起；驱使         |                                                                  | pro + voke                 |                       |
| inverse       | 相反的              | inverted                                                         |                            |                       |
| scourge       | 鞭笞；磨难            | whip                                                             | courage                    |                       |
| deviant       | 越出常规的            |                                                                  | deviate 偏离；defiant 反抗的，挑衅的 |                       |
| sobriety      | 节制；庄重            | moderation, gravity                                              |                            |                       |
| partiality    | 偏袒，偏心            | bais, projudice, proclivity, propensity, tendency                | partial is the adj. form   |                       |
| spiritedness  | 有精神，活泼           | liveliness, vivaciousness                                        |                            |                       |
| slipshod      | 马虎的，草率的          | careless, slovenly                                               |                            |                       |
| perilous      | 危险的，冒险的          |                                                                  | peril is the n. form       |                       |
| engrave       | 雕刻，铭刻；牢记，铭记      | carve, impress deeply                                            |                            |                       |
| suffrage      | 选举权，投票权          | the right of voting                                              | suf + frage                |                       |
| elaborate     | 精致的，复杂的；详尽地说明，阐明 |                                                                  | labor 劳动                   |                       |
| stumble       | 绊倒               | lurch, stagger, trip                                             |                            |                       |
| scrappy       | 碎片的；好斗的；坚毅的      | disconnected pieces, determined, gusty                           |                            |                       |
| finicky       | 苛求的，过分讲究的        | dainty, fastidious, finicking, too particular or exacting, fussy | fine                       |                       |
| touchy        | 敏感的，易怒的          |                                                                  | touch                      |                       |
| agoraphobic   | 患荒野恐惧症的（人）       |                                                                  |                            |                       |
| sedate        | 镇静的              | sober, staid                                                     |                            |                       |
| cursory       | 粗略的；草率的          |                                                                  |                            |                       |
| 2              | 2                     | 2                                                                     | 2                      | 2                          |
| demotic        | 民众的；通俗的               |                                                                       |                        |                            |
| scruple        | 顾忌，迟疑                 | boggle, stickle, an ethical consideration, hesitate                   |                        |                            |
| resilient      | 有弹性的；能恢复活力的，适应力强的     |                                                                       |                        |                            |
| circumlocution | 迂回累赘的陈述               | roundabout, lengthy way of expressing sth.                            | circum 绕圈圈             |                            |
| hereditary     | 祖传的，世袭的；遗传的           |                                                                       | heredi                 |                            |
| sparing        | 节俭的                   | frugal, thrifty                                                       | spare v.               |                            |
| remunerative   | 报酬高的，有利润的             | gainful, lucrative                                                    |                        |                            |
| wavy           | 波状的，多浪的；波动起伏的         |                                                                       | wave n.                |                            |
| fluffy         | 有绒毛的；无聊的，琐碎的          |                                                                       | fluffy n.              |                            |
| profligacy     | 放荡；肆意挥霍               |                                                                       | profligate v.          |                            |
| reassure       | 使恢复信心；使确信             |                                                                       | re + assure            |                            |
| hardheaded     | （尤指做生意使）讲究实际的，冷静的，精明的 | shrewd and unsentimental, practical                                   |                        |                            |
| deprive        | 剥夺，使丧失                | denude, dismantle, divest, strip                                      | deprave 使堕落；deplore 痛惜 |                            |
| allusion       | 暗指，间接提到               |                                                                       | allude v.              |                            |
| ingratiating   | 讨人喜欢的，迷人的；讨好的，谄媚的     | capable of winning favor, calculated to please or win favor           |                        |                            |
| deciduous      | 非永久的；短暂的；脱落的；落叶的      |                                                                       | de + ciduous           |                            |
| admonish       | 训诫；警告                 | to warn, advise, to reprove mildly                                    |                        |                            |
| disabuse       | 打消（某人的）错误念头，使醒悟       | undeceive                                                             | dis + abuse            |                            |
| deferential    | 顺从的，恭顺的               | duteous, dutiful                                                      | deference n.           |                            |
| denigrate      | 污蔑，诽谤                 | defame, blacken, to disparage the character or reputation of          |                        |                            |
| reminiscent    | 回忆的；使人联想的             | tending to remind                                                     | reminiscence n.        | reminiscent of sb. or sth. |
| bumper         | 特大的；保险杠               | unusually large, a device for absorbing shock or preventing damage    |                        |                            |
| illusory       | 虚幻的                   | chimerical, fanciful, fictive, imaginary, deceptive, unreal, illusive | illusion n.            |                            |
| grind          | 磨碎，碾碎；苦差事             | crush into bits or fine particles, long, difficult, tedious task      | grand 宏大               |                            |
| consult        | 请教，咨询，商量              |                                                                       | insult 侮辱              |                            |
| transmute      | 变化                    | change or alter                                                       |                        |                            |
| prophesy       | 预言                    | adumbrate, augur, portend, presage, prognosticate                     |                        |                            |
| toady          | 谄媚者，马屁精               | bootlicker, brownnoser, cringer, fawner, one who flatters             | toad 癞蛤蟆               |                            |
| accomplice     | 共犯，同谋                 | associate, partner in a crime                                         | acco + police          |                            |
| defer          | 遵从，听从；延期              | yield with courtesy, delay                                            |                        |                            |
| drollery       | 滑稽                    | quaint or why humor                                                   | droll 蠢蠢欲动；滑稽的         |                            |
| stint          | 节制，限量，节省              | to restrict                                                           | stint on sth. 吝惜…      |                            |
| perspicuous    | 明晰的，明了的               | clearly expressed or presented                                        |                        |                            |
| proprietary    | 私有的                   | privately owned and managed                                           |                        |                            |
| requisite      | 必需物；必要的，必不可少的         | necessary, required                                                   | require + site         |                            |
| deign          | 惠允（做某事）；施惠于人          | stoop, to condescend to do sth.                                       |                        |                            |
| extant         | 现存的，现有的               | currently or actually existing                                        |                        |                            |
| provision      | 供应；（法律等）条款            | a stock of needed materials or supplies, stipulation                  |                        |                            |
| incorporate    | 合并，并入                 | assimilate, imbibe, inhaust, insorb, integrate, embody                |                        | incorporate sth. into sth. |
| ingenious      | 聪明的；善于创造发明的；心灵手巧的     | clever, inventive                                                     |                        |                            |
| imponderable   | （重量等）无法衡量的            | impalpable, imperceptible, indiscernible, insensible, unmeasurable    |                        |                            |
| intricate      | 错综复杂的；难懂的             | complicated, knotty, labyrinthine, sophisticated, elaborate, complex  |                        |                            |
| stratify       | （使）层化                 | to divide or arrange into classes, castes, or social strata           |                        |                            |
| dismal         | 沮丧的，阴沉的               | showing sadness                                                       | dies mail “不吉利的日子”     |                            |
| exterminate    | 消灭，灭绝                 | abolish, annihilate, extirpate, to wipe out, eradicate                | terminal               |                            |
| cordiality     | 诚恳，热诚                 | heartiness, geniality, warmth, sincere affection and kindness         |                        |                            |
| engender       | 产生，引起                 | cause, generate, induce, provoke, to produce, beget                   |                        |                            |
| obscurity      | 费解；不出名                | the quality of being obscure                                          | obscure adj.           |                            |
| 3                | 3                   | 3                                                                                     | 3                    | 3                   |
| undiscovered     | 未被发现的               | unexplored                                                                            |                      |                     |
| anecdotal        | 轶事的，趣闻的             |                                                                                       | anecdote n.          |                     |
| attest           | 证明，证实               | certify, testify, witness                                                             |                      | attest to sth.      |
| termite          | 白蚁                  |                                                                                       |                      |                     |
| trickle          | 细流；细细的流             | dribble, drip, filter                                                                 |                      |                     |
| anticipatory     | 预想的，预期的             |                                                                                       | anticipate v.        |                     |
| incontrovertible | 无可辩驳的；不容置疑的         | incapable of being disputed                                                           | in + controvertible  |                     |
| pompous          | 自大的                 | arrogant                                                                              |                      |                     |
| enamored         | 倾心的，被迷住             | bewitched, captivated, charmed, enchanted, infatuated, inflamed with love, fascinated |                      | be enamored of sth. |
| repertoire       | （剧团等）常备剧目           |                                                                                       |                      |                     |
| inure            | 使习惯于；生效             |                                                                                       |                      |                     |
| rigorous         | 严格的，严峻的             | austere, rigid, severe, stern, manifesting, exercising, favoring rigor                |                      |                     |
| diverse          | 不同的；多样的             | disparate, distant, divergent, different, dissimilar, diversified                     |                      |                     |
| rehabilitate     | 使恢复（健康、能力、地位等）      |                                                                                       | re + habilitate      |                     |
| entrenched       | （权力、传统）确立的，牢固的      | strongly established                                                                  |                      |                     |
| peripheral       | 周边的，外围的             | of a periphery or surface part                                                        | periphery n.         |                     |
| discrete         | 个别的，分离的；离散的         | different, diverse, several, various, individual, separate, distinct, discontinuous   |                      |                     |
| anaerobic        | 厌氧的；厌氧微生物           |                                                                                       |                      |                     |
| indigent         | 贫穷的，贫困的             | deficient, improverished                                                              |                      |                     |
| asunder          | 分离的；化为碎片的           | apart or separate, into pieces                                                        |                      |                     |
| mournful         | 悲伤的                 | doleful, dolorous, plaintive, woeful                                                  |                      |                     |
| bask             | 晒太阳；取暖              |                                                                                       |                      |                     |
| distasteful      | （令人）不愉快的；讨厌的        | unpleasant, disagreeable                                                              |                      |                     |
| incidental       | 作为自然结果的；伴随而来的；偶然发生的 |                                                                                       |                      |                     |
| officious        | 过于殷勤的；多管闲事的；非官方的    | meddlesome, informal, unofficial                                                      |                      |                     |
| vicissitude      | 变迁，兴衰               | natural change or mutation visible in nature or in human affairs                      |                      |                     |
| grandeur         | 壮丽，宏伟               | splendor, magnificence                                                                | grand adj.           |                     |
| restrict         | 限制；约束               | circumscribe, confine, delimit, prelimit                                              | re + strict          |                     |
| withhold         | 抑制；扣留，保留            | bridle, constrain, inhibit, restrain                                                  | with + hold          |                     |
| repeal           | 废除（法律）              |                                                                                       | re + peal            |                     |
| relegate         | 使降级，贬谪；交付，托付        | to send to exile, assign to oblivion, to refer or assign for decision or action       |                      |                     |
| undifferentiated | 无差别的，一致的            | uniform                                                                               |                      |                     |
| fallow           | 休耕的                 |                                                                                       |                      |                     |
| stagger          | 蹒跚，摇晃               | lurch, reel, sway, waver, wobble, move on unsteadily                                  |                      |                     |
| charter          | 特权或豁免权              | special privilege or immunity                                                         |                      |                     |
| debate           | 正式的辩论，讨论；辩论         | formal argument                                                                       |                      |                     |
| quagmire         | 沼泽地；困境              | dilemma, plight, scrape                                                               | mire 泥潭              |                     |
| preclude         | 预防，排除；阻止            | deter, forestall, forfend, obviate, ward, prevent, rule out, exclude                  |                      |                     |
| frisky           | 活泼的，快活的             | impish, mischievous, sportive, waggish, playful, merry, frolicsome                    |                      |                     |
| accessible       | 易接近的；易受影响的          | employable, operative, parcticable, unrestricted, usable                              |                      | accessible to sth.  |
| generalize       | 概括，归纳               |                                                                                       | general              |                     |
| aromatic         | 芬芳的，芳香的             |                                                                                       | aroma 芳香             |                     |
| composure        | 镇静，沉着；自若            | calmness, coolness, phlegm, sangfroid, self-possession, tranquility, equanimity       |                      |                     |
| overblown        | 盛期已过的；残败的；夸张的       | past the prime or bloom, inflated                                                     |                      |                     |
| wring            | 绞，拧，扭               | contort, deform, distort, to squeeze or twist                                         |                      |                     |
| pristine         | 原始的；质朴的，纯洁的；新鲜的，干净的 | prime；prestige 威望                                                                     |                      |                     |
| voyeur           | 窥淫癖者                |                                                                                       |                      |                     |
| impecunious      | 不名一文的；没钱的           | having very little or no money                                                        |                      |                     |
| consign          | 托运，托人看管             | con + sign                                                                            |                      |                     |
| seductive        | 诱人的                 | attractive, captivating, tending to seduce                                            | seduce v.            |                     |
| conspiracy       | 共谋，阴谋               |                                                                                       | conspire v.          | conspiracy against  |
| unprecedented    | 前所未有的               |                                                                                       | un + precedent + ed  |                     |
| epitome          | 典型；梗概               | showing all the typical qualities of sth., abstract, summary, abridgment              |                      |                     |
| confine          | 限制，禁闭               | circumscribe, delimit                                                                 | con + fine           |                     |
| disparage        | 贬低，轻蔑               | belittle, derogate, to speak slightingly of, depreciate, decry                        |                      |                     |
| pitiful          | 值得同情的，可怜的           | deserving pity                                                                        |                      |                     |
| obsolescent      | 逐渐荒废的               | in the process of becoming obsolete                                                   | obsolescence         |                     |
| referent         | 指示对象                |                                                                                       |                      |                     |
| dissent          | 异议；不同意，持异议          | difference of opinion, to differ in belief or opinion, disagree                       | dissident 唱反调者       |                     |
| tributary        | 支流，进贡国；支流的，辅助的，进贡的  | making additions or yielding supplies, contributory                                   |                      |                     |
| tawdry           | 华而不实的，俗丽的           | cheap but showy                                                                       |                      | tawdry clothing     |
| welter           | 混乱，杂乱无章             | a disordered mixture                                                                  | melter 熔炉            |                     |
| opacity          | 不透明性；晦涩             | the quality of being opaque, obscurity of sense                                       |                      |                     |
| abet             | 教唆；鼓励，帮助            | to incite, encourage, urge and help on                                                |                      |                     |
| 4            | 4                  | 4                                                                           | 4                    | 4                    |
| surly        | 脾气暴躁的；阴沉的          | bad tempered, sullen                                                        |                      |                      |
| shed         | 流出（眼泪等）；脱落，蜕，落     |                                                                             |                      | shed tears           |
| briny        | 盐水的，咸的             | of, or relating to, or resembling brine or the sea, salty                   | brine 盐水             |                      |
| calamity     | 大灾祸，不幸之事           | an extreme misfortune                                                       |                      |                      |
| incarnate    | 具有人体的；化身的，拟人化的     | given in a bodily form, personified                                         |                      |                      |
| slovenly     | 不整洁的，邋遢的           | dirty, untidy especially in personal appearance                             |                      |                      |
| sluggish     | 缓慢的，行动迟缓的；反应慢的     | markedly slow in movement, flow, or growth, slow to respond, torpid         |                      |                      |
| retract      | 撤回，取消；缩回，拉回        | to take back or withdraw, to draw or pull back                              | re + tract           |                      |
| obliging     | 乐于助人的              | helpful, accommodating                                                      | oblige v. 施恩惠于，帮助    |                      |
| autograph    | 亲笔稿；手迹；在…上亲笔签名；亲笔的 |                                                                             |                      |                      |
| appease      | 使平静，安抚             | assuage, conciliate, mollify, pacify, propitiate                            | ap + pease           |                      |
| choppy       | 波浪起伏的；（风）不断改变方向的   | rough with small waves, changeable, variable                                |                      |                      |
| boredom      | 厌烦；令人厌烦的事物         |                                                                             | boring adj.          |                      |
| redundant    | 累赘的，多余的            | diffuse, prolix, verbose, wordly, superfluous                               |                      |                      |
| rhetoric     | 修辞，修辞学；浮夸的言辞       | insincere or grandiloquent language                                         |                      |                      |
| commitment   | 承诺，许诺              |                                                                             |                      |                      |
| determinant  | 决定因素；决定性的          | decisive                                                                    | determine v.         |                      |
| plaintive    | 悲伤的，哀伤的            | expressive of woe, melancholy                                               | plaint n. 哀诉         |                      |
| impressed    | 被打动的，被感动的          |                                                                             |                      | be impressed by sth. |
| arable       | 适于耕种的              | suitable for plowing and planting                                           |                      | arable field         |
| perpetrate   | 犯罪；负责              | to commit, to be responsible for                                            |                      |                      |
| peerless     | 出类拔萃的，无可匹敌的        | matchless, incomparable                                                     |                      |                      |
| proofread    | 校正，校对              | to read and mark corrections                                                |                      |                      |
| impound      | 限制；依法没收，扣押         | to confine, to seize and hold in the custody of the law, take possession of |                      |                      |
| forested     | 树木丛生的              |                                                                             | forest n.            |                      |
| succor       | 救助，援助              | to go to the aid of                                                         |                      |                      |
| cavil        | 调毛病；吹毛求疵           | quibble                                                                     |                      | cavil at sth.        |
| adept        | 熟练的，擅长的            | highly skilled, experted                                                    |                      | be adept at sth.     |
| torpor       | 死气沉沉               | dullness, languor, lassitude, lethargy, extreme sluggishness of function    |                      |                      |
| superannuate | 使退休领养老金            | to retire and pension because of age or infirmity                           |                      |                      |
| artisan      | 技工                 | skilled workman or craftsman                                                |                      |                      |
| aberrant     | 越轨的；异常的            | turning away from what is right, deviating from what is normal              |                      |                      |
| piercing     | 冷的刺骨的；敏锐的          | shrill, penetratingly cold, perceptive                                      |                      |                      |
| rave         | 热切赞扬；胡言乱语，说疯话      |                                                                             |                      |                      |
| hazardous    | 危险的，冒险的            | marked by danger, perilous, risky                                           |                      |                      |
| divert       | 转移；（使）转向；使娱乐       |                                                                             |                      |                      |
| procurement  | 取得，获得；采购           |                                                                             | procure v.           |                      |
| arduous      | 费力的，艰难的            | marked by great labor or effort                                             |                      | arduous march        |
| idyllic      | 田园诗的               |                                                                             | idyll 田园诗            | an idyllic vacation  |
| sinuous      | 蜿蜒的，迂回的            | characterized by many curves and twists, winding                            |                      |                      |
| exasperate   | 激怒，使恼怒             | to make angry, vex, huff, irritate, peeve, pique, rile, roil                |                      |                      |
| archetypally | 典型的                |                                                                             | archetype            |                      |
| domesticated | 驯养的，家养的            |                                                                             | dome + sticated      |                      |
| graft        | 嫁接，结合；贪污           |                                                                             | go + raft 木筏         |                      |
| castigate    | 惩治；严责              | chasten, chastise, discipline, to punish or rebuke severely                 |                      |                      |
| relieved     | 宽慰的，如释重负的          |                                                                             |                      |                      |
| custodian    | 管理员，监护人            |                                                                             | custody 监护权          |                      |
| portend      | 预兆，预示              | to give an omen                                                             |                      |                      |
| precedent    | 在先的；在前的；先例，判例      |                                                                             |                      |                      |
| diffident    | 缺乏自信的              | bashful, coy, demure, modest, self-effacing                                 | confident            |                      |
| rustic       | 乡村的，乡土气的           | of or relating to, or suitable for the country                              |                      |                      |
| veracity     | 真实，诚实              | devotion to the truth, truthfulness                                         |                      |                      |
| topple       | 倾覆，推倒              | to overthrow                                                                |                      |                      |
| perishable   | 易腐烂的（东西），易变质的      | likely to decay or go bad quickly                                           |                      |                      |
| dismiss      | 解散；解雇              |                                                                             | dis + miss           |                      |
| cessation    | 中止；（短暂的）停止         | a short pause or a stop                                                     |                      |                      |
| contagious   | 传染的；有感染力的          | easily passed from person to person, communicable                           |                      |                      |
| prodigious   | 巨大的；惊人的，奇异的        | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童           |                      |
| abstruse     | 难懂的，深奥的            | hard to understand, recondite                                               |                      |                      |
| oval         | 卵形的，椭圆形的           | having the shape of an egg                                                  |                      |                      |
| 5            | 5                    | 5                                                                                   | 5                    | 5     |
| sectarianism | 宗派主义；教派意识            |                                                                                     |                      |       |
| gallant      | 勇敢的，英勇的；对（女人）献殷勤的    | brave and noble, polite and attentive to women                                      |                      |       |
| disarray     | 混乱；无秩序               | untidy condition, disorder, confusion                                               | dis + array          |       |
| constraint   | 强制，强迫；对感情的压抑         | sth. that limits sb.'s freedom of action or feelings                                |                      |       |
| chromatic    | 彩色的，五彩的              | having color or colors                                                              |                      |       |
| vigilant     | 机警的，警惕的              | alertly watchful to avoid danger                                                    |                      |       |
| replete      | 充满的，供应充足的            | fully or abundantly provided or filled                                              |                      |       |
| precursor    | 先驱，先兆                | forerunner, harbinger, herald, outrider                                             |                      |       |
| hoard        | 贮藏，秘藏                |                                                                                     | hoard behind board   |       |
| motley       | 混杂的；多色的，杂色的          | heterogeneous, of many colors                                                       |                      |       |
| scorn        | 轻蔑，瞧不起               | contemn, despise                                                                    |                      |       |
| reigning     | 统治的，起支配作用的           |                                                                                     |                      |       |
| antecedent   | 前事；祖先，先行的            |                                                                                     |                      |       |
| hearten      | 鼓励，激励                | to make sb. feel cheerful and encouraged                                            |                      |       |
| expository   | 说明的                  | explanatory, serving to explain                                                     | exposit v.           |       |
| treacherous  | 背叛的；叛逆的              | showing great disloyalty and deceit                                                 |                      |       |
| injurious    | 有害的                  | harmful                                                                             | injury n.            |       |
| placid       | 安静的，平和的              | serenely free of interruption                                                       |                      |       |
| tussle       | 扭打，争斗；争辩             | physical contest or struggle, an intense argument. Controversy, to struggle roughly |                      |       |
| dappled      | 有斑点的，斑驳的             | covered with spots of a different color                                             |                      |       |
| adversary    | 对手，敌手；对手的，敌手的        |                                                                                     |                      |       |
| realm        | 王国；领域，范围             |                                                                                     |                      |       |
| obtainable   | 能得到的                 |                                                                                     |                      |       |
| evince       | 表明，表示                | indicate, make manifest, show plainly                                               |                      |       |
| insidious    | 暗中危害的；阴险的            | working or spreading harmfully in a subtle or stealthy manner                       |                      |       |
| connotation  | 言外之意；内涵              | idea or notion suggested in addition to its explicit meaning or denotation          |                      |       |
| gripping     | 吸引人注意的；扣人心弦的         | holding the interest strongly                                                       |                      |       |
| stark        | 光秃秃的；荒凉的；（外表）僵硬的；完全的 | barren, desolate, rigid as if in death, utter, sheer                                |                      |       |
| deterrent    | 威慑的，制止的              | serving to deter                                                                    | deter v.             |       |
| effete       | 无生产力的；虚弱的            | spent and sterile, lacking vigor                                                    |                      |       |
| improvident  | 无远见的，不节俭的            | lacking foresight or thrift                                                         |                      |       |
| streak       | 条纹；加线条               |                                                                                     |                      |       |
| feckless     | 无效的，效率低的；不负责任的       | inefficient, irresponsible                                                          |                      |       |
| exotic       | 异国的，外来的；奇异的          | not native, foreign, strikingly unusual                                             |                      |       |
| inducible    | 可诱导的                 |                                                                                     | induce v.            |       |
| distortion   | 扭曲，曲解                |                                                                                     |                      |       |
| forlorn      | 孤独的；凄凉的              | abandoned or deserted, wretched, miserable                                          |                      |       |
| trepidation  | 恐惧，惶恐                | timorousness, uncertainty, agitation                                                |                      |       |
| verse        | 诗歌                   | a line of metrical writing, poems                                                   |                      |       |
| verse        | 诗歌                   | a line of metrical writing, poems                                                   |                      |       |
| indulge      | 放纵，纵容；满足             | cosset, pamper, spoil                                                               |                      |       |
| brook        | 小河                   | a small stream                                                                      |                      |       |
| propensity   | 嗜好，习性                | bent, leaning, tendency, penchant, proclivity                                       |                      |       |
| hiatus       | 空隙，裂缝                | a gap or interruption in space, time, or continuity, break                          |                      |       |
| elicit       | 得出，引出                | to draw forth or bring out                                                          |                      |       |
| exempt       | 被免除的；被豁免的；免除，豁免      |                                                                                     |                      |       |
| discomfited  | 困惑的，尴尬的              | frustrated, embarrassed                                                             |                      |       |
| 6              | 6                  | 6                                                                                       | 6                    | 6                        |
| unspotted      | 清白的，无污点的           | without spot, flawless                                                                  |                      |                          |
| theoretical    | 假设的；理论的            |                                                                                         | theory n.            |                          |
| reprisal       | 报复；报复行动            | practice in retaliation                                                                 |                      |                          |
| commission     | 委托；佣金              |                                                                                         | com + mission        |                          |
| surrender      | 投降；放弃；归还           | give in, give up, give back                                                             | surreptitious 鬼鬼祟祟的  |                          |
| shrug          | 耸肩，表示怀疑等           |                                                                                         |                      | shrug sth. off/aside     |
| compassionate  | 有同情心的              | sympathetic                                                                             |                      | compassionate nature     |
| inordinate     | 过度的；过分的            | immoderate, excessive                                                                   |                      |                          |
| treatise       | 论文                 | a long writing work dealing systemctically with one subject                             |                      |                          |
| gravitational  | 万有引力的，重力的          |                                                                                         |                      |                          |
| vainglorious   | 自负的                |                                                                                         |                      |                          |
| flatter        | 恭维，奉承              | adulate, blandish, honey, slaver                                                        |                      |                          |
| submission     | 从属，服从              |                                                                                         | sub + mission        |                          |
| far-reaching   | 影响深远的              | having a wide influence                                                                 |                      |                          |
| bewilder       | 使迷惑，混乱             | to confuse, befog, confound, perplex                                                    |                      |                          |
| ineptitude     | 无能；不适当             | the quality or state of being inept                                                     | inept adj.           |                          |
| touched        | 被感动的               | affected, inspired, moved, emotionally stirred                                          |                      |                          |
| scour          | 冲刷；擦掉；四处搜索         | to clear, dig, remove, rub, to range over in a search                                   |                      |                          |
| molecular      | 分子的                |                                                                                         | molecule n.          |                          |
| perfunctory    | 例行公事般的；敷衍的         | characterized by routine or superficiality                                              |                      |                          |
| spatial        | 有关空间的，在空间的         |                                                                                         | space n.             |                          |
| stride         | 大步行走               | long steps                                                                              |                      |                          |
| ephemeral      | 朝生暮死的；生命短暂的        | evanescent, fleeting, fugacious, momentary                                              |                      |                          |
| vehement       | 猛烈的，热烈的            | exquisite, fierce, furious, intense, violent, marked by forceful energy                 |                      |                          |
| promulgate     | 颁布（法令）；宣传，传播       | announce, annunciate, declear, disseminate, proclaim                                    |                      |                          |
| interpretation | 解释，说明；演绎           |                                                                                         |                      |                          |
| tardy          | 延迟；迟到的；缓慢的         | sluggish                                                                                |                      |                          |
| transitional   | 转变的，变迁的            |                                                                                         | transition           |                          |
| exodus         | 大批离去，成群外出          | a mass departure or emigration                                                          |                      |                          |
| infuriate      | 使恼怒；激怒             | to enrage                                                                               |                      |                          |
| unleash        | 发泄，释放              | set feelings or forces free from control                                                |                      |                          |
| clannish       | 排他的，门户之见的          |                                                                                         | clan 氏族              |                          |
| suppliant      | 恳求的，哀求的；恳求者，哀求者    |                                                                                         |                      |                          |
| bygone         | 过去的                | gone by                                                                                 |                      | a bygone age             |
| allusive       | 暗指的，间接提到的          | containing allusions                                                                    | allude v.            |                          |
| awe-struck     | 充满敬畏的              | filled with awe                                                                         |                      |                          |
| egocentric     | 利己的                | self-centered                                                                           |                      |                          |
| unwonted       | 不寻常的，不习惯的          | unusual, unaccustomed                                                                   |                      |                          |
| scavenge       | 清除，从废物中提取有用物质      | to cleanse, to salvage from discarded or refuse material                                |                      |                          |
| aristocratic   | 贵族（化）的             |                                                                                         |                      |                          |
| rebuke         | 指责，谴责              | admonish, chide, reproach                                                               |                      |                          |
| insurgent      | 叛乱的，起义的；叛乱分子       | rebellious, a person engaged in insurgent activity                                      | in + surge 激增，浪涌     |                          |
| septic         | 腐败的；脓毒性的           |                                                                                         | sepsis 脓毒症           |                          |
| warrant        | 正当理由；许可证；保证，批准     | certify, guarantee, vindicate, justification, a commission or document giving authority |                      |                          |
| enrage         | 激怒，触怒              |                                                                                         | rage 狂怒              |                          |
| thorny         | 有刺的，多刺的；多障碍的，引起争议的 |                                                                                         | thorn 刺 n.           |                          |
| exuberant      | （人）充满活力的；（植物）茂盛的   | lively and cheerful                                                                     |                      |                          |
| spinning       | 旋转的                | revolving, rotating, turning, twirling, wheeling, whirling                              |                      |                          |
| incense        | 香味，激怒              | enrage, infuriate, madden                                                               |                      |                          |
| distent        | 膨胀的；扩张的            | swollen, expanded                                                                       |                      |                          |
| complementary  | 互补的                | combining well to form a whole                                                          | complement n. 补充物    | be complementary to sth. |
| quantifiable   | 可以计量的；可量化的         |                                                                                         | quant 数量             |                          |
| rancor         | 深仇；怨恨              | bitter deep-seated ill will, enmity                                                     |                      |                          |
| pounce         | （猛禽的）爪；猛扑；突然袭击     |                                                                                         |                      | pounce on 突然袭击           |
| opponent       | 对手，敌手              | adversary, antagonist                                                                   |                      |                          |
| doctrinaire    | 教条主义者；教条的，迂腐的      | authoritarian, authoritative, dictatorial, dogmatic                                     | doctrine 教条          |                          |
| flagrant       | 罪恶昭著的；公然的          | conspicuously offensive                                                                 |                      |                          |
| hypocrisy      | 伪善；虚伪              | cant, pecksniff, sanctimony, sham                                                       |                      |                          |
| brassy         | 厚脸皮的；无礼的           | brazen, insolent                                                                        |                      |                          |
| abolition      | 废除，废止              | prohibition                                                                             | abolish v.           |                          |
| impassive      | 无动于衷的，冷漠的          | stolid, phlegmatic                                                                      |                      |                          |
| accede         | 同意                 | to give assent, consent                                                                 |                      | accede to 答应             |
| timeliness     | 及时，适时              |                                                                                         | timely               |                          |
| optimum        | 最有利的，最理想的          | most favorable or desirable                                                             |                      |                          |
| nonconformist  | 不墨守成规的（人）          |                                                                                         | conform 顺应           |                          |
| ethnic         | 民族的，种族的            | of a national, racial or tribal group that has a common culture tradition               |                      |                          |
| repugnant      | 令人厌恶的              | strong distaste or aversion                                                             |                      | be repugnant to sb.      |
| accuse         | 谴责，指责              | blame, arraign, charge, criminate, incriminate, inculpate                               |                      |                          |
| epidemic       | 传染性的；流行性的          |                                                                                         |                      |                          |
| tread          | 踏，践踏；行走；步态；车轮胎面    | step                                                                                    |                      |                          |
| cosmic         | 宇宙的                |                                                                                         | cosmos 宇宙            |                          |
| subservient    | 次要的，从属的；恭顺的        | useful in a inferior capacity, obsequiously submissive                                  |                      |                          |
| solvent        | 有偿债能力的；溶剂          | capable of meeting financial obligations                                                | solve v.             |                          |
| refrigerate    | 使冷却，冷藏             |                                                                                         |                      |                          |
| abstain        | 禁绝，放弃              | ab + stain                                                                              |                      |                          |
| 7                | 7                 | 7                                                                        | 7                    | 7                       |
| sporadic         | 不定时发生的            | occurring occasionally                                                   |                      |                         |
| impart           | 传授，赋予；传递；告知，透露    | bestow, transmit, to make known                                          |                      |                         |
| acoustic         | 听觉的；声音的           |                                                                          |                      |                         |
| monochromatic    | 单色的               | having only one color                                                    |                      |                         |
| ultimatum        | 最后通碟              | a final proposition, condition, or demand                                | ulti + matum         |                         |
| diverge          | 分歧，分开             | deviate                                                                  |                      |                         |
| escalate         | （战争等）升级；扩大，上升，攀升  | to make a conflict more serious, to grow or increase rapidly             |                      |                         |
| interrogation    | 审问，质问；疑问句         | inquiry, query, question                                                 |                      |                         |
|                  |                   |                                                                          |                      |                         |
| advertise        | 做广告；通知            |                                                                          |                      |                         |
| stringent        | （规定）严格的；苛刻的；缺钱的   | severe, rigorous, draconian, financial strain                            |                      |                         |
| idiomatic        | 符合语言习惯的；惯用的       |                                                                          | idiom 习惯用语           |                         |
| hypochondriac    | 忧郁症患者；忧郁症的        |                                                                          | hypochondria 忧郁症     |                         |
| inconclusive     | 非决定性的；无定论的        | leading to no conclusion or definite result                              | in + conclusive      |                         |
| proclamation     | 宣布，公布             | an official public statement                                             |                      |                         |
| giddy            | 轻浮的；轻率的           | not serious, flighty, frothy, frivolous                                  |                      |                         |
| disingenuous     | 不坦率的              | lacking in candor                                                        | dis + ingenuous      |                         |
| iconographic     | 肖像的；肖像学的；图解的      | representing something by pictures or diagrams                           |                      |                         |
| critical         | 挑毛病的；关键的，危急的      |                                                                          |                      | be critical of sth.     |
| frivolous        | 轻薄的，轻佻的           | marked by unbecoming levity                                              |                      |                         |
| didactic         | 教诲的；说教的           | morally instructive, boringly pedantic or moralistic                     |                      |                         |
| equivalent       | 相等的，等值的           | equal                                                                    |                      | be equivalent to sth.   |
| celibate         | 独身者；不结婚的          | an unmarried person                                                      |                      |                         |
| breed            | 繁殖；教养；品种，种类       | to produce offspring by hatching or gestation, to bring up               |                      |                         |
| offstage         | 台后的，幕后的           |                                                                          | off + stage          |                         |
| voluble          | 健谈的；易旋转的          | talkative, rotating                                                      |                      |                         |
| reprimand        | 训诫，谴责             | a severe or official reproof                                             |                      |                         |
| grope            | 摸索，探索             | to feel or search about blindly                                          | gouge 挖出，骗钱；半圆凿      |                         |
| bluff            | 虚张声势；悬崖峭壁         | pretense of strength, high cliff                                         |                      |                         |
| pantomime        | 哑剧                |                                                                          |                      |                         |
| soporific        | 催眠的；安眠药；麻醉剂       |                                                                          |                      |                         |
| propriety        | 礼节；适当；得体          | decorum, appropriateness                                                 |                      |                         |
| flimsy           | 轻而薄的；易损坏的         | easily broken or damaged                                                 | flim 胶卷              |                         |
| precise          | 精确的；准确的           |                                                                          |                      |                         |
| akin             | 同族的；类似的           | related by blood, essentially similar, related, or compatible            |                      |                         |
| assorted         | 各式各样的；混杂的         | consisting of various kinds, mixed                                       |                      |                         |
| incompetent      | 无能力的；不胜任的         |                                                                          | in + competent       |                         |
| craft            | 行业，手艺             | occupation, especially one that needs skills                             |                      | arts and crafts         |
| halting          | 踌躇的；迟疑不决的         | marked by hesitation or uncertainty                                      | halt v.              |                         |
| substantive      | 根本的；独立存在的         | dealing with essentials, being in a totally independently entity         |                      |                         |
| foliage          | 叶子                | mass of leaves, leafage                                                  |                      |                         |
| corroborate      | 支持或证实；强化          | to bolster, make more certain, to strengthen                             |                      |                         |
| apathetic        | 无感情的；无兴趣的         |                                                                          |                      | be apathetic about ath. |
| unpromising      | 无前途的，没有希望的        | not promising                                                            |                      |                         |
| willful          | 任性的；故意的           | deliberate, knowing, wilful, preversely self-willed, intentional         |                      |                         |
| overcast         | 阴天的；阴暗的           | bleak, cloudy, gloomy, comber                                            |                      |                         |
| comperhensible   | 可以理解的；易于理解的       |                                                                          |                      |                         |
| subsist          | 生存下去；继续存在；维持生活    | to exist                                                                 |                      |                         |
| iniquitous       | 邪恶的；不公正的          | wicked, unjust                                                           |                      |                         |
| grieve           | 使某人极为悲伤的          | aggrieve, bemoan, deplore, distress, mourn, to cause great sorrow to sb. |                      | grieve over ath.        |
| prevail          | 战胜；流行，盛行          | conquer, master, predominate, triumph                                    |                      |                         |
| hallow           | 把…视为神圣；尊敬，敬畏      | to make or set apart as holy, to respect or honor greatly, revere        |                      |                         |
| adhesive         | 黏着的；带黏性的；黏合剂      | tending to adhere or cause adherence, an adhesive substance              |                      |                         |
| contrite         | 悔罪的；痛悔的           | feeling contrition, repentant                                            | contrition n.        |                         |
| indiscriminately | 随意的，任意的           | in a random manner, promiscuously, arbitrary                             |                      |                         |
| deteriorate      | （使）变坏；恶化          | degenerate, descend, languish, weaken                                    |                      |                         |
| conjure          | 召唤，想起；变魔术，变戏法     | to call or bring to mind, invoke, to practise magic or legerdemain       |                      | conjure for 召唤          |
| unimpressed      | 没有印象的             |                                                                          |                      |                         |
| trenchant        | 犀利的，尖锐的           | sharply perceptive, penetrating                                          |                      |                         |
| spineless        | 没有骨气的，懦弱的         | lacking courage or willpower                                             | spine 脊椎，刺           |                         |
| converse         | 谈话，交谈；逆向的；相反的事物   | to exchange thoughts and opinions in speech, opposite                    |                      |                         |
| down             | 下；绒毛，羽毛           |                                                                          |                      |                         |
| fleeting         | 短暂的；飞逝的           | transient, passing swiftly                                               | fleet v.             |                         |
| shopworn         | 在商店里陈列很久的；陈旧的；磨损的 |                                                                          |                      |                         |
| guilt            | 罪行；内疚             | crime, sin, a painful feeling of self-reproach                           |                      | be free of guilt        |
| vertebrate       | 脊椎动物（的）           | an animal that has a spine                                               | vertebra 脊椎骨         |                         |
| derogatory       | 不敬的；贬损的           | disparaging, belittling                                                  |                      |                         |
| demur            | 表示抗议，反对           |                                                                          | de + mur             |                         |
| solitary         | 孤独的；隐士            | without companions, recluse, forsaken, lonesome, lorn                    | solit 孤独             |                         |
| adumbrate        | 预示                | to foreshadow in a vague way                                             |                      |                         |
| 8              | 8                    | 8                                                                         | 8                    | 8                               |
| gratuitous     | 无缘无故的；免费的            | without cause or justification, free                                      | gratuity 小费          |                                 |
| histrionic     | 做作的；戏剧的              | theatrical, dramatic                                                      | histrion 演员，组织者      |                                 |
| anachronistic  | 时代错误的                | an error in chronology                                                    | chron 时间；计时          |                                 |
| solicitous     | 热切的，挂念的              | full of desire, eager, expressing care or concern                         |                      | be solicitous of/for/about sth. |
| dehumanize     | 使失掉人性                | to deprive of human qualities, personality, or spirit                     | de humanize          |                                 |
| disseminate    | 传播，宣传                | to spread abroad, promulgate widely                                       |                      |                                 |
| imperious      | 傲慢的；专横的              | overbearing, arrogant, magisterial, peremptory                            |                      | imperious manner                |
| patronize      | 屈尊俯就，光顾，惠顾           |                                                                           | patron               |                                 |
| volatile       | 反复无常的；易挥发的           | fickle, inconstant, mercuial, capricious                                  |                      |                                 |
| illiterate     | 文盲的                  | ignorant, uneducated                                                      | reiterate 重申，反复地说    |                                 |
| denounce       | 指责                   | to accuse publicly                                                        |                      |                                 |
| garble         | 曲解，篡改                | to alter or distort as to create a wrong impression or change the meaning |                      |                                 |
| allude         | 暗指，影射，间接提到           |                                                                           |                      |                                 |
| serendipity    | 偶然性；偶然的事物            |                                                                           |                      |                                 |
| venial         | （错误）轻微的，可原谅的         | forgivable, pardonable, excusable, remittable                             |                      |                                 |
| gravity        | 严肃，庄重；重力             | solemnity or sedateness, seriousness                                      |                      |                                 |
| elusive        | 难懂的，难以描述的；不易被抓获的     |                                                                           |                      |                                 |
| flout          | 蔑视，违抗                | fleer, gibe, jeer, jest, sneer                                            |                      |                                 |
| cavity         | （牙齿等的）洞，腔            | a hollow place in a tooth                                                 | carve                |                                 |
| unscented      | 无气味的                 | without scent                                                             |                      |                                 |
| quixotic       | 不切实际的，空想的            | foolishly impractical                                                     |                      |                                 |
| warp           | 弯曲，变歪；歪斜             |                                                                           |                      |                                 |
| affinity       | 相互吸引；密切关系            |                                                                           |                      |                                 |
| fallible       | 易犯错误的                |                                                                           | fall + able          |                                 |
| substantiate   | 证实，确证                | embody, externalize, incarnate, materialize                               |                      |                                 |
| intrepid       | 勇敢的；刚毅的              | characterized by fearlessness and fortitude                               |                      |                                 |
| descend        | 下降；降格，屈尊             | decline, degenerate, deteriorate, sink                                    |                      |                                 |
| successively   | 接连的，继续的              | consecutively, continuously, uninterruptedly, in porper order or sequence |                      |                                 |
| glaze          | 上釉于，使光滑；釉            |                                                                           |                      |                                 |
| obfuscate      | 使困惑，使迷惑              | to muddle, confuse, bewilder                                              |                      |                                 |
| compensatory   | 补偿性的；报酬的             | compensating                                                              |                      |                                 |
| exquisite      | 精致的；近乎完美的            | elaborately made, delicate, consummate, perfected                         |                      |                                 |
| dilapidated    | 破旧的；毁坏的              | broken down                                                               |                      |                                 |
| concurrent     | 并发的；协作的，一致的          |                                                                           | con + current        |                                 |
| eventful       | 多事的；重要的              | momentous                                                                 | event + ful          |                                 |
| moratorium     | 延缓偿付；活动中止            |                                                                           |                      |                                 |
| aseptic        | 净化的；无菌的              | not septic                                                                | sepsis 脓毒症           |                                 |
| solder         | 焊接；焊在一起              |                                                                           | soldier              |                                 |
| impersonate    | 模仿；扮演                |                                                                           |                      |                                 |
| pottery        | 制陶（工艺）；陶器            |                                                                           |                      |                                 |
| avid           | 渴望的，热心的              |                                                                           |                      | be avid for sth.                |
| concentrate    | 聚集，浓缩                | compress, condense, converge, concenter                                   |                      |                                 |
| pseudonym      | 假名，笔名                |                                                                           | pseudo 伪装，假的         |                                 |
| intimidate     | 恐吓，胁迫                | to make timid                                                             |                      |                                 |
| competing      | 有竞争性的；不相上下的          |                                                                           | compete              |                                 |
| dissident      | 唱反调者                 | a person who disagrees, dissenter                                         |                      |                                 |
| apprehensive   | 害怕的；敏悟的              |                                                                           |                      |                                 |
| decorous       | 合宜的；高雅的              | marked by propriety and good state                                        |                      |                                 |
| stalk          | 隐伏跟踪（猎物）             | to pursue quarry or prey stealthily                                       |                      |                                 |
| nibble         | 一点点的咬，慢慢啃            |                                                                           | nipple 乳头            |                                 |
| transit        | 通过；改变；运输             | passage, change, transition, conveyance, to pass over or through          |                      |                                 |
| perfervid      | 过于热心的                | excessively fervent                                                       |                      |                                 |
| deter          | 威慑，吓住；阻止             | to discourage, inhibit                                                    |                      |                                 |
| mythic         | 神话的，虚构的              | legendary, mythical, mythological                                         |                      |                                 |
| contradict     | 反驳，驳斥                |                                                                           |                      |                                 |
| herald         | 宣布…的消息；预示…的来临；传令官，信使 |                                                                           |                      |                                 |
| sociable       | 好交际的；合群的；友善的         |                                                                           |                      |                                 |
| incommensurate | 不成比例的；不相称的           | not proportionate, not adequate                                           |                      |                                 |
| penal          | 惩罚的；刑罚的              |                                                                           | penalty n.           |                                 |
| sycophant      | 马屁精                  | a servile self-seeking flatterer                                          |                      |                                 |
| bowlegged      | 弯脚的；弓形腿的             |                                                                           | bow + legged         |                                 |
| rugged         | 高低不平的；崎岖的            | having a rough uneven surface, bumpy, craggy,                             |                      |                                 |
| affront        | 侮辱，冒犯                | to offend, confront, encounter                                            |                      |                                 |
| organism       | 有机体，生物               |                                                                           | organ 器官             |                                 |
| pragmatic      | 实际的；务实的；注重生效的；实用主义的  |                                                                           |                      |                                 |
| decline        | 拒绝，变弱，变小；消减          |                                                                           |                      |                                 |
| drowsy         | 昏昏欲睡的                | ready to fall asleep                                                      | drowse v. 打瞌睡        |                                 |
| speculate      | 沉思，思索；投机             | to meditate on or ponder                                                  |                      |                                 |
| 9                 | 9                           | 9                                                                          | 9                     | 9                              |
| notate            | 以符号表示                       | to put into notation                                                       | notation              |                                |
| contemplate       | 深思；凝视                       | excogitate, prepend, ponder, view, gaze, to think about intently           |                       | contemplate doing sth.         |
| apposite          | 适当的，贴切的                     | appropriate, apt, relevant                                                 |                       |                                |
| underplay         | 弱化…的重要性；表演不充分               | to make sth. less important, to underact                                   |                       |                                |
| innocuous         | （行为，言论等）无害的                 | harmless, innocent, innoxious, inoffensive, unoffensive                    |                       |                                |
| preservere        | 坚持不懈                        |                                                                            |                       |                                |
| humogenize        | 使均匀                         |                                                                            |                       |                                |
| stately           | 庄严的；宏伟的                     | august, dignified, majestic                                                |                       |                                |
| tenuous           | 纤细的；稀薄的；脆弱的，无力的             | not dense, rare, flimsy, weak                                              | onerous 繁重的，费力的       |                                |
| attenuation       | 变瘦，减小，减弱                    |                                                                            | attenuate v.          |                                |
| perquisite        | 额外收入；津贴；小费                  | cumshaw, lagniappe, largess                                                | per + quisite         |                                |
| haggle            | 讨价还价                        | bargain, dicker, wrangle                                                   |                       |                                |
| falter            | 蹒跚，支支吾吾地说                   | flounder, stagger, tumble, stammer                                         |                       |                                |
| affable           | 和蔼的；友善的                     | gentle, amiable, pleasant and easy to approach or talk to                  |                       |                                |
| flustered         | 慌张的，激动不安的                   | nervous and upset, perturbed, rattled                                      |                       |                                |
| adore             | 崇拜；爱慕                       | to worship as divine, to love greatly, revere                              |                       |                                |
| indigence         | 贫穷，贫困                       | poverty, destitution, impecuniousness, impoverishment, penury, privation   |                       |                                |
| composed          | 镇定的，沉着的；由…组成的               | tranquil, self-possessed                                                   |                       | be composed of sth.            |
| allure            | 引诱，诱惑                       | to entice by charm or attraction                                           | inure 使习惯于；生效         |                                |
| apparition        | 幽灵；特异景象                     | phantasm, phantom, revenant, specter                                       | appear + tion         |                                |
| xenophobic        | 恐外的；害怕外国人的                  | having abnormal fear or hatred of the strange or foreign                   |                       |                                |
| flip              | 用指轻弹；蹦蹦跳跳；无礼的，冒失的           |                                                                            |                       |                                |
| emulate           | 与…竞争，努力赶上                   | to strive to equal or excel                                                |                       |                                |
| collusion         | 共谋，勾结                       |                                                                            |                       | collusion with                 |
| panacea           | 万灵药                         | a remedy for all ills or difficulties                                      |                       |                                |
| smug              | 自满的，自命不凡的                   | highly self-satisfied                                                      |                       |                                |
| demystify         | 减少…神秘性                      | make sth, less mysterious                                                  | de + mystify          |                                |
| indistinguishable | 无法区分的                       |                                                                            | not + distinguishable | be indistinguishable from sth. |
| imbue             | 浸染；浸透；使充满，灌输，激发             | to permeate or influence as if by dyeing, to endow                         |                       |                                |
| astray            | 迷路的；误入歧途的                   | off the right path                                                         |                       |                                |
| cumbersome        | 笨重的，难处理的                    | clumsy                                                                     | cumber + some         |                                |
| overcrowd         | （使）过度拥挤                     |                                                                            |                       |                                |
| frantic           | 疯狂的；狂乱的                     | wild with anger, frenzied                                                  | fry + ant + tic       |                                |
| synergic          | 协同作用的                       | of combined action or coorperation                                         | synergy n.            |                                |
| subdued           | （光、声）柔和的，缓和的；（人）温和的         | unnaturally or unusually quiet in behavior                                 |                       |                                |
| consititution     | 宪法；体质                       |                                                                            |                       |                                |
| recessive         | 隐性遗传病的，后退的                  | tending to recede, withdraw                                               |                       |                                |
| reconnaissance    | 侦查，预先勘探                     | a preliminary survey to gain information                                   | renaissance 复兴        |                                |
| abridge           | 删减，缩短                       | to condence, to reduce in scope or extent                                  | a + bridge            |                                |
| fraught           | 充满的                         | filled, charged, loaded                                                    | freight n. 货物         |                                |
| tangible          | 可触摸的                        | touchable, palpable                                                        |                       |                                |
| inept             | 无能的，不适当的                    | inefficient, not suitable                                                  |                       |                                |
| amalgam           | 混合物                         | a combination or mixture, admixture, composite, compound, interfusion      |                       |                                |
| credulous         | 轻信的；易上当的                    | tending to believe too readily, easily convinced                           |                       |                                |
| recluse           | 隐士；隐居的                      | cloistered, seclusive, sequestered                                         | reclusive adj.        |                                |
| resort            | 求助，诉诸；度假胜地                  |                                                                            | report                |                                |
| spiny             | 针状的；多刺的，棘手的                 | thorny                                                                     | spine n.              |                                |
| ingrate           | 忘恩负义的人                      | an ungrateful person                                                       | in + grate            |                                |
| catalyze          | 促使，激励                       | to bring about, to inspire                                                 |                       |                                |
| intrigue          | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of         |                       |                                |
| propagate         | 繁殖；传播                       | to multiply, to cause to spread out, publicize                             |                       |                                |
| generic           | 种类的，类属的                     |                                                                            | genus                 |                                |
| burgeon           | 迅速成长，发展                     | to grow rapidly, proliferate, blossom, bloom, flower, outbloom, effloresce |                       |                                |
| invade            | 侵略，入侵                       | encroach, foray, infringe, raid, trespass                                  |                       |                                |
| equilibrium       | 平衡                          |                                                                            | equi + …              |                                |
| consort           | 陪伴；结交；配偶                    |                                                                            | con + sort            |                                |
| dull              | 不鲜明的；迟钝的；乏味的；变迟钝            | blunt, doltish, imbecile, moronic, obtuse                                  |                       |                                |
| decadence         | 衰落，颓废                       | the process of becoming decadent                                           |                       |                                |
| revise            | 校订，修订                       | redraft, redraw, revamp                                                    |                       |                                |
| aloof             | 远离的；冷漠的                     |                                                                            |                       |                                |
| blatant           | 厚颜无耻的；显眼的；炫耀的               | brazen, completely obvious, conspicuous, showy                             |                       |                                |
| braid             | 编织；麦穗；辫子                    | plait, twine, weave                                                        |                       |                                |
| arid              | 干旱的；枯燥的                     | dry, dull, uninteresting                                                   |                       | an arid discussion             |
| clamber           | 吃力的爬上；攀登                    | to climb awkwardly                                                         |                       |                                |
| counterpart       | 相对应或者具有相同功能的人或事物            |                                                                            | counter + part        |                                |
| surrogate         | 代替品；代理人                     | substitution                                                               |                       |                                |
| narcotic          | 麻醉剂；催眠的                     | soporific                                                                  |                       |                                |
| emphatic          | 重视的，强调的                     | showing emphasis                                                           | emphasize v.          |                                |
| phlegmatic        | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                        |                       |                                |
| decry             | 责难；贬低                       | to denounce, depreciate officially, disparage                              |                       |                                |
| transient         | 转瞬即逝的                       | ephemeral, evanescent, fleeting, fugacious, fugitive                       |                       |                                |
| penetrating       | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                          |                       |                                |
| vital             | 极其重要的；充满活力的                 | full of life and force                                                     |                       |                                |
| deploy            | 部署；拉长（战线），展开                |                                                                            |                       |                                |
| axiom             | 公理，定理                       | maxim, an established principle                                            |                       |                                |
| paean             | 赞美歌，诗歌                      | a song of joy, praise, triumph, chorale                                    |                       |                                |
| viable            | 切实可行的；能活下去的                 |                                                                            |                       |                                |
| foible            | 小缺点，小毛病                     | a small weakness, fault                                                    |                       |                                |
| undistorted       | 未失真的                        |                                                                            | un + distort + ed     |                                |
| exploit           | 剥削；充分利用                     |                                                                            |                       |                                |
| schism            | 分裂，教会分裂                     | division, separation, fissure, fracture, rift                              |                       |                                |
| duplicity         | 欺骗，口是心非                     | deceit, dissemblance, dissimluation, guile                                 |                       |                                |
