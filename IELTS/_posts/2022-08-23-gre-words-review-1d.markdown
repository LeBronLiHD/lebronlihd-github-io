---
layout: post
title:  "GRE Words 1 Day Review"
date:   2022-08-23 13:52:01 +0800
category: IELTSPosts
---

# New Words (1-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English        | Chinese            | Synonyms                                               | Helpful Memory Ideas      | Usage                 |
|:--------------:|:------------------:|:------------------------------------------------------:|:-------------------------:|:---------------------:|
| 1| 1| 1| 1| 1|
| agility       | 敏捷              |                                                                    |                                                   |                       |
| fickleness    | 浮躁；变化无常         | inconstancy                                                        |                                                   |                       |
| contrition    | 悔罪，痛悔           | remorse                                                            | contrite is the v. form                           |                       |
| congenital    | 先天的，天生的         | innate                                                             |                                                   |                       |
| irradiate     | 照射；照耀，照亮        | shine, light up                                                    |                                                   |                       |
| platitudinous | 陈腐的             |                                                                    | platitude is the v. form                          | platitudinous remarks |
| piety         | 孝顺，孝敬；虔诚        | fidelity to natural obligations, reverence, devoutness             |                                                   |                       |
| excise        | 切除，切去           | remove, cut off                                                    |                                                   |                       |
| refractory    | 倔强的，难管理的；（病）难治的 | stubborn, unmanageable, resistant                                  | fract 断裂                                          |                       |
| clemency      | 温和，仁慈；宽厚        | caritas, charity, lenity                                           | cement 水泥                                         |                       |
| defamation    | 诽谤，中伤           | aspersion, calmniation, denigration, vlification                   |                                                   |                       |
| vitriolic     | 刻薄的             | virulent                                                           | vitri 矾；玻璃                                        |                       |
| proclivity    | 倾向              | leaning, inclination, predisposition                               |                                                   |                       |
| connive       | 默许；纵容；共谋        | feign ignorance, conspire                                          | con + nive                                        |                       |
| corroboration | 证实；支持           | corroborate                                                        | corroborate 支持，强化                                 |                       |
| succinct      | 简明的，简洁的         | compact, precise, concise, compendiary, compendious, curt, laconic | suc + cinct                                       |                       |
| hysteria      | 歇斯底里症；过度兴奋      |                                                                    |                                                   |                       |
| indigenous    | 土产的，本地的；生来的，固有的 | native, innate                                                     | indi + gen + ous                                  |                       |
| scourge       | 鞭笞，磨难           | whip                                                               | courage                                           |                       |
| retaliate     | 报复，反击           | revenge                                                            | re + taliate                                      |                       |
| rigid         | 严格的；僵硬的，坚硬的     | stiff, incompliant, inflexible, unpliable, unyielding              |                                                   |                       |
| deviant       | 超越常规的           |                                                                    | deviate 偏离                                        |                       |
| complacence   | 自满              | satisfaction with oneself                                          |                                                   |                       |
| sobriety      | 节制，庄重           | moderation, gravity                                                |                                                   |                       |
| understated   | 轻描淡写的，低调的       | unostentatious, unpretentious                                      |                                                   |                       |
| rival         | 竞争者，对手；与…竞争     | compete, contend, vie                                              | river                                             |                       |
| resuscitate   | 使复活，使苏醒         | restore life or consciousness                                      |                                                   |                       |
| obstinacy     | 固执，倔强，顽固        | stubbornness                                                       | obstinate                                         |                       |
| nonch         | V字形切口，刻痕；等级，档次  |                                                                    |                                                   | top nonch             |
| reaffirmation | 再肯定             | renewed affirmation                                                | re + affirmation                                  |                       |
| slipshod      | 马虎的，草率的         | careless, slovenly                                                 |                                                   |                       |
| perilous      | 危险的，冒险的         |                                                                    | peril is the n. form                              |                       |
| engrave       | 雕刻，铭刻；铭记，牢记     | carve, impress deeply                                              |                                                   |                       |
| repudiate     | 拒绝接受，回绝，抛弃      | dismiss, reprobate, spum                                           |                                                   |                       |
| zealot        | 狂热者             | bigot, enthusiast, fanatic                                         | zealous is the adj. form                          |                       |
| jeopardize    | 危及，危害           | compromise, hazard, imperil, jeopard, to endanger                  |                                                   |                       |
| possessed     | 着迷的，入迷的         |                                                                    | possess                                           |                       |
| suffrage      | 投票权，选举权         | the right of voting                                                | suf + frage                                       |                       |
| premediate    | 预谋，预先考虑或安排      |                                                                    | pre + mediate                                     |                       |
| stumble       | 绊倒              | lurch, stagger, trip                                               |                                                   |                       |
| contemptible  | 令人轻视的           |                                                                    | contempt n.                                       |                       |
| galvanize     | 刺激，激起；电镀；通电     |                                                                    | stimulate, plate metal, apply an electric current | galvanic adj.         |
| finicky       | 苛求的，过分讲究的       | dainty, fastidious, finicking, too particular or exacting, fussy   | fine                                              |                       |
| touchy        | 敏感的，易怒的         |                                                                    | touch                                             |                       |
| agoraphobic   | 荒野恐惧症的（人）       |                                                                    |                                                   |                       |
| insipid       | 乏味的，枯燥的         | distateful, savorless, unappeitizing, unpalatable, unsavory        |                                                   |                       |
| sedate        | 镇静的             | sober, staid                                                       |                                                   |                       |
| cursory       | 粗略的，草率的         |                                                                    |                                                   |                       |
|2|2|2|2|2|
| scruple      | 顾忌，迟疑                   | boggle, stickle, an ethical consideration, hesitate                  |                      |                            |
| sanitary     | （有关）卫生的，清洁的             |                                                                      | sanat 隔离区            |                            |
| resilient    | 有弹性的；能恢复活力的；适应力强的       |                                                                      |                      |                            |
| judicious    | 有判断力的，审慎的               | judgmatic, prudent, sane, sapient, sensible                          | judge                |                            |
| revelation   | 显示，揭露的事实                |                                                                      | reveal v.            |                            |
| remunerative | 报酬高的；有利润的               | gainful, lucrative                                                   |                      |                            |
| verified     | 已查清的，已证实的               |                                                                      |                      |                            |
| profligacy   | 放荡，肆意挥霍                 |                                                                      | profligate v.        |                            |
| instinctive  | 本能的                     |                                                                      | instinct n.          |                            |
| deprive      | 剥夺，使丧失                  | denude, dismantle, divest, strip                                     |                      |                            |
| allusion     | 暗指，间接提到                 |                                                                      |                      |                            |
| allegory       | 寓言                      | fable                                                                            |                                   |                                                 |
| vestigial    | 退化的                     | rudimentary, degraded                                                |                      |                            |
| defiant      | 反抗的，挑衅的                 | bold, impudent                                                       |                      |                            |
| stash        | 藏匿                      |                                                                      | stay + ash           |                            |
| deciduous    | 非永久的；短暂的；脱落的；落叶的        |                                                                      |                      |                            |
| murky        | 黑暗的，昏暗的；朦胧的             | drak, gloomy, vague                                                  | murk n.              |                            |
| admonish     | 训诫；警告                   | to warn, advise, to reprove mildly                                   |                      |                            |
| glamorous    | 迷人的，富有魅力的               | full of glamour, fascinating, alluring                               | glamour 魅力           |                            |
| disabuse     | 打消（某人的）错误念头，使醒悟         | undeceive                                                            | dis + abuse          |                            |
| deferential    | 顺从的，恭顺的                 | duteous, dutiful                                                                 | deference n.                      |                                                 |
| denigrate    | 污蔑，诽谤                   | defame, blacken, to disparage the character or reputation of         |                      |                            |
| fickle       | （尤指在感情方面）易变的，变化无常的，不坚定的 |                                                                      | tickle 痒痒            |                            |
| reminiscent  | 回忆的；使人联想的               | tending to remind                                                    | reminiscence n.      | reminiscent of sb. or sth. |
| bumper       | 特大的；保险杠                 | unusually large, a device for absorbing shock or preventing damage   |                      |                            |
| grind        | 磨碎，碾碎；苦差事               | crush into bits or fine particles, long, difficult, tedious task     | grand 宏大             |                            |
| regimental   | 团的，团队的                  |                                                                      | regiment n.          |                            |
| transmute    | 变化                      | change or alter                                                      |                      |                            |
| prophesy     | 预言                      | adumbrate, augur, portend, presage, prognosticate                    |                      |                            |
| palpable     | 可触知的，可查觉的；明显的           | tangible, perceptible, noticeable                                    | palp 悸动，触觉           |                            |
| defer        | 遵从，听从；延期                | yield with courtesy, delay                                           |                      |                            |
| stasis       | 停滞                      | motionlessness                                                       |                      |                            |
| drollery     | 滑稽                      | quaint or why humor                                                  | droll 蠢蠢欲动；滑稽的       |                            |
| stint        | 节制，限量，节省                | to restrict                                                          | stint on sth. 吝惜…    |                            |
| perspicuous  | 明晰的，明了的                 | clearly expressed or presented                                       |                      |                            |
| seclusion    | 隔离；隔离地                  |                                                                      | seclude v.           | be in seclusion            |
| repellent    | 令人厌恶的                   | loathsome, odious, repugnant, revolting, arousing disgust, repulsive |                      |                            |
| intrusively  | 入侵的                     |                                                                      | intrusive adj.       |                            |
| trite        | 陈腐的，陈词滥调的               | bathetic, commonplace, hackneyed or boring                           |                      |                            |
| deign        | 惠允（做某事）；施惠于人            | stoop, to condescend to do sth.                                      |                      |                            |
| tint         | 色泽；给…淡淡的着色；染            | slight degree of a color, to give a slight color to                  |                      |                            |
| extant       | 现存的，现有的                 | currently or actually existing                                       |                      |                            |
| conjecture   | 推测，臆测                   | presume, suppose, surmise                                            |                      |                            |
| deprave        | 使堕落；使恶化                 | to make bad                                                                      |                                   |                                                 |
| provision    | 供应；（法律等）条款              | a stock of needed materials or supplies, stipulation                 |                      |                            |
| default      | 违约；未履行的责任；拖欠；不履行        |                                                                      | de + fault           |                            |
| propel       | 推进，促进                   | to drive forward or onward, push                                     |                      |                            |
| intricate    | 错综复杂的；难懂的               | complicated, knotty, labyrinthine, sophisticated, elaborate, complex |                      |                            |
| allot        | 分配；拔出                   | to assign, to distribute                                             | allocate 拨出；分配       |                            |
| stratify     | （使）层化                   | to divide or arrange into classes, castes, or social strata          |                      |                            |
| dismal       | 沮丧的，阴沉的                 | showing sadness                                                      | dies mail “不吉利的日子”   |                            |
| fluctuate      | 波动，变动                   | to undulate as waves, to be continually changing                                 |                                   |                                                 |
| querulous    | 抱怨的，爱发牢骚的               | habitually, complaining, fretful                                     |                      |                            |
| 3               | 3                  | 3                                                                                     | 3                    | 3                   |
| misalliance     | 不正当的结合             | an improper alliance                                                                  | mis + alliance       |                     |
| ultimate        | 最后的                |                                                                                       |                      |                     |
| plainspoken     | 直言不讳的              | candid, frank                                                                         |                      |                     |
| corruption      | 腐败，堕落              |                                                                                       | corrupt v.           |                     |
| supplement      | 增补，补充              | appendix,                                                                             | supply               |                     |
| termite         | 白蚁                 |                                                                                       |                      |                     |
| trickle         | 细流；细细的流            | dribble, drip, filter                                                                 |                      |                     |
| anticipatory    | 预想的，预期的            |                                                                                       | anticipate v.        |                     |
| pompous         | 自大的                | arrogant                                                                              |                      |                     |
| enamored        | 倾心的，被迷住            | bewitched, captivated, charmed, enchanted, infatuated, inflamed with love, fascinated |                      | be enamored of sth. |
| patronizing     | 以恩惠的态度对待的；要人领情的    |                                                                                       |                      |                     |
| inure           | 使习惯于；生效            |                                                                                       |                      |                     |
| rigorous        | 严格的，严峻的            | austere, rigid, severe, stern, manifesting, exercising, favoring rigor                |                      |                     |
| rehabilitate    | 使恢复（健康、能力、地位等）     |                                                                                       | re + habilitate      |                     |
| entrenched      | （权力、传统）确立的，牢固的     | strongly established                                                                  |                      |                     |
| incredulity     | 怀疑，不相信             | disbelief                                                                             | in + credit          |                     |
| anaerobic       | 厌氧的；厌氧微生物          |                                                                                       |                      |                     |
| indigent        | 贫穷的，贫困的            | deficient, improverished                                                              |                      |                     |
| homespun        | 朴素的；家织的            | homely                                                                                |                      |                     |
| asunder         | 分离的；化为碎片的          | apart or separate, into pieces                                                        |                      |                     |
| mournful        | 悲伤的                | doleful, dolorous, plaintive, woeful                                                  |                      |                     |
| bask            | 晒太阳；取暖             |                                                                                       |                      |                     |
| officious       | 过于殷勤的；多管闲事的；非官方的   | meddlesome, informal, unofficial                                                      |                      |                     |
| pine            | 松树；（因疾病等）憔悴；渴望     | crave, dream, hanker, thrist, to lose vigor, anguish                                  |                      |                     |
| dogmatist       | 独断家，独断论者           |                                                                                       | dogma 教条             |                     |
| submerged       | 在水中的；淹没的           | dipped, immersed, sunken                                                              |                      |                     |
| vicissitude     | 变迁，兴衰              | natural change or mutation visible in nature or in human affairs                      |                      |                     |
| grandeur        | 壮丽，宏伟              | splendor, magnificence                                                                |                      |                     |
| amorphous       | 无固定形状的             | without definite form, shapeless                                                      | morph 形态             |                     |
| withhold        | 抑制；扣留，保留           | bridle, constrain, inhibit, restrain                                                  | with + hold          |                     |
| repeal          | 废除（法律）             |                                                                                       | re + peal            |                     |
| relegate        | 使降级，贬谪；交付，托付       | to send to exile, assign to oblivion, to refer or assign for decision or action       |                      |                     |
| gaudy           | 俗丽的                | bright and showy                                                                      |                      |                     |
| sane            | 神志清楚的；明智的          | rational, normal and health mind, sensible                                            |                      |                     |
| fallow          | 休耕的                |                                                                                       |                      |                     |
| unsubstantiated | 未经证实的，无事实依据的       | uncorroborated                                                                        |                      |                     |
| exonerate       | 免除责任；确定无罪          | to relieve from an obligation, to clear from guilt, absolve                           |                      |                     |
| stagger         | 蹒跚，摇晃              | lurch, reel, sway, waver, wobble, move on unsteadily                                  |                      |                     |
| agitation       | 焦虑，不安；公开辩论，鼓动宣传    | anxiety, public argument or action for social or political change                     |                      |                     |
| charter         | 特权或豁免权             | special privilege or immunity                                                         |                      |                     |
| quagmire        | 沼泽地；困境             | dilemma, plight, scrape                                                               | mire 泥潭              |                     |
| infuse          | 注入，灌输；鼓励           | to instill, impart, to inspire                                                        |                      |                     |
| articulate      | 清楚的说话；（用关节）连接      |                                                                                       |                      |                     |
| raucous         | 刺耳的，沙哑的；喧嚣的        | disagreeably harsh, hoarse, boisterously disorderly                                   |                      |                     |
| frisky          | 活泼的，快活的            | impish, mischievous, sportive, waggish, playful, merry, frolicsome                    |                      |                     |
| stagnant        | 停滞的                | not advancing or developing                                                           |                      |                     |
| aromatic        | 芬芳的，芳香的            |                                                                                       | aroma 芳香             |                     |
| composure       | 镇静，沉着；自若           | calmness, coolness, phlegm, sangfroid, self-possession, tranquility, equanimity       |                      |                     |
| overblown       | 盛期已过的；残败的；夸张的      | past the prime or bloom, inflated                                                     |                      |                     |
| voyeur          | 窥淫癖者               |                                                                                       |                      |                     |
| impecunious     | 不名一文的；没钱的          | having very little or no money                                                        |                      |                     |
| proficient      | 熟练的，精通的            | skillful, expert                                                                      |                      |                     |
| diminution      | 减少，缩减              | min                                                                                   |                      |                     |
| prefabricated   | 预制构件的              | prefabricate v.                                                                       |                      |                     |
| consign         | 托运，托人看管            | con + sign                                                                            |                      |                     |
| conspiracy      | 共谋，阴谋              |                                                                                       | conspire v.          | conspiracy against  |
| unprecedented   | 前所未有的              |                                                                                       | un + precedent + ed  |                     |
| epitome         | 典型；梗概              | showing all the typical qualities of sth., abstract, summary, abridgment              |                      |                     |
| disparage       | 贬低，轻蔑              | belittle, derogate, to speak slightingly of, depreciate, decry                        |                      |                     |
| obsolescent     | 逐渐荒废的              | in the process of becoming obsolete                                                   | obsolescence         |                     |
| dissent         | 异议；不同意，持异议         | difference of opinion, to differ in belief or opinion, disagree                       |                      |                     |
| tributary       | 支流，进贡国；支流的，辅助的，进贡的 | making additions or yielding supplies, contributory                                   |                      |                     |
| eulogize        | 称赞，颂扬              | praise highly in speech or writing                                                    |                      |                     |
| tawdry          | 华而不实的，俗丽的          | cheap but showy                                                                       |                      | tawdry clothing     |
| overwrought     | 紧张过度的；兴奋过度的        | very nervous or excited                                                               |                      |                     |
| opacity         | 不透明性；晦涩            | the quality of being opaque, obscurity of sense                                       |                      |                     |
| susceptible     | 易受影响的，敏感的          | unresistant to some stimulus, influence, or agency                                    |                      |                     |
| gluttonous      | 贪吃的，暴食的            | edacious, hoggish, ravenous, voracious, very greedy for food                          |                      |                     |
| abet            | 教唆；鼓励，帮助           | to incite, encourage, urge and help on                                                |                      |                     |
|4|4|4|4|4|
| penchant      | 爱好，嗜好              | liking                                                                      | pend 悬而未决            |                       |
| egoistic(al)  | 自我中心的，自私自利的        | egocentric, egomaniacal, self-centered, selfish                             |                      |                       |
| surly         | 脾气暴躁的；阴沉的          | bad tempered, sullen                                                        |                      |                       |
| briny         | 盐水的，咸的             | of, or relating to, or resembling brine or the sea, salty                   | brine 盐水             |                       |
| calamity      | 大灾祸，不幸之事           | an extreme misfortune                                                       |                      |                       |
| incarnate     | 具有人体的；化身的，拟人化的     | given in a bodily form, personified                                         |                      |                       |
| slovenly      | 不整洁的，邋遢的           | dirty, untidy especially in personal appearance                             |                      |                       |
| retract       | 撤回，取消；缩回，拉回        | to take back or withdraw, to draw or pull back                              | re + tract           |                       |
| surmount      | 克服，战胜；登上           | to prevail over, overcome, to get to the top of                             | sur + mount          |                       |
| ingrained     | 根深蒂固的              | firmed, fixed or established                                                |                      | ingrained prejudices  |
| autograph     | 亲笔稿；手迹；在…上亲笔签名；亲笔的 |                                                                             |                      |                       |
| choppy        | 波浪起伏的；（风）不断改变方向的   | rough with small waves, changeable, variable                                |                      |                       |
| relinquish    | 放弃，让出              | to give up, withdraw or retreat from                                        | leave                |                       |
| tantalize     | 挑逗                 | to tease or torment by a sight of sth. desired but cannot be reached        |                      |                       |
| prolific      | 多产的，多果实的           | fruitful, fertile, fecund, productive                                       |                      |                       |
| depreciate    | 轻视；贬值              | belittle, disparage                                                         |                      |                       |
| tout          | 招徕顾客，极力赞扬          | to praise or publicize loudly                                               |                      |                       |
| beneficent    | 慈善的，仁爱的；有益的        | doing or producing good, beneficial                                         |                      |                       |
| rhetoric      | 修辞，修辞学；浮夸的言辞       | insincere or grandiloquent language                                         |                      |                       |
| virtuous      | 有美德的               | showing virtue                                                              | virtue n.            |                       |
| plaintive     | 悲伤的，哀伤的            | expressive of woe, melancholy                                               | plaint n. 哀诉         |                       |
| insular       | 岛屿的，心胸狭隘的          | illiberal, narrow-minded                                                    |                      |                       |
| frugal        | 节约的；节俭的            | careful and thrifty                                                         |                      |                       |
| penitent      | 悔过的，忏悔的            | expressing regretful pain, repentant                                        |                      |                       |
| benign        | （疾病）良性的；亲切和蔼的；慈祥的  |                                                                             |                      |                       |
| peerless      | 出类拔萃的，无可匹敌的        | matchless, incomparable                                                     |                      |                       |
| arable        | 适于耕种的              | suitable for plowing and planting                                           |                      | arable field          |
| perpetrate    | 犯罪；负责              | to commit, to be responsible for                                            |                      |                       |
| arcane        | 神秘的，秘密的            | cabalistic, impenetrable, inscrutable, mystic, mysterious, hidden or secret |                      |                       |
| proofread     | 校正，校对              | to read and mark corrections                                                |                      |                       |
| amenable      | 顺从的，愿接受的           | willing, submissive                                                         |                      | be amenable to        |
| impound       | 限制；依法没收，扣押         | to confine, to seize and hold in the custody of the law, take possession of |                      |                       |
| succor        | 救助，援助              | to go to the aid of                                                         |                      |                       |
| cavil         | 调毛病；吹毛求疵           | quibble                                                                     |                      | cavil at sth.         |
| torpor        | 死气沉沉               | dullness, languor, lassitude, lethargy, extreme sluggishness of function    |                      |                       |
| superannuate  | 使退休领养老金            | to retire and pension because of age or infirmity                           |                      |                       |
| artisan       | 技工                 | skilled workman or craftsman                                                |                      |                       |
| valorous      | 勇敢的                | brave                                                                       |                      |                       |
| aberrant      | 越轨的；异常的            | turning away from what is right, deviating from what is normal              |                      |                       |
| piercing      | 冷的刺骨的；敏锐的          | shrill, penetratingly cold, perceptive                                      |                      |                       |
| rave          | 热切赞扬；胡言乱语，说疯话      |                                                                             |                      |                       |
| hazardous     | 危险的，冒险的            | marked by danger, perilous, risky                                           |                      |                       |
| flaunty       | 炫耀的，虚化的            | ostentatious                                                                | flaunt v.            |                       |
| procurement   | 取得，获得；采购           |                                                                             | procure v.           |                       |
| arduous       | 费力的，艰难的            | marked by great labor or effort                                             |                      | arduous march         |
| sinuous       | 蜿蜒的，迂回的            | characterized by many curves and twists, winding                            |                      |                       |
| sprawling     | 植物蔓省的；（城市）无法计划开展的  | spreading out ungracefully                                                  |                      | sprawling handwriting |
| exasperate    | 激怒，使恼怒             | to make angry, vex, huff, irritate, peeve, pique, rile, roil                |                      |                       |
| vacillate     | 游移不定，踌躇            | to waver in mind, will or feeling                                           |                      |                       |
| parasitic     | 寄生的                |                                                                             |                      |                       |
| benevolent    | 善心的，仁心的            | altruistic, humane, philanthropic, kindly, charitable                       |                      |                       |
| castigate     | 惩治；严责              | chasten, chastise, discipline, to punish or rebuke severely                 |                      |                       |
| custodian     | 管理员，监护人            |                                                                             | custody 监护权          |                       |
| juncture      | 关键时刻，危急关头；结合处，接合点  | a critical point, a joining point                                           |                      |                       |
| rustic        | 乡村的，乡土气的           | of or relating to, or suitable for the country                              |                      |                       |
| veracity      | 真实，诚实              | devotion to the truth, truthfulness                                         |                      |                       |
| tenacious     | 坚韧的，顽强的            |                                                                             |                      |                       |
| surreptitious | 鬼鬼祟祟的              | clandestinely                                                               |                      |                       |
| relentless    | 无情的，残酷的            | cruel, merciless, pitiless, ruthless                                        |                      |                       |
| perishable    | 易腐烂的（东西），易变质的      | likely to decay or go bad quickly                                           |                      |                       |
| cessation     | 中止；（短暂的）停止         | a short pause or a stop                                                     |                      |                       |
| contagious    | 传染的；有感染力的          | easily passed from person to person, communicable                           |                      |                       |
| prodigious    | 巨大的；惊人的，奇异的        | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童           |                       |
| abstruse      | 难懂的，深奥的            | hard to understand, recondite                                               |                      |                       |
| 5              | 5                    | 5                                                                                   | 5                    | 5                             |
| wanderlust     | 漫游癖；旅游癖              | strong longing for or impulse toward wandering                                      | wander + lust 欲望     |                               |
| scant          | 不足的，缺乏的              | barely or scarcely                                                                  |                      | scant attention/consideration |
| reverberate    | 发出回声，回响              | to resound, echo                                                                    | re + verberate       |                               |
| gallant        | 勇敢的，英勇的；对（女人）献殷勤的    | brave and noble, polite and attentive to women                                      |                      |                               |
| valiant        | 勇敢的，英勇的              | valorous, courageous                                                                |                      |                               |
| chromatic      | 彩色的，五彩的              | having color or colors                                                              |                      |                               |
| vigilant       | 机警的，警惕的              | alertly watchful to avoid danger                                                    |                      |                               |
| replete        | 充满的，供应充足的            | fully or abundantly provided or filled                                              |                      |                               |
| precursor      | 先驱，先兆                | forerunner, harbinger, herald, outrider                                             |                      |                               |
| interplay      | 相互影响                 | interaction, interact                                                               |                      |                               |
| motley         | 混杂的；多色的，杂色的          | heterogeneous, of many colors                                                       |                      |                               |
| scorn          | 轻蔑，瞧不起               | contemn, despise                                                                    |                      |                               |
| reigning       | 统治的，起支配作用的           |                                                                                     |                      |                               |
| cohesive       | 凝聚的                  | sticking together                                                                   |                      |                               |
| disenfranchise | 剥夺…的权利               |                                                                                     | franchise 专营权        |                               |
| humiliate      | 羞辱，使丢脸               | to hurt the pride or dignity, mortify, degrade                                      |                      |                               |
| antecedent     | 前事；祖先，先行的            |                                                                                     |                      |                               |
| negligent      | 疏忽的，粗心大意的            | neglectful, regardless, remiss, slack                                               |                      |                               |
| injurious      | 有害的                  | harmful                                                                             | injury n.            |                               |
| placid         | 安静的，平和的              | serenely free of interruption                                                       |                      |                               |
| tussle         | 扭打，争斗；争辩             | physical contest or struggle, an intense argument. Controversy, to struggle roughly |                      |                               |
| dappled        | 有斑点的，斑驳的             | covered with spots of a different color                                             |                      |                               |
| enigma         | 谜一样的人或事物             |                                                                                     |                      |                               |
| heartfelt      | 衷心的，诚挚的              |                                                                                     |                      |                               |
| evince         | 表明，表示                | indicate, make manifest, show plainly                                               |                      |                               |
| bemuse         | 使昏头昏脑；使迷惑            | bedaze, daze, paralyze, petrify, stun, to puzzle, distract, absorb                  |                      |                               |
| insidious      | 暗中危害的；阴险的            | working or spreading harmfully in a subtle or stealthy manner                       |                      |                               |
| connotation    | 言外之意；内涵              | idea or notion suggested in addition to its explicit meaning or denotation          |                      |                               |
| stark          | 光秃秃的；荒凉的；（外表）僵硬的；完全的 | barren, desolate, rigid as if in death, utter, sheer                                |                      |                               |
| deterrent      | 威慑的，制止的              | serving to deter                                                                    | deter v.             |                               |
| efface         | 擦掉，抹去                | to wipe out, erase                                                                  |                      |                               |
| dilettante     | 一知半解者，业余爱好者          | dabbler, amateur                                                                    |                      |                               |
| interpolate    | 插入，（通过插入新语句）篡改       |                                                                                     | inter                |                               |
| camaraderie    | 同志之情；友情              | a spirit of friendly good-fellowship                                                |                      |                               |
| effete         | 无生产力的；虚弱的            | spent and sterile, lacking vigor                                                    |                      |                               |
| improvident    | 无远见的，不节俭的            | lacking foresight or thrift                                                         |                      |                               |
| streak         | 条纹；加线条               |                                                                                     |                      |                               |
| feckless       | 无效的，效率低的；不负责任的       | inefficient, irresponsible                                                          |                      |                               |
| reclusive      | 隐遁的，隐居的              | seeking or preferring seclusion or isolation                                        |                      |                               |
| stained        | 污染的，玷污的              | blemished, discolored, marked, spotted, tarnished                                   |                      |                               |
| habituate      | 使习惯于                 | to make use to, accustom                                                            | habit + uate         |                               |
| intangible     | 触摸不到的；无形的            | imperceptible, indiscernible, untouchable, impalpable, incorporeal                  |                      |                               |
| titular        | 有名无实的，名义上的           | existing in title only                                                              | title n.             |                               |
| forlorn        | 孤独的；凄凉的              | abandoned or deserted, wretched, miserable                                          |                      |                               |
| rampant        | 猖獗的，蔓生的              |                                                                                     |                      |                               |
| trepidation    | 恐惧，惶恐                | timorousness, uncertainty, agitation                                                |                      |                               |
| suspense       | 悬念；挂念                | apprehension, uncertainty, anxiety                                                  |                      |                               |
| indulge        | 放纵，纵容；满足             | cosset, pamper, spoil                                                               |                      |                               |
| brook          | 小河                   | a small stream                                                                      |                      |                               |
| tranquil       | 平静的                  | free from agitation of mind or spirit                                               |                      |                               |
| propensity     | 嗜好，习性                | bent, leaning, tendency, penchant, proclivity                                       |                      |                               |
| visionary      | 有远见的；幻想的；空想家         | dreamy, idealistic, utopain                                                         |                      |                               |
| elicit         | 得出，引出                | to draw forth or bring out                                                          |                      |                               |
| exempt         | 被免除的；被豁免的；免除，豁免      |                                                                                     |                      |                               |
| genus          | （动植物的）属，类            |                                                                                     |                      |                               |
| discomfited    | 困惑的，尴尬的              | frustrated, embarrassed                                                             |                      |                               |
| pivotal        | 中枢的，枢纽的；极其重要的        |                                                                                     | pivot 枢              |                               |
| humble         | 卑微的；谦虚的；使谦卑          |                                                                                     |                      |                               |
| 6              | 6                         | 6                                                                                       | 6                    | 6                   |
| atheist        | 无神论者                      | no deity                                                                                |                      |                     |
| aspiring       | 有抱负的，有理想的                 |                                                                                         | aspire v.            |                     |
| paltry         | 无价值的，微不足道的                | trashy, trivial, petty                                                                  |                      |                     |
| tentatively    | 实验性的                      | experimentally                                                                          |                      |                     |
| reprisal       | 报复；报复行动                   | practice in retaliation                                                                 |                      |                     |
| surrender      | 投降；放弃；归还                  | give in, give up, give back                                                             |                      |                     |
| sagacious      | 聪明的，睿智的                   | showing keen perception and foresight                                                   | sage 智者              |                     |
| inordinate     | 过度的；过分的                   | immoderate, excessive                                                                   |                      |                     |
| treatise       | 论文                        | a long writing work dealing systemctically with one subject                             |                      |                     |
| vainglorious   | 自负的                       |                                                                                         |                      |                     |
| congenial      | 意气相投的；性情好的；适意的            | companionable, amiable, agreeable                                                       |                      |                     |
| flatter        | 恭维，奉承                     | adulate, blandish, honey, slaver                                                        |                      |                     |
| eminence       | 卓越，显赫，杰出                  | distinction, illustriousness, preeminence, a position of prominence or superiority      |                      |                     |
| submission     | 从属，服从                     |                                                                                         | sub + mission        |                     |
| irreversible   | 不能撤回的，不能取消的               | not reversible, irrevocable, unalterable                                                |                      |                     |
| ineptitude     | 无能；不适当                    | the quality or state of being inept                                                     | inept adj.           |                     |
| salvation      | 拯救；救助                     | deliverance from difficulty or danger                                                   |                      |                     |
| scour          | 冲刷；擦掉；四处搜索                | to clear, dig, remove, rub, to range over in a search                                   |                      |                     |
| perfunctory    | 例行公事般的；敷衍的                | characterized by routine or superficiality                                              |                      |                     |
| spatial        | 有关空间的，在空间的                |                                                                                         | space n.             |                     |
| ephemeral      | 朝生暮死的；生命短暂的               | evanescent, fleeting, fugacious, momentary                                              |                      |                     |
| vehement       | 猛烈的，热烈的                   | exquisite, fierce, furious, intense, violent, marked by forceful energy                 |                      |                     |
| promulgate     | 颁布（法令）；宣传，传播              | announce, annunciate, declear, disseminate, proclaim                                    |                      |                     |
| dearth         | 缺乏；短缺                     | scarcity                                                                                | dear + th            |                     |
| idiosyncratic  | 特殊物质的，特殊的，异质的             | characteristic, distinctive                                                             |                      |                     |
| tardy          | 延迟；迟到的；缓慢的                | sluggish                                                                                |                      |                     |
| unleash        | 发泄，释放                     | set feelings or forces free from control                                                |                      |                     |
| clannish       | 排他的，门户之见的                 |                                                                                         | clan 氏族              |                     |
| stilted        | （文章、谈话）不自然的，夸张的           | pompous                                                                                 | stilt 高跷             |                     |
| suppliant      | 恳求的，哀求的；恳求者，哀求者           |                                                                                         |                      |                     |
| allusive       | 暗指的，间接提到的                 | containing allusions                                                                    | allude v.            |                     |
| canny          | 精明仔细的                     | shrewd and careful                                                                      |                      |                     |
| egocentric     | 利己的                       | self-centered                                                                           |                      |                     |
| unwonted       | 不寻常的，不习惯的                 | unusual, unaccustomed                                                                   |                      |                     |
| scavenge       | 清除，从废物中提取有用物质             | to cleanse, to salvage from discarded or refuse material                                |                      |                     |
| gloom          | 昏暗，忧郁                     | darkness, dimness, obscurity, deep sadness or hopelessness                              |                      |                     |
| insurgent      | 叛乱的，起义的；叛乱分子              | rebellious, a person engaged in insurgent activity                                      | in + surge 激增，浪涌     |                     |
| septic         | 腐败的；脓毒性的                  |                                                                                         | sepsis 脓毒症           |                     |
| warrant        | 正当理由；许可证；保证，批准            | certify, guarantee, vindicate, justification, a commission or document giving authority |                      |                     |
| potent         | 强有力的，有影响力的，有权力的；有效力的，有效能的 |                                                                                         |                      |                     |
| unseemly       | 不适宜的；不得体的                 |                                                                                         |                      |                     |
| falsify        | 篡改；说谎                     | to alter a record, etc. fraudulently, to tell sb. falsehoods, lie                       |                      |                     |
| incense        | 香味，激怒                     | enrage, infuriate, madden                                                               |                      |                     |
| overdraw       | 透支；夸大                     |                                                                                         | over + draw          |                     |
| distent        | 膨胀的；扩张的                   | swollen, expanded                                                                       |                      |                     |
| quantifiable   | 可以计量的；可量化的                |                                                                                         | quant 数量             |                     |
| rancor         | 深仇；怨恨                     | bitter deep-seated ill will, enmity                                                     |                      |                     |
| opponent       | 对手，敌手                     | adversary, antagonist                                                                   |                      |                     |
| doctrinaire    | 教条主义者；教条的，迂腐的             | authoritarian, authoritative, dictatorial, dogmatic                                     | doctrine 教条          |                     |
| flagrant       | 罪恶昭著的；公然的                 | conspicuously offensive                                                                 |                      |                     |
| hypocrisy      | 伪善；虚伪                     | cant, pecksniff, sanctimony, sham                                                       |                      |                     |
| impassive      | 无动于衷的，冷漠的                 | stolid, phlegmatic                                                                      |                      |                     |
| accede         | 同意                        | to give assent, consent                                                                 |                      | accede to 答应        |
| nonconformist  | 不墨守成规的（人）                 |                                                                                         | conform 顺应           |                     |
| ethnic         | 民族的，种族的                   | of a national, racial or tribal group that has a common culture tradition               |                      |                     |
| repugnant      | 令人厌恶的                     | strong distaste or aversion                                                             |                      | be repugnant to sb. |
| notorious      | 臭名昭著的                     | widely and unfavorably known                                                            | not + orious         |                     |
| claustrophobic | （患，导致）幽闭恐惧症的              |                                                                                         | claustrophobia 幽闭恐惧症 |                     |
| tread          | 踏，践踏；行走；步态；车轮胎面           | step                                                                                    |                      |                     |
| solvent        | 有偿债能力的；溶剂                 | capable of meeting financial obligations                                                | solve v.             |                     |
| abstain        | 禁绝，放弃                     | ab + stain                                                                              |                      |                     |
| 7                | 7                | 7                                                                          | 7                    | 7                  |
| sporadic         | 不定时发生的           | occurring occasionally                                                     |                      |                    |
| wilt             | 凋谢，枯萎            | mummify, shrivel, wither, to lose vigor from lack of water                 |                      |                    |
| antipathy        | 反感，厌恶            | animosity, animus, antagonism, enmity, hostility                           |                      |                    |
| carnivorous      | 肉食动物的            | flesh-eating                                                               |                      |                    |
| monochromatic    | 单色的              | having only one color                                                      |                      |                    |
| escalate         | （战争等）升级；扩大，上升，攀升 | to make a conflict more serious, to grow or increase rapidly               |                      |                    |
| interrogation    | 审问，质问；疑问句        | inquiry, query, question                                                   |                      |                    |
| sarcastic        | 讽刺的              | sneering, caustic, ironic                                                  |                      | sarcastic comments |
| stringent        | （规定）严格的；苛刻的；缺钱的  | severe, rigorous, draconian, financial strain                              |                      |                    |
| hypochondriac    | 忧郁症患者；忧郁症的       |                                                                            | hypochondria 忧郁症     |                    |
| subtractive      | 减去的，负的           |                                                                            | subtract 减去          |                    |
| inconclusive     | 非决定性的；无定论的       | leading to no conclusion or definite result                                | in + conclusive      |                    |
| proclamation     | 宣布，公布            | an official public statement                                               |                      |                    |
| iconographic     | 肖像的；肖像学的；图解的     | representing something by pictures or diagrams                             |                      |                    |
| ablaze           | 着火的；燃烧的；闪耀的      | being on fire, radiant with light or emotion                               |                      | ablaze with sth.   |
| boisterous       | 喧闹的；猛烈的          | disorderly, rambunctious, tumultuous, turbulent, noisy and unruly, violent |                      |                    |
| elegiac          | 哀歌的，挽歌的；哀歌       |                                                                            | elegy 哀歌             |                    |
| frivolous        | 轻薄的，轻佻的          | marked by unbecoming levity                                                |                      |                    |
| didactic         | 教诲的；说教的          | morally instructive, boringly pedantic or moralistic                       |                      |                    |
| celibate         | 独身者；不结婚的         | an unmarried person                                                        |                      |                    |
| voluble          | 健谈的；易旋转的         | talkative, rotating                                                        | volume               |                    |
| reprimand        | 训诫，谴责            | a severe or official reproof                                               |                      |                    |
| grope            | 摸索，探索            | to feel or search about blindly                                            |                      |                    |
| bluff            | 虚张声势；悬崖峭壁        | pretense of strength, high cliff                                           |                      |                    |
| pantomime        | 哑剧               |                                                                            |                      |                    |
| soporific        | 催眠的；安眠药；麻醉剂      |                                                                            |                      |                    |
| transitory       | 短暂的              | transient                                                                  |                      |                    |
| propriety        | 礼节；适当；得体         | decorum, appropriateness                                                   |                      |                    |
| akin             | 同族的；类似的          | related by blood, essentially similar, related, or compatible              |                      |                    |
| assorted         | 各式各样的；混杂的        | consisting of various kinds, mixed                                         |                      |                    |
| substantive      | 根本的；独立存在的        | dealing with essentials, being in a totally independently entity           |                      |                    |
| irreverent       | 不尊敬的             | disrespectful                                                              |                      |                    |
| apathetic        | 无感情的；无兴趣的        |                                                                            |                      |                    |
| chorale          | 赞美诗；合唱队          |                                                                            | chorus 合唱班           |                    |
| unpromising      | 无前途的，没有希望的       | not promising                                                              |                      |                    |
| overcast         | 阴天的；阴暗的          | bleak, cloudy, gloomy, comber                                              |                      |                    |
| infer            | 推断，推论，推理         | to reach in an opinion from reasoning                                      |                      |                    |
| subsist          | 生存下去；继续存在；维持生活   | to exist                                                                   |                      |                    |
| iniquitous       | 邪恶的；不公正的         | wicked, unjust                                                             |                      |                    |
| grieve           | 使某人极为悲伤的         | aggrieve, bemoan, deplore, distress, mourn, to cause great sorrow to sb.   |                      | grieve over ath.   |
| prevail          | 战胜；流行，盛行         | conquer, master, predominate, triumph                                      |                      |                    |
| hallow           | 把…视为神圣；尊敬，敬畏     | to make or set apart as holy, to respect or honor greatly, revere          |                      |                    |
| adhesive         | 黏着的；带黏性的；黏合剂     | tending to adhere or cause adherence, an adhesive substance                |                      |                    |
| entangle         | 使卷入              | to involve in a perplexing or troublesome situation                        |                      |                    |
| indiscriminately | 随意的，任意的          | in a random manner, promiscuously, arbitrary                               |                      |                    |
| deteriorate      | （使）变坏；恶化         | degenerate, descend, languish, weaken                                      |                      |                    |
| trenchant        | 犀利的，尖锐的          | sharply perceptive, penetrating                                            |                      |                    |
| down             | 下；绒毛，羽毛          |                                                                            |                      |                    |
| fleeting         | 短暂的；飞逝的          | transient, passing swiftly                                                 | fleet v.             |                    |
| derogatory       | 不敬的；贬损的          | disparaging, belittling                                                    |                      |                    |
| crumple          | 把…弄皱；起皱；破裂       | crush together into creases or wrinkles, to fall apart                     |                      |                    |
| demur            | 表示抗议，反对          |                                                                            | de + mur             |                    |
| solitary         | 孤独的；隐士           | without companions, recluse, forsaken, lonesome, lorn                      | solit 孤独             |                    |
| adumbrate        | 预示               | to foreshadow in a vague way                                               |                      |                    |
| 8              | 8                    | 8                                                                         | 8                    | 8                |
| gratuitous     | 无缘无故的；免费的            | without cause or justification, free                                      | gratuity 小费          |                  |
| disseminate    | 传播，宣传                | to spread abroad, promulgate widely                                       |                      |                  |
| imperious      | 傲慢的；专横的              | overbearing, arrogant, magisterial, peremptory                            |                      | imperious manner |
| colony         | 菌群；殖民地               |                                                                           |                      |                  |
| patronize      | 屈尊俯就，光顾，惠顾           |                                                                           | patron               |                  |
| volatile       | 反复无常的；易挥发的           | fickle, inconstant, mercuial, capricious                                  |                      |                  |
| suspend        | 暂停，中止；吊，悬            |                                                                           | sus + pend           |                  |
| garble         | 曲解，篡改                | to alter or distort as to create a wrong impression or change the meaning |                      |                  |
| serendipity    | 偶然性；偶然的事物            |                                                                           |                      |                  |
| venial         | （错误）轻微的，可原谅的         | forgivable, pardonable, excusable, remittable                             |                      |                  |
| elusive        | 难懂的，难以描述的；不易被抓获的     |                                                                           |                      |                  |
| flout          | 蔑视，违抗                | fleer, gibe, jeer, jest, sneer                                            |                      |                  |
| cavity         | （牙齿等的）洞，腔            | a hollow place in a tooth                                                 | carve                |                  |
| quixotic       | 不切实际的，空想的            | foolishly impractical                                                     |                      |                  |
| garrulous      | 唠叨的，话多的              | loquacious, talkative, chatty, conversational, voluble                    |                      |                  |
| substantiate   | 证实，确证                | embody, externalize, incarnate, materialize                               |                      |                  |
| antithetical   | 相反的；对立的              | opposite, contradictory, contrary, converse, counter, reverse             | antithesis n.        |                  |
| intrepid       | 勇敢的；刚毅的              | characterized by fearlessness and fortitude                               |                      |                  |
| glaze          | 上釉于，使光滑；釉            |                                                                           |                      |                  |
| obfuscate      | 使困惑，使迷惑              | to muddle, confuse, bewilder                                              |                      |                  |
| exquisite      | 精致的；近乎完美的            | elaborately made, delicate, consummate, perfected                         |                      |                  |
| dilapidated    | 破旧的；毁坏的              | broken down                                                               |                      |                  |
| moratorium     | 延缓偿付；活动中止            |                                                                           |                      |                  |
| intimidate     | 恐吓，胁迫                | to make timid                                                             |                      |                  |
| dissident      | 唱反调者                 | a person who disagrees, dissenter                                         |                      |                  |
| stalk          | 隐伏跟踪（猎物）             | to pursue quarry or prey stealthily                                       |                      |                  |
| perfervid      | 过于热心的                | excessively fervent                                                       |                      |                  |
| truculent      | 残暴的，凶狠的              | feeling or displaying ferocity, cruel                                     |                      |                  |
| transit        | 通过；改变；运输             | passage, change, transition, conveyance, to pass over or through          |                      |                  |
| deter          | 威慑，吓住；阻止             | to discourage, inhibit                                                    |                      |                  |
| control        | 控制；对照物（组）            |                                                                           |                      |                  |
| herald         | 宣布…的消息；预示…的来临；传令官，信使 |                                                                           |                      |                  |
| incommensurate | 不成比例的；不相称的           | not proportionate, not adequate                                           |                      |                  |
| penal          | 惩罚的；刑罚的              |                                                                           | penalty n.           |                  |
| sycophant      | 马屁精                  | a servile self-seeking flatterer                                          |                      |                  |
| rugged         | 高低不平的；崎岖的            | having a rough uneven surface, bumpy, craggy,                             |                      |                  |
| affront        | 侮辱，冒犯                | to offend, confront, encounter                                            |                      |                  |
| pragmatic      | 实际的；务实的；注重生效的；实用主义的  |                                                                           |                      |                  |
| assuming       | 傲慢的，自负的；假设           | pretentious, presumptuous                                                 |                      |                  |
| critique       | 批评性的分析               | critical analysis                                                         |                      |                  |
| speculate      | 沉思，思索；投机             | to meditate on or ponder                                                  |                      |                  |
| 9               | 9                           | 9                                                                      | 9                    | 9                      |
| banal           | 乏味的；陈腐的                     | dull or stale, commonplace, insipid, bland, flat, sapless, vapid       |                      |                        |
| estranged       | 疏远的；不知的                     | alienated                                                              | e + strange + d      |                        |
| confrontation   | 对抗                          | the clashing of forces or ideas                                        | confront v.          |                        |
| contemplate     | 深思；凝视                       | excogitate, prepend, ponder, view, gaze, to think about intently       |                      | contemplate doing sth. |
| impassable      | 不能通行的                       |                                                                        | im + pass + able     |                        |
| inimical        | 敌意的，不友善的                    | hostile, unfriendly                                                    |                      |                        |
| innocuous       | （行为，言论等）无害的                 | harmless, innocent, innoxious, inoffensive, unoffensive                |                      |                        |
| swampy          | 沼泽的，湿地的                     | marshy                                                                 | swampy lake          |                        |
| fortitude       | 坚毅，坚忍不拔                     |                                                                        |                      |                        |
| stately         | 庄严的；宏伟的                     | august, dignified, majestic                                            |                      |                        |
| tenuous         | 纤细的；稀薄的；脆弱的，无力的             | not dense, rare, flimsy, weak                                          |                      |                        |
| attenuation     | 变瘦，减小，减弱                    |                                                                        | attenuate v.         |                        |
| perquisite      | 额外收入；津贴；小费                  | cumshaw, lagniappe, largess                                            | per + quisite        |                        |
| haggle          | 讨价还价                        | bargain, dicker, wrangle                                               |                      |                        |
| falter          | 蹒跚，支支吾吾地说                   | flounder, stagger, tumble, stammer                                     |                      |                        |
| affable         | 和蔼的；友善的                     | gentle, amiable, pleasant and easy to approach or talk to              |                      |                        |
| solemn          | 严肃的，庄严的；隆重的；黑色的             | made with great seriousness                                            |                      |                        |
| flustered       | 慌张的，激动不安的                   | nervous and upset, perturbed, rattled                                  |                      |                        |
| peripatetic     | 巡游的，游动的                     | itinerant                                                              |                      |                        |
| whimsy          | 古怪，异想天开                     | whim, a fanciful creation, boutade, caprice, crotchet, freak           |                      |                        |
| allure          | 引诱，诱惑                       | to entice by charm or attraction                                       |                      |                        |
| xenophobic      | 恐外的；害怕外国人的                  | having abnormal fear or hatred of the strange or foreign               |                      |                        |
| flip            | 用指轻弹；蹦蹦跳跳；无礼的，冒失的           |                                                                        |                      |                        |
| emulate         | 与…竞争，努力赶上                   | to strive to equal or excel                                            |                      |                        |
| collusion       | 共谋，勾结                       |                                                                        |                      | collusion with         |
| peevish         | 不满的，抱怨的；暴躁的；易怒的             | huffy, irritable, pettish, discontented, querulous, fractious, fretful | peeve v.             |                        |
| imbue           | 浸染；浸透；使充满，灌输，激发             | to permeate or influence as if by dyeing, to endow                     |                      |                        |
| astray          | 迷路的；误入歧途的                   | off the right path                                                     |                      |                        |
| impractical     | 不切实际的                       |                                                                        | im + practical       |                        |
| frantic         | 疯狂的；狂乱的                     | wild with anger, frenzied                                              | fry + ant + tic      |                        |
| synergic        | 协同作用的                       | of combined action or coorperation                                     | synergy n.           |                        |
| subdued         | （光、声）柔和的，缓和的；（人）温和的         | unnaturally or unusually quiet in behavior                             |                      |                        |
| consititution   | 宪法；体质                       |                                                                        |                      |                        |
| recessive       | 隐性遗传病的，后退的                  | tending to recede, withdraw                                           |                      |                        |
| fraught         | 充满的                         | filled, charged, loaded                                                | freight n. 货物        |                        |
| amalgam         | 混合物                         | a combination or mixture, admixture, composite, compound, interfusion  |                      |                        |
| credulous       | 轻信的；易上当的                    | tending to believe too readily, easily convinced                       |                      |                        |
| recluse         | 隐士；隐居的                      | cloistered, seclusive, sequestered                                     | reclusive adj.       |                        |
| resort          | 求助，诉诸；度假胜地                  |                                                                        | report               |                        |
| hackneyed       | 陈腐的，老一套的                    | overfamiliar through overuse, trite                                    |                      |                        |
| spiny           | 针状的；多刺的，棘手的                 | thorny                                                                 | spin n.              |                        |
| die             | 死亡；金属模具                     |                                                                        |                      |                        |
| catalyze        | 促使，激励                       | to bring about, to inspire                                             |                      |                        |
| intrigue        | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of     |                      |                        |
| courteous       | 有礼貌的                        | complaisant, gallant, polite, mannerly                                 |                      |                        |
| consort         | 陪伴；结交；配偶                    |                                                                        | con + sort           |                        |
| revise          | 校订，修订                       | redraft, redraw, revamp                                                |                      |                        |
| aloof           | 远离的；冷漠的                     |                                                                        |                      |                        |
| blatant         | 厚颜无耻的；显眼的；炫耀的               | brazen, completely obvious, conspicuous, showy                         |                      |                        |
| braid           | 编织；麦穗；辫子                    | plait, twine, weave                                                    |                      |                        |
| fraudulent      | 欺骗的；不诚实的                    | acting with fraud, deceitful                                           |                      |                        |
| arid            | 干旱的；枯燥的                     | dry, dull, uninteresting                                               |                      | an arid discussion     |
| clamber         | 吃力的爬上；攀登                    | to climb awkwardly                                                     |                      |                        |
| surrogate       | 代替品；代理人                     | substitution                                                           |                      |                        |
| narcotic        | 麻醉剂；催眠的                     | soporific                                                              |                      |                        |
| reciprocally    | 相互的；相反的                     | mutually                                                               |                      |                        |
| decry           | 责难；贬低                       | to denounce, depreciate officially, disparage                          |                      |                        |
| phlegmatic      | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                    |                      |                        |
| penetrating     | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                      |                      |                        |
| amicably        | 友善的                         | in an amicable manner                                                  |                      |                        |
| deploy          | 部署；拉长（战线），展开                |                                                                        |                      |                        |
| axiom           | 公理，定理                       | maxim, an established principle                                        |                      |                        |
| paean           | 赞美歌，诗歌                      | a song of joy, praise, triumph, chorale                                |                      |                        |
| foible          | 小缺点，小毛病                     | a small weakness, fault                                                |                      |                        |
| undistorted     | 未失真的                        |                                                                        | un + distort + ed    |                        |
| schism          | 分裂，教会分裂                     | division, separation, fissure, fracture, rift                          |                      |                        |
| duplicity       | 欺骗，口是心非                     | deceit, dissemblance, dissimluation, guile                             |                      |                        |
| 10            | 10                    | 10                                                                                              | 10                   | 10                          |
| collateral    | 平行的；附属的；旁系的；担保品       | parallel, subordinate                                                                           | col + later + al     |                             |
| permeable     | 可渗透的                  | penetrable                                                                                      |                      |                             |
| revert        | 恢复；重新考虑               | to go back, to consider again                                                                   | re + vert            |                             |
| hamper        | 妨碍；阻挠；有盖的大篮子          | hinder, impede, a large basket with a cover                                                     |                      |                             |
| annex         | 兼并；附加                 | to obtain or take for oneself, to attach                                                        |                      |                             |
| garish        | 俗丽的，过于艳丽的             | too bright or gaudy, tastelessly showy                                                          |                      |                             |
| prosecute     | 起诉；告发                 | to carry on a legal suit, to carry on a prosecution                                             |                      |                             |
| inexorable    | 不为所动的；无法改变的           | incapable of being moved or influenced, cannot be altered                                       |                      |                             |
| redeem        | 弥补，赎回，偿还              | to atone for, expiate                                                                           |                      |                             |
| slimy         | 黏滑的                   | of, relating to, or resembling slime, glutinous, miry, muddy, viscous                           |                      |                             |
| animated      | 活生生的，生动的              | full of vigor and spirit                                                                        | animate adj.         |                             |
| coarse        | 粗糙的，低劣的；粗俗的           | of low quality, not refined                                                                     |                      |                             |
| interlocking  | 连锁的，关联的               |                                                                                                 | interlock v.         |                             |
| improvise     | 即席创作                  | impromptu, extemporize                                                                          |                      |                             |
| benefactor    | 行善者，捐助者               | a person who has given financial help, patron                                                   |                      |                             |
| stoic         | 坚忍克己之人                | a person who firmly retraining response to pain or stress                                       | a person named Stoic |                             |
| episodic      | 偶然发生的；分散性的            | occuring irregularly                                                                            | episode n. 片段        |                             |
| contention    | 争论；论点                 | the act of dispute, discord, a statement one argues for as valid                                | con + tention        |                             |
| withstand     | 反抗，经受                 | to oppose with force or resolution, to endure successfully                                      |                      |                             |
| desecrate     | 玷辱，亵渎                 | treat as not sacred, profane                                                                    | de + secra (sacred)  |                             |
| dichotomy     | 两分法；矛盾对立，分歧；具有两分特征的事物 | bifurcation, sth, with seemingly contradictory qualities                                        |                      |                             |
| rebellious    | 造反的；反叛的；难控制的          | given to or engaged in rebellion, refractory                                                    |                      |                             |
| mundane       | 现世的，世俗的；平凡的           | relating to the world, worldly, commonplace                                                     |                      |                             |
| predatory     | 掠夺的；食肉的               | predatorial, rapacious, raptorial                                                               |                      |                             |
| aggrandize    | 增大，扩张；吹捧              | augment, extend, heighten, magnify, multiply                                                    |                      | aggrandize oneself          |
| slothful      | 迟钝的；懒惰的               |                                                                                                 |                      |                             |
| ribaldry      | 下流的语言；粗鄙的幽默           | ribald language or humor, obscenity                                                             |                      |                             |
| tyrannical    | 暴虐的；残暴的               | tyrannic, despotic, oppressive                                                                  |                      |                             |
| grating       | （声音）刺耳的；恼人的           | hoarse, raucous, irritating, annoying, harsh and rasping                                        |                      |                             |
| perspicacious | 独具慧眼的；敏锐的             | of acute mental vision or discernment                                                           |                      |                             |
| specter       | 鬼怪，幽灵；缠绕心头的恐惧，凶兆      | eidolon, phantasm, phantom, spirit, a ghostly apparition, sth. that haunts or perturbs the mind |                      |                             |
| ferment       | 使发酵；使激动；发酵；骚动         | to cause fermentation in                                                                        | fermentation n.      |                             |
| solicit       | 恳求；教唆                 | to make petition to, to entice into evil                                                        |                      |                             |
| pitfall       | 陷阱；隐患                 | a hidden or not easily recognized danger or difficulty                                          |                      |                             |
| euphonious    | 悦耳的                   | having a pleasant sound, harmonious                                                             |                      |                             |
| atrophy       | 萎缩，衰退                 | decadence, declination, degeneracy, degeneration, deterioration                                 |                      |                             |
| omit          | 疏忽，遗漏；不做，未能做          | to leave out, to leave undone, disregard, ignore, neglect                                       |                      |                             |
| speculative   | 推理的；思索的；投机的           | conjectured, guessed, supposed, surmised, risky                                                 | speculation n.       |                             |
| elapse        | 消逝，过去                 | pass, go by                                                                                     |                      |                             |
| allegiance    | 忠诚，拥护                 | loyalty or devotion to a cause or a person                                                      |                      |                             |
| whittle       | 削（木头）；削减              | to pare or cut off chips, to reduce, pare                                                       | whistle 口哨           |                             |
| divisive      | 引起分歧的；导致分裂的           | creating disunity or dissension                                                                 |                      |                             |
| crooked       | 不诚实的；弯曲的              | dishonest, not straight                                                                         | crook v.             |                             |
| bombastic     | 夸夸其谈的                 |                                                                                                 |                      |                             |
| indent        | 切割成锯齿状                | notch, cut serratedly                                                                           |                      |                             |
| creed         | 教义，信条                 | belief, tenet                                                                                   |                      |                             |
| chubby        | 丰满的；圆胖的               | plump                                                                                           |                      | chubby cheeks               |
| underhanded   | 秘密的；狡诈的               | marked by secrecy and deception, sly                                                            |                      |                             |
| tantamount    | 同等的；相当于               | equivalent in value, significance, or effect                                                    |                      |                             |
| shrivel       | （使）枯萎                 | mummify, wilt, wither                                                                           |                      |                             |
| containment   | 阻止，遏制                 | keeping sth, within limits                                                                      |                      | a policy of containment     |
| sparse        | 稀少的，贫乏的               | not thickly grown or settled                                                                    | spark 火花             |                             |
| attrition     | 摩擦，磨损                 | by friction                                                                                     |                      |                             |
| accentuate    | 强调；重读                 | emphasis, pronounce with an stress                                                              | accent + uate        |                             |
| tension       | 紧张，焦虑；张力              | anxiety, stretching force                                                                       |                      |                             |
| consternation | 大为吃惊；惊骇               | great fear or shock                                                                             |                      |                             |
| delineate     | 勾画，描述                 | to sketch out, draw, describe                                                                   |                      |                             |
| preservative  | 保护的；防腐的；防腐剂           |                                                                                                 | preserve v.          |                             |
| genial        | 友好的；和蔼的               | cheerful, friendly, amiable                                                                     |                      |                             |
| pliable       | 易弯曲的；柔软的；易受影响的        | ductile, easily influenced, easily bend, supple                                                 |                      |                             |
| symbiosis     | 共生（关系）                |                                                                                                 | sym + bio + sis      |                             |
| deride        | 嘲笑，愚弄                 | to laugh at, ridicule                                                                           |                      |                             |
| ignominious   | 可耻的；耻辱的               | disgraceful, humiliating, despicable, dishonorable                                              |                      |                             |
| snippet       | 小片，片段                 | a small part                                                                                    |                      | snippet of information/news |
| furtive       | 偷偷的，秘密的               | catlike, clandestine, covert, secret, surrepitious                                              |                      |                             |
| 12            | 12                   | 12                                                                              | 12                   | 12                 |
| tempting      | 诱惑人的                 | having an appeal, alluring, attractive, enticing, inviting, seductive           |                      |                    |
| recount       | 叙述；描写                | narrate                                                                         |                      |                    |
| populous      | 人口稠密的                | densely populated                                                               |                      |                    |
| skew          | 不直的，歪斜的              | crooked, slanting, running obliquely                                            |                      |                    |
| pliant        | 易受影响的；易弯的            | easily influenced, pliable, malleable                                           |                      |                    |
| impertinent   | 不切题的；无礼的             | irrelevant, rude, reckless                                                      |                      |                    |
| rancid        | 不新鲜的，变味的             | rank, stinking                                                                  |                      |                    |
| antiquated    | 陈旧的，过时的；年老的          | antique, archaic, dated                                                         |                      | antiquated ideas   |
| tenable       | 站得住脚的；合理的            | defensible, reasonable                                                          |                      |                    |
| fortuitous    | 偶然发生的；偶然的            | accidental, lucky, casual, contingent, incidental                               |                      |                    |
| flamboyant    | 艳丽的；显眼的；炫耀的          | extravagant, florid, too showy or ornate                                        |                      |                    |
| cast          | 演员阵容；剧团；扔；铸造         | troupe, throw                                                                   |                      |                    |
| gouge         | 挖出，骗钱；半圆凿            | scoop out, cheat out of money, a semicircular chisel                            | gauge 准则，规范          |                    |
| reticent      | 沉默寡言的                | inclined to be slient, reserved, taciturn, uncommounicative                     |                      |                    |
| ascent        | 上升，攀登；上坡路，提高，提升      |                                                                                 |                      |                    |
| discernible   | 可识别的，可辩别的            | being recognized or identified                                                  | discern + able       |                    |
| unwieldy      | 难控制的；笨重的             | cumbersome, not easily managed                                                  |                      |                    |
| coterie       | （有共同兴趣的）小团体          |                                                                                 | cote 小屋              |                    |
| formidable    | 令人畏惧的；可怕的；难以克服的      | causing fear or dread, hard to handle or overcome                               |                      |                    |
| decimate      | 毁掉大部分，大量杀死           | to destroy or kill a large part                                                 |                      |                    |
| embellish     | 装饰，美化                | decorate                                                                        |                      |                    |
| strip         | 剥去；狭长的一片             | denude, disrobe, a long narrow piece                                            |                      |                    |
| perfidious    | 不忠的；背信弃义的            | faithless                                                                       |                      |                    |
| eviscerate    | 取出内脏；去除主要部分          |                                                                                 | viscera 内脏           |                    |
| congruent     | 全等的，一致的              |                                                                                 |                      |                    |
| brusqueness   | 唐突，直率                | candor, abruptness                                                              | brusque adj.         |                    |
| contemplation | 注视，凝视；意图，期望          |                                                                                 |                      |                    |
| eccentric     | 古怪的，反常的；古怪的人；没有共同圆心的 | unconventional                                                                  |                      |                    |
| imperative    | 紧急的                  | urgent, pressing, importunate, exigent                                          |                      |                    |
| exalted       | 崇高的；高贵的              |                                                                                 |                      |                    |
| ascribe       | 归因于；归咎于              |                                                                                 |                      |                    |
| recapitulate  | 扼要概述                 | summarize                                                                       |                      |                    |
| eulogistic    | 颂扬的；歌颂功德的            | praising highly, laudatory                                                      |                      |                    |
| iconoclastic  | 偶像破坏的；打破习俗的          |                                                                                 |                      | iconoclastic ideas |
| nocturnal     | 夜晚的，夜间发生的            | happening in the night                                                          |                      |                    |
| hilarity      | 欢闹，狂欢                | glee, jocularity, mirth, jollity, jocundity                                     |                      |                    |
| buoyant       | 有浮力的；快乐的             | showing buoyancy, cheerful                                                      |                      |                    |
| synoptic      | 摘要的                  | affording a general view of the whole                                           |                      |                    |
| infiltrate    | 渗透，渗入                | to pass through                                                                 |                      |                    |
| prescient     | 有远见的，预知的             |                                                                                 |                      |                    |
| incoherent    | 不连贯的；语无伦次的           | lacking coherence                                                               |                      |                    |
| imperturbable | 冷静的；沉着的              | cool, disimpassionated, unflappable, unruffled                                  |                      |                    |
| cacophonous   | 发音不和谐的，不协调的          | marked by cacophony                                                             | euphonious 悦耳的       |                    |
| shoal         | 浅滩，浅水处；浅的            | shallow                                                                         |                      |                    |
| evanescent    | 易消失的；短暂的             | vanishing, transient, ephemeral                                                 |                      |                    |
| reinstate     | 回复                   |                                                                                 | re + in + state      |                    |
| quaint        | 离奇有趣的；古色古香的          | unusual and attractive                                                          |                      |                    |
| fast          | 禁食，斋戒；很快的，紧紧的，深沉的    |                                                                                 |                      |                    |
| somber        | 忧郁的；阴暗的              | melancholy, dark and gloomy, dim, grave, shadowy, shady, overcast, disconsolate |                      |                    |
| bootless      | 无益处的；无用的             | futile, otiose, unavailing, useless, without advantage or benefit               |                      |                    |
| bastion       | 堡垒，防御工事              |                                                                                 |                      |                    |
| hover         | 翱翔，徘徊                |                                                                                 |                      |                    |
| defy          | 违抗，藐视                |                                                                                 |                      |                    |
| diehard       | 顽固分子                 | a fanatically determined person                                                 |                      |                    |
| sanguine      | 乐观的；红润的              | optimistic, cheerful and confident                                              |                      |                    |
| blithe        | 快乐的，无忧无虑的            | cheerful, carefree                                                              |                      |                    |
| congruity     | 一致性，适合性；共同点          |                                                                                 |                      |                    |
| absurd        | 荒谬的，可笑的              |                                                                                 |                      |                    |
| morose        | 郁闷的                  | sullen, gloomy, disconsolate                                                    |                      |                    |
| spurious      | 假的；伪造的               | forged, false, falsified                                                        |                      |                    |
| prudent       | 审慎的；精明的；节俭的，精打细算的    | judicious, sage, sane                                                           |                      |                    |
| sanctimonious | 假装虔诚的                | hypocritically pious or devout                                                  |                      |                    |
| bland         | 情绪平稳的；无味的            | pleasantly smooth, insipid                                                      |                      |                    |
| span          | 跨度，两个界限的距离           | extent, length, reach, spread, a stretch between two limits                     |                      |                    |
| plethora      | 过多，过剩                | excess, superfluity, overabundance, overflow, overmuch, overplus                |                      |                    |
| prospect      | 勘探，期望；前景             |                                                                                 |                      |                    |
| petulant      | 暴躁的，易怒的              | insolent, peevish                                                               |                      |                    |
| tangential    | 切线的；离题的              | discursive, excursive, rambling                                                 |                      |                    |
| dossier       | 卷宗，档案                |                                                                                 |                      |                    |
| ulterior      | 较远的，将来的；隐秘的，别有用心的    |                                                                                 |                      |                    |
| 13            | 13                       | 13                                                                      | 13                             | 13                |
| inflamed      | 发炎的，红肿的                  |                                                                         | infection                      |                   |
| smother       | 熄灭，覆盖，使窒息                | to cover thickly                                                        |                                |                   |
| igneous       | 火的，似火的                   | fiery, having the nature of fire                                        |                                |                   |
| nonentity     | 无足轻重的人或事                 |                                                                         |                                |                   |
| reportorial   | 记者的，报道的                  |                                                                         |                                |                   |
| perpendicular | 垂直的，竖直的                  | exactly upright, vertical                                               |                                |                   |
| prodigal      | 挥霍的，挥霍者                  | lavish                                                                  | profuse 丰富的，浪费的；squander 浪费，挥霍 |                   |
| ill-paying    | 工资低廉的                    |                                                                         |                                |                   |
| multicellular | 多细胞的                     |                                                                         |                                |                   |
| cynical       | 愤世嫉俗的                    | captious, peevish                                                       | cynic 愤世嫉俗者                    |                   |
| arboreal      | 树木的                      |                                                                         | arbor 凉亭                       |                   |
| resignation   | 听从，顺从；辞职                 |                                                                         | resign v.                      |                   |
| quiescence    | 静止，沉寂                    | quietness, stillness, tranquillity                                      | quiescent adj.                 |                   |
| dormant       | 冬眠的；静止的                  | torpid in winter, quiet, still                                          |                                |                   |
| prerogative   | 特权                       | privilege, birthright, perquisite                                       |                                |                   |
| plumb         | 测探物，铅锤；垂直的；精确的；深入了解；测量深度 |                                                                         |                                |                   |
| farce         | 闹剧，滑稽剧；可笑的行为             |                                                                         |                                |                   |
| arbitrary     | 专横的，武断的                  | discretionary, despotic, dictatorial, monocratic, autarchic, autocratic | betray 背叛；暴露                   |                   |
| efficacious   | 有效的                      |                                                                         |                                |                   |
| fractious     | （脾气）易怒的，好争吵的             | peevish, irritable                                                      | friction n.                    |                   |
| sedimentary   | 沉积的，沉淀性的                 |                                                                         | sedentary 久坐的                  | sedimentary rock  |
| condescending | 谦逊的；故意屈尊的                |                                                                         |                                |                   |
| fetid         | 有恶臭的                     | foul, noisome, smelly                                                   |                                |                   |
| cognitive     | 认知的；感知的                  |                                                                         |                                |                   |
| devastate     | 摧毁，破坏                    | ravage, destroy, depredate, wreck                                       |                                |                   |
| assail        | 质问，攻击                    | aggress, beset, storm, strike                                           |                                |                   |
| inviolate     | 不受侵犯的；未受损害的              | not violated or profaned                                                | in + violate                   |                   |
| recompose     | 重写；重新安排                  |                                                                         |                                |                   |
| replenish     | 补充；把…再备足                 |                                                                         |                                |                   |
| jubilant      | 欢呼的，喜气洋洋的                | elated, exultant                                                        |                                |                   |
| irascible     | 易怒的                      | hot-tempered, peevish, petulant, touchy, querulous, fractious, fretful  |                                |                   |
| caustic       | 腐蚀性的；刻薄的；腐蚀剂             | corrosive, biting, sarcastic                                            | acoustic 听觉的；声音的               | caustic responses |
| abrasive      | 研磨的                      |                                                                         | abrade v.                      |                   |
| recurrent     | 循环的；一再发生的                |                                                                         | re + current                   |                   |
| acerbic       | 尖酸的，刻薄的                  | bitter, sharp, harsh                                                    |                                |                   |
| cognizant     | 知道的；认知的                  |                                                                         |                                |                   |
| dissect       | 解剖；剖析                    | dichotomize, disjoin, disjoint, dissever, separate                      | dis + section                  |                   |
| ranching      | 大牧场                      | a large farm                                                            |                                |                   |
| unfailing     | 无尽的；无穷的                  | everlasting, inexhaustible                                              |                                |                   |
| potable       | 适于饮用的                    |                                                                         |                                |                   |
| veer          | 转向，改变（话题等）               | curve, sheer, slew, slue, swever                                        |                                |                   |
| autonomous    | 自治的；自主的                  |                                                                         | autonomy n.                    |                   |
| supercilious  | 目中无人的                    | coolly or patronizingly haughty                                         |                                |                   |
| idiosyncracy  | 特质                       | innate idiosyncracy                                                     |                                |                   |
| salutary      | 有益的，有益健康的                |                                                                         |                                |                   |
| myriad        | 许多的，无数的                  | innumerable, legion, multitudinous, numerous                            |                                |                   |
| skimp         | 节省花费                     | barely sufficient funds                                                 |                                | be skimp on sth.  |
| enthral       | 迷惑                       |                                                                         | thrall 王座                      |                   |
| dogged        | 顽强的                      | stubborn, tenacious,                                                    |                                |                   |
| demobilize    | 遣散，使复员                   | disband                                                                 |                                |                   |
| avocational   | 副业的，爱好的                  |                                                                         | vocation 假期                    |                   |
| rhapsodic     | 狂热的，狂喜的；狂想曲的             |                                                                         |                                |                   |
| vindictive    | 报复的                      | vengeful                                                                |                                |                   |
| equitable     | 公正的，合理的                  | dispassionate, impartial, impersonal, nondiscriminatory, objective      |                                |                   |
| domineer      | 压制                       | dominate, predominate, preponderate, prevail, reign                     |                                |                   |
| reluctance    | 勉强，不情愿                   |                                                                         | reluctant adj.                 |                   |
| purchase      | 购买；支点（阻止下滑）              |                                                                         |                                |                   |
| transcendent  | 超越的，卓越的，出众的              | ultimate, unsurpassable, utmost, uttermost, supreme                     |                                |                   |
| foul          | 污秽的，肮脏的；恶臭的；邪恶的，弄脏；犯规    |                                                                         |                                |                   |
| prig          | 自命清高者，道学先生               |                                                                         |                                |                   |
| pomposity     | 自大的行为或言论                 |                                                                         | pompous adj. pomp n.           |                   |
| blissful      | 极幸福的                     |                                                                         | bliss n. 天赐之福                  |                   |
| recondite     | 深奥的，晦涩的                  |                                                                         |                                |                   |
| culpable      | 有罪的，该受谴责的                | blameworthy, deserving blame                                            |                                |                   |
| tactile       | 有触觉的                     |                                                                         |                                |                   |
| 14            | 14            | 14                                                                                           | 14                                 | 14                                 |
| viscous       | 粘滞的，粘性的       | glutinous                                                                                    |                                    |                                    |
| venerate      | 崇敬，敬仰         | adore, revere, worship, regard with reverential respect                                      |                                    |                                    |
| flounder      | 挣扎；艰难的移动；比目鱼  | struggle                                                                                     | founder                            |                                    |
| capricious    | 变化无常的，任性的     | fickleness, fickle, inconstant, lubricious, mercurial, whimsical, whimsied, erratic, flighty |                                    |                                    |
| insolent      | 粗野的，无礼的       | impudent, boldly disrespectful                                                               |                                    |                                    |
| deplete       | 大量减少，使枯竭      | drain, impoverish, exhaust                                                                   |                                    |                                    |
| oust          | 驱逐            | force out, expel                                                                             |                                    |                                    |
| caterpillar   | 毛毛虫，蝴蝶的幼虫     |                                                                                              | cater 猫 + pillar 毛                 |                                    |
| fecund        | 肥沃的；多产的；创造力强的 | fertile, prolific, intellectually productive                                                 |                                    |                                    |
| rift          | 裂口，断裂；矛盾      | fissure, crevasse, separation                                                                |                                    |                                    |
| recuperate    | 恢复（健康、体力）；复原  | regain, recover                                                                              |                                    |                                    |
| vulnerable    | 易受伤的；脆弱的      |                                                                                              | vulnerability 脆弱性                  |                                    |
| prudish       | 过分守礼的；假道学的    | marked by prudery, priggish                                                                  |                                    |                                    |
| defecate      | 澄清，净化         | clarify                                                                                      |                                    |                                    |
| anarchy       | 无政府；政治混乱      | absence of government, political disorder                                                    |                                    |                                    |
| detriment     | 损害，伤害         | injury, damage                                                                               |                                    |                                    |
| belligerent   | 发动战争的；好斗的，挑衅的 | defiant, provoke, provocation, rebellious                                                    |                                    |                                    |
| fallacious    | 欺骗的；误导的；谬误的   | misleading, deceptive, erroneous                                                             |                                    |                                    |
| bolster       | 枕垫；支持，鼓励      | cushion, pillow, support, strengthen, reinforce                                              |                                    |                                    |
| opprobrious   | 辱骂的；侮辱的       | expressing scorn, abusive                                                                    | abrasive 研磨的                       |                                    |
| consummate    | 完全的；完善的；完成    | complete, perfect, accomplish                                                                |                                    |                                    |
| flickering    | 闪烁的，摇曳的；忽隐忽现的 | glittery, twinkling                                                                          | flicker v.                         |                                    |
| fuse          | 融化，融合         | combine                                                                                      |                                    |                                    |
| rescind       | 废除，取消         | make void                                                                                    |                                    |                                    |
| neolithic     | 新石器时代的        |                                                                                              | neo 新 + lith 石头 + ic               |                                    |
| rivet         | 铆钉；吸引（注意）     | attract                                                                                      |                                    | be riveted to the spot/ground 呆若木鸡 |
| facile        | 易做到的；肤浅的      | superficial, easily accomplished or attained                                                 |                                    |                                    |
| turmoil       | 混乱，骚乱         | tumult, turbulence                                                                           |                                    |                                    |
| impunity      | 免受惩罚          | exemption from punishment                                                                    | im + punity                        |                                    |
| insinuate     | 暗指，暗示         | imply                                                                                        |                                    |                                    |
| grim          | 冷酷的，可怕的       | ghastly, forbidding, appearing stern                                                         |                                    |                                    |
| unobtrusive   | 不引人注目的        | not + noticeable                                                                             | obtrusive 碍眼的                      |                                    |
| reprehensible | 应受谴责的         | deserving reprehension, culpable, blameworthy, blamable, blameful, censurable                | reprehend v.                       |                                    |
| potted        | 盆栽的；瓶装的，罐装的   |                                                                                              | pot                                |                                    |
| provident     | 深谋远虑的，节俭的     | prudent, frugal, thrifty                                                                     |                                    |                                    |
| concoction    | （古怪或少见的）混合物   |                                                                                              |                                    |                                    |
| anomaly       | 异常（事物）        |                                                                                              | anomalous adj.                     |                                    |
| omnipotent    | 全能的，万能的       | almighty, all-powerful                                                                       | omni 全能                            |                                    |
| countenance   | 支持，赞成；表情      | advocate, approbate, approve, encourage, favor, sanction                                     |                                    |                                    |
| detour        | 绕道，迂回；弯路，绕行之路 |                                                                                              |                                    | detour around                      |
| sanctum       | 圣所            | a sacred place                                                                               |                                    | inner sanctum                      |
| uneventful    | 平凡的，平安无事的     |                                                                                              |                                    |                                    |
| detest        | 厌恶，憎恨         | dislike intensely, hate, abhor                                                               |                                    |                                    |
| indubitably   | 无疑的，确实的       | without doubt                                                                                |                                    |                                    |
| overrule      | 驳回，否决         |                                                                                              |                                    |                                    |
| choreography  | 舞蹈术的；舞台舞蹈的    |                                                                                              | choreography n.                    |                                    |
| spectator     | 目击者；观众        | eyewitness, bystander, beholder, observer of an event                                        |                                    |                                    |
| devious       | 不坦诚的；弯曲的，迂回的  | not straightforward                                                                          |                                    |                                    |
| coercive      | 强制性的，强迫性的     |                                                                                              | coerce n.                          |                                    |
| eclectic      | 折衷的，兼容并蓄的     |                                                                                              |                                    |                                    |
| forte         | 长处，特长；强音的     | special accomplishment                                                                       |                                    |                                    |
| flock         | 羊群，鸟群         | crowd, drove, horde, host, legion, mass                                                      |                                    |                                    |
| embroider     | 刺绣，镶边；装饰      | needlework, provide embellishment                                                            |                                    |                                    |
| preempt       | 以优先权获得的；取代    |                                                                                              |                                    |                                    |
| compel        | 强迫            | coercive, concuss, oblige, force or constrain                                                |                                    |                                    |
| veracious     | 诚实的；说真话的      | truthful, honest, faithful, veridical                                                        | valorous 勇敢的                       |                                    |
| recourse      | 求助，依靠         | turning sb, for help                                                                         |                                    |                                    |
| evoke         | 引起；唤起         | educe, induce, call forth, summon, elicit, draw forth                                        |                                    |                                    |
| pervasive     | 弥漫的，遍布的       |                                                                                              | pervade v.；persuasive 易使人信服的；有说服力的 |                                    |
| circumscribe  | 划界限；限制        | restrict, restrain, limit                                                                    | circum 圆 + scribe 画                |                                    |
| impediment    | 妨碍，障碍物        | obstacle                                                                                     |                                    | an impediment to reform            |
| indolent      | 懒惰的           | idle, lazy, faineant, slothful, slowgoing, work-shy                                          |                                    |                                    |
| slanderous    | 诽谤的           | abusive, false and defamatory                                                                |                                    |                                    |
| 15             | 15                      | 15                                                                                                                         | 15                                       | 15                   |
| genre          | （文学艺术等）类型，体裁            |                                                                                                                            | genus；motif （文艺作品等的）主题，主旨                |                      |
| fluorescent    | 荧光的，发亮的                 | producing light                                                                                                            | fluo 荧光                                  |                      |
| recalcitrant   | 顽抗的                     | unruly, tenacious, stubborn, hardly                                                                                        |                                          |                      |
| succumb        | 屈从，屈服；因…死亡              |                                                                                                                            | sus 下面 + sumb 躺                          |                      |
| noisome        | 有恶臭的；令人作呕的，令人讨厌的        | annoy, foul, smelly, fetid                                                                                                 |                                          |                      |
| consecutive    | 连续的                     | serial, sequential, successively, continuously, uninterruptedly                                                            |                                          |                      |
| inaugurate     | 举行就职典礼的，创始，开创           | instate, institute, launch, originate, initiate, commence                                                                  | augur 蛟龙出海                               |                      |
| dietary        | 饮食的                     |                                                                                                                            | diet                                     |                      |
| propitiate     | 讨好；抚慰                   | assuage, conciliate, mollify, consolate, pacify, placate                                                                   |                                          |                      |
| extensive      | 广大的；多方面的；广泛的            |                                                                                                                            |                                          |                      |
| painstakingly  | 细心的，专注的；辛苦的             | fastidiously, assiduously, exhaustively, meticulously                                                                      |                                          |                      |
| precarious     | 根据不足的；未证实的；不稳固的，危险的     |                                                                                                                            | pre + car + ious 在车前面                    |                      |
| conflagration  | （建筑、森林）大火               | big, destructive fire                                                                                                      |                                          |                      |
| dwindle        | 变少，减少                   | diminish, shrink, decrease                                                                                                 |                                          |                      |
| teem           | 充满；到处都是；（雨水）倾注          | abound, in large quality                                                                                                   |                                          |                      |
| indenture      | 契约；合同                   |                                                                                                                            | indent 切割成锯齿状（古代分割成锯齿状的契约）               |                      |
| distort        | 扭曲；弄歪                   | contort, deform, misshape, torture, warp                                                                                   |                                          |                      |
| roam           | 漫步，漫步                   |                                                                                                                            | roam in room                             |                      |
| raciness       | 生动活泼                    |                                                                                                                            |                                          |                      |
| retribution    | 报应，惩罚                   |                                                                                                                            |                                          | retribution for sth. |
| ungainly       | 笨拙的                     | clumsy, awkward, gawky, lumbering, lumpish, lacking smooth or dexterity                                                    |                                          |                      |
| futile         | 无效的，无用的；没出息的            | bootless, otiose, unavailing, useless, without advantage or benefit                                                        | fertile, fertilizer, fertilize 多产、化肥、使受精 |                      |
| graze          | （让动物）吃草；放牧              |                                                                                                                            | grass 草；glaze 上釉，装玻璃                     |                      |
| antidote       | 解药                      | a remedy to counteract a poison                                                                                            | anecdotal 轶事的，趣闻的                        |                      |
| renegade       | 叛徒，叛教者                  | apostate, defector                                                                                                         | re + neg 反 + ade                         |                      |
| ostentatious   | 华美的；炫耀的                 | flamboyant, peacockish, splashy, swank, marked by or fond of conspicuous or vainglorious and sometimes pretentious display |                                          |                      |
| presumptuous   | 放肆的；过分的                 | overweening, presuming, uppity                                                                                             |                                          |                      |
| philanthropic  | 慈善（事业）的；博爱的             | characterized by philanthropy, humanitarian                                                                                |                                          |                      |
| peculiar       | 独特的；古怪的                 | distinctive, eccentric, queer, characteristic, idiosyncratic, individual                                                   |                                          |                      |
| pervade        | 弥漫，遍及                   | become diffused throughout                                                                                                 | pervasive adj.                           |                      |
| unheralded     | 未预先通知的                  | not previously mentioned, happening without any warning                                                                    |                                          |                      |
| patriarchal    | 家长的，族长的；父权制的            |                                                                                                                            | patriarchy, patriarch                    |                      |
| rowdy          | 吵闹的；粗暴的                 |                                                                                                                            |                                          |                      |
| docile         | 驯服的；听话的                 | easy to control                                                                                                            |                                          |                      |
| hitherto       | 到目前为止                   |                                                                                                                            | a hitherto unknown fact                  |                      |
| sedulous       | 聚精会神的；勤勉的               | diligent                                                                                                                   |                                          |                      |
| reside         | 居住                      | dwell permanently or continuously                                                                                          |                                          |                      |
| antiquarianism | 古物研究；好古癖                |                                                                                                                            | antique 古董；古代的                           |                      |
| contravene     | 违背（法规、习俗等）              | violate, conflict with, breach, infract, infringe, offend, transgress                                                      |                                          |                      |
| discreet       | 小心的；言行谨慎的               | prudent, modest, calculating, cautious, chary, circumspect, gingerly                                                       |                                          |                      |
| forebode       | 预感，预示；预兆                | foretell, predict, premonition, prenotion                                                                                  |                                          |                      |
| fusion         | 融合；核聚变                  | admixture, amalgam, blend, merger, mixture                                                                                 | fuse v.                                  |                      |
| esoteric       | 秘传的，机密的，隐秘的             | abstruse, occult, recondite, arcane, furtive                                                                               |                                          |                      |
| exclusive      | （人）孤僻的；（物）专用的           |                                                                                                                            |                                          |                      |
| dilate         | 使膨胀；使扩大                 | swell, expand, distent,                                                                                                    |                                          |                      |
| bane           | 祸根                      | the cause of distress, death, or ruin                                                                                      |                                          |                      |
| commiserate    | 同情，怜悯                   | sorrow or pity for sb.                                                                                                     |                                          |                      |
| spruce         | 云杉；整洁的                  | neat or smart, trim                                                                                                        |                                          |                      |
| crawl          | 爬，爬行                    |                                                                                                                            | prone position；brawl 争吵，斗殴               |                      |
| oblivious      | 遗忘的；忘却的；疏忽的             | forgetful, unmindful                                                                                                       |                                          |                      |
| disinclination | 不愿意，不情愿                 | aversion, disfavor, dislike, disrelish, dissatisfaction                                                                    |                                          |                      |
| baroque        | 过分装饰的                   | gaudily ornate                                                                                                             |                                          |                      |
| ameliorate     | 改善，改良                   | improve                                                                                                                    |                                          |                      |
| aberration     | 越轨                      | being aberrant                                                                                                             |                                          |                      |
| shunt          | 使（火车）转到另一个轨道；改变（某物的）方向的 |                                                                                                                            |                                          |                      |
| edifice        | 宏伟的建筑                   | a large, imposing building                                                                                                 |                                          |                      |
| 16             | 16                    | 16                                                                                  | 16                                                  | 16                        |
| concomitant    | 伴随的                   | accompanying, attendant                                                             | con + come + itant                                  |                           |
| irritating     | 刺激的；使生气的              | irritative, annoying                                                                |                                                     |                           |
| gush           | 涌出；滔滔不绝的说             | pour out, spout, talk effusively                                                    |                                                     |                           |
| compendium     | 简要，概略                 | digest, pandect, sketch, syllabus, summary, abstract                                | recapitulate 扼要概述；synoptic 摘要的                      |                           |
| relieve        | 减轻，解除                 | mitigate, allay, alleviate, assuage, mollify                                        |                                                     |                           |
| triumph        | 成功，胜利                 | conquer, overcome, prevail                                                          | trump                                               |                           |
| ambidextrous   | 十分灵巧的                 | bimanal, very skillful and versatile                                                | dexterous 灵巧的，熟练的                                   |                           |
| boastfulness   | 自吹自擂；浮夸               |                                                                                     | boast v.                                            |                           |
| infrared       | 红外线的                  |                                                                                     |                                                     |                           |
| deprecatory    | 不赞成的，反对的              | disapproving                                                                        | deprecate 反对，轻视                                     | be deprecatory about sth. |
| base           | 基本；卑鄙的                | devoid of high values or ethics                                                     |                                                     |                           |
| faddish        | 流行一时的，时尚的             |                                                                                     | fad n.；vogue 时尚，流行                                  |                           |
| optical        | 视觉的，光学的               | vision, scientific optics                                                           |                                                     |                           |
| disperse       | 消散，驱散                 | dispel, dissipate, scatter                                                          |                                                     |                           |
| causal         | 原因的，有因果关系的            |                                                                                     | cause v.                                            |                           |
| perennial      | 终年的，全年的；永久的，持久的       | continuing, eternal, lifelong, permanent, perpetual, enduring                       |                                                     |                           |
| ebb            | 退潮；衰退                 | decline, recede from the flood, wane                                                |                                                     |                           |
| evasive        | 回避的，逃避的；推脱的           |                                                                                     | evade v.                                            |                           |
| descriptive    | 描述的                   | serving to describe                                                                 |                                                     |                           |
| curtail        | 缩减，缩短                 | abridge, diminish, lessen, minify                                                   | curtain 窗帘                                          |                           |
| predisposed    | 使预先有倾向；使易受感染          | dispose in advance, make susceptible                                                |                                                     |                           |
| copious        | 丰富的，多产的               | plentiful, abundant                                                                 |                                                     |                           |
| monotony       | 单调，千篇一律               | tedious sameness, humdrum, monotone                                                 | autonomy 自治                                         |                           |
| exhaustiveness | 全面，彻底，详尽              |                                                                                     |                                                     |                           |
| passively      | 被动的，顺从的               |                                                                                     | passive adj.                                        |                           |
| convoluted     | 旋绕的；费解的               | coiled, spiraled, intricate, complicated, extremely involved                        |                                                     |                           |
| attribute      | 把…归于；属性，特质            | trait, virtue                                                                       |                                                     | attribute…to…             |
| huddle         | 挤成一团；一推人              | crowd, nestle close                                                                 | huddle together to handle sth.                      |                           |
| ingest         | 咽下，吞下                 | swallow                                                                             | digest 消化                                           |                           |
| servile        | 奴性的，百依百顺的             | meanly or cravenly submissive, abject                                               | serve                                               |                           |
| untainted      | 无污点的，未被玷污的            | stainless, unstained, unsullied, untarnished                                        |                                                     |                           |
| thatch         | 以茅草覆盖；茅草屋顶            |                                                                                     |                                                     |                           |
| infirm         | 虚弱的                   | physically weak                                                                     |                                                     |                           |
| scarlet        | 猩红的，鲜红的               | bright red                                                                          | scarlet fever 猩红热                                   |                           |
| unstinting     | 慷慨的，大方的               | very generous                                                                       | skinflint 吝啬鬼；stint 节制，限量，节省                        |                           |
| abysmal        | 极深的；糟透的               | bottomless, unfathomable, wretched, immeasurably bad                                |                                                     |                           |
| competence     | 胜任，能力                 |                                                                                     |                                                     |                           |
| postulate      | 要求，假定                 | demand, claim, assume, presume                                                      |                                                     |                           |
| nonviable      | 无法生存的，不能存活的           |                                                                                     | non + viable                                        |                           |
| calummy        | 诽谤，中伤                 | defamation, aspersion, calumniation, denigration, vilification                      | slanderous 诽谤的；denigrate 诽谤                         |                           |
| predator       | 食肉动物                  |                                                                                     | predatory adj.；carnivorous, flesh-eating 肉食动物的      |                           |
| commodious     | 宽敞的                   | spacious, roomy                                                                     |                                                     |                           |
| parable        | 寓言                    |                                                                                     |                                                     |                           |
| cosmopolitan   | 世界性的；全球的；世界主义者；四海为家的人 |                                                                                     | cosmos 宇宙                                           |                           |
| sterile        | 贫瘠且没有植被的；无细菌的         |                                                                                     |                                                     |                           |
| expeditiously  | 迅速的，敏捷的               | promptly, efficiently, rapidly, speedily, swiftly, agile                            |                                                     |                           |
| clutter        | 弄乱；凌乱                 |                                                                                     | scatter 四散逃窜                                        |                           |
| auspices       | 支持，赞助                 | approval and support                                                                |                                                     |                           |
| respite        | 休息，暂缓                 |                                                                                     | despite, in spite of 总是                             |                           |
| dispel         | 驱散，消除                 | scatter or drive away, disperse                                                     | prosper 繁荣                                          |                           |
| endorse        | 赞同，背书                 | approve openly                                                                      |                                                     |                           |
| probity        | 刚直，正直                 | uprightness, honesty                                                                |                                                     |                           |
| fastidious     | 挑剔的，难取悦的              | critical, discriminating                                                            |                                                     |                           |
| untapped       | 未开发的，未利用的             |                                                                                     | tap 开发，利用                                           |                           |
| impetuous      | 冲动的，鲁莽的               | impulsive, sudden                                                                   | an impetuous decision                               |                           |
| antagonize     | 使…敌对；与…对抗             | to arouse hostility, show opposition                                                |                                                     |                           |
| dilute         | 稀释，冲淡                 |                                                                                     | 假酒                                                  |                           |
| caricature     | 讽刺画；滑稽模仿              | burlesque, mock, mockery, travesty                                                  | sarcastic, sarcastic, sneering, caustic, ironic 讽刺的 |                           |
| abeyance       | 中止，搁置                 | temporary suspension                                                                | “又被压死”                                              | in abeyance               |
| expediency     | 方便；权宜之计               | advantageousness                                                                    | expeditiously 迅速的，敏捷的                               |                           |
| beset          | 镶嵌；困扰                 | harass                                                                              |                                                     |                           |
| asceticism     | 禁欲主义                  |                                                                                     | ascetic 禁欲的；苦行者                                     |                           |
| ineligible     | 不合格的                  | not legally or morally qualified                                                    | in + eligible                                       |                           |
| indented       | 锯齿状的；高低不平的            |                                                                                     | indenture 契约；合同                                     |                           |
| indented       | 锯齿状的；高低不平的            |                                                                                     | indenture 契约；合同                                     |                           |
| succinctness   | 简洁，简明                 | conciseness, concision                                                              |                                                     |                           |
| saturnine      | 忧郁的，阴沉的               | sluggish, sullen, gloom, darkness, dimness, obscurity, deep sadness or hopelessness | Saturn 土星                                           |                           |
| mordant        | 讥讽的，尖酸的               | sarcastic, sneering, caustic, ironic                                                |                                                     |                           |
| impostor       | 冒充者，骗子                |                                                                                     |                                                     |                           |
| tribulation    | 苦难，忧患                 | distress or suffering resulting from oppression or persecution                      |                                                     |                           |
| entitle        | 使有权（做某事）              |                                                                                     | en + title 权力、名义、许可                                 |                           |
| turbulent      | 混乱的，骚乱的               | causing unrest, violence, disturbance, tempestuous                                  | truculent 残暴的，凶狠的                                   |                           |
| angular        | 生硬的，笨拙的               | lacking grace or soomthness, awkward, having angles, thin and bony                  |                                                     |                           |
| precede        | 在…之前，早于               | be earlier than                                                                     |                                                     |                           |
| purport        | 意义，涵义，主旨              | meaning conveyed, implied, gist                                                     | motif （文艺作品等的）主题，主旨                                 |                           |
| altruism       | 利他主义，无私               | selflessness, altruistic                                                            |                                                     |                           |
| deceptive      | 欺骗的，导致误解的             |                                                                                     | deceive v.                                          |                           |
| curt           | （言辞，行为）简略而草率的         | brief, rude, terse                                                                  |                                                     |                           |
| 17               | 17                 | 17                                                                                                 | 17                                                                                  | 17                    |
| taunt            | 嘲笑，讥讽；嘲弄，嘲讽        |                                                                                                    | burlesque, mock, mockery, travesty, sarcastic, sarcastic, sneering, caustic, ironic |                       |
| impeccable       | 无瑕疵的               | faultless, flawless, fleckless, immaculate, indefectible, irreproachable, perfect                  |                                                                                     |                       |
| gospel           | 教义，信条              | doctrine, belief, tenet                                                                            |                                                                                     |                       |
| rationale        | 理由；基本原理            | fundamental reasons, maxim, an established principle, axiom                                        |                                                                                     |                       |
| ominous          | 恶兆的，不详的            | portentous, of an evil omen                                                                        | omen n.                                                                             |                       |
| urbane           | 温文尔雅的              | notably polite or polished in manner, refined, svelte                                              |                                                                                     |                       |
| designate        | 指定，任命；指明，指出        | indicate and set apart for a specific purpose                                                      |                                                                                     |                       |
| protrude         | 突出，伸出              | jut out                                                                                            | """convex 凸的；concave 凹的"                                                            |                       |
| illustrative     | 解说性的；用作说明的         | serving, tending, designed to illustrate                                                           | for illustrative purpose                                                            |                       |
| potshot          | 盲目射击；肆意抨击          |                                                                                                    |                                                                                     |                       |
| spherical        | 球的，球状的             | globe-shaped, globular, rotund, round                                                              |                                                                                     |                       |
| delude           | 欺骗，哄骗              | mislead, deceive, trick, beguile, bluff                                                            |                                                                                     |                       |
| cogent           | 有说服力的              | compelling, convincing, valid                                                                      |                                                                                     | cogent arguments      |
| connoisseur      | 鉴赏家，行家             |                                                                                                    | con + noissse (know) + ur                                                           |                       |
| mite             | 极小量，小虫子            |                                                                                                    | mite 原指螨虫                                                                           |                       |
| uphold           | 维护，支持              | give support to …                                                                                  |                                                                                     |                       |
| pithy            | （讲话或文章）简练有力的；言简意赅的 | concise, tersely cogent                                                                            |                                                                                     |                       |
| jolting          | 令人震惊的              | astonishing, astounding, startling, surprising                                                     |                                                                                     |                       |
| confound         | 使迷惑，搞混             | confuse                                                                                            |                                                                                     |                       |
| impute           | 归咎于，归于             | attribute                                                                                          |                                                                                     |                       |
| aspect           | （问题等）方面，面貌，外表      |                                                                                                    | a + spect                                                                           |                       |
| unremitting      | 不间断的，持续的           | never stopping                                                                                     |                                                                                     |                       |
| incomprehensible | 难以理解的，难懂的          | impossible to comprehend                                                                           |                                                                                     |                       |
| incipient        | 初期的，起初的            | inceptive, initial, initiatory, introductory, nascent, beginning                                   |                                                                                     |                       |
| exert            | 运用，行使，施加           | apply with great energy or straining effort                                                        |                                                                                     |                       |
| recur            | 重现，再发生；复发          |                                                                                                    | re + occur                                                                          |                       |
| combative        | 好斗的                |                                                                                                    | combat 与…搏斗                                                                         |                       |
| momentarily      | 暂时的，片刻，立刻          | for a moment, in a moment                                                                          | momentary adj.                                                                      |                       |
| astigmatic       | 散光的，乱视的            |                                                                                                    | a + stigma 污点 + tic 看不见污点的                                                          |                       |
| guile            | 欺骗，欺诈，狡猾           | deceit, cunning, underhanded, trickery, impostor, cheat, dissemblance, dissimluation               |                                                                                     |                       |
| drudgery         | 苦工，苦活              | dull and fatiguing work                                                                            | grudge v.                                                                           |                       |
| discredit        | 怀疑，丧失名誉            | disbelieve, disgrace, dishonor, infamous                                                           | dis + credit                                                                        |                       |
| effervescence    | 冒泡；活泼              | bubbling, hissing, foaming, liveliness, exhilaration, buoyancy, ebullience, exuberance, exuberancy |                                                                                     |                       |
| heinous          | 十恶不赦的，可憎的          | reprehensible, abominable                                                                          | hate                                                                                |                       |
| abhor            | 憎恨，厌恶              | detest, abominate, loathe, execrate, hate, antipathy,                                              | repellent, repugnant 令人厌恶的                                                          |                       |
| ostracize        | 排斥；放逐              | banish, expatriate, expulse, oust, exile by ostracism                                              |                                                                                     |                       |
| bravado          | 故作勇敢，虚张声势          | pretended courage                                                                                  | brav 勇敢 + ado                                                                       |                       |
| wistful          | 惆怅的，渴望的            | thoughtful and rather sad, pensive, yearning                                                       |                                                                                     |                       |
| anatomical       | 解剖学的               |                                                                                                    | anatomy 解剖学；autonomous 自治的，自主的                                                      |                       |
| redirect         | 改寄（信件）；改变方向        |                                                                                                    |                                                                                     |                       |
| conscientious    | 尽责的；小心谨慎的          | careful, scrupulous                                                                                |                                                                                     | be conscientious of … |
| obliqueness      | 斜度，倾斜              |                                                                                                    | oblique                                                                             |                       |
| fret             | （使）烦躁；焦虑；烦躁        | irritate, annoy, agitation, cark, pother, ruffle, vex, tension                                     |                                                                                     |                       |
| proselytize      | （使）皈依              | recruit or convert to a new faith                                                                  |                                                                                     |                       |
| despicable       | 可鄙的，卑劣的            | contemptible, deserving to be despired                                                             |                                                                                     |                       |
| adamant          | 坚决的，固执的；强硬的        | obdurate, rigid, unbending, unyielding, inflexible, unbreakable                                    | remain adamant in …                                                                 |                       |
| debase           | 贬低，贬损              |                                                                                                    | de + base                                                                           |                       |
| imperial         | 帝王的，至尊的            | IC                                                                                                 | empire 帝国                                                                           |                       |
| discursive       | 散漫的，不得要领的          | rambling and wandering                                                                             | despicable 可鄙的，卑劣的                                                                  |                       |
| dictate          | 口述；命令              |                                                                                                    | speak, read aloud, prescribe                                                        |                       |
| secular          | 世俗的，尘世的            | worldly rather than spiritual                                                                      |                                                                                     |                       |
| egoist           | 自我主义者              |                                                                                                    | egoism 自我主义；egoistic(al) 自我中心的，自私自利的                                                |                       |
| scarcity         | 不足的，缺乏的            |                                                                                                    | scarce adj.                                                                         | scarcity of …         |
| vestige          | 痕迹，遗迹              | relic, trace                                                                                       |                                                                                     |                       |
| intensification  | 增强，加剧，激烈化          |                                                                                                    | intensify v.                                                                        |                       |
| reciprocate      | 回报，答谢              | make a return                                                                                      |                                                                                     |                       |
| chronological    | 按年代顺序排列的；时间序的      |                                                                                                    |                                                                                     |                       |
| ratify           | 批准                 | confirm, approve formally                                                                          |                                                                                     |                       |
| pensive          | 沉思的；忧心忡忡的          | reflective, meditative, anxious                                                                    | pension 养老金                                                                         |                       |
| drizzly          | 毛毛细雨的              |                                                                                                    |                                                                                     |                       |
| barren           | 不育的，贫瘠的，不结果实的      | sterile, bare                                                                                      | “拔了”                                                                                |                       |
| glossary         | 词汇表，难词表，           |                                                                                                    |                                                                                     |                       |
| salient          | 显著的，突出的            | noticeable, conspicuous, prominent, outstanding                                                    |                                                                                     |                       |
| regale           | 款待，宴请；使…快乐         | feast with delicacies (delicacy)                                                                   |                                                                                     |                       |
| eloquent         | 雄辩的，流利的            | articulate, forceful and fluent expression                                                         |                                                                                     |                       |
| fused            | 熔化的                |                                                                                                    | fuse v.                                                                             |                       |
| inferable        | 能推理的，能推论的          |                                                                                                    | infer 推导                                                                            |                       |
| nubile           | 适婚的，性感的            | marriageable, sexually attractive                                                                  |                                                                                     |                       |
| fraternity       | 同行；友爱              |                                                                                                    | camaraderie 同志之情；友情                                                                 |                       |
| unadorned        | 未装饰的，朴素的           | austere, homespun, plain                                                                           |                                                                                     |                       |
| discomfort       | 使难堪，使困惑            | to make uneasy, disconcert, embarrass                                                              |                                                                                     |                       |
| 18           | 18                      | 18                                                                                                                                               | 18                                                | 18              |
| irrevocable  | 不可撤销的                   |                                                                                                                                                  | ir + revoke + able；rebuke 斥责                      |                 |
| corrosive    | 腐蚀性的，腐蚀的                | erosive, caustic                                                                                                                                 | corrode v.                                        |                 |
| exorcise     | 驱除妖魔；去除                 | expel, get rid of                                                                                                                                |                                                   |                 |
| iterate      | 重申，重做                   | do or utter repeatedly                                                                                                                           |                                                   |                 |
| debilitate   | 使衰弱                     | weaken, make weak or feeble, depress, enervate                                                                                                   |                                                   |                 |
| debauch      | 使堕落，败坏；堕落               | corrupt, deprave, pervert                                                                                                                        |                                                   |                 |
| render       | 呈递，提供；给予，归还             |                                                                                                                                                  | lender 借出者，render 归还                              |                 |
| pop          | 发出砰的一声；突然出现             |                                                                                                                                                  |                                                   |                 |
| opinionated  | 固执己见的                   |                                                                                                                                                  | opinion                                           |                 |
| scramble     | 攀登；搅乱，使混乱；争夺            |                                                                                                                                                  | scale 攀登 + amble 行走                               |                 |
| nonplussed   | 不知所措的，陷于困境的             |                                                                                                                                                  | nonplus v.                                        |                 |
| exacerbate   | 使加重，使恶化                 | aggravate disease, pain, annoyance, etc                                                                                                          |                                                   |                 |
| propitious   | 吉利的，顺利的；有利的             | auspicious, favorable, advantageous                                                                                                              |                                                   |                 |
| rekindle     | 再点火；使重新振作               |                                                                                                                                                  |                                                   | a rekindle hope |
| squalid      | 污秽的，肮脏的                 | dirty, seedy, slummy, sordid, unclean                                                                                                            |                                                   |                 |
| disarm       | 使缴械；使缓和                 |                                                                                                                                                  | dis + arm 武器                                      |                 |
| unfettered   | 自由的，不受约束的               | free, unrestrained                                                                                                                               |                                                   |                 |
| assimilate   | 同化，吸收                   | to absorb and incorporate                                                                                                                        | a + simil 相同 + ate；dissemblance, dissimluation 欺骗 |                 |
| antithesis   | 对立，相对                   | contract or opposition                                                                                                                           |                                                   |                 |
| endow        | 捐赠；赋予                   | contribute, bequeath                                                                                                                             |                                                   |                 |
| impugn       | 提出异议；对…表示怀疑             | to challenge as false or questionable                                                                                                            |                                                   |                 |
| sequester    | （使）隐退；使隔离               | seclude, withdraw, set apart                                                                                                                     | sequestrate 扣押                                    |                 |
| capitulate   | （有条件的）投降                | surrender conditionally                                                                                                                          |                                                   |                 |
| parsimony    | 过分节俭，吝啬                 | being stringy, thrift                                                                                                                            |                                                   |                 |
| singularity  | 独特；奇点（天文上密度无限大，体积无限小的点） | peculiarity, unusual or distinctive manner                                                                                                       | singular 单数的；非凡的                                  |                 |
| paramount    | 最重要的；至高无上的              |                                                                                                                                                  | para + mount 山；surmount 超过                        |                 |
| bellicose    | 好战的，好斗的                 | warlike, belligerent                                                                                                                             | bell 战斗                                           |                 |
| unliterary   | 不矫揉造作的；不咬文嚼字的           | nonliterary                                                                                                                                      |                                                   |                 |
| revile       | 辱骂；恶言相向                 | rail, use abusive language                                                                                                                       |                                                   |                 |
| acrimony     | 尖刻，刻薄                   | asperity                                                                                                                                         | bitter, sharp, harsh, acerbic, mordant 尖酸的        |                 |
| sprawl       | 散乱的延伸；四肢摊开坐、卧、躺         |                                                                                                                                                  | sprawling, rampant 植物蔓生的；（城市）无法计划开展的              |                 |
| upstage      | 高傲的                     | haughty                                                                                                                                          | up + stage                                        |                 |
| egregious    | 极端恶劣的                   | conspicuously bad, flagrant, heinous, reprehensible, abominable, repellent, repugnant, loathsome, odious, revolting, arousing disgust, repulsive |                                                   |                 |
| unscathed    | 未受损伤的；未受伤害的             | wholly unharmed                                                                                                                                  | un + scathed                                      |                 |
| converge     | 聚合；集中于一点；汇聚；收敛          |                                                                                                                                                  | con + verge 旋转到一起                                 |                 |
| perceptive   | 感知的；知觉的；有洞察力的；敏锐的       |                                                                                                                                                  |                                                   |                 |
| dissonant    | 不和谐的，不一致的               | opposing in opinion, temperament, discordant                                                                                                     |                                                   |                 |
| defect       | 缺点，瑕疵；变节，脱党             | fault, flaw, forsake, blemish                                                                                                                    | infect 感染                                         |                 |
| ethereal     | 太空的；轻巧的；轻飘飘的            |                                                                                                                                                  | ether 太空，苍天                                       |                 |
| stipulate    | 要求以…为条件；约定；规定           | to make an agreement                                                                                                                             |                                                   |                 |
| trespass     | 侵犯；闯入私人空间               | make an unwarranted or uninvited incursion                                                                                                       |                                                   |                 |
| extravagance | 奢侈，挥霍                   |                                                                                                                                                  | extravagant adj.                                  |                 |
| appall       | 使惊骇；使胆寒                 | shock, fill with horror or dismay                                                                                                                | a + pale 苍白                                       |                 |
| tortuous     | 曲折的，拐弯抹角的；弯弯曲曲的         | devious, indirect tactics, winding                                                                                                               |                                                   |                 |
| fray         | 吵架，打斗；磨破                | noisy quarrel or fight, become worn, ragged, or reveled by rubbing                                                                               |                                                   |                 |
| pestilential | 引起瘟疫的；致命的；极讨厌的          | causing pestilence, deadly, irritating                                                                                                           | pestilence 瘟疫                                     |                 |
| ramshackle   | 摇摇欲坠的                   | rickety                                                                                                                                          | ram + shake                                       |                 |
| truncate     | 截短，缩短                   | shorten by cutting off                                                                                                                           |                                                   |                 |
| inflict      | 使遭受（痛苦、损伤等）             | cause sth, unpleasant or be endured                                                                                                              |                                                   |                 |
| communal     | 全体共同的，共享的               | held in common                                                                                                                                   |                                                   |                 |
| unctuous     | 油质的；油腔滑调的               | fatty, oily, greasy, oleaginous                                                                                                                  |                                                   |                 |
| espouse      | 支持，拥护                   | auspices, uphold, corroborate, advocate, bolster, support, strengthen, reinforce, approbate, approve, encourage, favor, sanction, countenance    | a + spouse 配偶，约定                                  |                 |
| venerable    | 值得尊敬的，庄严的               | august, revered, respectable                                                                                                                     | venerate v.                                       |                 |
| frivolity    | 轻浮的行为                   | frivolous act                                                                                                                                    |                                                   |                 |
| incorrigible | 积习难改的，不可救药的             |                                                                                                                                                  | in + correct + able                               |                 |
| 19              | 19              | 19                                                                                                         | 19                       | 19                     |
| specimen        | 范例，样本，标本        |                                                                                                            | speci 物种 + men           |                        |
| schematic       | 纲要的，图解的         | of schema or diagram                                                                                       |                          |                        |
| apprise         | 通知，告知           | inform, notify, acquaint, advise, warn                                                                     | appear 出现 + rise 升起      |                        |
| defunct         | 死亡的             | dead or extinct                                                                                            |                          |                        |
| compliance      | 顺从，遵从           | obedience                                                                                                  | comply 顺从；complacence自满  |                        |
| germinate       | 发芽，发展           |                                                                                                            | germ 种子 + minate 开始发展、发芽 |                        |
| civility        | 彬彬有礼，斯文         | politeness, etiquette, courtesy, decorum                                                                   |                          |                        |
| recollect       | 回忆，想起           | remember, reminisce, retrospect, remind … of …                                                             |                          |                        |
| serviceable     | 可用的，耐久的         | fit for use                                                                                                | service n.               |                        |
| besiege         | 围攻，困扰           | overwhelm, harass, beset, beleaguer                                                                        |                          |                        |
| endemic         | 地方性的            | native, restricted in a locality or region                                                                 |                          |                        |
| denote          | 指示，表示           | mark, indicate, signify                                                                                    |                          |                        |
| confide         | 吐露（心事）；倾诉       | tell confidentially, show confidence by imparting secrets                                                  |                          |                        |
| arresting       | 醒目的，引人注意的       | catching the attention, gripping                                                                           | assert v.                |                        |
| obviate         | 排除，消除           | remove, get rid of, eliminate, exclude, preclude                                                           |                          |                        |
| rueful          | 抱憾的；后悔的，悔恨的     | showing sympathy or pity, mournful, regretful                                                              |                          |                        |
| afflict         | 折磨，使痛苦          | cause persistent pain or suffering                                                                         |                          |                        |
| chagrin         | 失望，懊恼           | annoyance and disappointment                                                                               | cha 茶 + grin 笑           |                        |
| chary           | 小心的，审慎的         | careful, cautious, calculating, circumspect, discreet, gingerly, wary, conscientious, judicious, prudent   |                          | be chary of/about      |
| scrupulous      | 恪守道德规范的，一丝不苟的   | having moral integrity, punctiliously exact, conscientious, meticulous, punctilious                        |                          |                        |
| hypocritical    | 虚伪的，伪善的         | characterized by hypocrisy                                                                                 |                          | hypocritical affection |
| abatement       | 减少，减轻           |                                                                                                            | abate v.                 |                        |
| spongy          | 像海绵的，不坚实的       | resembling a sponge, not firm or solid                                                                     |                          | spongy topsoil 松软的表土   |
| harass          | 侵扰，烦扰           | annoy persistently                                                                                         |                          | harass the border area |
| circumvent      | 回避，用计谋战胜或回避     | bypass                                                                                                     | circum 圈圈 + vent 闪现      |                        |
| deluge          | 大洪水；暴雨          | great flood, heavy rainfall                                                                                | indulge 放纵，纵容；满足         |                        |
| insane          | 无意义的，空洞的，愚蠢的    | empty, lacking sense, void, silly, innocuous, insipid, jejune, sapless, vapid                              | sane 神志清楚的；明智的           |                        |
| extricate       | 可解救的，能脱险的       | capable of being freed from difficulty                                                                     |                          |                        |
| ignominy        | 羞耻，耻辱           | shame or dishonor, infamy, disgrace, disrepute, opprobrium                                                 | ig 不 + nominy 名声         |                        |
| heretical       | 异端的，异教的         | dissident, heterodox, unorthodox                                                                           | heresy, heretic n.       |                        |
| vindicate       | 辩白；证明…正确        | free from allegation or blame                                                                              |                          |                        |
| inconsequential | 不重要的，微不足道的      | unimportant, trivial                                                                                       | in + consequential       |                        |
| indiscriminate  | 不加选择的，随意的，任意的   | without careful distinction, haphazard, random                                                             |                          |                        |
| antiquity       | 古老；古人；古迹        |                                                                                                            | antique 古董，古老的           |                        |
| uncanny         | 神秘的，离奇的         | wired, supernatural                                                                                        | un + canny 安静的，谨慎的       |                        |
| radically       | 根本上的，激进的        |                                                                                                            |                          |                        |
| overdue         | 到期未付的，晚来的，延误的   | left unpaid too long, coming or arriving after scheduled or expected time                                  | over 过了 + due 约定         |                        |
| venal           | 腐败的，贪赃枉法的       | characterized by corrupt, bribery                                                                          |                          |                        |
| quiescent       | 不动的，静止的         | inactivate, abeyant, dormant, latent                                                                       |                          |                        |
| acute           | 灵敏的，敏锐的；剧烈的，急性的 | keen, shrewd, sensitive, sharpness, severity                                                               |                          |                        |
| unnoteworthy    | 不显著的，不值得注意的     |                                                                                                            | un + noteworthy          |                        |
| gloat           | 幸灾乐祸的看，心满意足的看   |                                                                                                            |                          | gloat over sth.        |
| brevity         | 短暂              | shortness or duraion                                                                                       |                          |                        |
| pernicious      | 有害的，致命的         | noxious, deadly                                                                                            |                          |                        |
| impenetrable    | 不能穿透的；不可理解的     | incapable of being penetrated, unfathomable, inscrutable, impassable, impermeable, imperviable, impervious |                          |                        |
| enervate        | 使虚弱，使无力         | lessen the vitality ot strength of                                                                         |                          |                        |
| 20            | 20               | 20                                                                                                                   | 20                                             | 20                               |
| outlast       | 比…持久             |                                                                                                                      | out + last                                     |                                  |
| preconception | 先入之见，偏见          | prejudice, discrimination                                                                                            | preconceive adj.；pre 先 + conception 看法         |                                  |
| balk          | 梁木；大梁；妨碍；畏缩不前    | a large squared wooden beam, baffle, bilk, foil, thwart                                                              |                                                |                                  |
| carnage       | 大屠杀，残杀           | NANJING CARNAGE, bloodbath, massacre                                                                                 | carnivorous 肉食动物的；carnivore 肉食动物               |                                  |
| devout        | 虔诚的；忠诚的          | seriously concerned, totally committed                                                                               | devote 投身于                                     |                                  |
| spanking      | 强烈的，疾行的          | strong, fast                                                                                                         |                                                | a spanking pace/rate             |
| steller       | 星的，星球的           | of or relating to stars                                                                                              |                                                |                                  |
| devour        | 狼吞虎咽的吃；吞食；贪婪的看   | eat hungrily, enjoy avidly                                                                                           |                                                |                                  |
| bleach        | 去色，漂白；漂白剂        |                                                                                                                      | b + leach 过滤                                   |                                  |
| baffle        | 使困惑，难到           | confuse, puzzle, confound                                                                                            |                                                |                                  |
| edify         | 陶冶，启发            | enlighten, uplift morally and spiritually, illume, illuminate, illumine                                              |                                                |                                  |
| pejorative    | 轻视的，贬低的          | tending to disparage, depreciatory, belittle, derogate, to speak slightingly of, depreciate, decry, denounce, debase |                                                |                                  |
| ponderous     | 笨重的，（因太重太大）不便搬运的 | unwieldy or clumsy because of weight or size                                                                         | pond 大的，沉的                                     |                                  |
| particular    | 特别是；细节，详情        | fact, detail, item                                                                                                   |                                                |                                  |
| pungent       | 辛辣的，刺激的；尖锐的，尖刻的  | piquant, sharp and to the point                                                                                      | pung 尖刺 + ent                                  |                                  |
| encroach      | 侵占，蚕食            |                                                                                                                      | en 进入 + croach 钩                               |                                  |
| superb        | 上乘的，出色的          | highest degree of excellence, brilliance, or competence, lofty, sublime                                              |                                                |                                  |
| supersede     | 淘汰；取代            | take the position, force out as inferior                                                                             |                                                |                                  |
| ambivalent    | （对人对物）有矛盾看法的     | having contradictory attitudes towards sth.                                                                          |                                                |                                  |
| soliloquy     | 自言自语；戏剧独白        | talking to oneself, dramtic monologue                                                                                | solit 孤独；solitary 孤独的；隐士                       |                                  |
| heed          | 注意，留心            | give careful attention to                                                                                            | need 需要                                        |                                  |
| incentive     | 刺激，诱因，动机；刺激因素    | motive, motivate, goad, impetus, impulse, stimulus, incite                                                           |                                                |                                  |
| forsake       | 遗弃；放弃            | leave, abandon, give up, renounce                                                                                    | seek 寻觅                                        |                                  |
| stout         | 肥胖的；强壮的          | bulky in body, vigorous, sturdy                                                                                      |                                                |                                  |
| incendiary    | 防火的，纵火的          |                                                                                                                      | in + cend 发（火）光+ iary                          |                                  |
| incandescent  | 遇热发光的；发白热光的      | Incandescent lamps 白炽灯                                                                                               | candy 蜡烛，糖果                                    |                                  |
| fussy         | 爱挑剔的，难取悦的        | dainty, exacting, fastidious, finicky, meticulous                                                                    | fuzzy 模糊的                                      |                                  |
| puncture      | 刺穿，戳破，刺孔         |                                                                                                                      | punct 点 + ture                                 |                                  |
| wearisome     | 令人厌倦、疲倦          |                                                                                                                      | weary v.                                       |                                  |
| truce         | 停战，休战            | Moratorium                                                                                                           |                                                |                                  |
| publicize     | 宣传，宣扬；引人注意       |                                                                                                                      | public + ize                                   |                                  |
| nominally     | 名义上的，有名无实的       |                                                                                                                      | nominal 名义上的                                   |                                  |
| desultory     | 不连贯的，散漫的         | disconnected, not methodical, random                                                                                 |                                                |                                  |
| quash         | 镇压，（依法）取消        | suppress, nullify with judicial action, abrogate, repeal, rescind                                                    |                                                |                                  |
| incisive      | 尖锐的，深刻的          | keen, penetrating, sharp                                                                                             | incisive comments                              |                                  |
| elude         | 逃避，搞不清；理解不了      | avoid adroitly, escape the perception or understanding, eschew, evade, shun                                          |                                                |                                  |
| tenacity      | 坚持，固执            | being tenacious, doggedness, persistence, persevereness, stubbornness                                                |                                                |                                  |
| refrain       | 抑制；叠句            | curb, restrain, regular recurring phrase or verse                                                                    | re + frain 笼头                                  |                                  |
| pack          | 包裹；兽群            | a number of wild animals                                                                                             |                                                |                                  |
| sprout        | 长出，萌芽；嫩芽         | spring up, young shoot                                                                                               | spr (spring) + out                             |                                  |
| squirt        | 喷出，溅             | to spurt                                                                                                             |                                                | squirt sth. with sth.            |
| philistine    | 庸俗的人；对文化艺术无知的人   | a smug, ignorant, especially middle-class person                                                                     | Philistia 腓力斯人；煤老板                             |                                  |
| sabotage      | 阴谋破坏，颠覆活动        | deliberate subversion                                                                                                |                                                | an act of military sabotage      |
| monetary      | 金钱的；货币的          | about money, nation's currency or coinage, financial, fiscal, pecuniary                                              | money n.                                       |                                  |
| humility      | 谦逊；谦恭            | being humble, modesty                                                                                                | humble adj.                                    |                                  |
| wane          | 减少，衰落            | decrease in size, extent, scope, or degree, dwindle, abate, ebb, shrink, slacken, subside                            | swan 天鹅 wane 衰退（天鹅湖被制裁）                        |                                  |
| ingeniousness | 独创性              | ingenuity                                                                                                            |                                                |                                  |
| indignation   | 愤慨，义愤            | anger or scorn, righteous anger                                                                                      |                                                | be full of righteous indignation |
| misdirect     | 误导               | give wrong direction to                                                                                              |                                                |                                  |
| sonorous      | （声音）洪亮的          | full or loud in sound                                                                                                |                                                |                                  |
| sustain       | 承受（困难）；支撑（重量或压力） | undergo, withstand, bolster, prop, underprop                                                                         |                                                |                                  |
| actuate       | 开动，促使            | motivate, activate                                                                                                   | accurate 准确的                                   |                                  |
| counterfeit   | 伪造，仿造；伪造的        | imitate                                                                                                              | counter 反的，假的 + feit 制作                        |                                  |
| impulsive     | 冲动的，由冲动引起的；易冲动的  | arising from an impulse, prone to act on impulse, automatic, instinctive, involuntar, spontaneous                    |                                                |                                  |
| subdue        | 征服；压制；减轻         | crush, overpower, subjugate, conquer, vanquish, reduce                                                               |                                                |                                  |
| vitiate       | 削弱，损害            | make faulty or defective, impair                                                                                     | vice 恶的 + tate；revile 辱骂                       |                                  |
| dissipate     | （使）消失，消散；浪费      | break up and scatter or vanish, to waste or squander                                                                 |                                                |                                  |
| excavate      | 开洞，凿洞；挖掘         | scoop, shovel, unearth                                                                                               | ex + cave 洞 + ate                              |                                  |
| discretion    | 谨慎，审慎            | circumspection, prudence                                                                                             |                                                |                                  |
| instantaneous | 立即的，即刻的；瞬间的      | immediate, occuring                                                                                                  | instant 立即的，顷刻                                 |                                  |
| sentient      | 有知觉的；知悉的         | responsive to or conscious of sense impressions, aware                                                               |                                                |                                  |
| unregulated   | 未受控制的，未受约束的      | not controlled                                                                                                       |                                                |                                  |
| debatable     | 未决定的，有争论的        | open to dispute                                                                                                      | debate v.                                      |                                  |
| specious      | 似是而非的；华而不实的      | tawdry                                                                                                               | spec 看 + ious                                  |                                  |
| contempt      | 轻视，鄙视            | defiance, disdain, disparagement                                                                                     | depreciate, belittle, disparage, disapprove v. | contempt for …                   |
| gall          | 胆汁；怨恨            | bile, hatred, bitter feeling                                                                                         | wall 墙                                         |                                  |
| 21             | 21                  | 21                                                                                           | 21                                                                                | 21                   |
| envision       | 想象，预想               |                                                                                              | en + vision                                                                       |                      |
| intersect      | 相交；贯穿，横穿            |                                                                                              | inter + sect (section)                                                            |                      |
| aphorism       | 格言                  | maxim, adage                                                                                 |                                                                                   |                      |
| frail          | 脆弱的，不坚实的            | fragile, delicate, slender, feeble, flimsy                                                   |                                                                                   |                      |
| decompose      | （使）腐烂               | rot, decay                                                                                   | de 分解 + compose                                                                   |                      |
| instigate      | 怂恿，鼓动，煽动            | urge on, foment, incite, agitate, abet, provoke, stir                                        | in 进入 + stig (sting 刺激) + ate                                                     |                      |
| vulgar         | 无教养的，庸俗的            | morally crude, undeveloped                                                                   |                                                                                   |                      |
| formidably     | 可怕的；难对付的，强大的        | strongly                                                                                     | formidable                                                                        |                      |
| diffusion      | 扩散，弥漫；冗长，反射，漫射      | diffuseness, prolixity, reflection, transmission                                             |                                                                                   |                      |
| candor         | 坦白，直率               | frankness                                                                                    | frank, openhearted, plain, straightforward, plainspoken, unconcealed, candid adj. |                      |
| conflate       | 合并                  | combine or mix                                                                               |                                                                                   |                      |
| formalized     | 形式化的，正式的            |                                                                                              | formalize v.                                                                      |                      |
| misanthrope    | 厌恶人类者               | heats humankind                                                                              | mis + anthrop 人类                                                                  |                      |
| subsidize      | 津贴，补助               | finance, fund, sponsor, furnish with a subsidy                                               |                                                                                   |                      |
| caste          | 社会等级，等级             | class distinction                                                                            |                                                                                   |                      |
| tact           | 机智，圆滑               | keen sense of what to do or say                                                              |                                                                                   |                      |
| impersonal     | 不受个人感情影响的           | having no personal reference or connection                                                   |                                                                                   |                      |
| beaded         | 以珠装饰的               | decorated with beads                                                                         | bead n.                                                                           | beaded with sth. 缀着… |
| responsive     | 响应的，作出反应的；敏感的，反应快的  | giving response, respondent, sensitive                                                       |                                                                                   |                      |
| rage           | 盛怒；狂怒，大发雷霆          | violent and uncontrolled anger                                                               |                                                                                   | be all the rage 十分流行 |
| convivial      | 欢乐的，乐观的             |                                                                                              | con + vivial；vivid 生动的                                                            |                      |
| overshadow     | 使蒙上阴影；使黯然失色         | cast a shadow over                                                                           | over + shadow                                                                     |                      |
| incidence      | 发生，出现；发生率           | an instance of happening, the rate of occurrence or influcence                               |                                                                                   |                      |
| decrepit       | 衰老的，破旧的             | broken down or worn out                                                                      | de + crepit 破裂声                                                                   |                      |
| precipitate    | 使突然降临，加速，促成；鲁莽的，轻率的 | bring about abruptly, hasten, impetuous                                                      | peripatetic 巡游的，游动的                                                               | precipitate … into … |
| chafe          | （将皮肤等）擦热，磨破；激怒      | warm by rubbing, annoy                                                                       | café + hot                                                                        |                      |
| scheme         | 阴谋，（作品等）体系结构        | crafty or secret plan, systematic or organized framework, design                             | schema 图表                                                                         |                      |
| verdant        | 葱郁的，翠绿的             | green in tint or color                                                                       |                                                                                   |                      |
| exigency       | 紧急要求，迫切需要           | be required in particular situation                                                          | exi + gency 紧急                                                                    |                      |
| incumbent      | 在职者，现任者；义不容辞的       | holder of an office or benefice, obligatory                                                  | in + cumbent (cumbersome 阻碍的，笨重的)                                                 |                      |
| certitude      | 确定无疑                | certainty                                                                                    |                                                                                   |                      |
| stocky         | （人或动物）矮而结实的，粗壮的     | compact, sturdy, and relatively thick in build                                               | stock 树桩                                                                          |                      |
| circumstantial | 不重要的，偶然的；描述详细的      | incidental, marked by careful attention to detail                                            |                                                                                   |                      |
| euphemistic    | 委婉的                 |                                                                                              | euphemism 婉言，委婉的说法                                                                |                      |
| irreconcilable | 无法调和的，矛盾的           | incompatible, conflicting                                                                    | in + reconcilable                                                                 |                      |
| modify         | 修改，更改               | alter partially, amend                                                                       |                                                                                   |                      |
| recreational   | 娱乐的，休闲的             |                                                                                              | recreation n.                                                                     |                      |
| exorbitant     | 过分的，过度的             | excessive, extravagant, extreme, immoderate, inordinate                                      |                                                                                   | exorbitant fees      |
| overstate      | 夸张，对…言过其实           | """exaggerate, overemphasize, intensify, magnify"                                            | """"                                                                              | over + state         |
| auspicious     | 幸运的；吉兆的             | propitious, favored by future, favorable, advantageous                                       | auspices 支持，赞助                                                                    |                      |
| grave          | 严肃的，庄重的；墓穴          | serious                                                                                      | gravity n.                                                                        |                      |
| transgress     | 冒犯，违背               | violate                                                                                      |                                                                                   |                      |
| dupe           | 易上当者                | easily fooled, tricked                                                                       | “丢谱”                                                                              |                      |
| cringe         | 畏缩，谄媚               | shrink from sth, dangerous or painful, servile manner, fawn                                  |                                                                                   | cringe from …        |
| affective      | 感情的，表达感情的           | expression emotion                                                                           | affection n.                                                                      |                      |
| plump          | 丰满的，使丰满             | well-rounded                                                                                 |                                                                                   |                      |
| revival        | 苏醒，恢复，复兴            | reanimation, renaissance, renascence, resuscitation, revivification                          |                                                                                   |                      |
| grudge         | 吝惜，勉强给或承认；不满，怨恨     | be reluctant to give or submit, feel resentful                                               | drudgery 苦工，苦活                                                                    |                      |
| concede        | 承认，让步               | admit, make a conc                                                                           |                                                                                   |                      |
| haphazardly    | 偶然的，随意的，杂乱的         | accidentally, randomly, in a jumble                                                          |                                                                                   |                      |
| extraneous     | 外来的，无关的             | from the outside, not pertinent                                                              | extra + neous                                                                     |                      |
| analogous      | 相似的，可比拟的            | akin, comparable, corresponding, parallel, undifferentiated                                  |                                                                                   | be analogous to …    |
| deft           | 灵巧的，熟练的             | dexterous, skillful, adroit, handy, nimble                                                   |                                                                                   |                      |
| animosity      | 憎恶，仇恨               | feeling of strong dislike or hatred, animus, antagonism, enmity, hostility, rancor           |                                                                                   |                      |
| supple         | 柔软的，灵活的；柔顺的，顺从的     |                                                                                              |                                                                                   |                      |
| emancipate     | 解放，释放               | free from restraint                                                                          |                                                                                   |                      |
| incessant      | 不停的，不断的             | continuing without interruption                                                              |                                                                                   |                      |
| stronghold     | 要塞，堡垒，根据地           | fortified place, place of security or survival, fortress                                     | strong + hold                                                                     |                      |
| venomous       | 有毒的                 | full of venom, poisonous, deadly, vicious, virulent                                          |                                                                                   |                      |
| alloy          | 合金                  | substance composed of two or more metals, admixture, amalgam, composite, fusion, interfusion |                                                                                   |                      |
| dumbfound      | 使…惊讶                | astonish, amaze, astound                                                                     | dumb 哑 + found 被发现                                                                |                      |
| taut           | 绷紧的，拉紧的             | having no slack, tightly drawn, stiff, tense                                                 |                                                                                   |                      |
| surveillance   | 监视，盯梢               | close observation of a person                                                                | inspection, supervision                                                           |                      |
| nonporous      | 五孔的，不渗透的            | impervious, impermeable, impenetrable                                                        | non + porous 多孔的，能渗透的                                                             |                      |
| bonanza        | 富矿脉；贵金属矿            | an exceptionally large and rich mineral deposit, golconda, eldorado                          |                                                                                   |                      |
| transitoriness | 暂时，短暂               | being not persistent                                                                         |                                                                                   |                      |
| preternatural  | 异常的，超自然的            | extraordinary, existing outside the nature                                                   | preter + natural                                                                  |                      |
| reverse        | 反面，相反的事物；倒车，反转，彻底转变 | invert, revert, transplace, transpose, opposite                                              |                                                                                   |                      |
| pugnacious     | 好斗的                 | having a quarrelsome and combative nature                                                    | PUBG                                                                              |                      |
| spurn          | 拒绝，摒弃               | disdainful rejection                                                                         | squirt, spurt 喷出，溅                                                                |                      |
| dilatory       | 慢吞吞的，磨蹭的            | inclined to decay, slow or late in doing wth.                                                |                                                                                   |                      |
| 22             | 22                   | 22                                                                                                     | 22                                       | 22                      |
| infest         | 大批出没于，骚扰；寄生于         | parasite                                                                                               | in + fest 集会                             |                         |
| hunch          | 直觉，预感                | an intuitive feeling or a premonition                                                                  | have a hunch that ...                    |                         |
| regulatory     | 按规矩来的，依照规章的；调整的      |                                                                                                        |                                          |                         |
| plausible      | 看似有理的，似是而非的；有道理的，可信的 | superficially reasonable, believable, colorable, credible, creditable                                  |                                          |                         |
| audit          | 旁听；审计，审查             |                                                                                                        |                                          |                         |
| nomadic        | 游牧的；流浪的              | itinerant, peripatetic, vagabond, vagrant                                                              |                                          |                         |
| diurnal        | 白昼的，白天的              | of daytime                                                                                             | di (day) + urnal 的                       |                         |
| sanctuary      | 圣地，庇护所；庇护            | wildlife sanctuary                                                                                     |                                          |                         |
| contrive       | 计划，设计                | think up, devise, scheme, connive, intrigue, machinate, plot                                           |                                          |                         |
| cardinal       | 首要的，主要的；红衣教主         | of basic importance                                                                                    | carnivorous 肉食动物的；carnivore 肉食动物         |                         |
| vicious        | 邪恶的，堕落的；恶毒的；凶猛的，危险的  | of immorality, spiteful, malicious, dangerously, aggressive                                            | vice (邪恶) + ous                          |                         |
| unbecoming     | 不合身的；不得体的            | improper, unsuitable to the wearer                                                                     |                                          |                         |
| approbation    | 赞许；认可                | commendation, official approval                                                                        | ap + prove + bation                      |                         |
| slavish        | 卑屈的；效仿的，无创造性的        |                                                                                                        | slave + ish                              |                         |
| aghast         | 惊骇的，吓呆的              | terrified, frightened, scared, scary                                                                   | ghost 鬼                                  |                         |
| intimate       | 亲密的；密友；暗示            | closely acquainted, an intimate friend, hint or imply, suggest                                         |                                          |                         |
| paragon        | 模范，典范                | paradigm, exemplar, a model of excellence or perfection                                                |                                          |                         |
| pitcher        | 有柄水罐                 | jug, a container of liquids with a handle                                                              |                                          |                         |
| rudimentary    | 初步的；未充分发展的           | fundamental, elementary                                                                                |                                          |                         |
| hieroglyph     | 象形文字，神秘符号            |                                                                                                        | hieroglyphic writing                     |                         |
| disdain        | 轻视，鄙视                | contempt, contemn, despise                                                                             | disdain for …                            |                         |
| grandiloquence | 豪言壮语，夸张之言            |                                                                                                        | grand                                    |                         |
| picturesque    | 如画的；独特的，别具风格的        | resembling a picture, charming or quaint in appearance                                                 |                                          |                         |
| unspoiled      | 未损坏的，未宠坏的            |                                                                                                        | un + spoiled                             |                         |
| nonradioactive | 非放射性的                |                                                                                                        | non + radioactive                        | nonradioactive labeling |
| snub           | 冷落，不理睬               | treat with contempt or neglect, cold-shoulder, rebuff                                                  |                                          |                         |
| prototype      | 原型；典型                | an original model, archetype, standard or typical example                                              |                                          |                         |
| paleolithic    | 旧石器时代的               |                                                                                                        | neolithic 新石器时代的                         |                         |
| undercut       | 削价（与竞争者）抢生意          |                                                                                                        |                                          |                         |
| dehydrate      | 使脱水                  | remove water from                                                                                      | de + hydr + ate                          |                         |
| recoil         | 弹回，反冲；退却，萎缩          |                                                                                                        | re 反 + coil 卷，盘绕                         |                         |
| evocative      | 唤起的，激起的              | tending to evoke                                                                                       |                                          |                         |
| distraught     | 心神狂乱的；发狂的；心烦意乱的      | mentally confused, distressed                                                                          | distract v. 分散注意力，使不安                    |                         |
| digressive     | 离题的，枝节的              | characterized by digressions                                                                           |                                          | digressive remarks      |
| blackmail      | 敲诈，勒索                | payment extorted by threatening                                                                        | black (黑) + mail                         |                         |
| replicate      | 复制                   | produce a replica                                                                                      | re + plic 折叠 + ate                       |                         |
| snug           | 温暖舒适的                | warm and comfortable, cozy                                                                             |                                          | snug harbor             |
| murderous      | 蓄意谋杀的，凶残的；极厉害的，要命的   |                                                                                                        | murder v.                                |                         |
| odious         | 可憎的；令人作呕的            | disgusting, offensive                                                                                  | “呕得要死”                                   |                         |
| untold         | 无数的，数不清的             | too numerous to count                                                                                  |                                          |                         |
| dubious        | 可疑的；有问题的，靠不住的        | slightly suspicious, disputable, dubitable, equivocal, problematic                                     |                                          |                         |
| astute         | 机敏的，精明的              | cunning, crafty, shrewd mind                                                                           | astus 灵活                                 |                         |
| divulge        | 泄漏，透露                | disclose, betray, reveal                                                                               |                                          |                         |
| repudiation    | 拒绝接受，否认              |                                                                                                        | repudiate v.                             |                         |
| perpetuate     | 使永存                  |                                                                                                        | perpetual adj.                           |                         |
| vent           | 发泄（感情，尤指愤怒）；开孔；孔，口   | discharge, expel, an opening                                                                           |                                          |                         |
| onerous        | 繁重的，费力的              | burdensome                                                                                             | oner (over 负担) + ous                     |                         |
| sap            | 树液；汁液；活力；削弱，耗尽       | liquid, vigor, vitality, weaken, exhaust, attenuate, cripple, debilitate, enfeeble, unbrace, undermine |                                          |                         |
| raisin         | 葡萄干，提子               | dried grape                                                                                            |                                          |                         |
| impressionable | 易受影响的                | easily affected by impressions                                                                         |                                          |                         |
| voluptuous     | 撩人的，沉溺酒色的            |                                                                                                        | volupt 享乐,挥霍 + uous                      |                         |
| intemperance   | 放纵，不节制；过度            | lack of temperance, excess, surfeit, overindulgence                                                    | in +temperance                           |                         |
| inert          | 惰性的；呆滞的              | chemical inactive, dull                                                                                | in 不 + ert 动的                            |                         |
| untutored      | 未受教育的                |                                                                                                        | un + tutored                             |                         |
| discourse      | 演讲，论述                | dissertation                                                                                           | dis + course 课程                          |                         |
| contact        | 接触；互通信息              | touch, get in communication with, contingence, commerce, communion, intercourse                        |                                          |                         |
| ductile        | 易延展的；可塑的             | capable of being stretched, easily molded, pliable                                                     | Doctrine 教义                              |                         |
| crestfallen    | 挫败的，失望的              | dejected, disheartened, humbled                                                                        | crest 鸡冠 + fallen 下垂                     |                         |
| restitution    | 归还；赔偿                | restoration                                                                                            |                                          |                         |
| opportune      | 合适的，适当的              | right for a particular purpose                                                                         |                                          |                         |
| modulate       | 调整，调节；调音             | adjust, tune                                                                                           |                                          |                         |
| conversant     | 精通的，熟练的              | versed, familiar and acquainted                                                                        | versed                                   |                         |
| dental         | 牙齿的，牙科的              | of or relating to teeth or dentistry                                                                   | dent 牙齿 + al 的                           |                         |
| essentially    | 本质上，基本上              | basically                                                                                              | essential adj.                           |                         |
| porous         | 可渗透的；多孔的             | capable of being penetrated, full of pores                                                             | pore 孔                                   |                         |
| disposable     | 一次性的；可自由使用的          |                                                                                                        |                                          |                         |
| conserve       | 保存，保藏                | keep in safe and sound state                                                                           |                                          |                         |
| cyclical       | 循环的                  | recurring in cycles                                                                                    | cycle n.                                 |                         |
| misogyny       | 厌恶女人；厌女症             | hatred of women                                                                                        |                                          |                         |
| alternative    | 轮流的，交替的；二选一的         | alternate, backup, substitute, surrogate                                                               |                                          |                         |
| immutability   | 不变（性）                |                                                                                                        | mute v.                                  |                         |
| brownish       | 褐色的                  |                                                                                                        | drab 黄褐色的；单调的，乏味的                        |                         |
| coercion       | 强制，高压统治              | compulsion, constraint, enforcement                                                                    | coerce v.                                |                         |
| tribal         | 部落的，部族的              |                                                                                                        | tribe n.                                 |                         |
| immobile       | 稳定的，不动的，静止的          | fixed, motionless                                                                                      |                                          |                         |
| unbridled      | 放纵的；不受约束的            | unrestrained, unchecked, uncurbed, ungoverned, indulgent                                               |                                          |                         |
| bereave        | 夺去，丧亲                | deprive, dispossess, disinherit, divest, oust                                                          |                                          |                         |
| yearn          | 盼望，渴望                | long persistently, crave, hanker, lust, pine                                                           | year + n                                 |                         |
| informality    | 非正式，不拘礼节             |                                                                                                        |                                          |                         |
| intrinsic      | 固有的，内在的，本质的          | congenital, connate, elemental, inherent, innate                                                       |                                          |                         |
| downfall       | 垮台                   | sudden fall, overthrow, upset                                                                          |                                          |                         |
| trace          | 痕迹，追踪                | engram, relic                                                                                          | truce 停战，休战                              |                         |
| warrantable    | 可保证的，可承认的            |                                                                                                        | warrant v.                               |                         |
| resonant       | （声音）洪亮的；回响的，共鸣的      | enriched by resonance, echoing                                                                         |                                          |                         |
| trauma         | 创伤，外伤                | injury caused by extrinsic agent                                                                       |                                          |                         |
| sophomoric     | 一知半解的                |                                                                                                        | dilettante, dabbler, amateur 一知半解者，业余爱好者 |                         |
| formation      | 组成，形成；编队，排列          |                                                                                                        | form + ation                             |                         |
| pilgrim        | 朝圣者；（出国）旅行者          | wayfarer                                                                                               |                                          |                         |
| unreserved     | 无限制的；未被预定的           |                                                                                                        | un + reserved                            |                         |
| captious       | 吹毛求疵的                | carping                                                                                                | quibble, cavil 吹毛求疵                      |                         |
| sordid         | 卑鄙的，肮脏的              | dirty, filthy, foul, mean, seedy                                                                       |                                          |                         |
| 23            | 23                | 23                                                                            | 23                                        | 23                 |
| levy          | 征税；征兵             |                                                                               | impose a tax, draft into military service |                    |
| pledge        | 誓言，保证；发誓          | solemn promise, vow to do sth., commitment, swear                             |                                           |                    |
| sublimate     | 升华，净化             | sublime                                                                       |                                           |                    |
| myopic        | 近视眼的；目光短浅的        | lack of foresight or discernment                                              | "cryptic 秘密的，神秘的                          | "                  | a myopic child |
| serpentine    | 像蛇般蜷曲的；蜿蜒的        | winding or turning one way to another                                         | serpent 蛇                                 |                    |
| lump          | 块，肿块；形成块状         | become lumpy                                                                  |                                           | lump … together    |
| labile        | 易变化的；不稳定的         | open to change, unstable, fickle, unsteady                                    |                                           |                    |
| retort        | 反驳                | counter argument                                                              |                                           |                    |
| stultify      | 使显得愚蠢，使变的无用或无效    | impair, invalidate, negate, to make stupid, to render useless                 |                                           |                    |
| abstinent     | 饮食有度的；有节制的，禁欲的    | constraining from indulgence                                                  |                                           |                    |
| reel          | 卷轴，旋转；卷…于轴上       | wind on a reel                                                                |                                           | a reel of flim     |
| upheaval      | 动乱，剧变             | extreme agitation or disorder                                                 | upheave v.                                |                    |
| despotic      | 专横的，暴虐的           | autocratic, tyrannical                                                        | despot 暴君                                 |                    |
| lank          | 细长的；长、直且柔软的       | long, thin, straight, thin, and limp, slender                                 |                                           |                    |
| outmaneuver   | 以策略制胜             |                                                                               | out + maneuver                            |                    |
| epidermis     | 表皮，外皮             | the outmost layer of the skin                                                 |                                           |                    |
| felon         | 重罪犯               | guilty of a major crime                                                       |                                           |                    |
| ventriloquist | 口技表演者，腹语表演者       |                                                                               | ventriloquism                             |                    |
| maraud        | 抢劫，掠夺             | pillage, loot, rob                                                            |                                           |                    |
| synopsis      | 摘要，概要             | condensed statement                                                           | hypnotic 催眠的；催眠药                          |                    |
| stouthearted  | 刚毅的，大胆的           | brave or resolute                                                             | stout 勇敢的刚毅的                              |                    |
| picayunish    | 微不足道的，不值钱的        | of little value, petty, small-minded                                          | pica 少的                                   |                    |
| crimp         | 使皱起，使卷曲；抵制，束缚     |                                                                               |                                           |                    |
| pacify        | 使安静，抚慰            | make calm, quiet, and satisfied                                               |                                           |                    |
| ciliated      | 有纤毛的，有睫毛的         |                                                                               | cili 毛                                    |                    |
| rebuff        | 断然拒绝              | reject or criticize sharply, snub                                             | re 反+ buff 喷                              |                    |
| sacrament     | 圣礼，圣事             |                                                                               | sacred adj.                               |                    |
| hoist         | 提升，升起；起重机         | raise or haul up, lift, elevate                                               |                                           |                    |
| whet          | 磨快；刺激             | sharpen, excite, stimulate, acuminate, edge, hone                             |                                           |                    |
| concord       | 一致；和睦             | agreement, harmony                                                            | con                                       |                    |
| befuddle      | 使迷惑不解；使醉酒昏迷       | confuse, muddle, stupefy                                                      |                                           |                    |
| cuddle        | 搂抱，拥抱             | hold lovingly and gently, embrace and fondle                                  | “扑到”                                      |                    |
| swindle       | 诈骗                |                                                                               | wind                                      |                    |
| environ       | 包围，围绕             | encircle, surround                                                            | environment                               |                    |
| ordinance     | 法令，条例             | governmental statue of regulation                                             | ordin 命令 + ance                           |                    |
| salve         | 药膏；减轻，缓和          |                                                                               | salv 救援 + e                               |                    |
| canyon        | 峡谷                | a long, narrow valley between cliffs                                          |                                           |                    |
| pollster      | 民意调查员             | conducts a poll                                                               | poll + ster                               |                    |
| lurk          | 潜伏，埋伏             | stay hidden, lie in wait                                                      | lark 云雀                                   |                    |
| expunge       | 删除                | erase, delete, cancel                                                         |                                           |                    |
| insouciance   | 漠不关心，漫不经心         | lighthearted unconcern                                                        |                                           |                    |
| bouffant      | 蓬松的，膨胀的           | puffed out, puffy                                                             |                                           |                    |
| waddle        | 摇摇摆摆的走            |                                                                               | “歪道“                                      |                    |
| defile        | 弄污，弄脏；峡谷，隘路       | make filthy or dirty, pollute, narrow valley or mountain pass                 | de + file (vile 卑鄙)                       |                    |
| jaundiced     | 有偏见的              | prejudiced                                                                    | jaundice n.                               |                    |
| tinder        | 火绒，火种             | sth, that serves to incite or inflame                                         |                                           |                    |
| stench        | 臭气，恶臭             | stink                                                                         | annoy, foul, smelly, fetid 恶臭的            |                    |
| defuse        | 拆除引信，使去除危险性；平息    |                                                                               | de + fuse 导火索                             |                    |
| bouncing      | 活泼的，健康的           | lively, animated, good healthy                                                |                                           |                    |
| autocracy     | 独裁政府              |                                                                               | auto 自己 + cracy 统治                        |                    |
| svelte        | （女人）体态苗条的         | slender, lithe, slim, willowy                                                 |                                           |                    |
| wretched      | 可怜的，不幸的，悲惨的       | in a very unhappy and unfortunate state                                       |                                           |                    |
| lopsided      | 倾向一方的，不平衡的        | lacking in symmetry, balance, or proportion                                   | lop + side 一边 + d                         |                    |
| plain         | 简单的，清楚的；不漂亮的；平原   | simple, clear                                                                 |                                           |                    |
| swoop         | 猛扑，攫取             | move in a sudden sweep, seize or snatch                                       |                                           |                    |
| mesmerism     | 催眠术，催眠引导法         | hypnotic induction                                                            | invented by Mesmer                        |                    |
| pushy         | 有进取心的，爱出风头的，固执己见的 | aggressive                                                                    |                                           |                    |
| plunder       | 抢劫，掠夺             | maraud, pillage                                                               | pl (place) + under                        |                    |
| epilogue      | 收场白；尾声            | a closing section                                                             |                                           |                    |
| ensue         | 接着发生              | happen afterwards                                                             | en 进入 + sue跟从                             |                    |
| soggy         | 湿透的               | clammy, dank, sopping, waterlogged, saturated or heavy with water or moisture |                                           |                    |
| convene       | 集合；召集             | come together, assemble, call or meet                                         | con 一起 + ven 来                            |                    |
| presage       | 预感，预示             |                                                                               | per 早 + sage (sage 智者，智慧）                 |                    |
| guzzle        | 大吃大喝；大量消耗         | drink and eat greedily and immoderately                                       |                                           |                    |
| pall          | 令人发腻；失去吸引力        |                                                                               |                                           |                    |
| emaciation    | 消瘦；憔悴；衰弱          | being weaker, boniness, gauntness, maceration                                 | emaciate v.                               |                    |
| riot          | 暴动，闹事             | to create or engage in a riot, frolic, revel, carouse                         |                                           |                    |
| artifice      | 巧妙方法；诡计           | skill and ingenuity, a sly trick                                              |                                           |                    |
| capacious     | 容量大的，宽敞的          | containing a great deal, spacious                                             |                                           |                    |
| dispatch      | 派遣；迅速处理；匆匆吃完；迅速   | send off promptly, dispose efficiently, promptness, haste                     |                                           |                    |
| harp          | 竖琴，弹竖琴；喋喋不休的说或写   | excessive and tedious                                                         |                                           |                    |
| gutter        | 水槽，街沟             | channel at the edge of a street                                               | gut 肠胃 + ter                              |                    |
| gangling      | 瘦长的难看的            | tall, thin and awkward-looking                                                | “杠铃”                                      |                    |
| wispy         | 纤细的，脆弱的，一缕缕的      |                                                                               |                                           | wispy hair         |
| toll          | 通行费；代价，损失；敲       |                                                                               | “痛”                                       |                    |
| inveigle      | 诱骗，诱使             | win with deception, lure                                                      |                                           |                    |
| scamper       | 奔跑，蹦蹦跳跳           | run nimbly and playfully                                                      | s + camper 露营者                            |                    |
| mumble        | 咕哝，含糊不清的说         | grumble, murmur, mutter, speak or say unclearly                               |                                           |                    |
| thrust        | 猛力推；刺，戳           | push or drive with force, stab, pierce                                        |                                           | thrust … upon/on … |
| reproof       | 责备，斥责             | criticism for a fault, rebuke, admonishment, reprimand, reproach, scolding    |                                           |                    |
| offish        | 冷淡的               | distant and reserved                                                          | off + fish 没有鱼了                           |                    |
| contaminate   | 弄脏，污染             | make impure, pollute, smudge                                                  | con + tamin 接触 + ate                      |                    |
| tamp          | 夯实，捣实             |                                                                               | “踏”                                       |                    |
| gaiety        | 欢乐，快活             | cheerfulness                                                                  | gay adj.                                  |                    |
| impenitent    | 不知悔悟的             | without regret, unrepentant                                                   | im + penitent                             |                    |
| ken           | 视野，知识范围           | perception                                                                    |                                           | beyond one's ken   |
| 24            | 24                   | 24                                                                                           | 24                               | 24                            |
| voracity      | 贪食，贪婪                | greed, rapacity                                                                              | voracious adj.                   |                               |
| chide         | 斥责，责骂                | scold, reprove mildly                                                                        | chide child                      |                               |
| abate         | 减轻，减少                | wane                                                                                         | a + bate 减少                      |                               |
| indelible     | 擦拭不掉的，不可磨灭的          |                                                                                              | in + del(delete) + ible          |                               |
| spatter       | 洒，溅                  | slop, spray, swash, splash with a liquid                                                     |                                  |                               |
| apprentice    | 学徒                   |                                                                                              | ap 接近 + prent (prehend 抓住) + ice |                               |
| induct        | 使就职；使入伍              | install, enroll in the armed forces                                                          |                                  |                               |
| confidant     | 心腹朋友，知己，密友           | one to whom secrets are entrusted                                                            |                                  |                               |
| snide         | 挖苦的，讽刺的              | slyly disparaging, insinuating, sarcastic, sneering, caustic, ironic                         |                                  |                               |
| sport         | 炫耀，卖弄，运动             | display ostentatiously                                                                       |                                  | sport a beard                 |
| verbiage      | 冗词，废话                | verbose                                                                                      |                                  |                               |
| foreword      | 前言，序                 | prefatory comments                                                                           | fore + word                      |                               |
| ingress       | 进入                   | entering                                                                                     |                                  |                               |
| entwine       | 使缠绕，交织               | twine, weave, twist together                                                                 | en + twine 缠绕                    |                               |
| abound        | 大量存在；充满，富于           |                                                                                              | a 没有 + bound 边界                  |                               |
| linger        | 逗留，继续存留；徘徊           | continue to stay                                                                             | linguistic 语言的                   |                               |
| ripple        | （使）泛起涟漪；波痕，涟漪        |                                                                                              |                                  | a ripple of applause/laughter |
| lope          | 轻快的步伐；大步跑            | easy, swinging stride, run with steady gait                                                  | lobe 耳垂                          |                               |
| disclaim      | 放弃权利；拒绝承认            | renounce, deny                                                                               |                                  |                               |
| recline       | 斜倚，躺卧                | lie down                                                                                     |                                  |                               |
| recess        | 休假；凹槽                | alcove, cleft, suspension of business                                                        | re 反着 + cess 走                   |                               |
| enjoin        | 命令，吩咐                | command, direct or impose by authoritative order                                             | en 使 + join                      |                               |
| brew          | 酿酒；沏茶；酝酿             |                                                                                              |                                  | brew beer or ale              |
| earthy        | 粗俗的，土气的              | rough, plain in taste                                                                        | earth                            |                               |
| falsehood     | 谎言                   | untrue statement                                                                             | false + hood                     |                               |
| filibuster    | 妨碍议事，阻挠提案通过；阻挠议事的人   | obstruct the passage of                                                                      | “费力拍死他”                          |                               |
| upswing       | 上升，增长；进步，改进          | increase, improvement                                                                        | up + swing                       |                               |
| miserly       | 吝啬的                  | penurious                                                                                    | miser 吝啬鬼                        |                               |
| garnish       | 装饰                   | decorate, embellish                                                                          | gar 花 + nish                     |                               |
| analogy       | 相似，类比                | partial resemblance                                                                          | ana 并列 + log 说话 + y              |                               |
| distrait      | 心不在焉的                | absent-minded, distracted                                                                    |                                  |                               |
| slay          | 杀戮，杀死                | kill violently in great numbers                                                              |                                  |                               |
| chic          | 漂亮的，时髦的              | modish, posh, swanky, cleverly stylish, currently fashionable                                |                                  |                               |
| iconoclast    | 反传统的人；偶像破坏者          |                                                                                              | icon 偶像，圣像 + clast 破坏者           |                               |
| flay          | 剥皮；抢夺，掠夺；严厉指责        | strip off the skin, rob, pillage, criticize or scold mercilessly                             | fray 吵架，打架                       |                               |
| retch         | 作呕，恶心                | vomit                                                                                        |                                  |                               |
| occlude       | 使闭塞                  | prevent the passage of                                                                       | oc + clud 关闭                     |                               |
| traipse       | 漫步，闲荡                | wander, ramble, roam, jog                                                                    |                                  |                               |
| squeamish     | 易受惊的；易恶心的            | easily shocked or sickened                                                                   |                                  | the squeamish                 |
| unsettling    | 使人不安的，扰乱的            | upsetting, disturbing, discomposing                                                          | unsettle v.                      |                               |
| overhaul      | 彻底检查；大修              | check/repair thoroughly                                                                      |                                  |                               |
| dainty        | 美味，精致的食品；娇美的；挑剔的     | small tasty piece of food, especially a tiny cake, delicately pretty, fastidious, particular |                                  |                               |
| inoculate     | 注射预防针                | inject a serum, vaccine to create immunity                                                   |                                  |                               |
| rendering     | 表演，翻译                | performance, translation                                                                     |                                  |                               |
| procrastinate | 耽搁，拖延                | put off intentionally and habitually                                                         | pro + crastin 明天 + ate           |                               |
| cozen         | 欺骗，哄骗                | coax, deceive                                                                                | dozen 一打                         |                               |
| saddle        | 鞍，马鞍                 | a seat of one who rides on horseback                                                         |                                  |                               |
| scotch        | 镇压，扑灭                | put an end to                                                                                | Scotch 苏格兰                       |                               |
| consequential | 傲慢的，自大的；相应的          | self-important, arrogate                                                                     |                                  |                               |
| sequestrate   | 扣押，没收                | seclude, sequester, place property in custody                                                |                                  |                               |
| scare         | 惊吓，受惊，惊恐             | frighten especially suddenly                                                                 |                                  |                               |
| chipper       | 爽朗的，活泼的              | sprightly                                                                                    | chip 芯片                          |                               |
| loiter        | 闲逛，游荡；慢慢前行；消磨时光，速度光阴 | linger, move indolently                                                                      |                                  |                               |
| drub          | 重击，打败                | beat severely, defeat decisively                                                             |                                  |                               |
| obsolete      | 废弃的，过时的              | no longer in use, out of date, old-fashioned                                                 |                                  |                               |
| trim          | 修剪，井井有条的             | make neat by cutting and clipping                                                            |                                  |                               |
| complaisance  | 彬彬有礼；殷勤；柔顺           |                                                                                              |                                  |                               |
| brisk         | 敏捷的，活泼的；清新健康的        | quick, lively, healthy feeling                                                               |                                  |                               |
| amplify       | 放大；详述                | extend                                                                                       | ample 大的                         |                               |
| bust          | 半身（雕）像               |                                                                                              | bust in bush 灌木丛                 |                               |
| furor         | 喧闹，轰动；盛怒             | fashionable craze, frenzy, rage, uproar                                                      | fury 狂怒                          |                               |
| enfetter      | 给…上脚镣；束缚，使受制于        | bind in fetters, enchain                                                                     | en 进入 + fatter 镣铐                |                               |
| asinine       | 愚笨的                  | of asses, stupid, silly                                                                      | as (ass) + in + in + e           |                               |
| abash         | 使羞愧；使尴尬              | make embarrassed                                                                             | a+bash                           |                               |
| subterfuge    | 诡计，托辞                | a deceptive device or stratagem                                                              | subter 私下+ fuge 逃跑               |                               |
| hardihood     | 大胆，刚毅；厚颜无耻           | boldness, fortitude, brazen                                                                  | hardy 强壮的，大胆的，勇敢的                |                               |
| douse         | 把…浸在水中的；熄灭           | plunge into water, extinguish                                                                | do + use 又做又用                    |                               |
| dispense      | 分配，分法                | to distribute in portions                                                                    |                                  |                               |
| uproarious    | 骚动的，喧嚣的；令人捧腹大笑的      | boisterous, funny, rancorous                                                                 | uproar n.                        |                               |
| zoom          | 急速上升；猛增              | increase sharply, hike, skyrocket, surge                                                     |                                  |                               |
| mandate       | 命令，训诫                | authoritative order or command                                                               | mand 命令 + ate                    |                               |
| incriminate   | 连累，牵连                | involve in                                                                                   | in + crim (crime) + in + ate     |                               |
| affected      | 不自然的；假装的             | behaving in an artificial way, assumed, factitious, fictitious, unnatural                    |                                  |                               |
| elephantine   | 笨拙的，巨大的              | clumsy, having enormous size, massive                                                        | elephant 大象                      |                               |
| churl         | 粗鄙之人                 | surly, ill-bred person                                                                       | no churl in church               |                               |
| collage       | 拼贴画                  | artistic composition made of various materials                                               | college 学院                       |                               |
| ulcerate      | 溃烂                   |                                                                                              | ulcer 溃疡                         |                               |
| burlesque     | 讽刺或滑稽的戏剧             | derisive caricature, parody                                                                  | “不如乐死他”                          |                               |
| flimflam      | 欺骗；胡言乱语              | deception, deceptive nonsense, poppycock, swindle                                            | film 电影 + flam 说                 |                               |
| wheeze        | 喘息；发出呼哧呼哧声音          | breath with difficulty, laborious breathing sound                                            |                                  |                               |
| wince         | 畏缩，退缩                | flinch, cringe, quail, shrink back involuntarily                                             |                                  |                               |
| gobble        | 狼吞虎咽，贪婪的吃            | eat greedily, gulp, swallow, devour                                                          | gob n. 大块                        |                               |
| dint          | 击打出凹痕；力              | make a dent in                                                                               | by dint of … 凭借                  |                               |
| evacuate      | 撤离；疏散                | withdraw, remove inhabitants for protective purpose                                          |                                  |                               |
| plateau       | 高原；平稳时期              | tableland, relatively stable period                                                          |                                  |                               |
| snobbish      | 势利眼的；假充绅士的           |                                                                                              | snob 势利小人                        |                               |
| feline        | 猫的，猫科的               |                                                                                              | fel 猫 + ine                      |                               |
| bruise        | 受伤，擦伤                | to injure the skin                                                                           | cruise 乘船巡游                      |                               |
| lymphatic     | 无力的；迟缓的，淋巴的          | lacking in physical or mental energy                                                         | lymph 淋巴                         |                               |
| abase         | 降低…的地位；贬抑；使卑下        | lower/degrade one's dignity                                                                  | a + base 贬低                      |                               |
| infatuate     | 使迷恋；使糊涂              | inspire with a foolish or extravagant love or admiration                                     |                                  |                               |
| pecan         | 山核桃                  | reddish shell                                                                                | “皮啃”                             |                               |
| limp          | 跛行；软弱的，无力的；柔软的       | walk lamely, flaccid, drooping, floppy, loose                                                |                                  |                               |
| vitalize      | 赋予生命，使有生气            | endow with life, energize, exhilarate, invigorate, stimulate                                 |                                  |                               |
| execrable     | 可憎的，讨厌的              | deserving to be execrated, abominable, detestable                                            |                                  | execrable poetry              |
| harpsichord   | 键琴（钢琴前身）             |                                                                                              | harp 竖琴 + si + chord 琴弦          |                               |
| inmate        | 同居者                  |                                                                                              | in + mate 住在一起                   |                               |
| molest        | 骚扰，困扰                | bother or annoy                                                                              | mo 磨 + lest                      |                               |
| lattice       | 格子架；格子               |                                                                                              | l + attic 阁楼 + e                 |                               |
| 25           | 25                  | 25                                                                      | 25                   | 25    |
| traverse     | 横穿；横跨               | across or over                                                          | tra + vers + e       |       |
| insuperable  | 难以克服的               | impossible to overcome                                                  | in + super + able    |       |
| implore      | 哀求，恳求               | beg                                                                     | im 进 + plor 哭泣 + e   |       |
| mattress     | 床垫                  | sleep on                                                                |                      |       |
| nifty        | 极好的，极妙的             | good and attractive, great                                              |                      |       |
| furbish      | 磨光，刷新               | polish, renovate                                                        | furnish 装饰           |       |
| persnickety  | 势利的；爱挑剔的            | snobbish, fuzzy, fastidious                                             |                      |       |
| crass        | 愚钝的，粗糙的             | crude and unrefined                                                     |                      |       |
| confess      | 承认，供认               | admit                                                                   |                      |       |
| abrogate     | 废止，废除               | abolish, repeal by authority                                            | ab + rog + ate       |       |
| dissimulate  | 隐藏，掩饰               | pretend, dissemble                                                      |                      |       |
| duress       | 胁迫                  | force and threats, compulsion                                           |                      |       |
| aplomb       | 沉着，镇静               | complete and confident composure                                        |                      |       |
| convoy       | 护航，护送               | escort, accompany                                                       | con 一起 + voy 航行      |       |
| canopy       | 蚊帐；华盖               | drapery, awning, cloth covering                                         |                      |       |
| lubricant    | 润滑剂                 | reducing friction                                                       | lubric 光滑            |       |
| compunction  | 懊悔，良心不安             | guilt, remorse, penitence                                               |                      |       |
| blasé        | 厌倦享乐的，玩厌了的          | board with pleasure or dissipation                                      |                      |       |
| enclosure    | 圈地，围场               |                                                                         | enclose v.           |       |
| comatose     | 昏迷的                 | unconscious, torpid                                                     | coma n.              |       |
| ravish       | 使着迷；强夺              | overcome, take away                                                     |                      |       |
| spout        | 喷出；滔滔不绝的说           | eject in stream                                                         |                      |       |
| exculpate    | 开脱；申明无罪，证明无罪        | free from blame, declare and prove guiltless                            |                      |       |
| boulder      | 大石头                 |                                                                         | shoulder 肩膀          |       |
| illustrious  | 著名的，显赫的             | very distinguished, outstanding                                         |                      |       |
| depose       | 免职，宣誓作证             |                                                                         | de + pose 放          |       |
| grotesque    | 怪诞的，古怪的；（艺术）风格怪异的   | bizarre, fantastic                                                      |                      |       |
| jug          | 用陶罐等炖；水壶；关押         | jail, imprison, in an earthenware vessel                                |                      |       |
| embolden     | 鼓励                  | to give confidence to                                                   | em + bold + en       |       |
| buoy         | 浮标，救生圈；支持，鼓励        | floating object, encourage, animate, elate, exhilarate, flush, inspirit |                      |       |
| rumple       | 弄皱，弄乱               | make disheveled or tousled                                              |                      |       |
| fleece       | 生羊皮，羊毛；骗取，欺诈        |                                                                         | flee 逃跑              |       |
| impasse      | 僵局，死路               | deadlock, blind alley                                                   | im 无 + pass + e      |       |
| daub         | 涂抹；乱画               |                                                                         |                      |       |
| interlace    | 编织，交错               | weave together, connect intricately                                     |                      |       |
| intensify    | 使加剧                 |                                                                         | intense adj.         |       |
| wiry         | 瘦而结实的               | lean, supple, and vigorous                                              |                      |       |
| consent      | 同意，允许               | give agreement                                                          |                      |       |
| accrue       | （利息等）增加；积累          | increase the interest, accumulate                                       |                      |       |
| sullen       | 忧郁的                 | gloomy, dismal, brooding, morose, sulky                                 |                      |       |
| manifesto    | 宣言，声明               | public declaration                                                      | manifest v.          |       |
| enthralling  | 迷人的，吸引人的            |                                                                         | en + thrall 奴隶 + ing |       |
| anneal       | 使（金属、玻璃等）退火；使变强；使变硬 | strengthen and harden                                                   |                      |       |
| debark       | 下船，下飞机，下车；卸载        |                                                                         | de + bark 船          |       |
| celerity     | 快速，迅速               | swiftness                                                               |                      |       |
| scurvy       | 卑鄙的，下流的             | despicable                                                              | scurry v. 疾行         |       |
| cringing     | 谄媚的，奉承的             |                                                                         |                      |       |
| wheedle      | 哄骗，诱骗               | cajole, coax, seduce                                                    |                      |       |
| peculate     | 挪用（公款）              | embezzle                                                                |                      |       |
| entrance     | 使出神，使入迷             |                                                                         |                      |       |
| effulgent    | 灿烂的，光辉的             | of great brightness                                                     |                      |       |
| encomiast    | 赞美者                 | a eulogist                                                              |                      |       |
| carouse      | 狂饮寻乐                | noisy, merry drinking party                                             |                      |       |
| dally        | 闲荡，嬉戏               | loiter, trifle                                                          |                      |       |
| wan          | 虚弱的，病态的             | feeble, sickly, pallid, ghastly, haggard, sallow                        |                      |       |
| preponderate | 超过，胜过               | exceed                                                                  | pre + ponderate      |       |
| snuggle      | 紧靠，依偎               | draw close                                                              |                      |       |
| blackball    | 投票反对；排斥             | ostracize                                                               | black + ball         |       |
| hubris       | 傲慢，目中无人             |                                                                         |                      |       |
| riveting     | 非常精彩的，引人注目的         |                                                                         | rivet v.             |       |
| shabby       | 破旧的，卑鄙的             | scruffy, shoddy, dilapidated, despicable, contemptible                  |                      |       |
| moat         | 壕沟，护城河              |                                                                         |                      |       |
| deprivation  | 剥夺，丧失               |                                                                         | deprive v.           |       |
| compulsion   | 强迫；（难以抗拒的）冲动        |                                                                         |                      |       |
| musket       | 旧式步枪；毛瑟枪            |                                                                         |                      |       |
| evasion      | 躲避，借口               |                                                                         |                      |       |
| snarl        | 纠缠，混乱；咆哮，怒骂；怒吼声     | knot                                                                    |                      |       |
| grumpy       | 脾气暴躁的               | grouchy, peevish                                                        | grump v.             |       |
| compress     | 压缩，压紧               | contract                                                                |                      |       |
| tangy        | 气味刺激的，扑鼻的           | pleasantly sharp flavor                                                 |                      |       |
| impeach      | 控告；怀疑；弹劾            | accuse, charge, indict                                                  |                      |       |
| snappy       | 生气勃勃的；漂亮的，时髦的       | stylish                                                                 |                      |       |
| muster       | 召集，聚集               | summon, gather                                                          |                      |       |
| 26              | 26                | 26                                                              | 26                          | 26                        |
| cession         | 割让，转让             |                                                                 | cede v.                     |                           |
| pittance        | 微薄的奉新；少量          | meager                                                          | pit (pity) + tance          |                           |
| maul            | 打伤；伤害             | lacerate                                                        | haul 用力拖                    |                           |
| perverse        | 刚愎自用的，固执的；反常的     | obstinate in opposing, wrongheaded                              |                             |                           |
| tensile         | 张力的；可伸展的          |                                                                 | tension n.                  |                           |
| nadir           | 最低点               |                                                                 | zenith 顶点                   | the nadir of one's career |
| maple           | 枫树                |                                                                 |                             |                           |
| comeuppance     | 应得的惩罚；因果报应        | deserved rebuke or penalty                                      | come up + ance              |                           |
| forensic        | 公开辩论的，争论的         | public debate or formal argumentation                           | forum 论坛                    |                           |
| extrude         | 挤出，推出，逐出；伸出，突出    | thrust out, protrude                                            |                             |                           |
| motility        | 运动性               |                                                                 | mot 运动 + ility              |                           |
| contiguous      | 接壤的；接近的           | adjacent                                                        |                             | be contiguous with …      |
| abject          | 极可怜的；卑下的          | miserable, wretched, degraded, base                             | ab + ject                   |                           |
| shrewd          | 机灵的，精明的           | clever discerning awareness                                     | shrew 泼妇                    |                           |
| magnitude       | 重要性；星球的亮度         |                                                                 | magn 大 + tude               |                           |
| dawdle          | 闲荡，虚度光阴           | idle, loiter, trifle                                            |                             |                           |
| malaise         | 不适，不舒服            | illness                                                         | “没累死”                       |                           |
| dismantle       | 拆除                | disassemble                                                     |                             |                           |
| sneaking        | 鬼鬼祟祟的，私下的         | furtive, underhanded, surreptitious                             | snake 蛇                     |                           |
| protuberant     | 突出的，隆起的           | prominent, thrusting out                                        |                             |                           |
| lateral         | 侧面的               | side                                                            |                             |                           |
| twee            | 矫揉造作的，故作多情的       | dainty, delicate, cute or quaint                                |                             |                           |
| stain           | 玷污；染色             | taint, corruption, color                                        |                             |                           |
| doleful         | 悲哀的，忧郁的           | sorrow and sadness                                              | dole 悲哀 + ful               |                           |
| culprit         | 罪犯                | guilty of a crime                                               |                             |                           |
| nettle          | 荨麻；烦扰，激怒          | irritate, provoke                                               |                             |                           |
| reminisce       | 追忆，回想             |                                                                 | re + mind + sce             |                           |
| oleaginous      | 油腻的；圆滑的，满口恭维的     | falsely earnest, unctuous                                       |                             |                           |
| remit           | 免除，宽恕；汇款          | release                                                         |                             |                           |
| variegate       | 使多样化；使色彩斑斓        |                                                                 | varie + gate                |                           |
| wallow          | 打滚；沉迷             |                                                                 |                             |                           |
| lumber          | 跌跌撞撞的走，笨拙的走；杂物；木材 | move with heavy clumsiness                                      |                             |                           |
| menthol         | 薄荷醇               |                                                                 | thol 化学物质                   |                           |
| stealth         | 秘密行动              |                                                                 | steal + th                  |                           |
| stanza          | （诗歌的）节、段          |                                                                 | stan (stand) + za           |                           |
| rumpus          | 喧闹，骚乱             | noisy commotion, clamor, tumult, uproar                         |                             |                           |
| spleen          | 怒气                | rancor, resentment, wrath                                       |                             |                           |
| writ            | 令状；书面令            |                                                                 | write - e                   |                           |
| lukewarm        | 微温的；不冷不热的         |                                                                 | luke 微 + warm               |                           |
| fold            | 羊栏；折叠             |                                                                 |                             |                           |
| mirage          | 海市蜃楼；幻影           |                                                                 | mir 惊奇的 + age               |                           |
| peremptory      | 不容反抗的；专横的         | masterful                                                       | per + empt + ory            |                           |
| beget           | 产生，引起             | bring into being                                                |                             |                           |
| scatter         | 散开，驱散             | dispel, disperse, dissipate                                     |                             |                           |
| choleric        | 易怒的，暴躁的           | of irascible nature, irritable                                  | choler 胆汁 + ic              |                           |
| jeer            | 嘲笑                | mock, taunt, scoff at                                           |                             | jeer at …                 |
| spate           | 许多，大量；暴涨，发洪水      |                                                                 |                             | a spate of …              |
| aspirant        | 有抱负者              | aspirer                                                         |                             |                           |
| incubation      | 孵卵期，潜伏期           |                                                                 |                             |                           |
| merited         | 该得的；理所当然的         | deserving, worthy of                                            | merit 价值                    |                           |
| invoke          | 祈求，恳求；使生效         | implore, entreat                                                |                             |                           |
| predilection    | 偏爱，嗜好             |                                                                 | pre + dilection (direction) |                           |
| exiguous        | 太少的，不足的           | scanty, meager, scarce, skimpy, sparse                          |                             |                           |
| numismatist     | 钱币学家；钱币收藏家        |                                                                 |                             |                           |
| acumen          | 敏锐，精明             |                                                                 | acu + men                   |                           |
| wade            | 涉水，跋涉             | pold, slog, slop, toil, trudge                                  |                             |                           |
| spur            | 刺激；用马刺刺马          |                                                                 | NBA Spurs                   |                           |
| provenance      | 出处，起源             | origin, source                                                  |                             |                           |
| modish          | 时髦的               | fashionable, stylish                                            |                             |                           |
| redolent        | 芬芳的，芳香的           | scented, aromatic                                               |                             |                           |
| gourmand        | 贪食者               | glutton                                                         |                             |                           |
| profiteer       | 奸商，牟取暴利者          |                                                                 |                             |                           |
| agog            | 兴奋的，有强烈兴趣的        |                                                                 | 词根：引导；demagog 煽动者           |                           |
| dimple          | 酒窝                |                                                                 |                             |                           |
| pod             | 豆荚；剥豆荚            |                                                                 |                             | like peas in a pod        |
| toothsome       | 可口的，美味的           | appetizing, delectable, divine, savory, yummy                   |                             |                           |
| hurdle          | 跨栏，障碍；克服          | barrier, block, obstruction, snag, obstacle, surmount, overcome |                             |                           |
| gargoyles       | 滴水嘴；面貌丑恶的人；石像鬼    |                                                                 | gargle 漱口                   |                           |
| rabid           | 患狂犬病的；狂暴的         |                                                                 | rabies 狂犬病                  |                           |
| particularize   | 详述，列举             |                                                                 |                             |                           |
| vie             | 竞争                | compete, contend, contest, emulate, rival                       |                             |                           |
| hinge           | 铰链；关键             | joint, determining factor                                       |                             |                           |
| gander          | 雄鹅；笨蛋，傻瓜；闲逛       |                                                                 | gender 性别                   |                           |
| barn            | 谷仓                |                                                                 |                             |                           |
| putrid          | 腐臭的               | rotten, malodorous                                              |                             |                           |
| enchant         | 使陶醉；施法于           | rouse to ecstatic admiration, bewitch                           |                             |                           |
| plush           | 豪华的               | notably luxurious                                               |                             |                           |
| remonstrance    | 抗议，抱怨             |                                                                 |                             |                           |
| gruff           | 粗鲁的，板着脸的；粗哑的      | rough, hoarse                                                   |                             |                           |
| filial          | 子女的               | of a son or daughter                                            |                             |                           |
| abnegate        | 否认，放弃             | deny, renounce                                                  |                             |                           |
| ensconce        | 安置，安坐             | shelter, establish, settle                                      | en + sconce 小堡垒             |                           |
| hoodoo          | 厄运；招来不幸的人         | bring bad luck                                                  |                             |                           |
| vista           | 远景；展望             | distant view, prospect, extensive mental view                   |                             |                           |
| egoism          | 利己主义              |                                                                 |                             |                           |
| cloy            | （甜食）生腻，吃腻         | satiate                                                         |                             |                           |
| discern         | 识别，看出             | recognize, distinguish                                          |                             |                           |
| tusk            | （大象等）长牙           |                                                                 |                             |                           |
| trounce         | 痛击；严惩             | thrash or punish severely                                       |                             |                           |
| brutal          | 残忍的，野蛮的；冷酷的       | savage, violent, harsh and rigorous                             | brute n.                    |                           |
| discriminate    | 区别，歧视             |                                                                 |                             |                           |
| winkle          | 挑出，剔出；取出          |                                                                 |                             |                           |
| district        | 地区，行政区            |                                                                 |                             |                           |
| leak            | 泄露；裂缝             |                                                                 |                             |                           |
| parturition     | 生产，分娩             |                                                                 |                             |                           |
| beacon          | 烽火，灯塔             |                                                                 | beach                       |                           |
| stoop           | 弯腰，俯身；屈尊          |                                                                 |                             |                           |
| shriek          | 尖叫                | utter a sharp, shrill, sound                                    |                             |                           |
| accessory       | 附属的，次要的           | additional, supplementary, subsidiary                           |                             |                           |
| differentiate   | 辨别，区别             |                                                                 |                             |                           |
| curdle          | 使凝结，变稠            | coagulate, congeal                                              | curd n.                     |                           |
| gaffe           | 失礼，失态             | social or diplomatic blunder                                    |                             |                           |
| wend            | 行，走，前进            | proceed on                                                      |                             | wend one's way            |
| traduce         | 中伤，诽谤             | slander or defame                                               |                             |                           |
| vicissitudinous | 有变化的，变迁的          |                                                                 | vicissitude n.              |                           |
| scud            | 疾行，飞奔             |                                                                 |                             |                           |
| gutless         | 没有勇气的；怯懦的         | cowardly, spineless                                             |                             |                           |
| potboiler       | 粗制滥造的文艺作品；锅炉      |                                                                 | potboil v. 为了混饭吃粗制滥造        |                           |
| boding          | 凶兆，前兆；先兆的         | omen especially for coming evil                                 |                             |                           |
| demarcate       | 划分，划界             |                                                                 |                             |                           |
| opulence        | 富裕；丰富             | affluence, profusion                                            |                             |                           |
| smudge          | 污迹；污点；弄脏          |                                                                 | s + mud 泥 + age             |                           |
| consecrate      | 奉献；使神圣            | dedicate, sanctify                                              | con + sacre + ate           |                           |
| flux            | 不断的变动；变迁，动荡不安     |                                                                 |                             |                           |
| resigned        | 顺从的，听从的           | acquiescent                                                     |                             | be resigned to…           |
| 27            | 27                  | 27                                               | 27                   | 27                        |
| shove         | 推挤，猛推               | move by force                                    | shovel 铁锹            |                           |
| dote          | 溺爱；昏聩               |                                                  |                      |                           |
| corporeal     | 肉体的，身体的；物质的         | of the body, material, rather than spiritual     | corpor + eal         |                           |
| harshly       | 严酷的，无情的             | severely, toughly                                |                      |                           |
| neurosis      | 神经官能症               |                                                  | neu + osis           |                           |
| queer         | 奇怪的，反常的             | eccentric, unconventional                        | queen                |                           |
| overriding    | 最主要的，优先的            | chief, principal                                 |                      |                           |
| judicial      | 法庭的，法官的             | of law, courts, judges, judiciary                |                      | judicial decisions        |
| zealotry      | 狂热                  |                                                  |                      | religious zealotry        |
| pother        | 喧扰，骚动；烦扰            |                                                  |                      |                           |
| stranded      | 搁浅的，处于困境的           |                                                  |                      | a stranded ship           |
| ensign        | 船舰上的国旗              |                                                  | en + sign            |                           |
| perish        | 死，暴卒                |                                                  |                      |                           |
| beholden      | 因受恩惠而心存感激，感谢；欠人情    | grateful, owing                                  |                      |                           |
| obtrude       | 突出，强加               | thrust out, force or impose                      |                      |                           |
| thump         | 重击，锤击               | pound, punch, smack, whack                       |                      |                           |
| retard        | 妨碍，使减速              | impede, slow down                                |                      |                           |
| perforate     | 打洞                  |                                                  |                      |                           |
| dissertation  | 专题论文                | long essay on a particular subject               |                      |                           |
| striking      | 引人注目的，显著的           |                                                  | strike v.            |                           |
| hone          | 磨刀石；磨刀              |                                                  | horn n. 号角           |                           |
| dislocate     | 使脱臼；把…弄乱            | disarrange, disrupt                              |                      |                           |
| niggling      | 琐碎的                 | petty, trival                                    | nig 小                |                           |
| ersatz        | 代用的；假的              | artificial, substitute or synthetic              |                      |                           |
| shuttle       | 穿梭移动，往返运送           |                                                  |                      |                           |
| surge         | 波涛汹涌                |                                                  |                      |                           |
| gaseous       | 气体的，气态的             |                                                  | gas n.               |                           |
| fruition      | 实现，完成               | fulfillment of hopes, plans                      | fruit n.             |                           |
| cramp         | 铁箍，夹子；把…箍紧          |                                                  |                      | cramp one's style         |
| fabulous      | 难以置信的；寓言的           | incredible, astounding, imaginary, fictitious    |                      |                           |
| extinguish    | 使…熄灭；使…不复存在         |                                                  |                      |                           |
| fallibility   | 易出错，不可靠             | liability to error                               | fall v.              |                           |
| gadget        | 小工具，小机械             | small mechanical contrivance or device           |                      |                           |
| homage        | 效忠；敬意               | allegiance, honor                                |                      |                           |
| odoriferous   | 有气味的                | give off an odor                                 |                      |                           |
| pulpit        | 讲道坛                 |                                                  |                      |                           |
| natal         | 出生的，诞生时的            |                                                  | nat 出生 +al           |                           |
| pertain       | 属于；关于               | belong, reference                                |                      |                           |
| liability     | 责任，义务               | the state of being liable, obligation, debt      |                      |                           |
| outlet        | 出口                  |                                                  |                      |                           |
| cupidity      | 贪婪                  | avarice, greed                                   |                      |                           |
| figurine      | 小雕像                 | statuette                                        | figure + ine         |                           |
| aleatory      | 侥幸的，偶然的             | depending on an uncertainty or contigency        |                      |                           |
| bungle        | 笨拙的做                | botch, bumble, fumble, muff, stumble             |                      |                           |
| drivel        | 胡说，糊涂话              | nonsense                                         |                      |                           |
| palaver       | 空谈；奉承               | idle chatter, flatter or cajole                  | pala 宫殿 + ver        |                           |
| jot           | 草草记下                |                                                  | lot 多                |                           |
| blasphemy     | 亵渎（神明）              | cursing                                          |                      |                           |
| waive         | 放弃；推迟               | relinquish, postpone                             |                      |                           |
| complicity    | 合谋，串通               | participation, involvement in a crime            |                      |                           |
| rote          | 死记硬背                |                                                  |                      |                           |
| atonal        | 无调的                 |                                                  | a + tone + al        |                           |
| fang          | （蛇的）毒牙              |                                                  | tang 强烈的气味           |                           |
| snare         | 圈套，陷阱               | trap, gin                                        | ensnare 使进入圈套        |                           |
| deluxe        | 豪华的，华丽的             | notably luxurious, elegant, or expensive         |                      | a deluxe hotel            |
| famine        | 饥荒                  | instance of extremely scarcity                   |                      |                           |
| languor       | 无精打采；衰弱无力           | lack of vigor or vitality, weakness              | lang 松弛 + uor        |                           |
| malfunction   | 发生故障；失灵；故障          |                                                  | mal 坏 + function     |                           |
| endearing     | 讨人喜欢的               |                                                  |                      |                           |
| renal         | 肾脏的；肾的              | kidney                                           |                      |                           |
| serried       | 密集的                 | compact                                          |                      | serried ranks of soldiers |
| topsy-turvy   | 颠倒的，相反的；乱七八糟的；混乱的   |                                                  |                      |                           |
| obsess        | 迷住；使困扰；使烦扰          |                                                  | ob + sess            |                           |
| slouch        | 没精打采的样子；无精打采的       |                                                  | “似老去”                |                           |
| mendacity     | 不诚实                 | untruthfulness                                   |                      |                           |
| rout          | 溃败                  | overwhelming defeat                              |                      |                           |
| kudos         | 声誉，名声               | fame or renown, reputation, prestige             |                      |                           |
| depredation   | 劫掠；蹂躏               | plundering                                       |                      |                           |
| wily          | 诡计多端的；狡猾的           | crafty                                           | wile 诡计              |                           |
| convict       | 定罪，罪犯               |                                                  |                      |                           |
| carp          | 鲤鱼；吹毛求疵             |                                                  |                      |                           |
| onslaught     | 猛攻，猛袭               | fierce attack                                    | on + slaught         |                           |
| monarch       | 君主，帝王               | hereditary sovereign                             |                      |                           |
| crook         | 使弯曲；钩状物             | bend or curve                                    |                      |                           |
| bustle        | 奔忙；忙乱；喧闹，熙熙攘攘       | noisy, energetic, and often obstrusive activity  |                      |                           |
| caprice       | 奇思怪想；反复无常；任性        | sudden change                                    |                      |                           |
| rapprochement | 友好，友善关系建立           |                                                  | re + approachment    |                           |
| excrete       | 排泄，分泌               | pass out waste matter                            |                      |                           |
| jerk          | 猛拉                  |                                                  |                      | jerk off 结结巴巴的说           |
| punch         | 以拳猛击；打孔             | strike with the fist, make a hole, pierce        |                      |                           |
| dyspeptic     | 消化不良的               | indigestible, morose, grouchy                    |                      |                           |
| wacky         | （行为等）古怪的；乖僻的        | eccentric or irrational                          |                      |                           |
| amuse         | 使愉快；逗某人笑            |                                                  |                      |                           |
| agnostic      | 不可知论的；不可知论者         |                                                  |                      |                           |
| bogus         | 伪造的；假的              | not genuine, spurious                            |                      |                           |
| plait         | 发辫；编成辫              | pigtail, braid                                   |                      |                           |
| delectation   | 享受，愉快               | delight, enjoyment, entertainment, joy, pleasure |                      |                           |
| foray         | 突袭；偷袭；劫掠；掠夺         |                                                  |                      |                           |
| friable       | 脆的；易碎的              | easily broken up or crumbled                     | friable soil         |                           |
| skullduggery  | 欺骗；使诈               | underhanded or unscrupulous behavior             |                      |                           |
| blemish       | 损害，玷污；瑕疵，缺点         | defect, spoil the perfection of                  |                      |                           |
| badge         | 徽章                  | distinctive token, emblem, or sign               |                      |                           |
| lucre         | 利益；钱                | profit                                           |                      |                           |
| brusque       | 唐突的；鲁莽的             | blunt, rough and abrupt                          |                      |                           |
| piddle        | 鬼混；浪费               | diddle, dawdle, putter                           |                      |                           |
| shear         | 剪（羊毛）；剪发            |                                                  |                      |                           |
| josh          | （无恶意的）戏弄；戏耍         | banter, jest, tease good-naturedly               |                      |                           |
| vendetta      | 血仇；世仇；宿怨；深仇         | blood, bitter, destructive feud                  |                      |                           |
| ritzy         | 高雅的；势利的             | elegant, snobbish, exclusive, refined, polished  |                      |                           |
| rankle        | 怨恨；激怒               | cause resentment, anger, irritation              |                      |                           |
| lunatic       | 疯子；极蠢的              | insane person, utterly foolish                   |                      |                           |
| gracious      | 大方的；和善的；奢华的；优美的；雅致的 | polite, generous, luxurious                      |                      |                           |
| rollicking    | 欢乐的，喧闹的             | noisy and jolly                                  |                      |                           |
| ecstatic      | 狂喜的，心花怒放的           | enraptured                                       | ecstasy              |                           |
| choice        | 上等的，精选的             | dainty, delicate, elegant, exquisite, superior   |                      |                           |
| 28               | 28                  | 28                                                                       | 28                   | 28                             |
| lapse            | 失误；（时间等）流逝          |                                                                          |                      | lapse of time                  |
| redemptive       | 赎回的；救赎的，挽回的         | redeeming                                                                |                      |                                |
| caudal           | 尾部的；像尾部的            |                                                                          |                      |                                |
| parochial        | 教区的；地方性的，狭小的        | parish, narrow                                                           |                      |                                |
| lap              | 舔食                  | with tongue                                                              |                      |                                |
| periphrastic     | 迂回的，冗余的             |                                                                          | peripheral 外围的       |                                |
| purported        | 传言的，据称的             | reputed, alleged                                                         |                      |                                |
| zest             | 刺激性；兴趣，热心           | keen                                                                     | zealot               |                                |
| trinket          | 小装饰品；不值钱的珠宝；琐事      | trivial                                                                  |                      |                                |
| encompass        | 包围，围绕               | enclose, envelop                                                         |                      |                                |
| dicker           | 讨价还价                | bargain, haggle, higgle, huckster, negotiate, palter                     |                      |                                |
| unscrupulousness | 狂妄，不择手段             |                                                                          | unscrupulous adj.    |                                |
| plagiarize       | 剽窃，抄袭               |                                                                          |                      |                                |
| clammy           | 湿冷的；发粘的             | being damp, soft, sticky, and usually cool                               |                      |                                |
| sterilize        | 使不育，杀菌              |                                                                          |                      | sterilize surgical instruments |
| temerity         | 鲁莽，大胆               | audacity, rashness, recklessness                                         |                      |                                |
| boor             | 粗野的人；农民             | rude, awkward person, peasant                                            |                      |                                |
| swig             | 痛饮                  | gulp, guzzle                                                             |                      |                                |
| mollycoddle      | 过分爱惜，娇惯；娇惯的人        | overly coddle, pamper                                                    |                      |                                |
| loaf             | 一条面包；虚度光阴           | idle, dawdle, dillydally, loiter, lounge                                 |                      |                                |
| hie              | 疾走，快速               | go quickly, hasten, hurry, speed                                         |                      |                                |
| ribald           | 下流的，粗俗的             | crude                                                                    |                      |                                |
| bristling        | 竖立的                 | stiffly erect                                                            |                      |                                |
| aquiline         | 鹰的                  |                                                                          | aquil 鹰 + ine        |                                |
| derivation       | 发展，起源；词源            |                                                                          | derive v.            |                                |
| scamp            | 草率的做；流氓；顽皮的家伙       | rogue, rescal                                                            |                      |                                |
| slit             | 撕裂；裂缝               | sever, long narrow or opening                                            |                      |                                |
| steep            | 陡峭的；过高的；浸泡；浸透       | lofty, high, soak in a liquid                                            |                      |                                |
| conceit          | 自负，自大               | vanity                                                                   |                      |                                |
| ineluctable      | 不能逃避的               | certain, inevitable                                                      |                      |                                |
| auxiliary        | 辅助的；附加的；补充的         | subordinate, additional, supplementary                                   |                      |                                |
| pavid            | 害怕的；胆小的             | timid                                                                    |                      |                                |
| disheveled       | 使蓬乱；使（头发）凌乱         |                                                                          | dishevel v.          |                                |
| grisly           | 恐怖的，可怕的             | frightening, ghastly, horrible                                           |                      |                                |
| droop            | 低垂；萎靡               |                                                                          | drop                 |                                |
| outwit           | 以机智胜过               | overcome by cleverness                                                   | out + wit 机智         |                                |
| envisage         | 正视；想象               | confront, visualize, imagine                                             |                      |                                |
| affix            | 黏上；贴上；词缀            |                                                                          |                      |                                |
| indignant        | 愤慨的；愤愤不平的           | angry                                                                    |                      |                                |
| neutralize       | 使无效；中和              | nullify, make neutral, ineffective                                       |                      |                                |
| overweening      | 自负的，过于自信的           | arrogant, excessively proud                                              |                      |                                |
| crib             | 抄袭，剽窃               | plagiarize                                                               | crab 发牢骚             |                                |
| feint            | 佯攻；伪装               |                                                                          | faint 伪装             |                                |
| aroma            | 芳香，香气               | fragrance                                                                |                      |                                |
| ornery           | 顽固的，爱争吵的            | irritable disposition, cantankerous                                      |                      |                                |
| veritable        | 名副其实的；真正的；确实的       | authentic, unquestionably                                                | verify v.            |                                |
| abhorrent        | 可恨的，讨厌的             | obscene, repugnant, repulsive, hateful, detestable                       |                      |                                |
| pilfer           | 偷窃                  | filch, pinch                                                             |                      |                                |
| oversee          | 监督                  | supervise                                                                | over + see           |                                |
| virtual          | 实质上的，实际上的           | in essence                                                               |                      |                                |
| tempestuous      | 狂暴的                 | turbulent, stormy                                                        |                      | a tempestuous relationship     |
| superimpose      | 重叠；叠加               | place or lay over                                                        | super + impose       |                                |
| strife           | 纷争，冲突               | bitter conflict or dissension, fight, struggle                           |                      |                                |
| acclaim          | 欢呼，称赞               | hail, greet with luad applause                                           |                      | universal acclaim              |
| funk             | 怯懦；恐惧；懦夫            | paralyzing fear                                                          |                      |                                |
| atrocious        | 残忍的；凶恶的             | cruel, brute, outrageous                                                 |                      |                                |
| malice           | 恶意；怨恨               | spite, do mischief                                                       |                      |                                |
| despise          | 鄙视；轻视               | look down with contempt or aversion, contemn, disdain, scorn, scout      |                      |                                |
| gnawing          | 啃，咬；痛苦的，折磨人的        | excruciating                                                             | gnaw 咬 v.            | gnawing doubts                 |
| ague             | 冷战；发冷               | a fit of shivering                                                       |                      |                                |
| poach            | 偷猎；窃取               | catch without permission                                                 | poach ideas of sb.   |                                |
| augury           | 占卜术；预兆              |                                                                          | augur 占卜 v.          |                                |
| shamble          | 蹒跚而行；踉跄的走           | walk awkwardly with dragging feet, shuffle                               |                      |                                |
| plangent         | 轰鸣的，凄凉的             | plaintive quality                                                        |                      |                                |
| sheen            | 光辉；光泽               | bright or shining condition                                              |                      |                                |
| shuckle          | 轻声的笑；咯咯的笑           | laugh softly in a low tone, chortle                                      |                      |                                |
| muniments        | 契据                  |                                                                          |                      |                                |
| sophism          | 诡辩；诡辩法              |                                                                          |                      |                                |
| tinkle           | 发出叮当声               | emit light metallic sounds                                               |                      |                                |
| preside          | 担任主席；负责；指挥          |                                                                          | president            |                                |
| steer            | 掌舵；驾驶；公牛；食用牛        | control the course                                                       |                      |                                |
| rampage          | 乱冲乱跑；狂暴行为           |                                                                          |                      |                                |
| offbeat          | 不规则的；不平常的           | unconventional                                                           |                      |                                |
| bid              | 命令；出价，投标            | command                                                                  |                      |                                |
| slue             | （使）旋转               | rotate, slew                                                             |                      |                                |
| analgesia        | 无痛觉；痛觉丧失            | insensibility to pain without loss of consciousness                      |                      |                                |
| sleight          | 巧妙手法；诡计，灵巧          |                                                                          |                      |                                |
| sneer            | 嘲笑；鄙视               | express scorn or contempt, deride, fleer, scoff, taunt                   |                      |                                |
| scabrous         | 粗糙的                 | scabby, rough                                                            |                      |                                |
| clairvoyant      | 透视的；有洞察力的           | clair (clear) + voy 看 + ant                                              |                      |                                |
| reconnoiter      | 侦察，勘察               | make a reconnaissance of                                                 |                      |                                |
| supine           | 仰卧的；懒散的             | prone, inactive, lying, slack                                            |                      |                                |
| badger           | 獾；烦扰，纠缠不安           | torment, nag, beleaguer, bug, pester, tease                              |                      |                                |
| smirch           | 弄脏；污点               | blemish, soil, sully, tarnish                                            |                      |                                |
| virulent         | 剧毒的；恶毒的             | extremely poisonous, venomous, full of malice                            |                      |                                |
| glow             | 发光，发热；(脸）发红；发光，兴高采烈 | blush, flush, incandescence                                              |                      |                                |
| intestate        | 未留遗憾的               | having made no legal will                                                |                      |                                |
| rue              | 后悔，懊悔               | repent or regret, compunction, contrition, penitence, remorse            |                      |                                |
| wobble           | 摇晃，摇摆；犹豫            | hesitate, move with a staggering motion, falter, stumble, teeter, tooter |                      |                                |
| sulky            | 生气的                 | moodily silent                                                           | sulk + y             |                                |
| imbroglio        | 纠纷；纠葛；纠缠不清          | confusion                                                                |                      |                                |
| providential     | 幸运的；适时的             | fortunate, opportune                                                     |                      |                                |
| sartorial        | 裁缝的，缝制的             |                                                                          | tailor 裁缝 n.         |                                |
| knotty           | 多节的；多瘤的；困难的，棘手的     | puzzlin                                                                  |                      |                                |
| 29           | 29                      | 29                                                  | 29                      | 29                   |
| abstentious  | 有节制的                    | temperate                                           |                         |                      |
| ordain       | 任命（神职）；颁布命令             | decree                                              |                         |                      |
| rapt         | 入迷的；全神贯注的               | engrossed, absorbed, enchanted                      |                         |                      |
| exclaim      | 惊叫；呼喊                   | cry out                                             |                         | exclaim over 感叹 …    |
| sibyl        | 女预言家；女先知                | prophetess                                          |                         |                      |
| cordial      | 热诚的；兴奋的；兴奋剂             | gracious, heartfelt, simulating medicine            |                         |                      |
| mingle       | （使）混合                   | blend, merge                                        |                         |                      |
| snatch       | 强夺；攫取                   | grasp abruptly without permission                   | snap 迅速的 + catch 拿走     |                      |
| zephyr       | 和风；西风                   | gentle breeze, breeze from the west                 |                         |                      |
| preposterous | 荒谬的                     | absurd                                              |                         |                      |
| quack        | 冒牌医生；庸医（的）              |                                                     |                         |                      |
| piquant      | 辛辣的；开胃的；刺激的             | spicy, stimulate the palate, engagingly provocative |                         |                      |
| fluke        | 侥幸；意想不到的事情              | accidentally successful                             | flake 薯片                |                      |
| double-cross | 欺骗；出卖                   | betray or swindle                                   |                         |                      |
| elated       | 得意洋洋的；振奋的               | exultant, high spirits                              |                         | be elated by …       |
| toil         | 苦干；辛苦劳作；辛劳              | long strenuous, fatiguing labor                     |                         |                      |
| flit         | 掠过；迅速飞过                 |                                                     | fly + it                |                      |
| behoove      | 理应，有必要                  | right or necessary                                  |                         |                      |
| vicinity     | 附近，临近                   | proximity, neighborhood                             |                         |                      |
| footle       | 说胡话，做傻事；浪费（时间）          | foolishly                                           |                         |                      |
| damp         | 减弱，抑制；潮湿的               | moist                                               |                         |                      |
| deadlock     | 相持不下；僵局                 | standstill, stalemate                               |                         |                      |
| striated     | 有条纹的                    |                                                     | striate v. striation n. |                      |
| morale       | 士气；民心                   |                                                     |                         |                      |
| exaltation   | （成功带来的）得意，高兴            | elation, rapture                                    |                         |                      |
| substance    | 主旨；实质；物质                | essence                                             |                         |                      |
| slew         | （使）旋转；大量                |                                                     |                         |                      |
| relent       | 变温和；变宽厚；减弱              | become compassionate, soften, mollify               |                         |                      |
| sag          | 松弛，下垂                   | lose firmness, resiliency, vigor                    |                         |                      |
| welsh        | 欠债不还；失信                 | avoid payment, break one's word                     |                         |                      |
| invective    | 谩骂；痛骂                   | violent verbal attack, diatribe                     |                         |                      |
| staid        | 稳重的，沉着的                 | self-restraint, sober                               |                         |                      |
| advert       | 注意，留意；提及                | call attention, refer                               |                         |                      |
| inveigh      | 痛骂；猛烈抨击                 | utter censure or invective                          |                         |                      |
| lithe        | 柔软的，易弯曲的；自然优雅的          | easily bent, flexibility and grace                  |                         |                      |
| forbidding   | 令人生畏的；（形势）险恶的；令人反感的；讨厌的 | threatening, disagreeable                           | forbid v.               |                      |
| bleary       | 视线模糊的；朦胧的；精疲力尽的         | dull or dimmed, tired                               |                         |                      |
| clasp        | 钩子；扣子；紧握                | clench, clutch, grasp, grip                         |                         |                      |
| tract        | 传单；大片土地                 | leaflet of political, large stretch of land         |                         |                      |
| spiel        | 滔滔不绝的讲话                 | pitch                                               |                         |                      |
| iota         | 极少量，极少                  | infinitesimal                                       | ι                       |                      |
| gulp         | 吞食；咽下；抵制；忍住             | gobble, swallow hastily                             |                         |                      |
| stolid       | 无动于衷的                   | unemotional                                         | solid                   |                      |
| piddling     | 琐碎的；微不足道的               | trifling, trivial                                   |                         |                      |
| pact         | 协定，条约                   | covenant, treaty                                    |                         |                      |
| glimpse      | 瞥见；看一眼；一瞥               | glance                                              |                         |                      |
| garbled      | 引起误解的；篡改的               | misleading, falsifying                              |                         |                      |
| sorcery      | 巫术；魔术                   | magical                                             |                         |                      |
| antic        | 古怪的                     | fantastic, queer                                    | antique                 |                      |
| crescendo    | （音乐）渐强；高潮               |                                                     | crescend 变强 + o         |                      |
| hobble       | 蹒跚；跛行                   | lamely, limp                                        | hobby                   |                      |
| acarpous     | 不结果实的                   | impotent to bear fruit                              |                         |                      |
| nag          | 不断叨扰；指责，抱怨              | complain incessantly                                |                         |                      |
| escort       | 护送；护送者                  |                                                     | e + scor (score) + t    |                      |
| secretive    | 守口如瓶的                   |                                                     |                         | be secretive about … |
| gustation    | 品尝；味觉                   | sensation of tasting                                |                         |                      |
| coddle       | 溺爱；悉心照料                 | cater, cosset, paper, spoil                         | mollycoddle 过分爱惜，娇惯     |                      |
| narcissistic | 自恋狂                     |                                                     |                         |                      |
| slump        | 大幅度下降；暴跌                | plunge, plummet, tumble                             |                         |                      |
| lissome      | 柔软的                     | lithe, supple, limber                               | liss 可弯曲的 + ome         |                      |
| damn         | （严厉的）批评；谴责；该死的          | criticise severely                                  |                         |                      |
| parch        | 烘烤；烧焦                   | toast                                               | porch 火把                |                      |
| goldbrisk    | 逃避责任，偷懒                 | shirk                                               |                         |                      |
| mutter       | 咕哝；嘀咕                   | speak in low and indistinct voice                   |                         |                      |
| bombast      | 高调；夸大之词                 | pompous language                                    |                         |                      |
| glower       | 怒目而视                    | scowl                                               |                         |                      |
| expel        | 排出；开除                   | eject, discharge, cut off                           |                         |                      |
| parity       | 同等；相等                   | equality                                            |                         |                      |
| monograph    | 专题论文                    | learned treatise                                    |                         |                      |
| impose       | 征收；强加                   |                                                     |                         |                      |
| senile       | 衰老的                     |                                                     |                         |                      |
| detraction   | 贬低，诽谤                   | unfair criticise                                    |                         |                      |
| epithet      | （贬低人用的）短语或形容词；绰号        |                                                     |                         |                      |
| captivate    | 迷住；迷惑                   | fascinate, attract                                  |                         |                      |
| sloppy       | 邋遢的，粗心的                 | slovenly, careless                                  | slop 弄脏 v.              |                      |
| den          | 窝；兽穴                    | burrow, hole, lair                                  |                         |                      |
| advocate     | 提倡，主张；支持者               |                                                     |                         |                      |
| gauche       | 笨拙的，不会社交的               | tactless, awkward, lacking social polish            |                         |                      |
| scintillate  | 闪烁；（言谈举止中）焕发才智          | sparkle, emit spark                                 |                         |                      |
| skirmish     | 小规模战斗；小冲突               | minor dispute or contest                            |                         |                      |
| smirk        | 假笑；得意的笑                 | simper                                              |                         |                      |
| wrought      | 做成的；形成的；精制的             | made delicately or elaborately                      |                         |                      |
| flossy       | 华丽的，时髦的；丝绵般的，柔软的        |                                                     | floss n. 牙线，棉签          |                      |
| grovel       | 摇尾乞怜；奴颜婢膝；匍匐            | stoop, humbly and abjectly                          |                         |                      |
| sift         | 筛选，过滤                   | separate out by a sieve                             |                         | sift … out           |
| 30            | 30                  | 30                                                                                | 30                   | 30                    |
| wage          | 开始，进行               |                                                                                   |                      | wage a war            |
| straiten      | 使为难；使变窄             |                                                                                   | strait adj.          |                       |
| encyclopedic  | 广博的；知识渊博的           |                                                                                   | encyclopedia n.      |                       |
| slippage      | 滑动，下降               | slipping                                                                          |                      |                       |
| sever         | 断绝；分离               | divide                                                                            | severe 严重的           |                       |
| incertitude   | 不确定（性）；无把握，怀疑       |                                                                                   | in + certitude       |                       |
| treachery     | 背叛                  | violation of allegiance, treason                                                  |                      |                       |
| levelheaded   | 头脑冷静的；清醒的           | self-composed, sensible                                                           |                      |                       |
| coruscate     | 闪亮                  | glitter, sparkle                                                                  |                      |                       |
| funky         | 有霉臭味的；有恶臭的          | offensive odor, foul                                                              |                      |                       |
| delirium      | 精神错乱                | insanity, mania, extreme mental disorder                                          |                      |                       |
| mope          | 抑郁；闷闷不乐；情绪低落        | gloomy and dispirited                                                             |                      |                       |
| juxtapose     | 并列，并置               | side by side, abreast                                                             |                      |                       |
| debris        | 碎片，残骸               |                                                                                   | “堆玻璃”                |                       |
| regurgitate   | 涌回，流回；反胃，反刍         | pour back                                                                         |                      |                       |
| glamor        | 迷惑；魔法，魔力；魅力         |                                                                                   |                      |                       |
| wallop        | 重击，猛击               | severe blow                                                                       |                      |                       |
| advisable     | 适当的，可取的             | recommended                                                                       |                      |                       |
| slattern      | 不整洁的；邋遢的女人          | slut, slovenly, sloppy                                                            |                      |                       |
| hoe           | 锄头                  |                                                                                   | use hoe to dig hole  |                       |
| tart          | 酸的；尖酸的              | acid, acrimonious, biting                                                         | tart reply           |                       |
| stupor        | 昏迷；恍惚               | swoon, torpor, trance, no sensibility                                             |                      |                       |
| mince         | 切碎；装腔作势的小步走         |                                                                                   | minutia 细节           |                       |
| brace         | 支撑，加固；支撑物           | prop up, fastener                                                                 |                      |                       |
| murmur        | 柔声的说；抱怨             | complain, grumble                                                                 |                      | murmur against        |
| extenuate     | 掩饰（罪行）；减轻（刑法）       | lessen                                                                            |                      |                       |
| spasmodic     | 痉挛的；间歇性的            | spasm, intermittent                                                               |                      | spasmodic fighting    |
| refraction    | 折射                  | bending beam                                                                      |                      | atmosphere refraction |
| matriculate   | 录取                  | enroll in college or graduate school                                              |                      |                       |
| drench        | 使湿透                 | soak                                                                              |                      |                       |
| grit          | 沙粒；决心，勇气；下定决心       | stubborn courage, pluck                                                           |                      |                       |
| mulish        | 骡一样的；固执的            | stubborn as a mule, adamant, headstrong, obstinate                                |                      |                       |
| remonstrate   | 抗议；告诫               | in opposition, expostulate                                                        |                      |                       |
| buffet        | 反复敲打，连续打击；自助餐       |                                                                                   |                      |                       |
| rudder        | 船舵，领导者              |                                                                                   |                      |                       |
| sticky        | 湿热的；闷热的             | humid, muggy                                                                      | stick v.             |                       |
| hanker        | 渴望，追求               | strong and persistent desire                                                      |                      |                       |
| membrane      | 薄膜；膜                |                                                                                   |                      |                       |
| staunch       | 坚定的；忠诚的             | constant, inflexible, stalwart, tried-and-true, steadfast in loyalty or principle |                      |                       |
| jar           | 震动，摇晃；冲突；震惊；发出刺耳的声音 | clash, sudden shock                                                               |                      |                       |
| catharsis     | 宣泄；净化               | purifying the emotions                                                            |                      |                       |
| cogitate      | 慎重思考；思索             | ponder                                                                            |                      |                       |
| chauvinistic  | 沙文主义的；盲目爱国的         | excessive or blind patriotism                                                     |                      |                       |
| quail         | 畏缩，发抖；恐惧            | cower, coil in dread or fear                                                      |                      |                       |
| distress      | 痛苦，悲伤               | pain, suffering, agony, anguish                                                   |                      |                       |
| rescission    | 撤销，废除               |                                                                                   | rescind v.           |                       |
| wicked        | 邪恶的；讨厌的；有害的；淘气的     | disgustingly unpleasant                                                           |                      |                       |
| satiny        | 光滑的；柔软的             | smooth, glossy                                                                    |                      |                       |
| crabbed       | 暴躁的                 | peevish, ill-tempered, cross                                                      |                      |                       |
| effrontery    | 厚颜无耻；放肆             | unashamed, boldness, impudence                                                    |                      |                       |
| plumber       | 管子工                 | water pipes or bathroom apparatus                                                 |                      |                       |
| unkempt       | 蓬乱的，未梳理的；不整洁的，乱糟糟的  | not combed, messy                                                                 |                      |                       |
| vacuous       | 空虚的，发呆的             | bare, blank, empty, inane, vacant                                                 |                      |                       |
| rupture       | 破裂，断裂               | burst, break apart                                                                |                      |                       |
| maleficent    | 有害的，作恶的，犯罪的         | doing evil                                                                        | male 坏 + fic 做 + ent |                       |
| maunder       | 胡扯；游荡               |                                                                                   |                      |                       |
| finagle       | 骗取，骗得               | obtain in tricky, cheat, defraud, swindle                                         |                      |                       |
| illusive      | 迷惑人的，迷幻的            | deceiving and unreal                                                              |                      |                       |
| tatter        | 碎片；撕碎               | be ragged                                                                         |                      | in tatters            |
| vest          | 授予，赋予               | grant, endow                                                                      |                      |                       |
| stroll        | 漫步，闲荡               | ramble                                                                            |                      |                       |
| coma          | 昏迷；恍惚               | prolonged unconscious                                                             |                      | go into/be in a coma  |
| forge         | 铁匠铺；使形成、达成；伪造；锻造    | smithy, counterfeit                                                               | “仿制”                 |                       |
| brackish      | 微咸的；难吃的             | somewhat saline, distasteful                                                      | black fish           |                       |
| saucy         | 粗鲁的；俏皮的；漂亮的         | impudent, pretty                                                                  |                      |                       |
| masticate     | 咀嚼；把…磨成浆            | chew, grind to a pulp                                                             | mast 乳房 + icate      |                       |
| spell         | 连续一段时间；拼写；咒语        |                                                                                   |                      |                       |
| locus         | 地点，所在地              | site, location                                                                    | loc + us             |                       |
| epideictic    | 夸耀的                 | pretentious                                                                       |                      |                       |
| premium       | 保险费；奖金              | consideration paid for insurance, reward or recompense                            |                      |                       |
| scrutable     | 可以了解的；可解读的          | capable of being deciphered                                                       |                      |                       |
| purgatory     | 炼狱                  | great suffering                                                                   |                      |                       |
| scald         | 烫伤                  | burn with hot liquid or steam                                                     |                      |                       |
| slake         | 满足，平息               | quench, satisfy                                                                   |                      |                       |
| scandal       | 丑闻；流言蜚语；诽谤          | malicious or defamatory gossip                                                    |                      |                       |
| coagulate     | 使凝结                 | curdle, clot                                                                      |                      |                       |
| thaw          | 解冻，融化               | melt, defrost                                                                     |                      |                       |
| pastiche      | 混合拼凑的作品             | artistic composition                                                              |                      |                       |
| namby-pamby   | 乏味的，懦弱的（人）          | insipid, infirm                                                                   |                      |                       |
| derogate      | 贬低，诽谤               | disparage, lower in esteem                                                        |                      |                       |
| regress       | 倒退，复归，逆行            |                                                                                   | re + gress           |                       |
| comma         | 逗号                  |                                                                                   | coma 昏迷              |                       |
| quotidian     | 每日的；平凡的             | commonplace                                                                       |                      | quotidian behavior    |
| bonhomie      | 好性情，和蔼              | good-natured easy friendliness                                                    |                      |                       |
| accost        | 搭话                  |                                                                                   | ac 靠近 + cost         |                       |
| chaste        | 贞洁的，朴实的             | virtuous, restrained and simple                                                   | chase 追              |                       |
| lurch         | 突然的倾斜；蹒跚而行          | stagger                                                                           |                      |                       |
| legislature   | 立法机关，立法团体           |                                                                                   |                      |                       |
| faze          | 打扰，扰乱               | disconcert, dismay, embarrass                                                     | laze 懒散              |                       |
| caulk         | 填塞使不漏水；填缝剂          | stop up the cracks; seams                                                         |                      |                       |
| croon         | 低声歌唱                | sing in a soft manner                                                             |                      |                       |
| penury        | 贫穷；吝啬               | destitution, severe poverty, niggardly frugality                                  |                      |                       |
| deed          | 行为，行动；转让契约、证书       |                                                                                   |                      |                       |
| raff          | 大量，许多               | a great deal                                                                      |                      |                       |
| nugatory      | 无价值的；琐碎的            | trifling, worthless                                                               | nug 玩笑 + atory       |                       |
| plaque        | 瘟疫；讨厌的人；烦扰          | fatal epidemic, nuisance, disturb or annoy persistently                           |                      |                       |
| imbecile      | 低能者；弱智者；极愚蠢的人       | dolt, fool, idiot                                                                 |                      |                       |
| squabble      | 争吵                  | noisy quarrel, bicker, fuss, row, spat, tiff                                      |                      |                       |
| slick         | 熟练的；圆滑的；光滑的         | skillful and effective, clever, smooth and slippery                               |                      |                       |
| hoodwink      | 蒙混；欺骗               | dupe, by tricky                                                                   |                      |                       |
| inappreciable | 微不足道的               | too small to be perceived                                                         |                      |                       |
| impel         | 推进；驱使               | push, propel, compel, urge                                                        |                      |                       |
| seamy         | 丑恶的，污秽的             | unpleasant, degraded, sordid                                                      |                      |                       |
| agitated      | 激动的，不安的             | excited, perturbed                                                                |                      |                       |
| inclement     | （天气）严酷的；严厉的         | stormy, rough                                                                     |                      |                       |
| guttle        | 狼吞虎咽                | gobble, gulp, swallow                                                             |                      |                       |
| cow           | 母牛；威胁               | threat                                                                            |                      |                       |
| slack         | 懈怠的；不活跃的；松弛的        | sluggish, loose                                                                   |                      |                       |
| extemporize   | 即兴演说                | speak extemporaneously                                                            |                      |                       |
| philology     | 语文学；语文研究            |                                                                                   |                      |                       |
