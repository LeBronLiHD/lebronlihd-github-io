---
layout: post
title:  "GRE Words 2 Day Review"
date:   2022-08-24 13:52:01 +0800
category: IELTSPosts
---

# New Words (2-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English       | Chinese | Synonyms                  | Helpful Memory Ideas | Usage |
|:-------------:|:-------:|:-------------------------:|:--------------------:|:-----:|
| 1| 1| 1| 1| 1|
| fickleness    | 浮躁，变化无常 | inconstancy               |                      |       |
| contrition    | 懊悔，痛悔   | remorse                   |                      |       |
| platitudinous | 陈腐的     |                           |                      |       |
| conformity    | 一致；遵从   | in accordance with        |                      |       |
| deviant       | 超越常规的   |                           |                      |       |
| sobriety      | 节制，庄重   | moderation, gravity       |                      |       |
| obstinacy     | 固执；顽固   | stubbornness              |                      |       |
| overbalance   | 使失去平衡   |                           |                      |       |
| repudiate     | 回绝      | dismiss, reprobate, spurn |                      |       |
| contemptible  | 令人轻视的   |                           | contempt v.          |       |
| 2             | 2                | 2                                                                    | 2                      | 2             |
| scruple       | 顾忌，迟疑            | boggle, stickle, an ethical consideration, hesitate                  |                        |               |
| sparing       | 节俭的              | frugal, thrifty                                                      | spartan adj., spare v. |               |
| profligacy    | 放荡，肆意挥霍          |                                                                      | profligate v.          |               |
| allusion      | 暗指，间接提到          |                                                                      | allude v.              |               |
| allegory      | 寓言               | fable                                                                |                        |               |
| vestigial     | 退化的              | rudimentary, degraded                                                |                        |               |
| defiant       | 反抗的，挑衅的          | bold, impudent                                                       |                        |               |
| deciduous     | 非永久的；短暂的；脱落的；落叶的 |                                                                      |                        |               |
| admonish      | 训诫；警告            | to warn, advise, to reprove mildly                                   |                        |               |
| glamorous     | 迷人的，富有魅力的        | full of glamour, fascinating, alluring                               | glamour 魅力             |               |
| disabuse      | 打消错误念头；使醒悟       | undeceive                                                            | dis + abuse            |               |
| deferential   | 顺从的，恭顺的          | duteous, dutiful                                                     | deference n.           |               |
| denigrate     | 污蔑，诽谤            | defame, blacken, to disparage the character or reputation of         |                        |               |
| tractable     | 易处理的，驯良的         | be easily taught or controlled, doclie                               |                        |               |
| bumper        | 特大的，保险杠          | unusually lage, a device for absorbing shock or preventing damage    |                        |               |
| grind         | 磨碎，碾碎，苦差事        | crush into pieces or fine particles, long, difficult, tedious task   | grand 宏大的              |               |
| therapeutic   | 治疗的              | of the treatment of a diseases                                       | therapy 治疗             |               |
| palpable      | 可触知的，可察觉的；明显的    | tangible, perceptible, noticeable                                    | palp 悸动，触觉             |               |
| stasis        | 停滞               | motionlessness                                                       | stay                   |               |
| stint         | 节制，限量；省流         | to restrict                                                          |                        | stint on sth. |
| perspicuous   | 清晰的，明了的          | clearly expressed or presented                                       |                        |               |
| repellent     | 令人厌恶的            | loathsome, odious, repugnant, revolting, arousing disgust, replusive |                        |               |
| intrusively   | 入侵的              |                                                                      | intrusive adj.         |               |
| deign         | 惠允；施惠于人          | stoop, to condescend to do sth.                                      |                        |               |
| conjecture    | 推测，臆测            | presume, suppose, surmise                                            |                        |               |
| infect        | 传染；使感染，侵染        | contaminate                                                          |                        |               |
| provision     | 供应；（法律等）条款       | a stock of needed materials or supplies, stipulation                 |                        |               |
| intricate     | 错综复杂的，难懂的        | complicated, knotty, labyrinthine, sophisticated, elaborate, complex |                        |               |
| heterogeneous | 异类的，多样的          | dissimilar, incongruous, foreign                                     |                        |               |
| stratify      | （使）层化            | to divide or arrange into classes, castes, or social strata          |                        |               |
| dismal        | 沮丧的，阴沉的          | showing sadness                                                      | dies mail “不吉利的日子”     |               |
| fluctuate     | 波动，变动            | to undulate as waves, to be continually changing                     |                        |               |
| querulous     | 抱怨的，爱发牢骚的        | habitually, complaining, fretful                                     |                        |               |
| 3 | 3 | 3 | 3 | 3|
| corruption       | 腐败，堕落              |                                                                                       | corrupt v.           |                     |
| trickle          | 细流；细细的流            | dribble, drip, filter                                                                 |                      |                     |
| anticipatory     | 预想的，预期的            |                                                                                       | anticipate v.        |                     |
| pompous          | 自大的                | arrogant                                                                              |                      |                     |
| enamored         | 倾心的，被迷住            | bewitched, captivated, charmed, enchanted, infatuated, inflamed with love, fascinated |                      | be enamored of sth. |
| repertoire       | （剧团等）常备剧目          |                                                                                       |                      |                     |
| rigorous         | 严格的，严峻的            | austere, rigid, severe, stern, manifesting, exercising, favoring rigor                |                      |                     |
| entrenched       | （权力、传统）确立的，牢固的     | strongly established                                                                  |                      |                     |
| incredulity      | 怀疑，不相信             | disbelief                                                                             | in + credit          |                     |
| anaerobic        | 厌氧的；厌氧微生物          |                                                                                       |                      |                     |
| indigent         | 贫穷的，贫困的            | deficient, improverished                                                              |                      |                     |
| inalienable      | 不可转让的；不可剥夺的        |                                                                                       | alien v.             |                     |
| officious        | 过于殷勤的；多管闲事的；非官方的   | meddlesome, informal, unofficial                                                      |                      |                     |
| vicissitude      | 变迁，兴衰              | natural change or mutation visible in nature or in human affairs                      |                      |                     |
| repeal           | 废除（法律）             |                                                                                       | re + peal            |                     |
| relegate         | 使降级，贬谪；交付，托付       | to send to exile, assign to oblivion, to refer or assign for decision or action       |                      |                     |
| undifferentiated | 无差别的，一致的           | uniform                                                                               |                      |                     |
| agitation        | 焦虑，不安；公开辩论，鼓动宣传    | anxiety, public argument or action for social or political change                     |                      |                     |
| preclude         | 预防，排除；阻止           | deter, forestall, forfend, obviate, ward, prevent, rule out, exclude                  |                      |                     |
| stagnant         | 停滞的                | not advancing or developing                                                           |                      |                     |
| disputable       | 有争议的               | arguable, debatable                                                                   |                      |                     |
| composure        | 镇静，沉着；自若           | calmness, coolness, phlegm, sangfroid, self-possession, tranquility, equanimity       |                      |                     |
| impecunious      | 不名一文的；没钱的          | having very little or no money                                                        |                      |                     |
| conspiracy       | 共谋，阴谋              |                                                                                       | conspire v.          | conspiracy against  |
| epitome          | 典型；梗概              | showing all the typical qualities of sth., abstract, summary, abridgment              |                      |                     |
| obsolescent      | 逐渐荒废的              | in the process of becoming obsolete                                                   | obsolescence         |                     |
| dissent          | 异议；不同意，持异议         | difference of opinion, to differ in belief or opinion, disagree                       |                      |                     |
| tributary        | 支流，进贡国；支流的，辅助的，进贡的 | making additions or yielding supplies, contributory                                   |                      |                     |
| eulogize         | 称赞，颂扬              | praise highly in speech or writing                                                    |                      |                     |
| welter           | 混乱，杂乱无章            | a disordered mixture                                                                  | melter 熔炉            |                     |
| susceptible      | 易受影响的，敏感的          | unresistant to some stimulus, influence, or agency                                    |                      |                     |
| abet             | 教唆；鼓励，帮助           | to incite, encourage, urge and help on                                                |                      |                     |
| 4            | 4                | 4                                                                           | 4                    | 4             |
| egoistic(al) | 自我中心的，自私自利的      | egocentric, egomaniacal, self-centered, selfish                             |                      |               |
| surly        | 脾气暴躁的；阴沉的        | bad tempered, sullen                                                        |                      |               |
| briny        | 盐水的，咸的           | of, or relating to, or resembling brine or the sea, salty                   | brine 盐水             |               |
| incarnate    | 具有人体的；化身的，拟人化的   | given in a bodily form, personified                                         |                      |               |
| daunt        | 使胆怯，使畏缩          | to dishearten, dismay                                                       |                      |               |
| slovenly     | 不整洁的，邋遢的         | dirty, untidy especially in personal appearance                             |                      |               |
| sluggish     | 缓慢的，行动迟缓的；反应慢的   | markedly slow in movement, flow, or growth, slow to respond, torpid         |                      |               |
| retract      | 撤回，取消；缩回，拉回      | to take back or withdraw, to draw or pull back                              | re + tract           |               |
| choppy       | 波浪起伏的；（风）不断改变方向的 | rough with small waves, changeable, variable                                |                      |               |
| relinquish   | 放弃，让出            | to give up, withdraw or retreat from                                        | leave                |               |
| depreciate   | 轻视；贬值            | belittle, disparage                                                         |                      |               |
| tout         | 招徕顾客，极力赞扬        | to praise or publicize loudly                                               |                      |               |
| commitment   | 承诺，许诺            |                                                                             |                      |               |
| plaintive    | 悲伤的，哀伤的          | expressive of woe, melancholy                                               | plaint n. 哀诉         |               |
| frugal       | 节约的；节俭的          | careful and thrifty                                                         |                      |               |
| penitent     | 悔过的，忏悔的          | expressing regretful pain, repentant                                        |                      |               |
| perpetrate   | 犯罪；负责            | to commit, to be responsible for                                            |                      |               |
| arcane       | 神秘的，秘密的          | cabalistic, impenetrable, inscrutable, mystic, mysterious, hidden or secret |                      |               |
| proofread    | 校正，校对            | to read and mark corrections                                                |                      |               |
| impound      | 限制；依法没收，扣押       | to confine, to seize and hold in the custody of the law, take possession of |                      |               |
| succor       | 救助，援助            | to go to the aid of                                                         |                      |               |
| cavil        | 调毛病；吹毛求疵         | quibble                                                                     |                      | cavil at sth. |
| torpor       | 死气沉沉             | dullness, languor, lassitude, lethargy, extreme sluggishness of function    |                      |               |
| superannuate | 使退休领养老金          | to retire and pension because of age or infirmity                           |                      |               |
| artisan      | 技工               | skilled workman or craftsman                                                |                      |               |
| aberrant     | 越轨的；异常的          | turning away from what is right, deviating from what is normal              |                      |               |
| rave         | 热切赞扬；胡言乱语，说疯话    |                                                                             |                      |               |
| divert       | 转移；（使）转向；使娱乐     |                                                                             |                      |               |
| flaunty      | 炫耀的，虚化的          | ostentatious                                                                | flaunt v.            |               |
| procurement  | 取得，获得；采购         |                                                                             | procure v.           |               |
| arduous      | 费力的，艰难的          | marked by great labor or effort                                             |                      | arduous march |
| sinuous      | 蜿蜒的，迂回的          | characterized by many curves and twists, winding                            |                      |               |
| exasperate   | 激怒，使恼怒           | to make angry, vex, huff, irritate, peeve, pique, rile, roil                |                      |               |
| domesticated | 驯养的，家养的          |                                                                             | dome + sticated      |               |
| captivating  | 吸引人的             | very attractive and interesting, in a way that holds your attention         |                      |               |
| vacillate    | 游移不定，踌躇          | to waver in mind, will or feeling                                           |                      |               |
| parasitic    | 寄生的              |                                                                             |                      |               |
| benevolent   | 善心的，仁心的          | altruistic, humane, philanthropic, kindly, charitable                       |                      |               |
| castigate    | 惩治；严责            | chasten, chastise, discipline, to punish or rebuke severely                 |                      |               |
| portend      | 预兆，预示            | to give an omen                                                             |                      |               |
| tenacious    | 坚韧的，顽强的          |                                                                             |                      |               |
| relentless   | 无情的，残酷的          | cruel, merciless, pitiless, ruthless                                        |                      |               |
| topple       | 倾覆，推倒            | to overthrow                                                                |                      |               |
| perishable   | 易腐烂的（东西），易变质的    | likely to decay or go bad quickly                                           |                      |               |
| cessation    | 中止；（短暂的）停止       | a short pause or a stop                                                     |                      |               |
| contagious   | 传染的；有感染力的        | easily passed from person to person, communicable                           |                      |               |
| prodigious   | 巨大的；惊人的，奇异的      | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童           |               |
| 5               | 5                    | 5                                                                  | 5                    | 5                             |
| scant           | 不足的，缺乏的              | barely or scarcely                                                 |                      | scant attention/consideration |
| hypnotic        | 催眠的；催眠药              | tending to produce sleep, a sleep-inducing agent                   |                      |                               |
| gallant         | 勇敢的，英勇的；对（女人）献殷勤的    | brave and noble, polite and attentive to women                     |                      |                               |
| chromatic       | 彩色的，五彩的              | having color or colors                                             |                      |                               |
| vigilant        | 机警的，警惕的              | alertly watchful to avoid danger                                   |                      |                               |
| replete         | 充满的，供应充足的            | fully or abundantly provided or filled                             |                      |                               |
| scorn           | 轻蔑，瞧不起               | contemn, despise                                                   |                      |                               |
| reigning        | 统治的，起支配作用的           |                                                                    |                      |                               |
| antecedent      | 前事；祖先，先行的            |                                                                    |                      |                               |
| charismatic     | 有魅力的                 | having, exhibiting, or based on charisma or charism                |                      |                               |
| treacherous     | 背叛的；叛逆的              | showing great disloyalty and deceit                                |                      |                               |
| placid          | 安静的，平和的              | serenely free of interruption                                      |                      |                               |
| hearten         | 鼓励，激励                | to make sb. feel cheerful and encouraged                           |                      |                               |
| evince          | 表明，表示                | indicate, make manifest, show plainly                              |                      |                               |
| insidious       | 暗中危害的；阴险的            | working or spreading harmfully in a subtle or stealthy manner      |                      |                               |
| stark           | 光秃秃的；荒凉的；（外表）僵硬的；完全的 | barren, desolate, rigid as if in death, utter, sheer               |                      |                               |
| deterrent       | 威慑的，制止的              | serving to deter                                                   | deter v.             |                               |
| interpolate     | 插入，（通过插入新语句）篡改       |                                                                    | inter                |                               |
| camaraderie     | 同志之情；友情              | a spirit of friendly good-fellowship                               |                      |                               |
| ritual          | 仪式，惯例                | ceremonial act or action                                           | rite n.              |                               |
| stained         | 污染的，玷污的              | blemished, discolored, marked, spotted, tarnished                  |                      |                               |
| intangible      | 触摸不到的；无形的            | imperceptible, indiscernible, untouchable, impalpable, incorporeal |                      |                               |
| forlorn         | 孤独的；凄凉的              | abandoned or deserted, wretched, miserable                         |                      |                               |
| rampant         | 猖獗的，蔓生的              |                                                                    |                      |                               |
| trepidation     | 恐惧，惶恐                | timorousness, uncertainty, agitation                               |                      |                               |
| suspense        | 悬念；挂念                | apprehension, uncertainty, anxiety                                 |                      |                               |
| indulge         | 放纵，纵容；满足             | cosset, pamper, spoil                                              |                      |                               |
| propensity      | 嗜好，习性                | bent, leaning, tendency, penchant, proclivity                      |                      |                               |
| visionary       | 有远见的；幻想的；空想家         | dreamy, idealistic, utopain                                        |                      |                               |
| uncommunicative | 不爱说话的；拘谨的            | incommunicative                                                    |                      |                               |
| exempt          | 被免除的；被豁免的；免除，豁免      |                                                                    |                      |                               |
| erratically     | 不规律的；不定的             | in an erratic and unpredictable manner                             | erratic adj.         |                               |
| 6              | 6              | 6                                                                                       | 6                    | 6                   |
| atheist        | 无神论者           | no deity                                                                                |                      |                     |
| unspotted      | 清白的，无污点的       | without spot, flawless                                                                  |                      |                     |
| reprisal       | 报复；报复行动        | practice in retaliation                                                                 |                      |                     |
| inordinate     | 过度的；过分的        | immoderate, excessive                                                                   |                      |                     |
| vainglorious   | 自负的            |                                                                                         |                      |                     |
| congenial      | 意气相投的；性情好的；适意的 | companionable, amiable, agreeable                                                       |                      |                     |
| irreversible   | 不能撤回的，不能取消的    | not reversible, irrevocable, unalterable                                                |                      |                     |
| ineptitude     | 无能；不适当         | the quality or state of being inept                                                     | inept adj.           |                     |
| scour          | 冲刷；擦掉；四处搜索     | to clear, dig, remove, rub, to range over in a search                                   |                      |                     |
| ephemeral      | 朝生暮死的；生命短暂的    | evanescent, fleeting, fugacious, momentary                                              |                      |                     |
| promulgate     | 颁布（法令）；宣传，传播   | announce, annunciate, declear, disseminate, proclaim                                    |                      |                     |
| dearth         | 缺乏；短缺          | scarcity                                                                                | dear + th            |                     |
| exodus         | 大批离去，成群外出      | a mass departure or emigration                                                          |                      |                     |
| infuriate      | 使恼怒；激怒         | to enrage                                                                               |                      |                     |
| underestimated | 低估的            |                                                                                         | underestimate        |                     |
| unwonted       | 不寻常的，不习惯的      | unusual, unaccustomed                                                                   |                      |                     |
| rebuke         | 指责，谴责          | admonish, chide, reproach                                                               |                      |                     |
| warrant        | 正当理由；许可证；保证，批准 | certify, guarantee, vindicate, justification, a commission or document giving authority |                      |                     |
| incense        | 香味，激怒          | enrage, infuriate, madden                                                               |                      |                     |
| distent        | 膨胀的；扩张的        | swollen, expanded                                                                       |                      |                     |
| rancor         | 深仇；怨恨          | bitter deep-seated ill will, enmity                                                     |                      |                     |
| flagrant       | 罪恶昭著的；公然的      | conspicuously offensive                                                                 |                      |                     |
| hypocrisy      | 伪善；虚伪          | cant, pecksniff, sanctimony, sham                                                       |                      |                     |
| brassy         | 厚脸皮的；无礼的       | brazen, insolent                                                                        |                      |                     |
| impassive      | 无动于衷的，冷漠的      | stolid, phlegmatic                                                                      |                      |                     |
| accede         | 同意             | to give assent, consent                                                                 |                      | accede to 答应        |
| ethnic         | 民族的，种族的        | of a national, racial or tribal group that has a common culture tradition               |                      |                     |
| repugnant      | 令人厌恶的          | strong distaste or aversion                                                             |                      | be repugnant to sb. |
| notorious      | 臭名昭著的          | widely and unfavorably known                                                            | not + orious         |                     |
| unmitigated    | 未缓和的；未减轻的      |                                                                                         | mitigate 缓和的         |                     |
| abstain        | 禁绝，放弃          | ab + stain                                                                              |                      |                     |
| 7                | 7             | 7                                                                  | 7                    | 7                |
| wilt             | 凋谢，枯萎         | mummify, shrivel, wither, to lose vigor from lack of water         |                      |                  |
| antipathy        | 反感，厌恶         | animosity, animus, antagonism, enmity, hostility                   |                      |                  |
| interrogation    | 审问，质问；疑问句     | inquiry, query, question                                           |                      |                  |
| ablaze           | 着火的；燃烧的；闪耀的   | being on fire, radiant with light or emotion                       |                      | ablaze with sth. |
| frivolous        | 轻薄的，轻佻的       | marked by unbecoming levity                                        |                      |                  |
| didactic         | 教诲的；说教的       | morally instructive, boringly pedantic or moralistic               |                      |                  |
| reprimand        | 训诫，谴责         | a severe or official reproof                                       |                      |                  |
| bluff            | 虚张声势；悬崖峭壁     | pretense of strength, high cliff                                   |                      |                  |
| transitory       | 短暂的           | transient                                                          |                      |                  |
| substantive      | 根本的；独立存在的     | dealing with essentials, being in a totally independently entity   | substantial          |                  |
| irreverent       | 不尊敬的          | disrespectful                                                      |                      |                  |
| chorale          | 赞美诗；合唱队       |                                                                    | chorus               |                  |
| iniquitous       | 邪恶的；不公正的      | wicked, unjust                                                     |                      |                  |
| prevail          | 战胜；流行，盛行      | conquer, master, predominate, triumph                              |                      |                  |
| hallow           | 把…视为神圣；尊敬，敬畏  | to make or set apart as holy, to respect or honor greatly, revere  |                      |                  |
| indiscriminately | 随意的，任意的       | in a random manner, promiscuously, arbitrary                       |                      |                  |
| conjure          | 召唤，想起；变魔术，变戏法 | to call or bring to mind, invoke, to practise magic or legerdemain |                      | conjure for 召唤   |
| down             | 下；绒毛，羽毛       |                                                                    |                      |                  |
| derogatory       | 不敬的；贬损的       | disparaging, belittling                                            |                      |                  |
| crumple          | 把…弄皱；起皱；破裂    | crush together into creases or wrinkles, to fall apart             |                      |                  |
| demur            | 表示抗议，反对       |                                                                    | de + mur             |                  |
| solitary         | 孤独的；隐士        | without companions, recluse, forsaken, lonesome, lorn              | solit 孤独             |                  |
| 8              | 8                    | 8                                                                         | 8                    | 8     |
| disseminate    | 传播，宣传                | to spread abroad, promulgate widely                                       |                      |       |
| brute          | 野兽（的）；残忍的（人）         | beast, bestial, beastly, brutal                                           |                      |       |
| trivial        | 琐碎的，没有价值的            | paltry, picayune, picayunish, trifling                                    |                      |       |
| volatile       | 反复无常的；易挥发的           | fickle, inconstant, mercuial, capricious                                  |                      |       |
| suspend        | 暂停，中止；吊，悬            |                                                                           | sus + pend           |       |
| garble         | 曲解，篡改                | to alter or distort as to create a wrong impression or change the meaning |                      |       |
| elusive        | 难懂的，难以描述的；不易被抓获的     |                                                                           |                      |       |
| flout          | 蔑视，违抗                | fleer, gibe, jeer, jest, sneer                                            |                      |       |
| cavity         | （牙齿等的）洞，腔            | a hollow place in a tooth                                                 | carve                |       |
| garrulous      | 唠叨的，话多的              | loquacious, talkative, chatty, conversational, voluble                    |                      |       |
| obstruct       | 阻塞（通道）；妨碍            | block by an obstacle                                                      |                      |       |
| antithetical   | 相反的；对立的              | opposite, contradictory, contrary, converse, counter, reverse             | antithesis n.        |       |
| glaze          | 上釉于，使光滑；釉            |                                                                           |                      |       |
| inhibit        | 阻止；抑制                | bridle, constrain, withhold                                               |                      |       |
| dilapidated    | 破旧的；毁坏的              | broken down                                                               |                      |       |
| gorgeous       | 华丽的；极好的              | brilliantly showy, splendid                                               |                      |       |
| aseptic        | 净化的；无菌的              | not septic                                                                | sepsis 脓毒症           |       |
| dissident      | 唱反调者                 | a person who disagrees, dissenter                                         |                      |       |
| stalk          | 隐伏跟踪（猎物）             | to pursue quarry or prey stealthily                                       | stop + talk          |       |
| perfervid      | 过于热心的                | excessively fervent                                                       |                      |       |
| moisten        | 弄湿；使湿润               |                                                                           | moist adj.           |       |
| herald         | 宣布…的消息；预示…的来临；传令官，信使 |                                                                           |                      |       |
| incommensurate | 不成比例的；不相称的           | not proportionate, not adequate                                           |                      |       |
| penal          | 惩罚的；刑罚的              |                                                                           | penalty n.           |       |
| sycophant      | 马屁精                  | a servile self-seeking flatterer                                          |                      |       |
| affront        | 侮辱，冒犯                | to offend, confront, encounter                                            |                      |       |
| assuming       | 傲慢的，自负的；假设           | pretentious, presumptuous                                                 |                      |       |
| speculate      | 沉思，思索；投机             | to meditate on or ponder                                                  |                      |       |
| 9             | 9                           | 9                                                                        | 9                    | 9                  |
| banal         | 乏味的；陈腐的                     | dull or stale, commonplace, insipid, bland, flat, sapless, vapid         |                      |                    |
| confrontation | 对抗                          | the clashing of forces or ideas                                          | confront v.          |                    |
| innocuous     | （行为，言论等）无害的                 | harmless, innocent, innoxious, inoffensive, unoffensive                  |                      |                    |
| tenuous       | 纤细的；稀薄的；脆弱的，无力的             | not dense, rare, flimsy, weak                                            |                      |                    |
| perquisite    | 额外收入；津贴；小费                  | cumshaw, lagniappe, largess                                              | per + quisite        |                    |
| haggle        | 讨价还价                        | bargain, dicker, wrangle                                                 |                      |                    |
| falter        | 蹒跚，支支吾吾地说                   | flounder, stagger, tumble, stammer                                       |                      |                    |
| affable       | 和蔼的；友善的                     | gentle, amiable, pleasant and easy to approach or talk to                |                      |                    |
| adore         | 崇拜；爱慕                       | to worship as divine, to love greatly, revere                            | adorn 装饰             |                    |
| peripatetic   | 巡游的，游动的                     | itinerant                                                                |                      |                    |
| whimsy        | 古怪，异想天开                     | whim, a fanciful creation, boutade, caprice, crotchet, freak             |                      |                    |
| indigence     | 贫穷，贫困                       | poverty, destitution, impecuniousness, impoverishment, penury, privation | indigent adj.        |                    |
| allure        | 引诱，诱惑                       | to entice by charm or attraction                                         |                      |                    |
| emulate       | 与…竞争，努力赶上                   | to strive to equal or excel                                              |                      |                    |
| peevish       | 不满的，抱怨的；暴躁的；易怒的             | huffy, irritable, pettish, discontented, querulous, fractious, fretful   | peeve v.             |                    |
| cumbersome    | 笨重的，难处理的                    | clumsy                                                                   | cumber + some        |                    |
| synergic      | 协同作用的                       | of combined action or coorperation                                       | synergy n.           |                    |
| subdued       | （光、声）柔和的，缓和的；（人）温和的         | unnaturally or unusually quiet in behavior                               |                      |                    |
| recessive     | 隐性遗传病的，后退的                  | tending to recede, withdraw                                             |                      |                    |
| amalgam       | 混合物                         | a combination or mixture, admixture, composite, compound, interfusion    |                      |                    |
| credulous     | 轻信的；易上当的                    | tending to believe too readily, easily convinced                         |                      |                    |
| ingrate       | 忘恩负义的人                      | an ungrateful person                                                     | in + grate           |                    |
| catalyze      | 促使，激励                       | to bring about, to inspire                                               |                      |                    |
| intrigue      | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of       |                      |                    |
| consort       | 陪伴；结交；配偶                    |                                                                          | con + sort           |                    |
| revise        | 校订，修订                       | redraft, redraw, revamp                                                  |                      |                    |
| aloof         | 远离的；冷漠的                     |                                                                          |                      |                    |
| blatant       | 厚颜无耻的；显眼的；炫耀的               | brazen, completely obvious, conspicuous, showy                           |                      |                    |
| braid         | 编织；麦穗；辫子                    | plait, twine, weave                                                      |                      |                    |
| arid          | 干旱的；枯燥的                     | dry, dull, uninteresting                                                 |                      | an arid discussion |
| surrogate     | 代替品；代理人                     | substitution                                                             |                      |                    |
| narcotic      | 麻醉剂；催眠的                     | soporific                                                                |                      |                    |
| reciprocally  | 相互的；相反的                     | mutually                                                                 |                      |                    |
| phlegmatic    | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                      |                      |                    |
| penetrating   | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                        |                      |                    |
| paean         | 赞美歌，诗歌                      | a song of joy, praise, triumph, chorale                                  |                      |                    |
| exploit       | 剥削；充分利用                     |                                                                          |                      |                    |
| duplicity     | 欺骗，口是心非                     | deceit, dissemblance, dissimluation, guile                               |                      |                    |
| 10            | 10              | 10                                                         | 10                    | 10                      |
| revert        | 恢复；重新考虑         | to go back, to consider again                              | re + vert             |                         |
| hamper        | 妨碍；阻挠；有盖的大篮子    | hinder, impede, a large basket with a cover                |                       |                         |
| aptitude      | 适宜；才能，资质        | a natural tendency, a natural ability to do sth.           |                       | aptitude for sth.       |
| garish        | 俗丽的，过于艳丽的       | too bright or gaudy, tastelessly showy                     |                       |                         |
| breach        | 打破；突破；违背；裂缝，缺口  | break, violate                                             | break                 |                         |
| inexorable    | 不为所动的；无法改变的     | incapable of being moved or influenced, cannot be altered  |                       |                         |
| redeem        | 弥补，赎回，偿还        | to atone for, expiate                                      |                       |                         |
| coarse        | 粗糙的，低劣的；粗俗的     | of low quality, not refined                                |                       |                         |
| tacit         | 心照不宣的           | understood without being put into words                    |                       |                         |
| banish        | 放逐              | to send sb. out of the country as a punishment             |                       |                         |
| improvise     | 即席创作            | impromptu, extemporize                                     |                       |                         |
| episodic      | 偶然发生的；分散性的      | occuring irregularly                                       | episode n. 片段         |                         |
| withstand     | 反抗，经受           | to oppose with force or resolution, to endure successfully |                       |                         |
| overbear      | 压倒；镇压；比…重要；超过   |                                                            |                       |                         |
| mundane       | 现世的，世俗的；平凡的     | relating to the world, worldly, commonplace                |                       |                         |
| predatory     | 掠夺的；食肉的         | predatorial, rapacious, raptorial                          |                       |                         |
| ribaldry      | 下流的语言；粗鄙的幽默     | ribald language or humor, obscenity                        |                       |                         |
| prerequisite  | 先决条件            |                                                            | perquisite 额外收入；津贴；小费 |                         |
| grating       | （声音）刺耳的；恼人的     | hoarse, raucous, irritating, annoying, harsh and rasping   |                       |                         |
| perspicacious | 独具慧眼的；敏锐的       | of acute mental vision or discernment                      |                       |                         |
| solicit       | 恳求；教唆           | to make petition to, to entice into evil                   | solicitous 热切的，挂念的    |                         |
| pitfall       | 陷阱；隐患           | a hidden or not easily recognized danger or difficulty     |                       |                         |
| euphonious    | 悦耳的             | having a pleasant sound, harmonious                        |                       |                         |
| omit          | 疏忽，遗漏；不做，未能做    | to leave out, to leave undone, disregard, ignore, neglect  |                       |                         |
| elapse        | 消逝，过去           | pass, go by                                                |                       |                         |
| allegiance    | 忠诚，拥护           | loyalty or devotion to a cause or a person                 |                       |                         |
| whittle       | 削（木头）；削减        | to pare or cut off chips, to reduce, pare                  | whistle 口哨            |                         |
| drone         | 嗡嗡的响；单调的说；单调的低音 |                                                            |                       |                         |
| divisive      | 引起分歧的；导致分裂的     | creating disunity or dissension                            |                       |                         |
| bombastic     | 夸夸其谈的           |                                                            |                       |                         |
| indent        | 切割成锯齿状          | notch, cut serratedly                                      |                       |                         |
| creed         | 教义，信条           | belief, tenet                                              |                       |                         |
| underhanded   | 秘密的；狡诈的         | marked by secrecy and deception, sly                       |                       |                         |
| tantamount    | 同等的；相当于         | equivalent in value, significance, or effect               |                       |                         |
| containment   | 阻止，遏制           | keeping sth, within limits                                 |                       | a policy of containment |
| attrition     | 摩擦，磨损           | by friction                                                |                       |                         |
| consternation | 大为吃惊；惊骇         | great fear or shock                                        |                       |                         |
| delineate     | 勾画，描述           | to sketch out, draw, describe                              |                       |                         |
| genial        | 友好的；和蔼的         | cheerful, friendly, amiable                                |                       |                         |
| pliable       | 易弯曲的；柔软的；易受影响的  | ductile, easily influenced, easily bend, supple            |                       |                         |
| deride        | 嘲笑，愚弄           | to laugh at, ridicule                                      |                       |                         |
| ignominious   | 可耻的；耻辱的         | disgraceful, humiliating, despicable, dishonorable         |                       |                         |
| furtive       | 偷偷的，秘密的         | catlike, clandestine, covert, secret, surrepitious         |                       |                         |
| 11             | 11             | 11                                                                               | 11                                        | 11                    |
| assiduous      | 勤勉的；专心的        | diligent, persevering, attentive                                                 |                                           | be assiduous in sth.  |
| averse         | 反对的；不愿意的       | not willing or inclined, opposed                                                 |                                           |                       |
| colloquial     | 口语的，口头的        | conversational                                                                   | loquacious 长话短说 loqu 说                    |                       |
| arrogant       | 自负的；自大的        | overbearing, haughty, proud, pompous, imperious                                  | interrogation 审问，质问；疑问句                   |                       |
| forfeit        | 丧失；被罚没收；丧失的东西  |                                                                                  |                                           |                       |
| untarnished    | 未失去光泽的         | unblemished, stainless, unstained, untained, unsullied                           |                                           |                       |
| antagonism     | 对抗，敌对          | animosity, animus, antipathy, opposition, rancor                                 |                                           |                       |
| obliterate     | 涂掉，擦掉          | efface, erase                                                                    | ob 去掉 liter 文字 ate                        |                       |
| itinerant      | 巡回的            | peripatetic, nomadic                                                             |                                           |                       |
| callous        | 结硬块的；无情的       | thick and hardened, lacking pity, unfeeling                                      | callus 老茧                                 |                       |
| unpremeditated | 无预谋的，非故意的      |                                                                                  |                                           |                       |
| premonition    | 预感，预兆          | foreboding, prenotion, presage, presentiment                                     | pre + monition                            |                       |
| trample        | 踩坏，践踏；蹂躏       | tread, override                                                                  |                                           |                       |
| euphoric       | 欢欣的            | feel happy and excited                                                           |                                           |                       |
| obsequiousness | 谄媚             | servility, subservience, abject submissiveness                                   |                                           |                       |
| immaculate     | 洁净的；无暇的        | perfectly clean, unsoiled, impeccable                                            |                                           |                       |
| imposing       | 给人深刻印象的；壮丽雄伟的  | impressive, grand                                                                |                                           |                       |
| subside        | （建筑物等）下陷；平息，减退 | tend downward, descend, become quiet or less, ebb, lull, moderate, slacken, wane |                                           |                       |
| retrospective  | 回顾的；回溯的        |                                                                                  | retrospection                             |                       |
| satirize       | 讽刺             | lampoon, mock, quip, scorch                                                      | sarcastic, sneering, caustic, ironic adj. |                       |
| outlying       | 边远的，偏僻的        | remote from a center or main body                                                |                                           |                       |
| shatter        | 使落下；使散开；粉碎；破坏  |                                                                                  |                                           |                       |
| subversive     | 颠覆性的；破坏性的      |                                                                                  | sub + ver + sive                          |                       |
| stalwart       | 健壮的；坚定的        | outstanding strength, inflexible, decisive                                       |                                           |                       |
| banter         | 打趣，玩笑          | playful, good-humored joking                                                     |                                           |                       |
| superfluous    | 多余的，累赘的        | exceeding what is needed                                                         |                                           |                       |
| tangle         | 纠缠；纷乱          |                                                                                  | tango 探戈舞；entangle 使卷入                    |                       |
| dorsal         | 背部的；脊背的        |                                                                                  |                                           |                       |
| untamed        | 未驯服的           | not controlled                                                                   |                                           |                       |
| negligibly     | 无足轻重的；不值一提的    |                                                                                  | negligible adj.                           |                       |
| saturate       | 浸湿；浸透；使大量吸收或充满 |                                                                                  |                                           |                       |
| brink          | （峭壁）边缘         | verge, border, edge                                                              | blink 眨眼                                  |                       |
| atrocity       | 邪恶；暴行          | evil, iniquitousness, ferocity, barbarousness, truculence,                       | atrocious adj.                            |                       |
| mortify        | 使丢脸；侮辱         | affront, insult, humiliate                                                       |                                           |                       |
| reciprocation  | 互换；报答；往复运动     | mutual exchange, return, alternating motion                                      |                                           | reciprocation of sth. |
| crimson        | 绯红色（的）         | deep purplish red                                                                |                                           |                       |
| reimburse      | 偿还             | pay back, repay                                                                  | re + im + burse                           |                       |
| surmise        | 推测，猜疑          | conjecture, speculate                                                            |                                           |                       |
| jumble         | 使混乱，混杂；杂乱      | to mix in disorder, a disorderly mixture                                         |                                           |                       |
| ascetic        | 禁欲的；苦行者        | astringent, austere, mortified, severe, stern                                    |                                           |                       |
| 12            | 12                   | 12                                                                              | 12                                  | 12                    |
| tempting      | 诱惑人的                 | having an appeal, alluring, attractive, enticing, inviting, seductive           |                                     |                       |
| recount       | 叙述；描写                | narrate                                                                         |                                     |                       |
| skew          | 不直的，歪斜的              | crooked, slanting, running obliquely                                            |                                     |                       |
| impertinent   | 不切题的；无礼的             | irrelevant, rude, reckless                                                      |                                     |                       |
| rancid        | 不新鲜的，变味的             | rank, stinking                                                                  |                                     |                       |
| compassion    | 同情，怜悯                |                                                                                 | dispassion 平心静气                     |                       |
| fortuitous    | 偶然发生的；偶然的            | accidental, lucky, casual, contingent, incidental                               | fortitude 坚毅，坚忍不拔；tenacious 坚韧的，顽强的 |                       |
| obsessed      | 着迷的；沉迷的              |                                                                                 | obsess v.；obese 极胖的                 |                       |
| gouge         | 挖出，骗钱；半圆凿            | scoop out, cheat out of money, a semicircular chisel                            | gauge 准则，规范                         |                       |
| jocular       | 滑稽的；幽默的；爱开玩笑的        | humorous                                                                        | joke                                |                       |
| reticent      | 沉默寡言的                | inclined to be slient, reserved, taciturn, uncommounicative                     |                                     |                       |
| ascent        | 上升，攀登；上坡路，提高，提升      |                                                                                 |                                     |                       |
| unwieldy      | 难控制的；笨重的             | cumbersome, not easily managed                                                  |                                     |                       |
| formidable    | 令人畏惧的；可怕的；难以克服的      | causing fear or dread, hard to handle or overcome                               |                                     |                       |
| embellish     | 装饰，美化                | decorate                                                                        | rebellious 造反的；反叛的；难控制的；rebellion   |                       |
| strip         | 剥去；狭长的一片             | denude, disrobe, a long narrow piece                                            |                                     |                       |
| apropos       | 适当的；有关               |                                                                                 |                                     |                       |
| perfidious    | 不忠的；背信弃义的            | faithless                                                                       |                                     |                       |
| eviscerate    | 取出内脏；去除主要部分          |                                                                                 | viscera 内脏                          |                       |
| compelling    | 引起兴趣的                | captivating, keenly interesting                                                 | a compelling subject                |                       |
| eccentric     | 古怪的，反常的；古怪的人；没有共同圆心的 | unconventional                                                                  |                                     |                       |
| imperative    | 紧急的                  | urgent, pressing, importunate, exigent                                          |                                     |                       |
| ascribe       | 归因于；归咎于              |                                                                                 |                                     |                       |
| thrive        | 茁壮成长；繁荣，兴旺           | flourish, prosper                                                               |                                     |                       |
| strenuous     | 奋发的；热烈的              | vigorously active, fervent, zealous, earnest, energetic, spirited               | tenacious 坚韧的，顽强的                  |                      |
| iconoclastic  | 偶像派的；打破习俗的           |                                                                                 |                                     | iconoclastic ideas    |
| nocturnal     | 夜晚的，夜间发生的            | happening in the night                                                          |                                     |                       |
| synoptic      | 摘要的                  | affording a general view of the whole                                           |                                     |                       |
| infiltrate    | 渗透，渗入                | to pass through                                                                 |                                     |                       |
| buoyant       | 有浮力的；快乐的             | showing buoyancy, cheerful                                                      |                                     |                       |
| prescient     | 有远见的，预知的             |                                                                                 |                                     |                       |
| cacophonous   | 发音不和谐的，不协调的          | marked by cacophony                                                             |                                     |                       |
| evanescent    | 易消失的；短暂的             | vanishing, transient, ephemeral                                                 |                                     |                       |
| somber        | 忧郁的；阴暗的              | melancholy, dark and gloomy, dim, grave, shadowy, shady, overcast, disconsolate |                                     |                       |
| defy          | 违抗，藐视                |                                                                                 |                                     |                       |
| sanguine      | 乐观的；红润的              | optimistic, cheerful and confident                                              |                                     |                       |
| absurd        | 荒谬的，可笑的              |                                                                                 |                                     | absurd assumptions    |
| spurious      | 假的；伪造的               | forged, false, falsified                                                        |                                     |                       |
| prudent       | 审慎的；精明的；节俭的，精打细算的    | judicious, sage, sane                                                           |                                     |                       |
| sedative      | （药物）镇静的；镇静剂          | calming, relaxing, soothing, tranquilizer                                       |                                     |                       |
| sanctimonious | 假装虔诚的                | hypocritically pious or devout                                                  |                                     |                       |
| bland         | 情绪平稳的；无味的            | pleasantly smooth, insipid                                                      |                                     |                       |
| span          | 跨度，两个界限的距离           | extent, length, reach, spread, a stretch between two limits                     |                                     |                       |
| prospect      | 勘探，期望；前景             |                                                                                 |                                     |                       |
| petulant      | 暴躁的，易怒的              | insolent, peevish                                                               |                                     |                       |
| tangential    | 切线的；离题的              | discursive, excursive, rambling, impertinent                                    |                                     |                       |
| ulterior      | 较远的，将来的；隐秘的，别有用心的    |                                                                                 |                                     |                       |
| squirt        | 喷出，溅                 | to spurt                                                                        |                                     | squirt sth. with sth. |
| 13            | 13                       | 13                                                                      | 13                           | 13                |
| inflamed      | 发炎的，红肿的                  |                                                                         | infection                    |                   |
| perpendicular | 垂直的，竖直的                  | exactly upright, vertical                                               |                              |                   |
| prodigal      | 挥霍的，挥霍者                  | lavish                                                                  |                              |                   |
| resignation   | 听从，顺从；辞职                 |                                                                         | resign v.                    |                   |
| prerogative   | 特权                       | privilege, birthright, perquisite, charter,                             |                              |                   |
| plumb         | 测探物，铅锤；垂直的；精确的；深入了解；测量深度 |                                                                         |                              |                   |
| arbitrary     | 专横的，武断的                  | discretionary, despotic, dictatorial, monocratic, autarchic, autocratic | tributary 支流，进贡国；支流的，辅助的，进贡的 |                   |
| disjunctive   | 分离的，转折的，反意的              |                                                                         | dis + junctive               |                   |
| fetid         | 有恶臭的                     | foul, noisome, smelly                                                   |                              |                   |
| aggravated    | 加重，恶化；激怒，使恼火             | exasperate, irritate, peeve, pique, provoke                             |                              |                   |
| assail        | 质问，攻击                    | aggress, beset, storm, strike                                           |                              |                   |
| irascible     | 易怒的                      | hot-tempered, peevish, petulant, touchy, querulous, fractious, fretful  |                              |                   |
| caustic       | 腐蚀性的；刻薄的；腐蚀剂             | corrosive, biting, sarcastic                                            | acoustic 听觉的；声音的             | caustic responses |
| ranching      | 大牧场                      | a large farm                                                            |                              |                   |
| reconcile     | 和解，调和                    | attune, harmonize, reconciliate                                         |                              |                   |
| supercilious  | 目中无人的                    | coolly or patronizingly haughty                                         |                              |                   |
| florid        | 华丽的；（脸）红润的               | highly decorated, showy, rosy, ruddy                                    |                              |                   |
| distinctive   | 出众的，有特色的                 | distinguish, characteristic, typical, eminent, prominent, conspicuous   |                              |                   |
| enthral       | 迷惑                       |                                                                         | thrall 王座                    |                   |
| dogged        | 顽强的                      | stubborn, tenacious,                                                    |                              |                   |
| reclaim       | 纠正，改造；开垦                 |                                                                         |                              |                   |
| dialect       | 方言                       |                                                                         |                              |                   |
| demobilize    | 遣散，使复员                   | disband                                                                 |                              |                   |
| rhapsodic     | 狂热的，狂喜的；狂想曲的             |                                                                         |                              |                   |
| vindictive    | 报复的                      | vengeful                                                                |                              |                   |
| equitable     | 公正的，合理的                  | dispassionate, impartial, impersonal, nondiscriminatory, objective      |                              |                   |
| recondite     | 深奥的，晦涩的                  |                                                                         |                              |                   |
| culpable      | 有罪的，该受谴责的                | blameworthy, deserving blame                                            |                              |                   |
| tactile       | 有触觉的                     |                                                                         |                              |                   |
| prig          | 自命清高者，道学先生               |                                                                         |                              |                   |
| 14            | 14            | 14                                                                                           | 14                           | 14                                 |
| viscous       | 粘滞的，粘性的       | glutinous                                                                                    |                              |                                    |
| venerate      | 崇敬，敬仰         | adore, revere, worship, regard with reverential respect                                      |                              |                                    |
| flounder      | 挣扎；艰难的移动；比目鱼  | struggle                                                                                     | founder                      |                                    |
| capricious    | 变化无常的，任性的     | fickleness, fickle, inconstant, lubricious, mercurial, whimsical, whimsied, erratic, flighty |                              |                                    |
| insolent      | 粗野的，无礼的       | impudent, boldly disrespectful                                                               |                              |                                    |
| deplete       | 大量减少，使枯竭      | drain, impoverish, exhaust                                                                   |                              |                                    |
| oust          | 驱逐            | force out, expel                                                                             |                              |                                    |
| recuperate    | 恢复（健康、体力）；复原  | regain, recover                                                                              |                              |                                    |
| prudish       | 过分守礼的；假道学的    | marked by prudery, priggish                                                                  |                              |                                    |
| defecate      | 澄清，净化         | clarify                                                                                      |                              |                                    |
| detriment     | 损害，伤害         | injury, damage                                                                               |                              |                                    |
| belligerent   | 发动战争的；好斗的，挑衅的 | defiant, provoke, provocation, rebellious                                                    |                              |                                    |
| fallacious    | 欺骗的；误导的；谬误的   | misleading, deceptive, erroneous                                                             |                              |                                    |
| bolster       | 枕垫；支持，鼓励      | cushion, pillow, support, strengthen, reinforce                                              |                              |                                    |
| opprobrious   | 辱骂的；侮辱的       | expressing scorn, abusive                                                                    | abrasive 研磨的                 |                                    |
| flickering    | 闪烁的，摇曳的；忽隐忽现的 | glittery, twinkling                                                                          | flicker v.                   |                                    |
| fuse          | 融化，融合         | combine                                                                                      |                              |                                    |
| rescind       | 废除，取消         | make void                                                                                    |                              |                                    |
| rivet         | 铆钉；吸引（注意）     | attract                                                                                      |                              | be riveted to the spot/ground 呆若木鸡 |
| impunity      | 免受惩罚          | exemption from punishment                                                                    |                              |                                    |
| grim          | 冷酷的，可怕的       | ghastly, forbidding, appearing stern                                                         |                              |                                    |
| unobtrusive   | 不引人注目的        | not + noticeable                                                                             | obtrusive 碍眼的                |                                    |
| reprehensible | 应受谴责的         | deserving reprehension, culpable, blameworthy, blamable, blameful, censurable                |                              |                                    |
| provident     | 深谋远虑的，节俭的     | prudent, frugal, thrifty                                                                     |                              |                                    |
| concoction    | （古怪或少见的）混合物   |                                                                                              |                              |                                    |
| anomaly       | 异常（事物）        |                                                                                              | anomalous adj.               |                                    |
| countenance   | 支持，赞成；表情      | advocate, approbate, approve, encourage, favor, sanction                                     |                              |                                    |
| detest        | 厌恶，憎恨         | dislike intensely, hate, abhor                                                               |                              |                                    |
| overrule      | 驳回，否决         |                                                                                              |                              |                                    |
| coercive      | 强制性的，强迫性的     |                                                                                              | coerce n.；coarse 粗糙的，低劣的，粗俗的 |                                    |
| eclectic      | 折衷的，兼容并蓄的     |                                                                                              |                              |                                    |
| forte         | 长处，特长；强音的     | special accomplishment                                                                       |                              |                                    |
| preempt       | 以优先权获得的；取代    |                                                                                              |                              |                                    |
| compel        | 强迫            | coercive, concuss, oblige, force or constrain                                                |                              |                                    |
| ingrain       | 根深蒂固的；本质      | confirmed, entrenched, ineradicable, inveterate, ingrained, irradicable                      |                              |                                    |
| salubrious    | 有益健康的         | healthful, promoting health                                                                  | salutary 有益的，有益健康的           |                                    |
| indolent      | 懒惰的           | idle, lazy, faineant, slothful, slowgoing, work-shy                                          |                              |                                    |
| slanderous    | 诽谤的           | abusive, false and defamatory                                                                |                              |                                    |
| 15             | 15                      | 15                                                                       | 15                                       | 15                   |
| genre          | （文学艺术等）类型，体裁            |                                                                          | genus；motif （文艺作品等的）主题，主旨                |                      |
| consecutive    | 连续的                     | serial, sequential, successively, continuously, uninterruptedly          |                                          |                      |
| inaugurate     | 举行就职典礼的，创始，开创           | instate, institute, launch, originate, initiate, commence                | augur 蛟龙出海                               |                      |
| propitiate     | 讨好；抚慰                   | assuage, conciliate, mollify, consolate, pacify, placate                 |                                          |                      |
| extensive      | 广大的；多方面的；广泛的            |                                                                          |                                          |                      |
| ignoble        | 卑鄙的                     | dishonorable, mean                                                       | ig + noble                               |                      |
| flaw           | 瑕疵；裂缝，变得有缺陷             | imperfection, defect, blemish, bug, fault, shortcoming                   | pounce, claw, paw 爪                      |                      |
| conflagration  | （建筑、森林）大火               | big, destructive fire                                                    |                                          |                      |
| indenture      | 契约；合同                   |                                                                          | indent 切割成锯齿状（古代分割成锯齿状的契约）               |                      |
| roam           | 漫步，漫步                   |                                                                          | roam in room                             |                      |
| retribution    | 报应，惩罚                   |                                                                          |                                          | retribution for sth. |
| ungainly       | 笨拙的                     | clumsy, awkward, gawky, lumbering, lumpish, lacking smooth or dexterity  |                                          |                      |
| futile         | 无效的，无用的；没出息的            | bootless, otiose, unavailing, useless, without advantage or benefit      | fertile, fertilizer, fertilize 多产、化肥、使受精 |                      |
| unimpeachable  | 无可指责的，无可怀疑的             | irreproachable, blameless, impeccable                                    |                                          |                      |
| prostrate      | 俯卧的；沮丧的；使下跪鞠躬           | prone, powerless, helpless                                               | pro (prone) + strate                     |                      |
| antidote       | 解药                      | a remedy to counteract a poison                                          |                                          |                      |
| presumptuous   | 放肆的；过分的                 | overweening, presuming, uppity                                           |                                          |                      |
| philanthropic  | 慈善（事业）的；博爱的             | characterized by philanthropy, humanitarian                              |                                          |                      |
| peculiar       | 独特的；古怪的                 | distinctive, eccentric, queer, characteristic, idiosyncratic, individual |                                          |                      |
| unenlightened  | 愚昧无知的；不文明的              |                                                                          | un + enlightened                         |                      |
| arbiter        | 权威人士，泰斗                 | arbitrator, a fully qualified person                                     |                                          |                      |
| keen           | 敏锐的；热心的                 | sensitive, enthusiastic                                                  |                                          |                      |
| patriarchal    | 家长的，族长的；父权制的            |                                                                          | patriarchy, patriarch                    |                      |
| rowdy          | 吵闹的；粗暴的                 |                                                                          |                                          |                      |
| docile         | 驯服的；听话的                 | easy to control                                                          |                                          |                      |
| hitherto       | 到目前为止                   |                                                                          | a hitherto unknown fact                  |                      |
| sedulous       | 聚精会神的；勤勉的               | diligent                                                                 |                                          |                      |
| antiquarianism | 古物研究；好古癖                |                                                                          | antique 古董；古代的                           |                      |
| contravene     | 违背（法规、习俗等）              | violate, conflict with, breach, infract, infringe, offend, transgress    |                                          |                      |
| discreet       | 小心的；言行谨慎的               | prudent, modest, calculating, cautious, chary, circumspect, gingerly     |                                          |                      |
| fusion         | 融合；核聚变                  | admixture, amalgam, blend, merger, mixture                               | fuse v.                                  |                      |
| esoteric       | 秘传的，机密的，隐秘的             | abstruse, occult, recondite, arcane, furtive                             |                                          |                      |
| deference      | 敬意，尊重                   | courteous regard or respect                                              |                                          | deference to sth.    |
| dilate         | 使膨胀；使扩大                 | swell, expand, distent,                                                  |                                          |                      |
| bane           | 祸根                      | the cause of distress, death, or ruin                                    |                                          |                      |
| commiserate    | 同情，怜悯                   | sorrow or pity for sb.                                                   |                                          |                      |
| spruce         | 云杉；整洁的                  | neat or smart, trim                                                      |                                          |                      |
| crawl          | 爬，爬行                    |                                                                          | prone position；brawl 争吵，斗殴               |                      |
| oblivious      | 遗忘的；忘却的；疏忽的             | forgetful, unmindful                                                     |                                          |                      |
| disinclination | 不愿意，不情愿                 | aversion, disfavor, dislike, disrelish, dissatisfaction                  |                                          |                      |
| topographical  | 地形学的                    |                                                                          | topography                               |                      |
| ameliorate     | 改善，改良                   | improve                                                                  |                                          |                      |
| aberration     | 越轨                      | being aberrant                                                           |                                          |                      |
| shunt          | 使（火车）转到另一个轨道；改变（某物的）方向的 |                                                                          |                                          |                      |
| 16           | 16              | 16                                                                 | 16                                             | 16                        |
| gush         | 涌出；滔滔不绝的说       | pour out, spout, talk effusively                                   |                                                |                           |
| compendium   | 简要，概略           | digest, pandect, sketch, syllabus, summary, abstract               | recapitulate 扼要概述；synoptic 摘要的                 |                           |
| boastfulness | 自吹自擂；浮夸         |                                                                    | boast v.                                       |                           |
| deprecatory  | 不赞成的，反对的        | disapproving                                                       | predatory 掠夺的；食肉的                              | be deprecatory about sth. |
| disperse     | 消散，驱散           | dispel, dissipate, scatter                                         |                                                |                           |
| dispute      | 争论              | argue, debate                                                      |                                                |                           |
| perennial    | 终年的，全年的；永久的，持久的 | continuing, eternal, lifelong, permanent, perpetual, enduring      |                                                |                           |
| ebb          | 退潮；衰退           | decline, recede from the flood, wane                               |                                                |                           |
| predisposed  | 预设的；有倾向的；使易受感染  | dispose in advance, make susceptible                               |                                                |                           |
| monotony     | 单调，千篇一律         | tedious sameness, humdrum, monotone                                | autonomy 自治                                    |                           |
| convoluted   | 旋绕的；费解的         | coiled, spiraled, intricate, complicated, extremely involved       |                                                |                           |
| overt        | 公开的，非秘密的        | apparent, manifest                                                 |                                                |                           |
| attribute    | 把…归于；属性，特质      | trait, virtue                                                      | ascribe 归因于；归咎于                                | attribute…to…             |
| huddle       | 挤成一团；一推人        | crowd, nestle close                                                | huddle together to handle sth.                 |                           |
| infirm       | 虚弱的             | physically weak                                                    |                                                |                           |
| abysmal      | 极深的；糟透的         | bottomless, unfathomable, wretched, immeasurably bad               |                                                |                           |
| competence   | 胜任，能力           |                                                                    |                                                |                           |
| coordinate   | 使各部分协调；同等的      |                                                                    |                                                |                           |
| calummy      | 诽谤，中伤           | defamation, aspersion, calumniation, denigration, vilification     | slanderous 诽谤的；denigrate 诽谤                    |                           |
| predator     | 食肉动物            | antedate 填写比实际日期早的日期，早于；postdate 填迟…的日期，过期的日期；precursor 先驱，先兆                      | predatory adj.；carnivorous, flesh-eating 肉食动物的 |                           |
| commodious   | 宽敞的             | spacious, roomy                                                    |                                                |                           |
| parable      | 寓言              |                                                                    | allegory 寓言                                    |                           |
| revere       | 尊敬              | deep respect                                                       | severe 严厉的                                     |                           |
| sterile      | 贫瘠且没有植被的；无细菌的   |                                                                    |                                                |                           |
| clutter      | 弄乱；凌乱           |                                                                    | scatter 四散逃窜                                   |                           |
| dispel       | 驱散，消除           | scatter or drive away, disperse                                    | prosper 繁荣                                     |                           |
| fastidious   | 挑剔的，难取悦的        | critical, discriminating                                           |                                                |                           |
| impetuous    | 冲动的，鲁莽的         | impulsive, sudden                                                  | an impetuous decision                          |                           |
| dilute       | 稀释，冲淡           |                 swell, expand, distent, dilate 使膨胀；使扩大                                                  | 假酒                                             |                           |
| permeate     | 扩撒，弥漫，穿透        | impenetrate, penetrate, percolate                                  |                                                |                           |
| beset        | 镶嵌；困扰           | harass                                                             |                                                |                           |
| asceticism   | 禁欲主义            |                                                                    | ascetic 禁欲的；苦行者                                |                           |
| ineligible   | 不合格的            | not legally or morally qualified                                   | in + eligible                                  |                           |
| temptation   | 诱惑，诱惑物          | sth. tempting, allurement, decoy, enticement, lure, inveiglement   |                                                |                           |
| indented     | 锯齿状的；高低不平的      |                                                                    | indenture 契约；合同                                |                           |
| succinctness | 简洁，简明           | conciseness, concision                                             |                                                |                           |
| turbulent    | 混乱的，骚乱的         | causing unrest, violence, disturbance, tempestuous                 |                                                |                           |
| mordant      | 讥讽的，尖酸的         | sarcastic, sneering, caustic, ironic                               |                                                |                           |
| tribulation  | 苦难，忧患           | distress or suffering resulting from oppression or persecution     |                                                |                           |
| angular      | 生硬的，笨拙的         | lacking grace or soomthness, awkward, having angles, thin and bony |                                                |                           |
| purport      | 意义，涵义，主旨        | meaning conveyed, implied, gist                                    |                                                |                           |
| curt         | （言辞，行为）简略而草率的   | brief, rude, terse                                                 |                                                |                           |
| 17            | 17                 | 17                                                                                                 | 17                                                                                  | 17                    |
| taunt         | 嘲笑，讥讽；嘲弄，嘲讽        |                                                                                                    | burlesque, mock, mockery, travesty, sarcastic, sarcastic, sneering, caustic, ironic |                       |
| impeccable    | 无瑕疵的               | faultless, flawless, fleckless, immaculate, indefectible, irreproachable, perfect                  |                                                                                     |                       |
| rationale     | 理由；基本原理            | fundamental reasons, maxim, an established principle, axiom                                        |                                                                                     |                       |
| fortify       | 加强，巩固              |                                                                                                    | fortitude 坚毅，坚忍不拔；fortuitous 偶然发生的                                                  |                       |
| scenic        | 风景如画的              | natural scenery                                                                                    | scenic lift                                                                         |                       |
| particulate   | 微粒的；微粒             | minute separate particle                                                                           |                                                                                     |                       |
| designate     | 指定，任命；指明，指出        | indicate and set apart for a specific purpose                                                      |                                                                                     |                       |
| protrude      | 突出，伸出              | jut out                                                                                            | """convex 凸的；concave 凹的"                                                            |                       |
| potshot       | 盲目射击；肆意抨击          |                                                                                                    |                                                                                     |                       |
| spherical     | 球的，球状的             | globe-shaped, globular, rotund, round                                                              |                                                                                     |                       |
| delude        | 欺骗，哄骗              | mislead, deceive, trick, beguile, bluff                                                            |                                                                                     |                       |
| pithy         | （讲话或文章）简练有力的；言简意赅的 | concise, tersely cogent                                                                            |                                                                                     |                       |
| jolting       | 令人震惊的              | astonishing, astounding, startling, surprising                                                     |                                                                                     |                       |
| confound      | 使迷惑，搞混             |                                                                                                    |                                                                                     |                       |
| impute        | 归咎于，归于             | attribute                                                                                          |                                                                                     |                       |
| incipient     | 初期的，起初的            | inceptive, initial, initiatory, introductory, nascent, beginning                                   |                                                                                     |                       |
| exert         | 运用，行使，施加           | apply with great energy or straining effort                                                        |                                                                                     |                       |
| combative     | 好斗的                |                                                                                                    | combat 与…搏斗                                                                         |                       |
| drudgery      | 苦工，苦活              | dull and fatiguing work                                                                            | grudge v.                                                                           |                       |
| effervescence | 冒泡；活泼              | bubbling, hissing, foaming, liveliness, exhilaration, buoyancy, ebullience, exuberance, exuberancy |                                                                                     |                       |
| heinous       | 十恶不赦的，可憎的          | reprehensible, abominable                                                                          | hate                                                                                |                       |
| blazing       | 炽烧的，闪耀的            |                                                                                                    | blaze 燃烧，照耀                                                                         |                       |
| abhor         | 憎恨，厌恶              | detest, abominate, loathe, execrate, hate, antipathy,                                              | repellent, repugnant 令人厌恶的                                                          |                       |
| ostracize     | 排斥；放逐              | banish, expatriate, expulse, oust, exile by ostracism                                              |                                                                                     |                       |
| repugnance    | 嫌恶，反感              | strong dislike, distaste, or antagnoism                                                            |                                                                                     |                       |
| redirect      | 改寄（信件）；改变方向        |                                                                                                    |                                                                                     |                       |
| conscientious | 尽责的；小心谨慎的          | careful, scrupulous                                                                                |                                                                                     | be conscientious of … |
| obliqueness   | 斜度，倾斜              |                                                                                                    | oblique                                                                             |                       |
| fret          | （使）烦躁；焦虑；烦躁        | irritate, annoy, agitation, cark, pother, ruffle, vex, tension                                     |                                                                                     |                       |
| ossify        | 骨化，僵化              | into bone                                                                                          | oss （骨头） + ify                                                                      |                       |
| proselytize   | （使）皈依              | recruit or convert to a new faith                                                                  |                                                                                     |                       |
| despicable    | 可鄙的，卑劣的            | contemptible, deserving to be despired                                                             |                                                                                     |                       |
| adamant       | 坚决的，固执的；强硬的        | obdurate, rigid, unbending, unyielding, inflexible, unbreakable                                    | remain adamant in …                                                                 |                       |
| secular       | 世俗的，尘世的            | worldly rather than spiritual                                                                      |                                                                                     |                       |
| scarcity      | 不足的，缺乏的            |                                                                                                    | scarce adj.                                                                         | scarcity of …         |
| vestige       | 痕迹，遗迹              | relic, trace                                                                                       |                                                                                     |                       |
| ratify        | 批准                 | confirm, approve formally                                                                          |                                                                                     |                       |
| pensive       | 沉思的；忧心忡忡的          | reflective, meditative, anxious                                                                    | pension 养老金                                                                         |                       |
| drizzly       | 毛毛细雨的              |                                                                                                    |                                                                                     |                       |
| barren        | 不育的，贫瘠的，不结果实的      | sterile, bare                                                                                      | “拔了”                                                                                |                       |
| glossary      | 词汇表，难词表，           |                                                                                                    |                                                                                     |                       |
| salient       | 显著的，突出的            | noticeable, conspicuous, prominent, outstanding                                                    |                                                                                     |                       |
| regale        | 款待，宴请；使…快乐         | feast with delicacies (delicacy)                                                                   |                                                                                     |                       |
| eloquent      | 雄辩的，流利的            | articulate, forceful and fluent expression                                                         |                                                                                     |                       |
| fused         | 熔化的                |                                                                                                    | fuse v.                                                                             |                       |
| nubile        | 适婚的，性感的            | marriageable, sexually attractive                                                                  |                                                                                     |                       |
| fraternity    | 同行；友爱              |                                                                                                    | camaraderie 同志之情；友情                                                                 |                       |
| aggressive    | 好斗的，有进取心的          | militant, assertive, full of enterprise and initiative, assertory, pushful, self-assertive         |                                                                                     |                       |
| 18                | 18                | 18                                                                                                                                               | 18                                                 | 18            |
| avaricious        | 贪婪的，贪心的           | greedy, insatiably, insatiable                                                                                                                   | avarice n.                                         |               |
| exorcise          | 驱除妖魔；去除           | expel, get rid of                                                                                                                                |                                                    |               |
| iterate           | 重申，重做             | do or utter repeatedly                                                                                                                           | illiterate 文盲的；literate 识字                         |               |
| debilitate        | 使衰弱               | weaken, make weak or feeble, depress, enervate                                                                                                   |                                                    |               |
| serenity          | 平静                | placidity, repose, tranquility                                                                                                                   | serene adj.；serendipity 偶然性；偶然的事物                  |               |
| repel             | 击退；使反感            | fight against, resist, cause aversion                                                                                                            |                                                    |               |
| stringy           | 吝啬的，小气的           | not generous or liberal, misery, chinchy, niggard, parsimonious, penurious, pinchpenny                                                           |                                                    |               |
| exhaust           | 耗尽，使筋疲力尽；（机器）废气   |                                                                                                                                                  |                                                    |               |
| belie             | 掩饰，证明为假           | disguise, misrepresent, prove false, distort, falsify, garble, misstate                                                                          |                                                    |               |
| exacerbate        | 使加重，使恶化           | aggravate disease, pain, annoyance, etc                                                                                                          |                                                    |               |
| propitious        | 吉利的，顺利的；有利的       | auspicious, favorable, advantageous                                                                                                              |                                                    |               |
| antithesis        | 对立，相对             | contract or opposition                                                                                                                           |                                                    |               |
| impugn            | 提出异议；对…表示怀疑       | to challenge as false or questionable                                                                                                            |                                                    |               |
| sequester         | （使）隐退；使隔离         | seclude, withdraw, set apart                                                                                                                     | sequestrate 扣押                                     |               |
| capitulate        | （有条件的）投降          | surrender conditionally                                                                                                                          |                                                    |               |
| bellicose         | 好战的，好斗的           | warlike, belligerent                                                                                                                             | bell 战斗                                            |               |
| unliterary        | 不矫揉造作的；不咬文嚼字的     | nonliterary                                                                                                                                      |                                                    |               |
| retrieve          | 寻回，取回；挽回，弥补       | regain, redeem, remedy                                                                                                                           |                                                    |               |
| retire            | 撤退，后撤；退休，退役       | withdraw, move back                                                                                                                              |                                                    | retire from … |
| egregious         | 极端恶劣的             | conspicuously bad, flagrant, heinous, reprehensible, abominable, repellent, repugnant, loathsome, odious, revolting, arousing disgust, repulsive | gorgeous 华丽的；极好的                                   |               |
| unscathed         | 未受损伤的；未受伤害的       | wholly unharmed                                                                                                                                  | un + scathed                                       |               |
| defiance          | 挑战，违抗，反抗          | open disobedience, contempt, contumacy, recalcitrance, stubbornness                                                                              | defiant 藐视                                         |               |
| perceptive        | 感知的；知觉的；有洞察力的；敏锐的 |                                                                                                                                                  |                                                    |               |
| dissonant         | 不和谐的，不一致的         | opposing in opinion, temperament, discordant                                                                                                     |                                                    |               |
| defect            | 缺点，瑕疵；变节，脱党       | fault, flaw, forsake, blemish                                                                                                                    | infect 感染                                          |               |
| ethereal          | 太空的；轻巧的；轻飘飘的      |                                                                                                                                                  | ether 太空，苍天；celestial 天体的，天上的；heavenly 天空的，天堂的，天上的 |               |
| stipulate         | 要求以…为条件；约定；规定     | to make an agreement                                                                                                                             |                                                    |               |
| trespass          | 侵犯；闯入私人空间         | make an unwarranted or uninvited incursion                                                                                                       |                                                    |               |
| extravagance      | 奢侈，挥霍             |                                                                                                                                                  | extravagant adj.                                   |               |
| appall            | 使惊骇；使胆寒           | shock, fill with horror or dismay                                                                                                                | a + pale 苍白                                        |               |
| tepid             | 微温的；不热情的          | moderately warm, lacking in emotional warmth or enthusiasm, halfhearted, lukewarm, unenthusiastic                                                |                                                    |               |
| counterproductive | 事与愿违的             |                                                                                                                                                  | counter + productive                               |               |
| tortuous          | 曲折的，拐弯抹角的；弯弯曲曲的   | devious, indirect tactics, winding                                                                                                               |                                                    |               |
| communal          | 全体共同的，共享的         | held in common                                                                                                                                   |                                                    |               |
| fray              | 吵架，打斗；磨破          | noisy quarrel or fight, become worn, ragged, or reveled by rubbing                                                                               |                                                    |               |
| venerable         | 值得尊敬的，庄严的         | august, revered, respectable                                                                                                                     | venerate v.                                        |               |
| contemplative     | 沉思的（人）            |                                                                                                                                                  |                                                    |               |
| placate           | 抚慰，平息             | soothe or mollify                                                                                                                                |                                                    |               |
| 19          | 19            | 19                                                                                  | 19                    | 19    |
| schematic   | 纲要的，图解的       | of schema or diagram                                                                |                       |       |
| compliance  | 顺从，遵从         | obedience                                                                           | comply 顺从             |       |
| eschew      | 避开，戒绝         | shun, avoid, abstain                                                                | es 出 + chew 咀嚼 吃一堑长一智 |       |
| denote      | 指示，表示         | mark, indicate, signify                                                             |                       |       |
| obviate     | 排除，消除         | remove, get rid of, eliminate, exclude, preclude                                    |                       |       |
| precipitous | 陡峭的           | steep, perpendicular, overchanging in rise or fall                                  |                       |       |
| scrupulous  | 恪守道德规范的，一丝不苟的 | having moral integrity, punctiliously exact, conscientious, meticulous, punctilious |                       |       |
| radicalism  | 激进主义          |                                                                                     | radical adj.          |       |
| insane      | 无意义的，空洞的，愚蠢的  | empty, lacking sense, void, silly, innocuous, insipid, jejune, sapless, vapid       |                       |       |
| compass     | 指南针，罗盘；界限，界定  | scope, range                                                                        |                       |       |
| ignominy    | 羞耻，耻辱         | shame or dishonor, infamy, disgrace, disrepute, opprobrium                          | ig 不 + nominy 名声      |       |
| antiquity   | 古老；古人；古迹      |                                                                                     | antique 古董，古老的        |       |
| repulse     | 击退，回绝；拒绝      | repel, rebuff, rejection                                                            | re + pulse            |       |
| venal       | 腐败的，贪赃枉法的     | characterized by corrupt, bribery                                                   |                       |       |
| quiescent   | 不动的，静止的       | inactivate, abeyant, dormant, latent                                                |                       |       |
| pernicious  | 有害的，致命的       | noxious, deadly                                                                     |                       |       |
| enervate    | 使虚弱，使无力       | lessen the vitality ot strength of                                                  |                       |       |
| 20            | 20                  | 20                                                                                                                   | 20                                                                 | 20                          |
| preconception | 先入之见，偏见             | prejudice, discrimination                                                                                            | preconceive adj.；pre 先 + conception 看法                             |                             |
| devout        | 虔诚的；忠诚的             | seriously concerned, totally committed                                                                               | devote 投身于                                                         |                             |
| spanking      | 强烈的，疾行的             | strong, fast                                                                                                         |                                                                    | a spanking pace/rate        |
| devour        | 狼吞虎咽的吃；吞食；贪婪的看      | eat hungrily, enjoy avidly                                                                                           |                                                                    |                             |
| explicate     | 详细解说                | make clear or explicit, explain fully, elucidate, illustrate, interpret                                              |                                                                    |                             |
| bleach        | 去色，漂白；漂白剂           |                                                                                                                      | b + leach 过滤                                                       |                             |
| pejorative    | 轻视的，贬低的             | tending to disparage, depreciatory, belittle, derogate, to speak slightingly of, depreciate, decry, denounce, debase |                                                                    |                             |
| ponderous     | 笨重的，（因太重太大）不便搬运的    | unwieldy or clumsy because of weight or size                                                                         | pond 大的，沉的；                                                        |                             |
| particular    | 特别是；细节，详情           | fact, detail, item                                                                                                   |                                                                    |                             |
| pedagogic     | 教育学的                | of or relating to education                                                                                          | Pedagogy 教育学；pedant 学究                                             |                             |
| parody        | 拙劣的模仿；滑稽模仿作品或表演     | a feeble or ridiculous imitation                                                                                     |                                                                    |                             |
| pungent       | 辛辣的，刺激的；尖锐的，尖刻的     | piquant, sharp and to the point                                                                                      | pung 尖刺 + ent                                                      |                             |
| superb        | 上乘的，出色的             | highest degree of excellence, brilliance, or competence, lofty, sublime                                              |                                                                    |                             |
| supersede     | 淘汰；取代               | take the position, force out as inferior                                                                             |                                                                    |                             |
| ambivalent    | （对人对物）有矛盾看法的        | having contradictory attitudes towards sth.                                                                          |                                                                    |                             |
| concentration | 专心，专注；集中            |                                                                                                                      | concentrate v.                                                     |                             |
| heed          | 注意，留心               | give careful attention to                                                                                            | need 需要                                                            |                             |
| incentive     | 刺激，诱因，动机；刺激因素       | motive, motivate, goad, impetus, impulse, stimulus, incite                                                           |                                                                    |                             |
| stout         | 肥胖的；强壮的             | bulky in body, vigorous, sturdy                                                                                      |                                                                    |                             |
| incandescent  | 遇热发光的；发白热光的         | Incandescent lamps 白炽灯                                                                                               | candy 蜡烛，糖果                                                        |                             |
| fussy         | 爱挑剔的，难取悦的           | dainty, exacting, fastidious, finicky, meticulous                                                                    | fuzzy 模糊的                                                          |                             |
| puncture      | 刺穿，戳破，刺孔            |                                                                                                                      | punct 点 + ture                                                     |                             |
| wearisome     | 令人厌倦、疲倦             |                                                                                                                      | weary v.                                                           |                             |
| persist       | 坚持不懈；坚持说；继续存在       | abide, endure, perdure, persevere, adhere, tenacious, fortitude                                                      |                                                                    |                             |
| nominally     | 名义上的，有名无实的          |                                                                                                                      | nominal 名义上的                                                       |                             |
| quash         | 镇压，（依法）取消           | suppress, nullify with judicial action, abrogate, repeal, rescind                                                    |                                                                    |                             |
| elude         | 逃避，搞不清；理解不了         | avoid adroitly, escape the perception or understanding, eschew, evade, shun                                          |                                                                    |                             |
| refrain       | 抑制；叠句               | curb, restrain, regular recurring phrase or verse                                                                    | re + frain 笼头                                                      |                             |
| humdrum       | 单调的，乏味的             | dull, monotonous, boring                                                                                             | hum 嗡嗡 + drum 鼓声                                                   |                             |
| nuance        | 细微差异                | subtle difference, nicety, shade, subtlety, refinement                                                               |                                                                    |                             |
| philistine    | 庸俗的人；对文化艺术无知的人      | a smug, ignorant, especially middle-class person                                                                     | Philistia 腓力斯人；煤老板                                                 |                             |
| sabotage      | 阴谋破坏，颠覆活动           | deliberate subversion                                                                                                |                                                                    | an act of military sabotage |
| divergent     | 分叉的，叉开的；发散的，扩散的，不同的 | diverging from each other, disparate, dissimilar, distant, diverse                                                   |                                                                    |                             |
| anathema      | 被诅咒的人，（天主教的）革出教门    | one that is cursed, a formal ecclesiastical ban, curse                                                               |                                                                    |                             |
| humility      | 谦逊；谦恭               | being humble, modesty                                                                                                | humble adj.                                                        |                             |
| ingeniousness | 独创性                 | ingenuity                                                                                                            |                                                                    |                             |
| irritable     | 易怒的，急躁的，易受到刺激的      | easily annoyed, choleric, fretful, irascible, responsive to stimulus                                                 |                                                                    |                             |
| misdirect     | 误导                  | give wrong direction to                                                                                              |                                                                    |                             |
| repressive    | 抑制的；镇压的，残暴的         | inhibitory, oppressive                                                                                               |                                                                    |                             |
| sonorous      | （声音）洪亮的             | full or loud in sound                                                                                                |                                                                    |                             |
| woolly        | 羊毛的；模糊的             | lacking sharp detail or clarity                                                                                      | wool n.                                                            |                             |
| sustain       | 承受（困难）；支撑（重量或压力）    | undergo, withstand, bolster, prop, underprop                                                                         |                                                                    |                             |
| actuate       | 开动，促使               | motivate, activate                                                                                                   | accurate 准确的                                                       |                             |
| strait        | 海峡；狭窄的              | narrow                                                                                                               | isthmus 地峡                                                         |                             |
| drab          | 黄褐色的；单调的，乏味的        | yellowish brown, not bright or lively, monotonous                                                                    |                                                                    |                             |
| subdue        | 征服；压制；减轻            | crush, overpower, subjugate, conquer, vanquish, reduce                                                               |                                                                    |                             |
| dissipate     | （使）消失，消散；浪费         | break up and scatter or vanish, to waste or squander                                                                 |                                                                    |                             |
| excavate      | 开洞，凿洞；挖掘            | scoop, shovel, unearth                                                                                               | ex + cave 洞 + ate                                                  |                             |
| discretion    | 谨慎，审慎               | circumspection, prudence                                                                                             |                                                                    |                             |
| sully         | 玷污，污染               | make soiled or tarnished, defile, stain, tarnish                                                                     | blemished, discolored, marked, spotted, tarnished, stained 污染的，玷污的 |                             |
| extol         | 赞美                  | laud, prise highly, acclaim, applaud, compliment, praise, recommend, commend, eulogize, tout, felicitate, rave       |                                                                    |                             |
| sentient      | 有知觉的；知悉的            | responsive to or conscious of sense impressions, aware                                                               |                                                                    |                             |
| unregulated   | 未受控制的，未受约束的         | not controlled                                                                                                       |                                                                    |                             |
| debatable     | 未决定的，有争论的           | open to dispute                                                                                                      | debate v.                                                          |                             |
| odorless      | 无嗅的，没有气味的           |                                                                                                                      | odor 气味 n.                                                         |                             |
| contempt      | 轻视，鄙视               | defiance, disdain, disparagement                                                                                     | depreciate, belittle, disparage, disapprove v.                     | contempt for …              |
| specious      | 似是而非的；华而不实的         | tawdry                                                                                                               | spec 看 + ious                                                      |                             |
| ballad        | 歌谣，小曲               |                                                                                                                      | “芭蕾的”                                                              |                             |
| gall          | 胆汁；怨恨               | bile, hatred, bitter feeling                                                                                         | wall 墙                                                             |                             |
| 21             | 21                  | 21                                                                                           | 21                            | 21                   |
| intersect      | 相交；贯穿，横穿            |                                                                                              | inter + sect (section)        |                      |
| aphorism       | 格言                  | maxim, adage                                                                                 |                               |                      |
| instigate      | 怂恿，鼓动，煽动            | urge on, foment, incite, agitate, abet, provoke, stir                                        | in 进入 + stig (sting 刺激) + ate |                      |
| vulgar         | 无教养的，庸俗的            | morally crude, undeveloped                                                                   |                  vogue 时尚，流行             |                      |
| formidably     | 可怕的；难对付的，强大的        | strongly                                                                                     | formidable                    |                      |
| caste          | 社会等级，等级             | class distinction                                                                            |                               |                      |
| convivial      | 欢乐的，乐观的             |                                                                                              | con + vivial；vivid 生动的        |                      |
| adverse        | 有害的，不利的，敌对的，相反的     | not favorable, hostile, contrary                                                             | ad + verse                    |                      |
| precipitate    | 使突然降临，加速，促成；鲁莽的，轻率的 | bring about abruptly, hasten, impetuous                                                      | peripatetic 巡游的，游动的           | precipitate … into … |
| scheme         | 阴谋，（作品等）体系结构        | crafty or secret plan, systematic or organized framework, design                             | schema 图表                     |                      |
| verdant        | 葱郁的，翠绿的             | green in tint or color                                                                       |                               |                      |
| circumstantial | 不重要的，偶然的；描述详细的      | incidental, marked by careful attention to detail                                            |                               |                      |
| exorbitant     | 过分的，过度的             | excessive, extravagant, extreme, immoderate, inordinate                                      |                               | exorbitant fees      |
| auspicious     | 幸运的；吉兆的             | propitious, favored by future, favorable, advantageous                                       | auspices 支持，赞助                |                      |
| transgress     | 冒犯，违背               | violate                                                                                      |                               |                      |
| dupe           | 易上当者                | easily fooled, tricked                                                                       | “丢谱”                          |                      |
| ruthless       | 无情的，残忍的             | merciless, cruel                                                                             |                               |                      |
| perspire       | 出汗                  | sweat                                                                                        | per + spire 呼吸                |                      |
| cringe         | 畏缩，谄媚               | shrink from sth, dangerous or painful, servile manner, fawn                                  |                               | cringe from …        |
| remiss         | 疏忽的，不留心的            | negligent                                                                                    |                               |                      |
| revival        | 苏醒，恢复，复兴            | reanimation, renaissance, renascence, resuscitation, revivification                          |                               |                      |
| grudge         | 吝惜，勉强给或承认；不满，怨恨     | be reluctant to give or submit, feel resentful                                               | drudgery 苦工，苦活                |                      |
| haphazardly    | 偶然的，随意的，杂乱的         | accidentally, randomly, in a jumble                                                          |                               |                      |
| extraneous     | 外来的，无关的             | from the outside, not pertinent                                                              | extra + neous                 |                      |
| analogous      | 相似的，可比拟的            | akin, comparable, corresponding, parallel, undifferentiated                                  |                               | be analogous to …    |
| animosity      | 憎恶，仇恨               | feeling of strong dislike or hatred, animus, antagonism, enmity, hostility, rancor           |                               |                      |
| emancipate     | 解放，释放               | free from restraint                                                                          |                               |                      |
| incessant      | 不停的，不断的             | continuing without interruption                                                              |                               |                      |
| therapy        | 治疗                  | therapeutic treatment                                                                        |                               |                      |
| stronghold     | 要塞，堡垒，根据地           | fortified place, place of security or survival, fortress                                     | strong + hold                 |                      |
| alloy          | 合金                  | substance composed of two or more metals, admixture, amalgam, composite, fusion, interfusion |                               |                      |
| taut           | 绷紧的，拉紧的             | having no slack, tightly drawn, stiff, tense                                                 |                               |                      |
| surveillance   | 监视，盯梢               | close observation of a person                                                                | inspection, supervision       |                      |
| obligatory     | 强制性的，义务的            | binding in law or conscience, compulsory, imperative, mandatory                              |                               |                      |
| unarticulated  | 表达不清的               |                                                                                              | not + articulated             |                      |
| bonanza        | 富矿脉；贵金属矿            | an exceptionally large and rich mineral deposit, golconda, eldorado                          |                               |                      |
| coalesce       | 联合，合并               | unite or merge into a single body, mix, associate, bracket, combine, conjion                 |                               |                      |
| pugnacious     | 好斗的                 | having a quarrelsome and combative nature                                                    |                               |                      |
| spurn          | 拒绝，摒弃               | disdainful rejection                                                                         | squirt, spurt 喷出，溅        |                  |
| dilatory       | 慢吞吞的，磨蹭的            | inclined to decay, slow or late in doing wth.                                                |                               |                      |
| 22           | 22                 | 22                                                                | 22                                       | 22    |
| infest       | 大批出没于，骚扰；寄生于       | parasite                                                          | in + fest 集会                             |       |
| hunch        | 直觉，预感              | an intuitive feeling or a premonition                             | have a hunch that ...                    |       |
| nomadic      | 游牧的；流浪的            | itinerant, peripatetic, vagabond, vagrant                         |                                          |       |
| contrive     | 计划，设计              | think up, devise, scheme, connive, intrigue, machinate, plot      |                                          |       |
| slavish      | 卑屈的；效仿的，无创造性的      |                                                                   | slave + ish                              |       |
| flaggy       | 枯萎的，松软无力的          | floppy, limp, soft, flaccid, weak, flabby                         | flabby （肌肉）不结实的，松弛的；意志薄弱的                |       |
| intimate     | 亲密的；密友；暗示          | closely acquainted, an intimate friend, hint or imply, suggest    |                                          |       |
| paragon      | 模范，典范              | paradigm, exemplar, a model of excellence or perfection           |                                          |       |
| disdain      | 轻视，鄙视              | contempt, contemn, despise                                        | disdain for …；sustain 承受（困难），支撑（重量或压力）   |       |
| recklessness | 鲁莽，轻率              |                                                                   | reckless adj.                            |       |
| snub         | 冷落，不理睬             | treat with contempt or neglect, cold-shoulder, rebuff             |                                          |       |
| recoil       | 弹回，反冲；退却，萎缩        |                                                                   | re 反 + coil 卷，盘绕                         |       |
| distraught   | 心神狂乱的；发狂的；心烦意乱的    | mentally confused, distressed                                     | distract v. 分散注意力，使不安                    |       |
| odious       | 可憎的；令人作呕的          | disgusting, offensive                                             | “呕得要死”                                   |       |
| divulge      | 泄漏，透露              | disclose, betray, reveal                                          | indulge 放纵，纵容，满足                         |       |
| repudiation  | 拒绝接受，否认            |                                                                   | repudiate v.                             |       |
| perpetuate   | 使永存                |                                                                   | perpetual adj.                           |       |
| vent         | 发泄（感情，尤指愤怒）；开孔；孔，口 | discharge, expel, an opening                                      |                                          |       |
| inert        | 惰性的；呆滞的            | chemical inactive, dull                                           | in 不 + ert 动的                            |       |
| ductile      | 易延展的；可塑的           | capable of being stretched, easily molded, pliable                | Doctrine 教义                              |       |
| restitution  | 归还；赔偿              | restoration                                                       |                                          |       |
| conversant   | 精通的，熟练的            | versed, familiar and acquainted                                   | versed                                   |       |
| conserve     | 保存，保藏              | keep in safe and sound state                                      |                                          |       |
| trait        | （人的）特征显著的          | a distinguishing feature, attribute, characteristic, peculiarity  |                                          |       |
| trace        | 痕迹，追踪              | engram, relic                                                     |                                          |       |
| resonant     | （声音）洪亮的；回响的，共鸣的    | enriched by resonance, echoing                                    |                                          |       |
| trauma       | 创伤，外伤              | injury caused by extrinsic agent                                  |                                          |       |
| sophomoric   | 一知半解的              |                                                                   | dilettante, dabbler, amateur 一知半解者，业余爱好者 |       |
| formation    | 组成，形成；编队，排列        |                                                                   | form + ation                             |       |
| pilgrim      | 朝圣者；（出国）旅行者        | wayfarer                                                          |                                          |       |
| captious     | 吹毛求疵的              | carping                                                           | quibble, cavil 吹毛求疵                      |       |
| sordid       | 卑鄙的，肮脏的            | dirty, filthy, foul, mean, seedy                                  |                                          |       |
| intransigent | 不妥协的               | uncompromising, incompliant, intractable, obstinate, pertinacious |                                          |       |
| 23          | 23                | 23                                                                            | 23                         | 23               |
| bemused     | 茫然的，困惑的           | confused                                                                      | be + muse 沉思               |                  |
| pledge      | 誓言，保证；发誓          | solemn promise, vow to do sth., commitment, swear                             |                            |                  |
| cumber      | 拖累，妨碍             | hamper                                                                        |                            |                  |
| serpentine  | 像蛇般蜷曲的；蜿蜒的        | winding or turning one way to another                                         | serpent 蛇                  |                  |
| lump        | 块，肿块；形成块状         | become lumpy                                                                  |                            | lump … together  |
| retort      | 反驳                | counter argument                                                              |                            |                  |
| abstinent   | 饮食有度的；有节制的，禁欲的    | constraining from indulgence                                                  |                            |                  |
| maraud      | 抢劫，掠夺             | pillage, loot, rob                                                            |                            |                  |
| crimp       | 使皱起，使卷曲；抵制，束缚     |                                                                               |                            |                  |
| pacify      | 使安静，抚慰            | make calm, quiet, and satisfied                                               |                            |                  |
| hoist       | 提升，升起；起重机         | raise or haul up, lift, elevate                                               |                            |                  |
| whet        | 磨快；刺激             | sharpen, excite, stimulate, acuminate, edge, hone                             |                            |                  |
| befuddle    | 使迷惑不解；使醉酒昏迷       | confuse, muddle, stupefy                                                      |                            |                  |
| swindle     | 诈骗                |                                                                               | wind                       |                  |
| ordinance   | 法令，条例             | governmental statue of regulation                                             | ordin 命令 + ance            |                  |
| salve       | 药膏；减轻，缓和          |                                                                               | salv 救援 + e                |                  |
| insouciance | 漠不关心，漫不经心         | lighthearted unconcern                                                        |                            |                  |
| candidacy   | 候选人资格的            |                                                                               | candidate n.；candid 诚实的直率的 |                  |
| defuse      | 拆除引信，使去除危险性；平息    |                                                                               | de + fuse 导火索              |                  |
| quota       | 定额，配额             | someone's share                                                               | aquatic 水生的，水中的            |                  |
| bouncing    | 活泼的，健康的           | lively, animated, good healthy                                                |                            |                  |
| potentiate  | 加强，强化             | make effective or active                                                      | potent                     |                  |
| mien        | 风采，态度             | air, bearing, demeanor                                                        | “迷你”                       |                  |
| obtuse      | 愚笨的；钝的            | dull or insensitive, blunt                                                    |                            |                  |
| gaff        | 大鱼钩；鱼叉            |                                                                               |                            |                  |
| bumble      | 说话含糊；拙劣的做         | stumble, proceed clumsily, burr, buzz, hum                                    | humble 谦逊的                 |                  |
| expulsion   | 驱逐，逐出             | the act of expelling                                                          |                            |                  |
| wretched    | 可怜的，不幸的，悲惨的       | in a very unhappy and unfortunate state                                       |                            |                  |
| swoop       | 猛扑，攫取             | move in a sudden sweep, seize or snatch                                       |                            |                  |
| pushy       | 有进取心的，爱出风头的，固执己见的 | aggressive                                                                    |                            |                  |
| plunder     | 抢劫，掠夺             | maraud, pillage                                                               | pl (place) + under         |                  |
| epilogue    | 收场白；尾声            | a closing section                                                             |                            |                  |
| unbidden    | 未经邀请的             | unasked, uninvited                                                            | un + bid 邀请 + den          |                  |
| soggy       | 湿透的               | clammy, dank, sopping, waterlogged, saturated or heavy with water or moisture |                            |                  |
| convene     | 集合；召集             | come together, assemble, call or meet                                         | con 一起 + ven 来             |                  |
| dispatch    | 派遣；迅速处理；匆匆吃完；迅速   | send off promptly, dispose efficiently, promptness, haste                     |                            |                  |
| harp        | 竖琴，弹竖琴；喋喋不休的说或写   | excessive and tedious                                                         |                            |                  |
| wispy       | 纤细的，脆弱的，一缕缕的      |                                                                               |                            | wispy hair       |
| toll        | 通行费；代价，损失；敲       |                                                                               | “痛”                        |                  |
| inveigle    | 诱骗，诱使             | win with deception, lure                                                      |                            |                  |
| mumble      | 咕哝，含糊不清的说         | grumble, murmur, mutter, speak or say unclearly                               |                            |                  |
| contaminate | 弄脏，污染             | make impure, pollute, smudge                                                  | con + tamin 接触 + ate       |                  |
| tamp        | 夯实，捣实             |                                                                               | “踏”                        |                  |
| ken         | 视野，知识范围           | perception                                                                    |                            | beyond one's ken |
| 24            | 24            | 24                                                                        | 24                                     | 24                            |
| voracity      | 贪食，贪婪         | greed, rapacity                                                           | voracious adj.                         |                               |
| smarmy        | 虚情假意的         | oily, smooth, sleek, unctuous                                             | false earnestness；s 假 + marm(warm) + y |                               |
| confidant     | 心腹朋友，知己，密友    | one to whom secrets are entrusted                                         |                                        |                               |
| snide         | 挖苦的，讽刺的       | slyly disparaging, insinuating, sarcastic, sneering, caustic, ironic      |                                        |                               |
| ripple        | （使）泛起涟漪；波痕，涟漪 |                                                                           |                                        | a ripple of applause/laughter |
| lope          | 轻快的步伐；大步跑     | easy, swinging stride, run with steady gait                               | lobe 耳垂                                |                               |
| recline       | 斜倚，躺卧         | lie down                                                                  |                                        |                               |
| recess        | 休假；凹槽         | alcove, cleft, suspension of business                                     | re 反着 + cess 走                         |                               |
| earthy        | 粗俗的，土气的       | rough, plain in taste                                                     | earth                                  |                               |
| upswing       | 上升，增长；进步，改进   | increase, improvement                                                     | up + swing                             |                               |
| miserly       | 吝啬的           | penurious                                                                 | miser 吝啬鬼                              |                               |
| distrait      | 心不在焉的         | absent-minded, distracted                                                 |                                        |                               |
| flay          | 剥皮；抢夺，掠夺；严厉指责 | strip off the skin, rob, pillage, criticize or scold mercilessly          | fray 吵架，打架                             |                               |
| retch         | 作呕，恶心         | vomit                                                                     |                                        |                               |
| occlude       | 使闭塞           | prevent the passage of                                                    | oc + clud 关闭                           |                               |
| traipse       | 漫步，闲荡         | wander, ramble, roam, jog                                                 |                                        |                               |
| squeamish     | 易受惊的；易恶心的     | easily shocked or sickened                                                |                                        | the squeamish                 |
| camouflage    | 掩饰，伪装         | disguise in order to conceal                                              | cam 出来 + ou + flag 旗帜                  |                               |
| overhaul      | 彻底检查；大修       | check/repair thoroughly                                                   |                                        |                               |
| saddle        | 鞍，马鞍          | a seat of one who rides on horseback                                      |                                        |                               |
| consequential | 傲慢的，自大的；相应的   | self-important, arrogate                                                  |                                        |                               |
| chipper       | 爽朗的，活泼的       | sprightly                                                                 | chip 芯片                                |                               |
| obsolete      | 废弃的，过时的       | no longer in use, out of date, old-fashioned                              |                                        |                               |
| complaisance  | 彬彬有礼；殷勤；柔顺    |                                                                           | com 一起 + plais 高兴 + ance               |                               |
| asinine       | 愚笨的           | of asses, stupid, silly                                                   | as (ass) + in + in + e                 |                               |
| subterfuge    | 诡计，托辞         | a deceptive device or stratagem                                           | subter 私下+ fuge 逃跑                     |                               |
| hardihood     | 大胆，刚毅；厚颜无耻    | boldness, fortitude, brazen                                               | hardy 强壮的，大胆的，勇敢的                      |                               |
| douse         | 把…浸在水中的；熄灭    | plunge into water, extinguish                                             | do + use 又做又用                          |                               |
| dispense      | 分配，分法         | to distribute in portions                                                 |                                        |                               |
| mandate       | 命令，训诫         | authoritative order or command                                            | mand 命令 + ate                          |                               |
| affected      | 不自然的；假装的      | behaving in an artificial way, assumed, factitious, fictitious, unnatural |                                        |                               |
| burlesque     | 讽刺或滑稽的戏剧      | derisive caricature, parody                                               | “不如乐死他”                                |                               |
| wince         | 畏缩，退缩         | flinch, cringe, quail, shrink back involuntarily                          |                                        |                               |
| gobble        | 狼吞虎咽，贪婪的吃     | eat greedily, gulp, swallow, devour                                       | gob n. 大块                              |                               |
| evacuate      | 撤离；疏散         | withdraw, remove inhabitants for protective purpose                       |                                        |                               |
| germicide     | 杀菌剂           | substance for killing germs                                               | germ + icide                           |                               |
| bruise        | 受伤，擦伤         | to injure the skin                                                        | cruise 乘船巡游                            |                               |
| infatuate     | 使迷恋；使糊涂       | inspire with a foolish or extravagant love or admiration                  |                                        |                               |
| execrable     | 可憎的，讨厌的       | deserving to be execrated, abominable, detestable                         |                                        | execrable poetry              |
| lattice       | 格子架；格子        |                                                                           | l + attic 阁楼 + e                       |                               |
| 25            | 25                  | 25                                                     | 25                   | 25       |
| traverse      | 横穿；横跨               | across or over                                         | tra + vers + e       |          |
| limousine     | 大型轿车；豪车             | large and luxurious car                                | limo                 |          |
| implore       | 哀求，恳求               | beg                                                    | im 进 + plor 哭泣 + e   |          |
| nifty         | 极好的，极妙的             | good and attractive, great                             |                      |          |
| furbish       | 磨光，刷新               | polish, renovate                                       | furnish 装饰           |          |
| persnickety   | 势利的；爱挑剔的            | snobbish, fuzzy, fastidious                            |                      |          |
| crass         | 愚钝的，粗糙的             | crude and unrefined                                    |                      |          |
| dissimulate   | 隐藏，掩饰               | pretend, dissemble                                     |                      |          |
| duress        | 胁迫                  | force and threats, compulsion                          |                      |          |
| aplomb        | 沉着，镇静               | complete and confident composure                       |                      |          |
| idle          | 无所事事的，无效的；懒散        |                                                        |                      |          |
| lactic        | 乳汁的                 | milk                                                   | lact 乳               |          |
| myopia        | 近视，缺乏远见             | lack of foresight or discernment                       |                      |          |
| episode       | 一段情节；插曲，片段          | circumstance, development, happening, incident         |                      |          |
| jab           | 猛刺                  |                                                        |                      |          |
| canopy        | 蚊帐；华盖               | drapery, awning, cloth covering                        |                      |          |
| lubricant     | 润滑剂                 | reducing friction                                      | lubric 光滑            |          |
| boreal        | 北方的，北风的             | northern regions                                       |                      |          |
| compunction   | 懊悔，良心不安             | guilt, remorse, penitence                              |                      |          |
| blasé         | 厌倦享乐的，玩厌了的          | board with pleasure or dissipation                     |                      |          |
| enclosure     | 圈地，围场               |                                                        | enclose v.           |          |
| comatose      | 昏迷的                 | unconscious, torpid                                    | coma n.              |          |
| vanquish      | 征服，击溃               | subdue, defeat, conquer, crush, rout, subjugate        |                      |          |
| ravish        | 使着迷；强夺              | overcome, take away                                    |                      |          |
| spout         | 喷出；滔滔不绝的说           | eject in stream                                        |                      |          |
| exculpate     | 开脱；申明无罪，证明无罪        | free from blame, declare and prove guiltless           |                      |          |
| deputy        | 代表；副手               | representative                                         | duty                 |          |
| illustrious   | 著名的，显赫的             | very distinguished, outstanding                        |                      |          |
| mellifluous   | 柔美流畅的               | sweetly and smoothly flowing                           |                      |          |
| depose        | 免职，宣誓作证             |                                                        | de + pose 放          |          |
| grotesque     | 怪诞的，古怪的；（艺术）风格怪异的   | bizarre, fantastic                                     |                      |          |
| jug           | 用陶罐等炖；水壶；关押         | jail, imprison, in an earthenware vessel               |                      |          |
| rumple        | 弄皱，弄乱               | make disheveled or tousled                             |                      |          |
| whoop         | 高喊，欢呼               | eagerness, exuberance, jubilation                      |                      |          |
| enunciate     | 发音，（清楚的）表达          |                                                        | e + nunci 发音 + ate   |          |
| fleece        | 生羊皮，羊毛；骗取，欺诈        |                                                        | flee 逃跑              |          |
| impasse       | 僵局，死路               | deadlock, blind alley                                  | im 无 + pass + e      |          |
| pullet        | 小母鸡                 |                                                        | bullet -> pullet     |          |
| allergic      | 过敏的；对…讨厌的           | averse or disinclined                                  | allergy n.           |          |
| martyr        | 烈士，殉道者              |                                                        | 词根：目击者               |          |
| ennoble       | 授予爵位，使高贵            | make noble                                             |                      |          |
| daub          | 涂抹；乱画               |                                                        |                      |          |
| interlace     | 编织，交错               | weave together, connect intricately                    |                      |          |
| wiry          | 瘦而结实的               | lean, supple, and vigorous                             |                      |          |
| consent       | 同意，允许               | give agreement                                         |                      |          |
| woo           | 求爱，求婚；恳求，争取         |                                                        |                      |          |
| accrue        | （利息等）增加；积累          | increase the interest, accumulate                      |                      |          |
| sullen        | 忧郁的                 | gloomy, dismal, brooding, morose, sulky                |                      |          |
| annoy         | 惹恼；打搅，骚扰            | bother, chafe, irritate, nettle, rile, vex             |                      |          |
| manifesto     | 宣言，声明               | public declaration                                     | manifest v.          |          |
| oxidize       | 氧化，生锈               |                                                        | oxy + dize           |          |
| spite         | 怨恨，恶臭               | petty ill will or hatred                               |                      |          |
| incogitant    | 未经过思考的，考虑不周全的       | thoughtless, inconsiderate                             | in + cogitant        |          |
| anneal        | 使（金属、玻璃等）退火；使变强；使变硬 | strengthen and harden                                  |                      |          |
| debark        | 下船，下飞机，下车；卸载        |                                                        | de + bark 船          |          |
| celerity      | 快速，迅速               | swiftness                                              |                      |          |
| forfeiture    | （名誉等）丧失             |                                                        | forfeit v.           |          |
| malfeasance   | 不法行为，渎职             | misconduct by a public official                        |                      |          |
| scurvy        | 卑鄙的，下流的             | despicable                                             | scurry v. 疾行         |          |
| ogle          | 送秋波，媚眼              |                                                        |                      |          |
| weld          | 焊接，熔接，锻接            |                                                        |                      | weld leg |
| pharmacology  | 药理学，药物学，药理          |                                                        |                      |          |
| vouch         | 担保，保证               | guarantee                                              |                      |          |
| cringing      | 谄媚的，奉承的             |                                                        |                      |          |
| wheedle       | 哄骗，诱骗               | cajole, coax, seduce                                   |                      |          |
| careen        | （船）倾斜；使倾斜           | lean sideways                                          |                      |          |
| peculate      | 挪用（公款）              | embezzle                                               |                      |          |
| vandalism     | （对公物等）恶意破坏          | willful, malicious                                     |                      |          |
| anguish       | 极大的痛苦               | great suffering, distress                              |                      |          |
| entreaty      | 恳求，哀求               | plea                                                   | entreat v.           |          |
| oratory       | 演讲术                 |                                                        | orate v.             |          |
| desert        | 遗弃，离弃               | abandon                                                |                      |          |
| effulgent     | 灿烂的，光辉的             | of great brightness                                    |                      |          |
| encomiast     | 赞美者                 | a eulogist                                             |                      |          |
| carouse       | 狂饮寻乐                | noisy, merry drinking party                            |                      |          |
| dally         | 闲荡，嬉戏               | loiter, trifle                                         |                      |          |
| cephalic      | 头的，头部的              | of head of skull                                       | cephal 头             |          |
| pellucid      | 清晰的，清澈的             | transparent, clear                                     |                      |          |
| wan           | 虚弱的，病态的             | feeble, sickly, pallid, ghastly, haggard, sallow       |                      |          |
| preponderate  | 超过，胜过               | exceed                                                 | pre + ponderate      |          |
| snuggle       | 紧靠，依偎               | draw close                                             |                      |          |
| disparity     | 不同，差异               | inequality or difference                               |                      |          |
| blackball     | 投票反对；排斥             | ostracize                                              | black + ball         |          |
| hubris        | 傲慢，目中无人             |                                                        |                      |          |
| shabby        | 破旧的，卑鄙的             | scruffy, shoddy, dilapidated, despicable, contemptible |                      |          |
| moat          | 壕沟，护城河              |                                                        |                      |          |
| heckle        | 诘问，责问               |                                                        | he + buckle          |          |
| musket        | 旧式步枪；毛瑟枪            |                                                        |                      |          |
| evasion       | 躲避，借口               |                                                        |                      |          |
| demonstrative | 证明的，论证的；感情泄露的       |                                                        |                      |          |
| snarl         | 纠缠，混乱；咆哮，怒骂；怒吼声     | knot                                                   |                      |          |
| grumpy        | 脾气暴躁的               | grouchy, peevish                                       | grump v.             |          |
| compress      | 压缩，压紧               | contract                                               |                      |          |
| tangy         | 气味刺激的，扑鼻的           | pleasantly sharp flavor                                |                      |          |
| impeach       | 控告；怀疑；弹劾            | accuse, charge, indict                                 |                      |          |
| snappy        | 生气勃勃的；漂亮的，时髦的       | stylish                                                |                      |          |
| acrid         | 辛辣的，刻薄的             | pungent, bitter, sharp                                 |                      |          |
| muster        | 召集，聚集               | summon, gather                                         |                      |          |
| 26            | 26                | 26                                                              | 26                          | 26              |
| cession       | 割让，转让             |                                                                 | cede v.                     |                 |
| maul          | 打伤；伤害             | lacerate                                                        | haul 用力拖                    |                 |
| shattered     | 破碎的               |                                                                 | shatter v.                  |                 |
| perverse      | 刚愎自用的，固执的；反常的     | obstinate in opposing, wrongheaded                              |                             |                 |
| maple         | 枫树                |                                                                 |                             |                 |
| extrude       | 挤出，推出，逐出；伸出，突出    | thrust out, protrude                                            |                             |                 |
| abject        | 极可怜的；卑下的          | miserable, wretched, degraded, base                             | ab + ject                   |                 |
| shrewd        | 机灵的，精明的           | clever discerning awareness                                     | shrew 泼妇                    |                 |
| magnitude     | 重要性；星球的亮度         |                                                                 | magn 大 + tude               |                 |
| dawdle        | 闲荡，虚度光阴           | idle, loiter, trifle                                            |                             |                 |
| malaise       | 不适，不舒服            | illness                                                         | “没累死”                       |                 |
| sneaking      | 鬼鬼祟祟的，私下的         | furtive, underhanded, surreptitious                             | snake 蛇                     |                 |
| protuberant   | 突出的，隆起的           | prominent, thrusting out                                        |                             |                 |
| twee          | 矫揉造作的，故作多情的       | dainty, delicate, cute or quaint                                |                             |                 |
| crafty        | 狡诈的；灵巧的           | sly, deceitful, proficient                                      | craft n.                    |                 |
| doleful       | 悲哀的，忧郁的           | sorrow and sadness                                              | dole 悲哀 + ful               |                 |
| culprit       | 罪犯                | guilty of a crime                                               |                             |                 |
| reminisce     | 追忆，回想             |                                                                 | re + mind + sce             |                 |
| oleaginous    | 油腻的；圆滑的，满口恭维的     | falsely earnest, unctuous                                       |                             |                 |
| remit         | 免除，宽恕；汇款          | release                                                         |                             |                 |
| ruminate      | 反刍，深思             | meditate, cogitate, contemplate, deliberate, ponder             |                             |                 |
| wallow        | 打滚；沉迷             |                                                                 |                             |                 |
| lumber        | 跌跌撞撞的走，笨拙的走；杂物；木材 | move with heavy clumsiness                                      |                             |                 |
| stanza        | （诗歌的）节、段          |                                                                 | stan (stand) + za           |                 |
| rumpus        | 喧闹，骚乱             | noisy commotion, clamor, tumult, uproar                         |                             |                 |
| spleen        | 怒气                | rancor, resentment, wrath                                       |                             |                 |
| writ          | 令状；书面令            |                                                                 | write - e                   |                 |
| fold          | 羊栏；折叠             |                                                                 |                             |                 |
| obeisance     | 鞠躬，敬礼             | gesture of respect or reverence                                 |                             |                 |
| mirage        | 海市蜃楼；幻影           |                                                                 | mir 惊奇的 + age               |                 |
| peremptory    | 不容反抗的；专横的         | masterful                                                       | per + empt + ory            |                 |
| choleric      | 易怒的，暴躁的           | of irascible nature, irritable                                  | choler 胆汁 + ic              |                 |
| jeer          | 嘲笑                | mock, taunt, scoff at                                           |                             | jeer at …       |
| spate         | 许多，大量；暴涨，发洪水      |                                                                 |                             | a spate of …    |
| incubation    | 孵卵期，潜伏期           |                                                                 |                             |                 |
| merited       | 该得的；理所当然的         | deserving, worthy of                                            | merit 价值                    |                 |
| invoke        | 祈求，恳求；使生效         | implore, entreat                                                |                             |                 |
| allowance     | 津贴，补助；允许，承认       | subsidize                                                       |                             |                 |
| predilection  | 偏爱，嗜好             |                                                                 | pre + dilection (direction) |                 |
| cavalry       | 骑兵部队；装甲部队         |                                                                 | cavalier                    |                 |
| exiguous      | 太少的，不足的           | scanty, meager, scarce, skimpy, sparse                          |                             |                 |
| wade          | 涉水，跋涉             | pold, slog, slop, toil, trudge                                  |                             |                 |
| provenance    | 出处，起源             | origin, source                                                  |                             |                 |
| disfranchise  | 剥夺…的权利（公民权）       |                                                                 | dis + franchise             |                 |
| modish        | 时髦的               | fashionable, stylish                                            |                             |                 |
| redolent      | 芬芳的，芳香的           | scented, aromatic                                               |                             |                 |
| agog          | 兴奋的，有强烈兴趣的        |                                                                 | 词根：引导；demagog 煽动者           |                 |
| hurdle        | 跨栏，障碍；克服          | barrier, block, obstruction, snag, obstacle, surmount, overcome |                             |                 |
| gargoyles     | 滴水嘴；面貌丑恶的人；石像鬼    |                                                                 | gargle 漱口                   |                 |
| rabid         | 患狂犬病的；狂暴的         |                                                                 | rabies 狂犬病                  |                 |
| particularize | 详述，列举             |                                                                 |                             |                 |
| vie           | 竞争                | compete, contend, contest, emulate, rival                       |                             |                 |
| hinge         | 铰链；关键             | joint, determining factor                                       |                             |                 |
| delusion      | 欺骗，幻想             | illusion, hallucination                                         | delude v.                   |                 |
| gander        | 雄鹅；笨蛋，傻瓜；闲逛       |                                                                 | gender 性别                   |                 |
| putrid        | 腐臭的               | rotten, malodorous                                              |                             |                 |
| enchant       | 使陶醉；施法于           | rouse to ecstatic admiration, bewitch                           |                             |                 |
| plush         | 豪华的               | notably luxurious                                               |                             |                 |
| remonstrance  | 抗议，抱怨             |                                                                 |                             |                 |
| gruff         | 粗鲁的，板着脸的；粗哑的      | rough, hoarse                                                   |                             |                 |
| liken         | 把…比作              | compare to …                                                    |                             |                 |
| ensconce      | 安置，安坐             | shelter, establish, settle                                      | en + sconce 小堡垒             |                 |
| cloy          | （甜食）生腻，吃腻         | satiate                                                         |                             |                 |
| tusk          | （大象等）长牙           |                                                                 |                             |                 |
| trounce       | 痛击；严惩             | thrash or punish severely                                       |                             |                 |
| winkle        | 挑出，剔出；取出          |                                                                 |                             |                 |
| parturition   | 生产，分娩             |                                                                 |                             |                 |
| stoop         | 弯腰，俯身；屈尊          |                                                                 |                             |                 |
| accessory     | 附属的，次要的           | additional, supplementary, subsidiary                           |                             |                 |
| curdle        | 使凝结，变稠            | coagulate, congeal                                              | curd n.                     |                 |
| gaffe         | 失礼，失态             | social or diplomatic blunder                                    |                             |                 |
| wend          | 行，走，前进            | proceed on                                                      |                             | wend one's way  |
| traduce       | 中伤，诽谤             | slander or defame                                               |                             |                 |
| scud          | 疾行，飞奔             |                                                                 |                             |                 |
| gutless       | 没有勇气的；怯懦的         | cowardly, spineless                                             |                             |                 |
| magniloquent  | 夸张的               |                                                                 | magn 大 + loqu 说话 + ent      |                 |
| potboiler     | 粗制滥造的文艺作品；锅炉      |                                                                 | potboil v. 为了混饭吃粗制滥造        |                 |
| boding        | 凶兆，前兆；先兆的         | omen especially for coming evil                                 |                             |                 |
| demarcate     | 划分，划界             |                                                                 |                             |                 |
| opulence      | 富裕；丰富             | affluence, profusion                                            |                             |                 |
| smudge        | 污迹；污点；弄脏          |                                                                 | s + mud 泥 + age             |                 |
| consecrate    | 奉献；使神圣            | dedicate, sanctify                                              | con + sacre + ate           |                 |
| flux          | 不断的变动；变迁，动荡不安     |                                                                 |                             |                 |
| jeopardy      | 危险                | peril                                                           |                             | in jeopardy     |
| resigned      | 顺从的，听从的           | acquiescent                                                     |                             | be resigned to… |
| 27            | 27                  | 27                                                   | 27                   | 27                 |
| shove         | 推挤，猛推               | move by force                                        | shovel 铁锹            |                    |
| dote          | 溺爱；昏聩               |                                                      |                      |                    |
| harshly       | 严酷的，无情的             | severely, toughly                                    |                      |                    |
| neurosis      | 神经官能症               |                                                      | neu + osis           |                    |
| overriding    | 最主要的，优先的            | chief, principal                                     |                      |                    |
| judicial      | 法庭的，法官的             | of law, courts, judges, judiciary                    |                      | judicial decisions |
| perish        | 死，暴卒                |                                                      |                      |                    |
| beholden      | 因受恩惠而心存感激，感谢；欠人情    | grateful, owing                                      |                      |                    |
| obtrude       | 突出，强加               | thrust out, force or impose                          |                      |                    |
| thump         | 重击，锤击               | pound, punch, smack, whack                           |                      |                    |
| retard        | 妨碍，使减速              | impede, slow down                                    |                      |                    |
| perforate     | 打洞                  |                                                      |                      |                    |
| dissertation  | 专题论文                | long essay on a particular subject                   |                      |                    |
| striking      | 引人注目的，显著的           |                                                      | strike v.            |                    |
| hone          | 磨刀石；磨刀              |                                                      | horn n. 号角           |                    |
| dislocate     | 使脱臼；把…弄乱            | disarrange, disrupt                                  |                      |                    |
| niggling      | 琐碎的                 | petty, trival                                        | nig 小                |                    |
| ersatz        | 代用的；假的              | artificial, substitute or synthetic                  |                      |                    |
| shuttle       | 穿梭移动，往返运送           |                                                      |                      |                    |
| surge         | 波涛汹涌                |                                                      |                      |                    |
| fruition      | 实现，完成               | fulfillment of hopes, plans                          | fruit n.             |                    |
| cramp         | 铁箍，夹子；把…箍紧          |                                                      |                      | cramp one's style  |
| fabulous      | 难以置信的；寓言的           | incredible, astounding, imaginary, fictitious        |                      |                    |
| fallibility   | 易出错，不可靠             | liability to error                                   | fall v.              |                    |
| merit         | 值得；价值，长处            | to be worthy of …                                    | 本身为词根                |                    |
| gadget        | 小工具，小机械             | small mechanical contrivance or device               |                      |                    |
| homage        | 效忠；敬意               | allegiance, honor                                    |                      |                    |
| odoriferous   | 有气味的                | give off an odor                                     |                      |                    |
| pulpit        | 讲道坛                 |                                                      |                      |                    |
| natal         | 出生的，诞生时的            |                                                      | nat 出生 +al           |                    |
| pertain       | 属于；关于               | belong, reference                                    |                      |                    |
| liability     | 责任，义务               | the state of being liable, obligation, debt          |                      |                    |
| outlet        | 出口                  |                                                      |                      |                    |
| cupidity      | 贪婪                  | avarice, greed                                       |                      |                    |
| aleatory      | 侥幸的，偶然的             | depending on an uncertainty or contigency            |                      |                    |
| bungle        | 笨拙的做                | botch, bumble, fumble, muff, stumble                 |                      |                    |
| drivel        | 胡说，糊涂话              | nonsense                                             |                      |                    |
| blasphemy     | 亵渎（神明）              | cursing                                              |                      |                    |
| complicity    | 合谋，串通               | participation, involvement in a crime                |                      |                    |
| rote          | 死记硬背                |                                                      |                      |                    |
| shirk         | 逃避，回避               | evade, avoid                                         | shirt 衬衣             |                    |
| atonal        | 无调的                 |                                                      | a + tone + al        |                    |
| fang          | （蛇的）毒牙              |                                                      | tang 强烈的气味           |                    |
| snare         | 圈套，陷阱               | trap, gin                                            | ensnare 使进入圈套        |                    |
| archer        | （运动或者战争中）弓箭手；射手     |                                                      |                      |                    |
| deluxe        | 豪华的，华丽的             | notably luxurious, elegant, or expensive             |                      | a deluxe hotel     |
| famine        | 饥荒                  | instance of extremely scarcity                       | female 女性            |                    |
| nip           | 小口饮用                |                                                      | lip 嘴唇               |                    |
| languor       | 无精打采；衰弱无力           | lack of vigor or vitality, weakness                  | lang 松弛 + uor        |                    |
| malfunction   | 发生故障；失灵；故障          |                                                      | mal 坏 + function     |                    |
| endearing     | 讨人喜欢的               |                                                      |                      |                    |
| renal         | 肾脏的；肾的              | kidney                                               |                      |                    |
| asterisk      | 星号                  |                                                      | aster 星星 + isk       |                    |
| topsy-turvy   | 颠倒的，相反的；乱七八糟的；混乱的   |                                                      |                      |                    |
| obsess        | 迷住；使困扰；使烦扰          |                                                      | ob + sess            |                    |
| slouch        | 没精打采的样子；无精打采的       |                                                      | “似老去”                |                    |
| vengeful      | 渴望复仇的；复仇心重的         | fierce desire to punish sb.                          |                      |                    |
| mendacity     | 不诚实                 | untruthfulness                                       |                      |                    |
| rout          | 溃败                  | overwhelming defeat                                  |                      |                    |
| status        | 身份，地位               | social standing, present condition                   |                      |                    |
| recant        | 撤回（声明），放弃（信仰）       | withdraw or repudiate                                |                      |                    |
| mushy         | 糊状的；感伤的，多情的         | consistency of mush, excessively tender or emotional |                      |                    |
| kudos         | 声誉，名声               | fame or renown, reputation, prestige                 |                      |                    |
| depredation   | 劫掠；蹂躏               | plundering                                           |                      |                    |
| wily          | 诡计多端的；狡猾的           | crafty                                               | wile 诡计              |                    |
| motile        | 能动的                 |                                                      | motility n.          |                    |
| convict       | 定罪，罪犯               |                                                      |                      |                    |
| diffuse       | 散布（的）；漫射（的）         |                                                      |                      |                    |
| carp          | 鲤鱼；吹毛求疵             |                                                      |                      |                    |
| onslaught     | 猛攻，猛袭               | fierce attack                                        | on + slaught         |                    |
| crook         | 使弯曲；钩状物             | bend or curve                                        |                      |                    |
| bustle        | 奔忙；忙乱；喧闹，熙熙攘攘       | noisy, energetic, and often obstrusive activity      |                      |                    |
| caprice       | 奇思怪想；反复无常；任性        | sudden change                                        |                      |                    |
| rapprochement | 友好，友善关系建立           |                                                      | re + approachment    |                    |
| excrete       | 排泄，分泌               | pass out waste matter                                |                      |                    |
| jerk          | 猛拉                  |                                                      |                      | jerk off 结结巴巴的说    |
| punch         | 以拳猛击；打孔             | strike with the fist, make a hole, pierce            |                      |                    |
| dyspeptic     | 消化不良的               | indigestible, morose, grouchy                        |                      |                    |
| agnostic      | 不可知论的；不可知论者         |                                                      |                      |                    |
| bogus         | 伪造的；假的              | not genuine, spurious                                |                      |                    |
| foray         | 突袭；偷袭；劫掠；掠夺         |                                                      |                      |                    |
| friable       | 脆的；易碎的              | easily broken up or crumbled                         | friable soil         |                    |
| skullduggery  | 欺骗；使诈               | underhanded or unscrupulous behavior                 |                      |                    |
| blemish       | 损害，玷污；瑕疵，缺点         | defect, spoil the perfection of                      |                      |                    |
| badge         | 徽章                  | distinctive token, emblem, or sign                   |                      |                    |
| poignancy     | 辛辣，尖锐               | being poignant                                       |                      |                    |
| brusque       | 唐突的；鲁莽的             | blunt, rough and abrupt                              |                      |                    |
| piddle        | 鬼混；浪费               | diddle, dawdle, putter                               |                      |                    |
| josh          | （无恶意的）戏弄；戏耍         | banter, jest, tease good-naturedly                   |                      |                    |
| vendetta      | 血仇；世仇；宿怨；深仇         | blood, bitter, destructive feud                      |                      |                    |
| contented     | 心满意足的               | showing content and satisfied                        |                      |                    |
| ritzy         | 高雅的；势利的             | elegant, snobbish, exclusive, refined, polished      |                      |                    |
| rankle        | 怨恨；激怒               | cause resentment, anger, irritation                  |                      |                    |
| lunatic       | 疯子；极蠢的              | insane person, utterly foolish                       |                      |                    |
| gracious      | 大方的；和善的；奢华的；优美的；雅致的 | polite, generous, luxurious                          |                      |                    |
| rollicking    | 欢乐的，喧闹的             | noisy and jolly                                      |                      |                    |
| ecstatic      | 狂喜的，心花怒放的           | enraptured                                           | ecstasy              |                    |
| 28            | 28                  | 28                                                                       | 28                   | 28                             |
| caudal        | 尾部的；像尾部的            |                                                                          |                      |                                |
| parochial     | 教区的；地方性的，狭小的        | parish, narrow                                                           |                      |                                |
| monolithic    | 坚若磐石的，巨大的           | huge, massive                                                            |                      |                                |
| zest          | 刺激性；兴趣，热心           | keen                                                                     | zealot               |                                |
| dicker        | 讨价还价                | bargain, haggle, higgle, huckster, negotiate, palter                     |                      |                                |
| trinket       | 小装饰品；不值钱的珠宝；琐事      | trivial                                                                  |                      |                                |
| clammy        | 湿冷的；发粘的             | being damp, soft, sticky, and usually cool                               |                      |                                |
| sterilize     | 使不育，杀菌              |                                                                          |                      | sterilize surgical instruments |
| temerity      | 鲁莽，大胆               | audacity, rashness, recklessness                                         |                      |                                |
| boor          | 粗野的人；农民             | rude, awkward person, peasant                                            |                      |                                |
| swig          | 痛饮                  | gulp, guzzle                                                             |                      |                                |
| hie           | 疾走，快速               | go quickly, hasten, hurry, speed                                         |                      |                                |
| ribald        | 下流的，粗俗的             | crude                                                                    |                      |                                |
| derivation    | 发展，起源；词源            |                                                                          | derive v.            |                                |
| scamp         | 草率的做；流氓；顽皮的家伙       | rogue, rescal                                                            | cramp 铁箍，夹子；把…箍紧     |                                |
| slit          | 撕裂；裂缝               | sever, long narrow or opening                                            |                      |                                |
| steep         | 陡峭的；过高的；浸泡；浸透       | lofty, high, soak in a liquid                                            |                      |                                |
| conceit       | 自负，自大               | vanity                                                                   |                      |                                |
| ineluctable   | 不能逃避的               | certain, inevitable                                                      |                      |                                |
| pavid         | 害怕的；胆小的             | timid                                                                    |                      |                                |
| humid         | 湿润的                 |                                                                          |                      |                                |
| grisly        | 恐怖的，可怕的             | frightening, ghastly, horrible                                           |                      |                                |
| affix         | 黏上；贴上；词缀            |                                                                          |                      |                                |
| crib          | 抄袭，剽窃               | plagiarize                                                               | crab 发牢骚             |                                |
| ornery        | 顽固的，爱争吵的            | irritable disposition, cantankerous                                      |                      |                                |
| veritable     | 名副其实的；真正的；确实的       | authentic, unquestionably                                                | verify v.            |                                |
| abhorrent     | 可恨的，讨厌的             | obscene, repugnant, repulsive, hateful, detestable                       |                      |                                |
| daredevil     | 大胆的，冒失的（人）          | bold and reckless                                                        |                      |                                |
| reliance      | 信赖，信任               |                                                                          |                      |                                |
| tempestuous   | 狂暴的                 | turbulent, stormy                                                        |                      | a tempestuous relationship     |
| superimpose   | 重叠；叠加               | place or lay over                                                        | super + impose       |                                |
| strife        | 纷争，冲突               | bitter conflict or dissension, fight, struggle                           |                      |                                |
| funk          | 怯懦；恐惧；懦夫            | paralyzing fear                                                          |                      |                                |
| atrocious     | 残忍的；凶恶的             | cruel, brute, outrageous                                                 |                      |                                |
| correspondent | 符合的；一致的；记者          | agreeing, matching                                                       |                      |                                |
| despise       | 鄙视；轻视               | look down with contempt or aversion, contemn, disdain, scorn, scout      |                      |                                |
| poach         | 偷猎；窃取               | catch without permission                                                 | poach ideas of sb.   |                                |
| shamble       | 蹒跚而行；踉跄的走           | walk awkwardly with dragging feet, shuffle                               |                      |                                |
| sheen         | 光辉；光泽               | bright or shining condition                                              |                      |                                |
| shuckle       | 轻声的笑；咯咯的笑           | laugh softly in a low tone, chortle                                      |                      |                                |
| steer         | 掌舵；驾驶；公牛；食用牛        | control the course                                                       |                      |                                |
| rampage       | 乱冲乱跑；狂暴行为           |                                                                          |                      |                                |
| offbeat       | 不规则的；不平常的           | unconventional                                                           |                      |                                |
| bid           | 命令；出价，投标            | command                                                                  |                      |                                |
| promissory    | 允诺的；约定的             |                                                                          |                      | promissory note                |
| slue          | （使）旋转               | rotate, slew                                                             |                      |                                |
| analgesia     | 无痛觉；痛觉丧失            | insensibility to pain without loss of consciousness                      |                      |                                |
| sleight       | 巧妙手法；诡计，灵巧          |                                                                          |                      |                                |
| scabrous      | 粗糙的                 | scabby, rough                                                            |                      |                                |
| reconnoiter   | 侦察，勘察               | make a reconnaissance of                                                 |                      |                                |
| supine        | 仰卧的；懒散的             | prone, inactive, lying, slack                                            |                      |                                |
| badger        | 獾；烦扰，纠缠不安           | torment, nag, beleaguer, bug, pester, tease                              |                      |                                |
| smirch        | 弄脏；污点               | blemish, soil, sully, tarnish                                            |                      |                                |
| glow          | 发光，发热；(脸）发红；发光，兴高采烈 | blush, flush, incandescence                                              |                      |                                |
| intestate     | 未留遗憾的               | having made no legal will                                                |                      |                                |
| languish      | 衰弱，憔悴               | lose vigor or vitality                                                   | lang + uish          |                                |
| wobble        | 摇晃，摇摆；犹豫            | hesitate, move with a staggering motion, falter, stumble, teeter, tooter |                      |                                |
| imbroglio     | 纠纷；纠葛；纠缠不清          | confusion                                                                |                      |                                |
| providential  | 幸运的；适时的             | fortunate, opportune                                                     |                      |                                |
| 29              | 29                      | 29                                                     | 29                   | 29         |
| rectitude       | 诚实，正直；公正                | righteousness, moral integrity                         |                      |            |
| rapt            | 入迷的；全神贯注的               | engrossed, absorbed, enchanted                         |                      |            |
| sibyl           | 女预言家；女先知                | prophetess                                             |                      |            |
| spurt           | 喷出，迸发                   | spout, gush, jet, spew, squirt                         |                      |            |
| preposterous    | 荒谬的                     | absurd                                                 |                      |            |
| piquant         | 辛辣的；开胃的；刺激的             | spicy, stimulate the palate, engagingly provocative    |                      |            |
| toil            | 苦干；辛苦劳作；辛劳              | long strenuous, fatiguing labor                        |                      |            |
| flit            | 掠过；迅速飞过                 |                                                        | fly + it             |            |
| vicinity        | 附近，临近                   | proximity, neighborhood                                |                      |            |
| footle          | 说胡话，做傻事；浪费（时间）          | foolishly                                              |                      |            |
| sustained       | 持久的，持续的                 | prolonged                                              |                      |            |
| slew            | （使）旋转；大量                |                                                        |                      |            |
| relent          | 变温和；变宽厚；减弱              | become compassionate, soften, mollify                  |                      |            |
| sag             | 松弛，下垂                   | lose firmness, resiliency, vigor                       |                      |            |
| welsh           | 欠债不还；失信                 | avoid payment, break one's word                        |                      |            |
| invective       | 谩骂；痛骂                   | violent verbal attack, diatribe                        |                      |            |
| deserter        | 背弃者；逃兵                  |                                                        | desert v. 遗弃         |            |
| staid           | 稳重的，沉着的                 | self-restraint, sober                                  |                      |            |
| libelous        | 诽谤的                     | publishing libels, defamatory                          |                      |            |
| advert          | 注意，留意；提及                | call attention, refer                                  |                      |            |
| inveigh         | 痛骂；猛烈抨击                 | utter censure or invective                             |                      |            |
| lithe           | 柔软的，易弯曲的；自然优雅的          | easily bent, flexibility and grace                     |                      |            |
| forbidding      | 令人生畏的；（形势）险恶的；令人反感的；讨厌的 | threatening, disagreeable                              | forbid v.            |            |
| bleary          | 视线模糊的；朦胧的；精疲力尽的         | dull or dimmed, tired                                  |                      |            |
| clasp           | 钩子；扣子；紧握                | clench, clutch, grasp, grip                            |                      |            |
| tract           | 传单；大片土地                 | leaflet of political, large stretch of land            |                      |            |
| spiel           | 滔滔不绝的讲话                 | pitch                                                  |                      |            |
| gulp            | 吞食；咽下；抵制；忍住             | gobble, swallow hastily                                |                      |            |
| beam            | （房屋等）大梁；光线              |                                                        |                      |            |
| piddling        | 琐碎的；微不足道的               | trifling, trivial                                      |                      |            |
| pact            | 协定，条约                   | covenant, treaty                                       |                      |            |
| glimpse         | 瞥见；看一眼；一瞥               | glance                                                 |                      |            |
| meek            | 温顺的；顺从的                 | gentle and uncomplaining, mild, patient, subdued, tame |                      |            |
| antic           | 古怪的                     | fantastic, queer                                       | antique              |            |
| hobble          | 蹒跚；跛行                   | lamely, limp                                           | hobby                |            |
| acarpous        | 不结果实的                   | impotent to bear fruit                                 |                      |            |
| unprepossessing | 不吸引人的                   | unattractive                                           | un + prepossessing   |            |
| nag             | 不断叨扰；指责，抱怨              | complain incessantly                                   |                      |            |
| escort          | 护送；护送者                  |                                                        |                      |            |
| ruthlessness    | 无情；冷酷；残忍                | cruelty                                                | ruthless adj.        |            |
| panic           | 恐慌的；惊慌                  |                                                        |                      |            |
| slump           | 大幅度下降；暴跌                | plunge, plummet, tumble                                |                      |            |
| lissome         | 柔软的                     | lithe, supple, limber                                  | liss 可弯曲的 + ome      |            |
| mutter          | 咕哝；嘀咕                   | speak in low and indistinct voice                      |                      |            |
| bombast         | 高调；夸大之词                 | pompous language                                       |                      |            |
| expel           | 排出；开除                   | eject, discharge, cut off                              |                      |            |
| protocol        | 外交礼节；协议；草案              | official etiquette                                     |                      |            |
| parity          | 同等；相等                   | equality                                               |                      |            |
| monograph       | 专题论文                    | learned treatise                                       |                      |            |
| impose          | 征收；强加                   |                                                        |                      |            |
| install         | 安装；使就职                  |                                                        |                      |            |
| senile          | 衰老的                     |                                                        |                      |            |
| detraction      | 贬低，诽谤                   | unfair criticise                                       |                      |            |
| sloppy          | 邋遢的，粗心的                 | slovenly, careless                                     | slop 弄脏 v.           |            |
| den             | 窝；兽穴                    | burrow, hole, lair                                     |                      |            |
| gauche          | 笨拙的，不会社交的               | tactless, awkward, lacking social polish               |                      |            |
| projectile      | 抛射体；射弹                  |                                                        | project v. 抛射        |            |
| scintillate     | 闪烁；（言谈举止中）焕发才智          | sparkle, emit spark                                    |                      |            |
| skirmish        | 小规模战斗；小冲突               | minor dispute or contest                               |                      |            |
| smirk           | 假笑；得意的笑                 | simper                                                 |                      |            |
| aristocracy     | 贵族；贵族政府、统治              |                                                        | aristo 最好的 + cracy   |            |
| wrought         | 做成的；形成的；精制的             | made delicately or elaborately                         |                      |            |
| flossy          | 华丽的，时髦的；丝绵般的，柔软的        |                                                        | floss n. 牙线，棉签       |            |
| grovel          | 摇尾乞怜；奴颜婢膝；匍匐            | stoop, humbly and abjectly                             |                      |            |
| sift            | 筛选，过滤                   | separate out by a sieve                                |                      | sift … out |
