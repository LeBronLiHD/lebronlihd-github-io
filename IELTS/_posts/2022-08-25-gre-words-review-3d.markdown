---
layout: post
title:  "GRE Words 3 Day Review"
date:   2022-08-25 13:52:01 +0800
category: IELTSPosts
---

# New Words (3-Day Review)

&copy; LeBronLiHD

## Vocabulary

| English   | Chinese     | Synonyms                                              | Helpful Memory Ideas | Usage |
|-----------|-------------|-------------------------------------------------------|----------------------|-------|
| 1| 1| 1| 1| 1|
| deplore   | 悲悼；哀叹；谴责    | bemoan, bewail, grieve, lament, moan                  |                      |       |
| rigid     | 严格的，僵硬的，刚硬的 | stiff, incompliant, inflexible, unpliable, unyielding |                      |       |
| sobriety  | 节制，庄重       | moderation, gravity                                   |                      |       |
| repudiate | 拒绝接受，回绝，抛弃  | dismiss, reprobate, spurm                             |                      |       |
| collusive | 共谋的         |                                                       | col 共同               |       |
| 2          | 2                 | 2                                                                    | 2                    | 2     |
| scruple    | 顾忌，迟疑             | boggle, stickle, an ethical consideration, hesitate                  |                      |       |
| profligacy | 放荡；肆意挥霍           |                                                                      | profligate v.        |       |
| allusion   | 暗指，间接提到           |                                                                      | allude v.            |       |
| defiant    | 反抗的，挑衅的           | bold, impudent                                                       |                      |       |
| deciduous  | 非永久的；短暂的；脱落的；落叶的  |                                                                      | de + ciduous         |       |
| denigrate  | 污蔑，诽谤             | defame, blacken, to disparage the character or reputation of         |                      |       |
| transmute  | 变化                | change or alter                                                      |                      |       |
| stint      | 节制，限量，节省          | to restrict                                                          | stint on sth. 吝惜…    |       |
| repellent  | 令人厌恶的             | loathsome, odious, repugnant, revolting, arousing disgust, repulsive |                      |       |
| conjecture | 推测，臆测             | presume, suppose, surmise                                            |                      |       |
| ingenious  | 聪明的；善于创造发明的；心灵手巧的 | clever, inventive                                                    |                      |       |
| intricate  | 错综复杂的；难懂的         | complicated, knotty, labyrinthine, sophisticated, elaborate, complex |                      |       |
| stratify   | （使）层化             | to divide or arrange into classes, castes, or social strata          |                      |       |
| dismal     | 沮丧的，阴沉的           | showing sadness                                                      | dies mail “不吉利的日子”   |       |
| querulous  | 抱怨的，爱发牢骚的         | habitually, complaining, fretful                                     |                      |       |
| 3                | 3                  | 3                                                                 | 3                    | 3     |
| weightless       | 无重力的，失重的           |                                                                   |                      |       |
| gaudy            | 俗丽的                | bright and showy                                                  |                      |       |
| undifferentiated | 无差别的，一致的           | uniform                                                           |                      |       |
| agitation        | 焦虑，不安；公开辩论，鼓动宣传    | anxiety, public argument or action for social or political change |                      |       |
| disputable       | 有争议的               | arguable, debatable                                               |                      |       |
| impecunious      | 不名一文的；没钱的          | having very little or no money                                    |                      |       |
| tributary        | 支流，进贡国；支流的，辅助的，进贡的 | making additions or yielding supplies, contributory               |                      |       |
| susceptible      | 易受影响的，敏感的          | unresistant to some stimulus, influence, or agency                |                      |       |
| 4           | 4                 | 4                                                                           | 4                    | 4                     |
| surly       | 脾气暴躁的；阴沉的         | bad tempered, sullen                                                        |                      |                       |
| surmount    | 克服，战胜；登上          | to prevail over, overcome, to get to the top of                             | sur + mount          |                       |
| choppy      | 波浪起伏的；（风）不断改变方向的  | rough with small waves, changeable, variable                                |                      |                       |
| depreciate  | 轻视；贬值             | belittle, disparage                                                         | de + (ap)preciate    |                       |
| tout        | 招徕顾客，极力赞扬         | to praise or publicize loudly                                               |                      |                       |
| virtuous    | 有美德的              | showing virtue                                                              | virtue n.            |                       |
| plaintive   | 悲伤的，哀伤的           | expressive of woe, melancholy                                               | plaint n. 哀诉         |                       |
| penitent    | 悔过的，忏悔的           | expressing regretful pain, repentant                                        |                      |                       |
| arcane      | 神秘的，秘密的           | cabalistic, impenetrable, inscrutable, mystic, mysterious, hidden or secret |                      |                       |
| impound     | 限制；依法没收，扣押        | to confine, to seize and hold in the custody of the law, take possession of |                      |                       |
| succor      | 救助，援助             | to go to the aid of                                                         |                      |                       |
| cavil       | 调毛病；吹毛求疵          | quibble                                                                     |                      | cavil at sth.         |
| aberrant    | 越轨的；异常的           | turning away from what is right, deviating from what is normal              |                      |                       |
| procurement | 取得，获得；采购          |                                                                             | procure v.           |                       |
| arduous     | 费力的，艰难的           | marked by great labor or effort                                             |                      | arduous march         |
| sprawling   | 植物蔓省的；（城市）无法计划开展的 | spreading out ungracefully                                                  |                      | sprawling handwriting |
| vacillate   | 游移不定，踌躇           | to waver in mind, will or feeling                                           |                      |                       |
| castigate   | 惩治；严责             | chasten, chastise, discipline, to punish or rebuke severely                 |                      |                       |
| tenacious   | 坚韧的，顽强的           |                                                                             |                      |                       |
| topple      | 倾覆，推倒             | to overthrow                                                                |                      |                       |
| cessation   | 中止；（短暂的）停止        | a short pause or a stop                                                     |                      |                       |
| prodigious  | 巨大的；惊人的，奇异的       | colossal, gigantic, immense, marvelous, stupendous                          | prodigy 神童           |                       |
| 5           | 5              | 5                                                             | 5                    | 5     |
| valiant     | 勇敢的，英勇的        | valorous, courageous                                          |                      |       |
| chromatic   | 彩色的，五彩的        | having color or colors                                        |                      |       |
| vigilant    | 机警的，警惕的        | alertly watchful to avoid danger                              |                      |       |
| scorn       | 轻蔑，瞧不起         | contemn, despise                                              |                      |       |
| reigning    | 统治的，起支配作用的     |                                                               |                      |       |
| disrupt     | 使混乱，使中断        |                                                               |                      |       |
| hearten     | 鼓励，激励          | to make sb. feel cheerful and encouraged                      |                      |       |
| charismatic | 有魅力的           | having, exhibiting, or based on charisma or charism           |                      |       |
| treacherous | 背叛的；叛逆的        | showing great disloyalty and deceit                           |                      |       |
| insidious   | 暗中危害的；阴险的      | working or spreading harmfully in a subtle or stealthy manner |                      |       |
| effete      | 无生产力的；虚弱的      | spent and sterile, lacking vigor                              |                      |       |
| improvident | 无远见的，不节俭的      | lacking foresight or thrift                                   |                      |       |
| feckless    | 无效的，效率低的；不负责任的 | inefficient, irresponsible                                    |                      |       |
| reclusive   | 隐遁的，隐居的        | seeking or preferring seclusion or isolation                  |                      |       |
| stained     | 污染的，玷污的        | blemished, discolored, marked, spotted, tarnished             |                      |       |
| distortion  | 扭曲，曲解          |                                                               |                      |       |
| forlorn     | 孤独的；凄凉的        | abandoned or deserted, wretched, miserable                    |                      |       |
| rampant     | 猖獗的，蔓生的        |                                                               |                      |       |
| trepidation | 恐惧，惶恐          | timorousness, uncertainty, agitation                          |                      |       |
| suspense    | 悬念；挂念          | apprehension, uncertainty, anxiety                            |                      |       |
| 6             | 6                | 6                                                                         | 6                    | 6                        |
| reprisal      | 报复；报复行动          | practice in retaliation                                                   |                      |                          |
| inordinate    | 过度的；过分的          | immoderate, excessive                                                     |                      |                          |
| congenial     | 意气相投的；性情好的；适意的   | companionable, amiable, agreeable                                         |                      |                          |
| scour         | 冲刷；擦掉；四处搜索       | to clear, dig, remove, rub, to range over in a search                     |                      |                          |
| canny         | 精明仔细的            | shrewd and careful                                                        |                      |                          |
| scavenge      | 清除，从废物中提取有用物质    | to cleanse, to salvage from discarded or refuse material                  |                      |                          |
| exuberant     | （人）充满活力的；（植物）茂盛的 | lively and cheerful                                                       |                      |                          |
| complementary | 互补的              | combining well to form a whole                                            | complement n. 补充物    | be complementary to sth. |
| rancor        | 深仇；怨恨            | bitter deep-seated ill will, enmity                                       |                      |                          |
| flagrant      | 罪恶昭著的；公然的        | conspicuously offensive                                                   |                      |                          |
| hypocrisy     | 伪善；虚伪            | cant, pecksniff, sanctimony, sham                                         |                      |                          |
| ethnic        | 民族的，种族的          | of a national, racial or tribal group that has a common culture tradition |                      |                          |
| repugnant     | 令人厌恶的            | strong distaste or aversion                                               |                      | be repugnant to sb.      |
| 7                | 7            | 7                                                                | 7                    | 7     |
| antipathy        | 反感，厌恶        | animosity, animus, antagonism, enmity, hostility                 | anti + pathy         |       |
| frivolous        | 轻薄的，轻佻的      | marked by unbecoming levity                                      |                      |       |
| adorn            | 装饰           | to decorate, bueatify                                            |                      |       |
| didactic         | 教诲的；说教的      | morally instructive, boringly pedantic or moralistic             |                      |       |
| incompetent      | 无能力的；不胜任的    |                                                                  | in + competent       |       |
| substantive      | 根本的；独立存在的    | dealing with essentials, being in a totally independently entity |                      |       |
| irreverent       | 不尊敬的         | disrespectful                                                    |                      |       |
| infer            | 推断，推论，推理     | to reach in an opinion from reasoning                            |                      |       |
| adhesive         | 黏着的；带黏性的；黏合剂 | tending to adhere or cause adherence, an adhesive substance      |                      |       |
| indiscriminately | 随意的，任意的      | in a random manner, promiscuously, arbitrary                     |                      |       |
| derogatory       | 不敬的；贬损的      | disparaging, belittling                                          |                      |       |
| crumple          | 把…弄皱；起皱；破裂   | crush together into creases or wrinkles, to fall apart           |                      |       |
| demur            | 表示抗议，反对      |                                                                  | de + mur             |       |
| 8            | 8                | 8                                                                         | 8                    | 8     |
| volatile     | 反复无常的；易挥发的       | fickle, inconstant, mercuial, capricious                                  |                      |       |
| denounce     | 指责               | to accuse publicly                                                        |                      |       |
| garble       | 曲解，篡改            | to alter or distort as to create a wrong impression or change the meaning |                      |       |
| flout        | 蔑视，违抗            | fleer, gibe, jeer, jest, sneer                                            |                      |       |
| elusive      | 难懂的，难以描述的；不易被抓获的 |                                                                           |                      |       |
| garrulous    | 唠叨的，话多的          | loquacious, talkative, chatty, conversational, voluble                    |                      |       |
| obstruct     | 阻塞（通道）；妨碍        | block by an obstacle                                                      |                      |       |
| antithetical | 相反的；对立的          | opposite, contradictory, contrary, converse, counter, reverse             | antithesis n.        |       |
| exquisite    | 精致的；近乎完美的        | elaborately made, delicate, consummate, perfected                         |                      |       |
| moratorium   | 延缓偿付；活动中止        |                                                                           |                      |       |
| competing    | 有竞争性的；不相上下的      |                                                                           | compete              |       |
| speculate    | 沉思，思索；投机         | to meditate on or ponder                                                  |                      |       |
| 9               | 9                           | 9                                                                      | 9                    | 9     |
| preservere      | 坚持不懈                        |                                                                        |                      |       |
| haggle          | 讨价还价                        | bargain, dicker, wrangle                                               |                      |       |
| falter          | 蹒跚，支支吾吾地说                   | flounder, stagger, tumble, stammer                                     |                      |       |
| peripatetic     | 巡游的，游动的                     | itinerant                                                              |                      |       |
| whimsy          | 古怪，异想天开                     | whim, a fanciful creation, boutade, caprice, crotchet, freak           |                      |       |
| allure          | 引诱，诱惑                       | to entice by charm or attraction                                       |                      |       |
| emulate         | 与…竞争，努力赶上                   | to strive to equal or excel                                            |                      |       |
| collusion       | 共谋，勾结                       |                                                                        | allusion 暗指，间接提到     |       |
| peevish         | 不满的，抱怨的；暴躁的；易怒的             | huffy, irritable, pettish, discontented, querulous, fractious, fretful | peeve v.             |       |
| subdued         | （光、声）柔和的，缓和的；（人）温和的         | unnaturally or unusually quiet in behavior                             |                      |       |
| intrigue        | 密谋；引起…的兴趣                   | to plot or scheme secretly, to arouse the interest or curiosity of     |                      |       |
| revise          | 校订，修订                       | redraft, redraw, revamp                                                |                      |       |
| aloof           | 远离的；冷漠的                     |                                                                        |                      |       |
| blatant         | 厚颜无耻的；显眼的；炫耀的               | brazen, completely obvious, conspicuous, showy                         |                      |       |
| surrogate       | 代替品；代理人                     | substitution                                                           |                      |       |
| narcotic        | 麻醉剂；催眠的                     | soporific                                                              |                      |       |
| phlegmatic      | 冷淡的，不动感情的                   | apathetic, impassive, stolid, stoic                                    |                      |       |
| penetrating     | （声音）响亮的；尖锐的；（气味）刺激的；（思想）敏锐的 | acute, discerning                                                      |                      |       |
| exploit         | 剥削；充分利用                     |                                                                        |                      |       |
| 10            | 10           | 10                                                              | 10                                    | 10    |
| permeable     | 可渗透的         | penetrable                                                      |                                       |       |
| revert        | 恢复；重新考虑      | to go back, to consider again                                   | re + vert                             |       |
| annex         | 兼并；附加        | to obtain or take for oneself, to attach                        |                                       |       |
| prosecute     | 起诉；告发        | to carry on a legal suit, to carry on a prosecution             |                                       |       |
| improvise     | 即席创作         | impromptu, extemporize                                          | improverished 贫穷的，贫困的                 |       |
| desecrate     | 玷辱，亵渎        | treat as not sacred, profane                                    | de + secra (sacred), prescribe 开处方；规定 |       |
| predatory     | 掠夺的；食肉的      | predatorial, rapacious, raptorial                               |                                       |       |
| grating       | （声音）刺耳的；恼人的  | hoarse, raucous, irritating, annoying, harsh and rasping        |                                       |       |
| perspicacious | 独具慧眼的；敏锐的    | of acute mental vision or discernment                           |                                       |       |
| solicit       | 恳求；教唆        | to make petition to, to entice into evil                        | solicitude 关怀，牵挂；solicitous 热切的，挂念的   |       |
| atrophy       | 萎缩，衰退        | decadence, declination, degeneracy, degeneration, deterioration |                                       |       |
| omit          | 疏忽，遗漏；不做，未能做 | to leave out, to leave undone, disregard, ignore, neglect       |                                       |       |
| whittle       | 削（木头）；削减     | to pare or cut off chips, to reduce, pare                       | whistle 口哨                            |       |
| crooked       | 不诚实的；弯曲的     | dishonest, not straight                                         | crook v. ；brook 小河                    |       |
| indent        | 切割成锯齿状       | notch, cut serratedly                                           |                                       |       |
| creed         | 教义，信条        | belief, tenet                                                   |                                       |       |
| underhanded   | 秘密的；狡诈的      | marked by secrecy and deception, sly                            |                                       |       |
| shrivel       | （使）枯萎        | mummify, wilt, wither, blight, flag                             |                                       |       |
| delineate     | 勾画，描述        | to sketch out, draw, describe                                   |                                       |       |
| portray       | 描述，描绘；描画     | depict, make a picture, delineate                               |                                       |       |
| deride        | 嘲笑，愚弄        | to laugh at, ridicule                                           |                                       |       |
| ignominious   | 可耻的；耻辱的      | disgraceful, humiliating, despicable, dishonorable              |                                       |       |
| furtive       | 偷偷的，秘密的      | catlike, clandestine, covert, secret, surrepitious              |                                       |       |
| 11             | 11             | 11                                                                               | 11                                        | 11                    |
| assiduous      | 勤勉的；专心的        | diligent, persevering, attentive                                                 |                                           | be assiduous in sth.  |
| averse         | 反对的；不愿意的       | not willing or inclined, opposed                                                 |                                           |                       |
| ravage         | 摧毁；使荒废         | desolate, devastate, havoc, ruin and destroy                                     | revenge 复仇                                |                       |
| arrogant       | 自负的；自大的        | overbearing, haughty, proud, pompous, imperious                                  |                                           |                       |
| forfeit        | 丧失；被罚没收；丧失的东西  |                                                                                  |                                           |                       |
| stunning       | 极富魅力的          | amazing, dazzling, marvellous, strikingly impressive                             |                                           |                       |
| antagonism     | 对抗，敌对          | animosity, animus, antipathy, opposition, rancor                                 |                                           |                       |
| extrapolate    | 预测，推测          | speculate                                                                        |                                           |                       |
| itinerant      | 巡回的            | peripatetic, nomadic                                                             |                                           |                       |
| nonchalant     | 冷淡的，冷漠的        |                                                                                  | nonchalance                               |                       |
| trample        | 踩坏，践踏；蹂躏       | tread, override                                                                  |                                           |                       |
| obsequiousness | 谄媚             | servility, subservience, abject submissiveness                                   |                                           |                       |
| immaculate     | 洁净的；无暇的        | perfectly clean, unsoiled, impeccable                                            |                                           |                       |
| subside        | （建筑物等）下陷；平息，减退 | tend downward, descend, become quiet or less, ebb, lull, moderate, slacken, wane |                                           |                       |
| retrospective  | 回顾的；回溯的        |                                                                                  | retrospection；introspective 内省的，自省的       |                       |
| satirize       | 讽刺             | lampoon, mock, quip, scorch                                                      | sarcastic, sneering, caustic, ironic adj. |                       |
| introvent      | 性格内向的人         | one whose thoughts and feelings are directed toward oneself                      |                                           |                       |
| subversive     | 颠覆性的；破坏性的      |                                                                                  | sub + ver + sive                          |                       |
| banter         | 打趣，玩笑          | playful, good-humored joking                                                     |                                           |                       |
| thwart         | 阻挠；挫败          | baffle, balk, foil, furstrate                                                    | throat 嗓子                                 |                       |
| superfluous    | 多余的，累赘的        | exceeding what is needed                                                         |                                           |                       |
| dorsal         | 背部的；脊背的        |                                                                                  |                                           |                       |
| imminent       | 即将发生的；逼近的      | close, impending, proximate                                                      |                                           |                       |
| saturate       | 浸湿；浸透；使大量吸收或充满 |                                                                                  |                                           |                       |
| brink          | （峭壁）边缘         |                                                                                  |                                           |                       |
| mortify        | 使丢脸；侮辱         | affront, insult, humiliate                                                       |                                           |                       |
| reciprocation  | 互换；报答；往复运动     | mutual exchange, return, alternating motion                                      |                                           | reciprocation of sth. |
| squander       | 浪费，挥霍          | to spead extravagantly                                                           |                                           |                       |
| surmise        | 推测，猜疑          | conjecture, speculate                                                            |                                           |                       |
| ascetic        | 禁欲的；苦行者        | astringent, austere, mortified, severe, stern                                    |                                           |                       |
| 12          | 12                | 12                                                                              | 12                                           | 12                 |
| terrorize   | 恐吓                | to fill with terror or anxiety, intimidate, threaten, menace                    | territorial 领土的，地方的；territory 领土             |                    |
| recount     | 叙述；描写             | narrate, depict, portray                                                        |                                              |                    |
| impertinent | 不切题的；无礼的          | irrelevant, rude, reckless                                                      |                                              |                    |
| fortuitous  | 偶然发生的；偶然的         | accidental, lucky, casual, contingent, incidental                               | fortitude 坚毅，坚忍不拔                            |                    |
| ill-will    | 敌意，仇视，恶感          | enmity, malice, antagonism, animosity, animus, antipathy, opposition, rancor    |                                              |                    |
| obsessed    | 着迷的；沉迷的           |                                                                                 | obsess v.；obese 极胖的                          |                    |
| gouge       | 挖出，骗钱；半圆凿         | scoop out, cheat out of money, a semicircular chisel                            | gauge 准则，规范                                  |                    |
| ascent      | 上升，攀登；上坡路，提高，提升   |                                                                                 |                                              |                    |
| formidable  | 令人畏惧的；可怕的；难以克服的   | causing fear or dread, hard to handle or overcome                               |                                              |                    |
| perfidious  | 不忠的；背信弃义的         | faithless                                                                       |                                              |                    |
| compelling  | 引起兴趣的             | captivating, keenly interesting                                                 | a compelling subject；rebellious 造反的，反叛的，难控制的 |                    |
| brusqueness | 唐突，直率             | candor, abruptness                                                              | brusque adj.                                 |                    |
| ascribe     | 归因于；归咎于           |                                                                                 | a + acribe 写（告状）                             |                    |
| strenuous   | 奋发的；热烈的           | vigorously active, fervent, zealous, earnest, energetic, spirited               |                                              |                    |
| nocturnal   | 夜晚的，夜间发生的         | happening in the night                                                          |                                              |                    |
| synoptic    | 摘要的               | affording a general view of the whole                                           | hypnotic, narcotic 催眠的；催眠药                   |                    |
| infiltrate  | 渗透，渗入             | to pass through                                                                 |                                              |                    |
| instantly   | 立即，即刻             | with importunity, directly, forthwith, immediately                              |                                              |                    |
| prescient   | 有远见的，预知的          |                                                                                 |                                              |                    |
| reinstate   | 回复                |                                                                                 | re + instate 表达                              |                    |
| somber      | 忧郁的；阴暗的           | melancholy, dark and gloomy, dim, grave, shadowy, shady, overcast, disconsolate | 哭鼻子                                          |                    |
| flaunt      | 炫耀，夸耀             | flash, parade, show off                                                         | flout 蔑视，违抗                                  |                    |
| defy        | 违抗，藐视             |                                                                                 | flout 蔑视，违抗                                  |                    |
| absurd      | 荒谬的，可笑的           |                                                                                 |                                              | absurd assumptions |
| spurious    | 假的；伪造的            | forged, false, falsified                                                        |                                              |                    |
| prudent     | 审慎的；精明的；节俭的，精打细算的 | judicious, sage, sane                                                           |                                              |                    |
| sedative    | （药物）镇静的；镇静剂       | calming, relaxing, soothing, tranquilizer                                       |                                              |                    |
| tangential  | 切线的；离题的           | discursive, excursive, rambling, impertinent                                    |                                              |                    |
| 13          | 13                       | 13                                                                      | 13                            | 13    |
| inflamed    | 发炎的，红肿的                  |                                                                         | "infection；nonflammable 不易燃的  | "     |
| segregate   | 分离，隔离                    |                                                                         | propagate 繁殖；传播               |       |
| reportorial | 记者的，报道的                  |                                                                         |                               |       |
| prodigal    | 挥霍的，挥霍者                  | lavish                                                                  |                               |       |
| dormant     | 冬眠的；静止的                  | torpid in winter, quiet, still                                          |                               |       |
| plumb       | 测探物，铅锤；垂直的；精确的；深入了解；测量深度 |                                                                         |                               |       |
| arbitrary   | 专横的，武断的                  | discretionary, despotic, dictatorial, monocratic, autarchic, autocratic | tributary 支流，进贡国；支流的，辅助的，进贡的  |       |
| devastate   | 摧毁，破坏                    | ravage, destroy, depredate, wreck                                       |                               |       |
| exaggerate  | 夸大，夸张；过分强调               | overstate, overemphasize, intensify, magnify                            |                               |       |
| poignant    | 令人痛苦的；伤心的；尖锐的；尖刻的        | painfully affecting the feelings, cutting                               |                               |       |
| disprove    | 证明…有误                    |                                                                         |                               |       |
| timorous    | 胆小的，胆怯的                  | fearful, of timid disposition                                           | audacious 胆大的； timely 及时的     |       |
| promptness  | 敏捷，迅速，机敏                 | agility                                                                 |                               |       |
| salutary    | 有益的，有益健康的                |                                                                         | sanitary （有关）卫生的，清洁的          |       |
| dialect     | 方言                       |                                                                         |                               |       |
| demobilize  | 遣散，使复员                   | disband                                                                 |                               |       |
| rhapsodic   | 狂热的，狂喜的；狂想曲的             |                                                                         |                               |       |
| vindictive  | 报复的                      | vengeful                                                                |                               |       |
| recondite   | 深奥的，晦涩的                  |                                                                         | reconcile, reconciliate 和解，调和 |       |
| culpable    | 有罪的，该受谴责的                | blameworthy, deserving blame                                            |                               |       |
| tactile     | 有触觉的                     |                                                                         |                               |       |
| vigorous    | 精力充沛的；有力的                | energetic, lusty, sternous                                              | rigorous   严格的，严峻的            |       |
| 14            | 14              | 14                                                                                           | 14                            | 14                      |
| venerate      | 崇敬，敬仰           | adore, revere, worship, regard with reverential respect                                      |                               |                         |
| ethos         | （个人、团体、民族）风貌、气质 |                                                                                              | ethnic 民族的，种族的                |                         |
| slash         | 大量削减            | reduce sharply                                                                               |                               |                         |
| capricious    | 变化无常的，任性的       | fickleness, fickle, inconstant, lubricious, mercurial, whimsical, whimsied, erratic, flighty |                               |                         |
| insolent      | 粗野的，无礼的         | impudent, boldly disrespectful                                                               |                               |                         |
| deplete       | 大量减少，使枯竭        | drain, impoverish, exhaust                                                                   |                               |                         |
| oust          | 驱逐              | force out, expel                                                                             |                               |                         |
| overawe       | 威慑              |                                                                                              | over + awe                    |                         |
| recuperate    | 恢复（健康、体力）；复原    | regain, recover                                                                              |                               |                         |
| prudish       | 过分守礼的；假道学的      | marked by prudery, priggish                                                                  |                               |                         |
| defecate      | 澄清，净化           | clarify                                                                                      |                               |                         |
| anarchy       | 无政府；政治混乱        | absence of government, political disorder                                                    |                               |                         |
| belligerent   | 发动战争的；好斗的，挑衅的   | defiant, provoke, provocation, rebellious                                                    |                               |                         |
| fallacious    | 欺骗的；误导的；谬误的     | misleading, deceptive, erroneous                                                             |                               |                         |
| bolster       | 枕垫；支持，鼓励        | cushion, pillow, support, strengthen, reinforce                                              |                               |                         |
| consummate    | 完全的；完善的；完成      | complete, perfect, accomplish                                                                |                               |                         |
| flickering    | 闪烁的，摇曳的；忽隐忽现的   | glittery, twinkling                                                                          | flicker v.                    |                         |
| rescind       | 废除，取消           | make void                                                                                    |                               |                         |
| insinuate     | 暗指，暗示           | imply                                                                                        |                               |                         |
| momentous     | 极为重要的；重大的       | considerable, significant, substantial, consequential                                        |                               |                         |
| reprehensible | 应受谴责的           | deserving reprehension, culpable, blameworthy, blamable, blameful, censurable                | "apprehensive 害怕的；敏悟的         | "                       |
| countenance   | 支持，赞成；表情        | advocate, approbate, approve, encourage, favor, sanction                                     |                               |                         |
| sanctum       | 圣所              | a sacred place                                                                               |                               | inner sanctum           |
| overrule      | 驳回，否决           |                                                                                              |                               |                         |
| irradicable   | 无法根除的；根深蒂固的     | confirmed, entrenched, ineradicable, inveterate, ingrained                                   |                               |                         |
| forte         | 长处，特长；强音的       | special accomplishment                                                                       |                               |                         |
| veracious     | 诚实的；说真话的        | truthful, honest, faithful, veridical                                                        | valorous 勇敢的；tenacious坚韧的，顽强的 |                         |
| jaded         | 疲惫的；厌倦的         | wearied, fatigued, dull, satiated                                                            | jest 笑话，俏皮话；说笑，开玩笑            | be jaded by sth.        |
| circumscribe  | 划界限；限制          | restrict, restrain, limit                                                                    | circum 圆 + scribe 画           |                         |
| impediment    | 妨碍，障碍物          | obstacle                                                                                     |                               | an impediment to reform |
| slanderous    | 诽谤的             | abusive, false and defamatory                                                                |                               |                         |
| 15             | 15                      | 15                                                                   | 15                                                                    | 15    |
| succumb        | 屈从，屈服；因…死亡              |                                                                      | sus 下面 + sumb 躺                                                       |       |
| propitiate     | 讨好；抚慰                   | assuage, conciliate, mollify, consolate, pacify, placate             |                                                                       |       |
| extensive      | 广大的；多方面的；广泛的            |                                                                      |                                                                       |       |
| futile         | 无效的，无用的；没出息的            | bootless, otiose, unavailing, useless, without advantage or benefit  | fertile, fertilizer, fertilize 多产、化肥、使受精；facile, superficial 易做到的，肤浅的 |       |
| prostrate      | 俯卧的；沮丧的；使下跪鞠躬           | prone, powerless, helpless                                           | pro (prone) + strate                                                  |       |
| renegade       | 叛徒，叛教者                  | apostate, defector                                                   | re + neg 反 + ade                                                      |       |
| presumptuous   | 放肆的；过分的                 | overweening, presuming, uppity                                       | impromptu 即兴的                                                         |       |
| patriarchal    | 家长的，族长的；父权制的            |                                                                      | patriarchy, patriarch                                                 |       |
| rowdy          | 吵闹的；粗暴的                 |                                                                      |                                                                       |       |
| sedulous       | 聚精会神的；勤勉的               | diligent                                                             | preoccupation                                                         |       |
| uninspired     | 没有灵感的；枯燥的               | having no intellectual, emotional, or spiritual excitement, dull     |                                                                       |       |
| antiquarianism | 古物研究；好古癖                |                                                                      | antique 古董；古代的                                                        |       |
| discreet       | 小心的；言行谨慎的               | prudent, modest, calculating, cautious, chary, circumspect, gingerly |                                                                       |       |
| bane           | 祸根                      | the cause of distress, death, or ruin                                |                                                                       |       |
| commiserate    | 同情，怜悯                   | sorrow or pity for sb.                                               | commensurate 同样大小的，相称的                                                |       |
| spruce         | 云杉；整洁的                  | neat or smart, trim                                                  |                                                                       |       |
| crawl          | 爬，爬行                    |                                                                      | prone position；brawl 争吵，斗殴                                            |       |
| oblivious      | 遗忘的；忘却的；疏忽的             | forgetful, unmindful                                                 | devious 不坦诚的，弯曲的，迂回的                                                  |       |
| aberration     | 越轨                      | being aberrant                                                       |                                                                       |       |
| shunt          | 使（火车）转到另一个轨道；改变（某物的）方向的 |                                                                      | shun 躲闪                                                               |       |
| 16           | 16            | 16                                                             | 16                             | 16                        |
| concomitant  | 伴随的           | accompanying, attendant                                        | con + come + itant             |                           |
| irritating   | 刺激的；使生气的      | irritative, annoying                                           |                                |                           |
| compendium   | 简要，概略         | digest, pandect, sketch, syllabus, summary, abstract           | recapitulate 扼要概述；synoptic 摘要的 |                           |
| boastfulness | 自吹自擂；浮夸       |                                                                | boast v.                       |                           |
| deprecatory  | 不赞成的，反对的      | disapproving                                                   |                                | be deprecatory about sth. |
| evasive      | 回避的，逃避的；推脱的   |                                                                | evade v.                       |                           |
| monotony     | 单调，千篇一律       | tedious sameness, humdrum, monotone                            |                                |                           |
| overt        | 公开的，非秘密的      | apparent, manifest                                             |                                |                           |
| infirm       | 虚弱的           | physically weak                                                |                                |                           |
| scarlet      | 猩红的，鲜红的       | bright red                                                     | scarlet fever 猩红热              |                           |
| unstinting   | 慷慨的，大方的       | very generous                                                  | skinflint 吝啬鬼                  |                           |
| competence   | 胜任，能力         |                                                                |                                |                           |
| calummy      | 诽谤，中伤         | defamation, aspersion, calumniation, denigration, vilification | slanderous 诽谤的；denigrate 诽谤    |                           |
| parable      | 寓言            |                                                                | allegory 寓言                    |                           |
| clutter      | 弄乱；凌乱         |                                                                | scatter 四散逃窜                   |                           |
| dispel       | 驱散，消除         | scatter or drive away, disperse                                | prosper 繁荣                     |                           |
| endorse      | 赞同，背书         | approve openly                                                 |                                |                           |
| impetuous    | 冲动的，鲁莽的       | impulsive, sudden                                              | an impetuous decision          |                           |
| permeate     | 扩撒，弥漫，穿透      | impenetrate, penetrate, percolate                              |                                |                           |
| succinctness | 简洁，简明         | conciseness, concision                                         |                                |                           |
| turbulent    | 混乱的，骚乱的       | causing unrest, violence, disturbance, tempestuous             |                                |                           |
| mordant      | 讥讽的，尖酸的       | sarcastic, sneering, caustic, ironic                           |                                |                           |
| tribulation  | 苦难，忧患         | distress or suffering resulting from oppression or persecution |                                |                           |
| purport      | 意义，涵义，主旨      | meaning conveyed, implied, gist                                |                                |                           |
| curt         | （言辞，行为）简略而草率的 | brief, rude, terse                                             |                                |                           |
| 17               | 17                 | 17                                                                                         | 17                                                                                  | 17    |
| taunt            | 嘲笑，讥讽；嘲弄，嘲讽        |                                                                                            | burlesque, mock, mockery, travesty, sarcastic, sarcastic, sneering, caustic, ironic |       |
| urbane           | 温文尔雅的              | notably polite or polished in manner, refined, svelte                                      |                                                                                     |       |
| spherical        | 球的，球状的             | globe-shaped, globular, rotund, round                                                      | peripheral 周边的，外围的                                                                  |       |
| uphold           | 维护，支持              | give support to …                                                                          |                                                                                     |       |
| pithy            | （讲话或文章）简练有力的；言简意赅的 | concise, tersely cogent                                                                    | 不是 pity                                                                             |       |
| confound         | 使迷惑，搞混             |                                                                                            |                                                                                     |       |
| impute           | 归咎于，归于             | attribute                                                                                  |                                                                                     |       |
| unremitting      | 不间断的，持续的           | never stopping                                                                             |                                                                                     |       |
| incomprehensible | 难以理解的，难懂的          | impossible to comprehend                                                                   |                                                                                     |       |
| platitude        | 陈词滥调               | banal, trite, stale remark, cliché                                                         |                                                                                     |       |
| combative        | 好斗的                |                                                                                            | "combat 与…搏斗；bombastic 夸夸其谈的                                                        | "     |   |
| ostracize        | 排斥；放逐              | banish, expatriate, expulse, oust, exile by ostracism                                      | orchestrate 给…配管弦乐；精心安排；组织                                                          |       |
| impermanent      | 暂时的                | temporary                                                                                  | im + permanent                                                                      |       |
| fret             | （使）烦躁；焦虑；烦躁        | irritate, annoy, agitation, cark, pother, ruffle, vex, tension                             |                                                                                     |       |
| despicable       | 可鄙的，卑劣的            | contemptible, deserving to be despired                                                     |                                                                                     |       |
| secular          | 世俗的，尘世的            | worldly rather than spiritual                                                              |                                                                                     |       |
| intensification  | 增强，加剧，激烈化          |                                                                                            | intensify v.                                                                        |       |
| reciprocate      | 回报，答谢              | make a return                                                                              |                                                                                     |       |
| barren           | 不育的，贫瘠的，不结果实的      | sterile, bare                                                                              | “拔了；brazen 厚颜无耻的                                                                    |       |
| evade            | 躲避，逃避              | duck, eschew, shun, avoid, escape, elude                                                   | evasive adj.                                                                        |       |
| salient          | 显著的，突出的            | noticeable, conspicuous, prominent, outstanding                                            |                                                                                     |       |
| emblematic       | 作为象征的              | symbolic, representative                                                                   | emblem n.                                                                           |       |
| eloquent         | 雄辩的，流利的            | articulate, forceful and fluent expression                                                 |                                                                                     |       |
| inferable        | 能推理的，能推论的          |                                                                                            | infer 推导                                                                            |       |
| fraternity       | 同行；友爱              |                                                                                            | camaraderie 同志之情；友情                                                                 |       |
| aggressive       | 好斗的，有进取心的          | militant, assertive, full of enterprise and initiative, assertory, pushful, self-assertive |                                                                                     |       |
| 18            | 18                | 18                                                                                                                                            | 18                         | 18    |
| irrevocable   | 不可撤销的             |                                                                                                                                               | ir + revoke + able         |       |
| repel         | 击退；使反感            | fight against, resist, cause aversion                                                                                                         |                            |       |
| thematic      | 主题的               |                                                                                                                                               | theme                      |       |
| impugn        | 提出异议；对…表示怀疑       | to challenge as false or questionable                                                                                                         |                            |       |
| sequester     | （使）隐退；使隔离         | seclude, withdraw, set apart                                                                                                                  | sequestrate 扣押             |       |
| subjective    | 主观的，想象的           |                                                                                                                                               | subject n.                 |       |
| parsimony     | 过分节俭，吝啬           | being stringy, thrift                                                                                                                         |                            |       |
| paramount     | 最重要的；至高无上的        |                                                                                                                                               | para + mount 山；surmount 超过 |       |
| retrieve      | 寻回，取回；挽回，弥补       | regain, redeem, remedy                                                                                                                        |                            |       |
| revile        | 辱骂；恶言相向           | rail, use abusive language                                                                                                                    |                            |       |
| perceptive    | 感知的；知觉的；有洞察力的；敏锐的 |                                                                                                                                               |                            |       |
| presume       | 推测，假定；认定          |                                                                                                                                               | pre 预先 + sum 抓住 + e        |       |
| fray          | 吵架，打斗；磨破          | noisy quarrel or fight, become worn, ragged, or reveled by rubbing                                                                            |                            |       |
| espouse       | 支持，拥护             | auspices, uphold, corroborate, advocate, bolster, support, strengthen, reinforce, approbate, approve, encourage, favor, sanction, countenance | a + spouse 配偶，约定           |       |
| contemplative | 沉思的（人）            |                                                                                                                                               |                            |       |
| frivolity     | 轻浮的行为             | frivolous act                                                                                                                                 |                            |       |
| 19             | 19            | 19                                                                                  | 19                                                      | 19    |
| denote         | 指示，表示         | mark, indicate, signify                                                             | annotate 注解                                             |       |
| obviate        | 排除，消除         | remove, get rid of, eliminate, exclude, preclude                                    |                                                         |       |
| arresting      | 醒目的，引人注意的；断言  | catching the attention, gripping                                                    | assert v.                                               |       |
| scrupulous     | 恪守道德规范的，一丝不苟的 | having moral integrity, punctiliously exact, conscientious, meticulous, punctilious |                                                         |       |
| obsequious     | 逢迎的，谄媚的       | menial, servile, slavish, subservient                                               | ubiquitous 无处不在的；iniquitous 邪恶的，不公正的；quixotic 不切实际的，空想的 |       |
| prod           | 戳，刺；刺激，激励     | poke, stir up, urge                                                                 | prod sb, into doing sth.                                |       |
| extricate      | 可解救的，能脱险的     | capable of being freed from difficulty                                              |                                                         |       |
| enzyme         | 酵素，酶          | biochemical catalyst                                                                |                                                         |       |
| coerce         | 强迫，压制         |                                                                                     | “可扼死”                                                   |       |
| repulse        | 击退，回绝；拒绝      | repel, rebuff, rejection                                                            | re + pulse                                              |       |
| venal          | 腐败的，贪赃枉法的     | characterized by corrupt, bribery                                                   |                                                         |       |
| undemonstrable | 无法证明的，难以证明的   |                                                                                     | un + demonstrable                                       |       |
| enervate       | 使虚弱，使无力       | lessen the vitality or strength of                                                  |                                                         |       |
| 20            | 20               | 20                                                                      | 20                       | 20                               |
| ambivalent    | （对人对物）有矛盾看法的     | having contradictory attitudes towards sth.                             |                          |                                  |
| soliloquy     | 自言自语；戏剧独白        | talking to oneself, dramtic monologue                                   | solit 孤独；solitary 孤独的；隐士 |                                  |
| incentive     | 刺激，诱因，动机；刺激因素    | motive, motivate, goad, impetus, impulse, stimulus, incite              |                          |                                  |
| stout         | 肥胖的；强壮的          | bulky in body, vigorous, sturdy                                         |                          |                                  |
| incendiary    | 防火的，纵火的          |                                                                         | in + cend 发（火）光+ iary    |                                  |
| incandescent  | 遇热发光的；发白热光的      | Incandescent lamps 白炽灯                                                  | candy 蜡烛，糖果              |                                  |
| puncture      | 刺穿，戳破，刺孔         |                                                                         | punct 点 + ture           |                                  |
| fussy         | 爱挑剔的，难取悦的        | dainty, exacting, fastidious, finicky, meticulous                       | fuzzy 模糊的                |                                  |
| wearisome     | 令人厌倦、疲倦          |                                                                         | weary v.                 |                                  |
| demean        | 贬抑；降低            | degrade, humble, belittle                                               |                          |                                  |
| nominally     | 名义上的，有名无实的       |                                                                         | nominal 名义上的             |                                  |
| incisive      | 尖锐的，深刻的          | keen, penetrating, sharp                                                | incisive comments        |                                  |
| tenacity      | 坚持，固执            | being tenacious, doggedness, persistence, persevereness, stubbornness   |                          |                                  |
| draft         | 草稿；草案；汇票         | preliminary written version of sth.                                     |                          |                                  |
| anathema      | 被诅咒的人，（天主教的）革出教门 | one that is cursed, a formal ecclesiastical ban, curse                  |                          |                                  |
| chastisement  | 惩罚               | punishment, castigation, penalty                                        |                          |                                  |
| monetary      | 金钱的；货币的          | about money, nation's currency or coinage, financial, fiscal, pecuniary | money n.                 |                                  |
| humility      | 谦逊；谦恭            | being humble, modesty                                                   | humble adj.              |                                  |
| ingeniousness | 独创性              | ingenuity                                                               |                          |                                  |
| indignation   | 愤慨，义愤            | anger or scorn, righteous anger                                         |                          | be full of righteous indignation |
| sustain       | 承受（困难）；支撑（重量或压力） | undergo, withstand, bolster, prop, underprop                            |                          |                                  |
| actuate       | 开动，促使            | motivate, activate                                                      | accurate 准确的             |                                  |
| strait        | 海峡；狭窄的           | narrow                                                                  | isthmus 地峡               |                                  |
| drab          | 黄褐色的；单调的，乏味的     | yellowish brown, not bright or lively, monotonous                       |                          |                                  |
| subdue        | 征服；压制；减轻         | crush, overpower, subjugate, conquer, vanquish, reduce                  |                          |                                  |
| vitiate       | 削弱，损害            | make faulty or defective, impair                                        | vice 恶的 + tate           |                                  |
| dissipate     | （使）消失，消散；浪费      | break up and scatter or vanish, to waste or squander                    |                          |                                  |
| excavate      | 开洞，凿洞；挖掘         | scoop, shovel, unearth                                                  | ex + cave 洞 + ate        |                                  |
| discretion    | 谨慎，审慎            | circumspection, prudence                                                |                          |                                  |
| 21             | 21                  | 21                                                                           | 21                            | 21                   |
| instigate      | 怂恿，鼓动，煽动            | urge on, foment, incite, agitate, abet, provoke, stir                        | in 进入 + stig (sting 刺激) + ate |                      |
| vulgar         | 无教养的，庸俗的            | morally crude, undeveloped                                                   |                               |                      |
| conflate       | 合并                  | combine or mix                                                               |                               |                      |
| caste          | 社会等级，等级             | class distinction                                                            |                               |                      |
| decrepit       | 衰老的，破旧的             | broken down or worn out                                                      | de + crepit 破裂声               |                      |
| precipitate    | 使突然降临，加速，促成；鲁莽的，轻率的 | bring about abruptly, hasten, impetuous                                      | peripatetic 巡游的，游动的           | precipitate … into … |
| scheme         | 阴谋，（作品等）体系结构        | crafty or secret plan, systematic or organized framework, design             | schema 图表                     |                      |
| euphemistic    | 委婉的                 |                                                                              | euphemism 婉言，委婉的说法            |                      |
| irreconcilable | 无法调和的，矛盾的           | incompatible, conflicting                                                    | in + reconcilable             |                      |
| exorbitant     | 过分的，过度的             | excessive, extravagant, extreme, immoderate, inordinate                      |                               | exorbitant fees      |
| auspicious     | 幸运的；吉兆的             | propitious, favored by future, favorable, advantageous                       | auspices 支持，赞助                |                      |
| grave          | 严肃的，庄重的；墓穴          | serious                                                                      | gravity n.                    |                      |
| embrace        | 拥抱；包含               |                                                                              | em + brace                    |                      |
| transgress     | 冒犯，违背               | violate                                                                      |                               |                      |
| cringe         | 畏缩，谄媚               | shrink from sth, dangerous or painful, servile manner, fawn                  |                               | cringe from …        |
| adversity      | 逆境，不幸               |                                                                              | adverse adj.                  |                      |
| affective      | 感情的，表达感情的           | expression emotion                                                           | affection n.                  |                      |
| plump          | 丰满的，使丰满             | well-rounded                                                                 |                               |                      |
| concede        | 承认，让步               | admit, make a concession                                                     | accede 同意，加入                  |                      |
| extraneous     | 外来的，无关的             | from the outside, not pertinent                                              | extra + neous                 |                      |
| deft           | 灵巧的，熟练的             | dexterous, skillful, adroit, handy, nimble                                   |                               |                      |
| incessant      | 不停的，不断的             | continuing without interruption                                              |                               |                      |
| verification   | 确认，查证               | process of verifying                                                         |                               |                      |
| coalesce       | 联合，合并               | unite or merge into a single body, mix, associate, bracket, combine, conjion |                               |                      |
| pugnacious     | 好斗的                 | having a quarrelsome and combative nature                                    |                               |                      |
| 22             | 22                   | 22                                                                    | 22                                                       | 22                 |
| plausible      | 看似有理的，似是而非的；有道理的，可信的 | superficially reasonable, believable, colorable, credible, creditable |                                                          |                    |
| nomadic        | 游牧的；流浪的              | itinerant, peripatetic, vagabond, vagrant                             |                                                          |                    |
| contrive       | 计划，设计                | think up, devise, scheme, connive, intrigue, machinate, plot          |                                                          |                    |
| vicious        | 邪恶的，堕落的；恶毒的；凶猛的，危险的  | of immorality, spiteful, malicious, dangerously, aggressive           | vice (邪恶) + ous                                          |                    |
| slavish        | 卑屈的；效仿的，无创造性的        |                                                                       | slave + ish                                              |                    |
| intimate       | 亲密的；密友；暗示            | closely acquainted, an intimate friend, hint or imply, suggest        |                                                          |                    |
| paragon        | 模范，典范                | paradigm, exemplar, a model of excellence or perfection               | parody 拙劣的模仿                                             |                    |
| reciprocity    | 相互性；互惠               | mutual exchange of privileges                                         |                                                          |                    |
| recklessness   | 鲁莽，轻率                |                                                                       | reckless adj.；fickleness 浮躁，变化无常                         |                    |
| recoil         | 弹回，反冲；退却，萎缩          |                                                                       | re 反 + coil 卷，盘绕                                         |                    |
| distraught     | 心神狂乱的；发狂的；心烦意乱的      | mentally confused, distressed                                         | distract v. 分散注意力，使不安                                    |                    |
| digressive     | 离题的，枝节的              | characterized by digressions                                          |                                                          | digressive remarks |
| blackmail      | 敲诈，勒索                | payment extorted by threatening                                       | black (黑) + mail                                         |                    |
| divulge        | 泄漏，透露                | disclose, betray, reveal                                              |                                                          |                    |
| repudiation    | 拒绝接受，否认              |                                                                       | repudiate v.                                             |                    |
| perpetuate     | 使永存                  |                                                                       | perpetual adj.                                           |                    |
| onerous        | 繁重的，费力的              | burdensome                                                            | oner (over 负担) + ous                                     |                    |
| impressionable | 易受影响的                | easily affected by impressions                                        |                                                          |                    |
| intemperance   | 放纵，不节制；过度            | lack of temperance, excess, surfeit, overindulgence                   | in +temperance                                           |                    |
| ductile        | 易延展的；可塑的             | capable of being stretched, easily molded, pliable                    | Doctrine 教义                                              |                    |
| crestfallen    | 挫败的，失望的              | dejected, disheartened, humbled                                       | crest 鸡冠 + fallen 下垂                                     |                    |
| restitution    | 归还；赔偿                | restoration                                                           |                                                          |                    |
| modulate       | 调整，调节；调音             | adjust, tune                                                          |                                                          |                    |
| conversant     | 精通的，熟练的              | versed, familiar and acquainted                                       | versed                                                   |                    |
| conserve       | 保存，保藏                | keep in safe and sound state                                          |                                                          |                    |
| unbridled      | 放纵的；不受约束的            | unrestrained, unchecked, uncurbed, ungoverned, indulgent              |                                                          |                    |
| trait          | （人的）特征显著的            | a distinguishing feature, attribute, characteristic, peculiarity      | strait 海峡，狭窄的                                            |                    |
| resonant       | （声音）洪亮的；回响的，共鸣的      | enriched by resonance, echoing                                        |                                                          |                    |
| sophomoric     | 一知半解的                |                                                                       | dilettante, dabbler, amateur 一知半解者，业余爱好者；euphemistic 委婉的 |                    |
| unreserved     | 无限制的；未被预定的           |                                                                       | un + reserved                                            |                    |
| captious       | 吹毛求疵的                | carping                                                               | quibble, cavil 吹毛求疵；capacious 容量大的，宽敞的                   |                    |
| sordid         | 卑鄙的，肮脏的              | dirty, filthy, foul, mean, seedy                                      |                                                          |                    |
| intransigent   | 不妥协的                 | uncompromising, incompliant, intractable, obstinate, pertinacious     |                                                          |                    |
| 23         | 23              | 23                                                                                | 23                   | 23    |
| pledge     | 誓言，保证；发誓        | solemn promise, vow to do sth., commitment, swear                                 |                      |       |
| labile     | 易变化的；不稳定的       | open to change, unstable, fickle, unsteady                                        |                      |       |
| retort     | 反驳              | counter argument                                                                  |                      |       |
| picayunish | 微不足道的，不值钱的      | of little value, petty, small-minded                                              |                      |       |
| crimp      | 使皱起，使卷曲；抵制，束缚   |                                                                                   |                      |       |
| hoist      | 提升，升起；起重机       | raise or haul up, lift, elevate                                                   |                      |       |
| upheaval   | 动乱，剧变           | extreme agitation or disorder                                                     | upheave v.           |       |
| environ    | 包围，围绕           | encircle, surround                                                                | environment          |       |
| pan        | 严厉批评            | criticize severely                                                                |                      |       |
| sentiment  | 多愁善感；思想感情       | tender feeling or emotion                                                         | sent 感觉 + ment       |       |
| bouffant   | 蓬松的，膨胀的         | puffed out, puffy                                                                 |                      |       |
| bearing    | 关系，意义；方位；轴承     | connection, influence, horizontal direction                                       |                      |       |
| defile     | 弄污，弄脏；峡谷，隘路     | make filthy or dirty, pollute, narrow valley or mountain pass                     | de + file (vile 卑鄙)  |       |
| defuse     | 拆除引信，使去除危险性；平息  |                                                                                   | de + fuse 导火索        |       |
| quota      | 定额，配额           | someone's share                                                                   |                      |       |
| obtuse     | 愚笨的；钝的          | dull or insensitive, blunt                                                        |                      |       |
| gaff       | 大鱼钩；鱼叉          |                                                                                   |                      |       |
| bumble     | 说话含糊；拙劣的做       | stumble, proceed clumsily, burr, buzz, hum                                        | humble 谦逊的           |       |
| expulsion  | 驱逐，逐出           | the act of expelling                                                              |                      |       |
| emaciate   | 使瘦弱             | become thin                                                                       |                      |       |
| autocracy  | 独裁政府            |                                                                                   | auto 自己 + cracy 统治   |       |
| wigwag     | 摇动，摆动，摇摆        | move back and forth steadily and rhythmically, sway, teeter, totter, waver, weave |                      |       |
| swoop      | 猛扑，攫取           | move in a sudden sweep, seize or snatch                                           |                      |       |
| epilogue   | 收场白；尾声          | a closing section                                                                 |                      |       |
| pall       | 令人发腻；失去吸引力      |                                                                                   |                      |       |
| riot       | 暴动，闹事           | to create or engage in a riot, frolic, revel, carouse                             |                      |       |
| capacious  | 容量大的，宽敞的        | containing a great deal, spacious                                                 |                      |       |
| dispatch   | 派遣；迅速处理；匆匆吃完；迅速 | send off promptly, dispose efficiently, promptness, haste                         |                      |       |
| knit       | 编织；密接，紧密相联      | threads & needles, connect closely                                                |                      |       |
| chirp      | （鸟虫）唧唧叫         | utter in a sharp, shrill tone                                                     |                      |       |
| inveigle   | 诱骗，诱使           | win with deception, lure                                                          |                      |       |
| mumble     | 咕哝，含糊不清的说       | grumble, murmur, mutter, speak or say unclearly                                   |                      |       |
| trapeze    | 高空秋千，吊架         |                                                                                   |                      |       |
| 24           | 24               | 24                                                                                           | 24                               | 24                            |
| spatter      | 洒，溅              | slop, spray, swash, splash with a liquid                                                     |                                  |                               |
| apprentice   | 学徒               |                                                                                              | ap 接近 + prent (prehend 抓住) + ice |                               |
| ingress      | 进入               | entering                                                                                     |                                  |                               |
| abound       | 大量存在；充满，富于       |                                                                                              | a 没有 + bound 边界                  |                               |
| ripple       | （使）泛起涟漪；波痕，涟漪    |                                                                                              |                                  | a ripple of applause/laughter |
| lope         | 轻快的步伐；大步跑        | easy, swinging stride, run with steady gait                                                  | lobe 耳垂                          |                               |
| recline      | 斜倚，躺卧            | lie down                                                                                     |                                  |                               |
| recess       | 休假；凹槽            | alcove, cleft, suspension of business                                                        | re 反着 + cess 走                   |                               |
| enjoin       | 命令，吩咐            | command, direct or impose by authoritative order                                             | en 使 + join                      |                               |
| brew         | 酿酒；沏茶；酝酿         |                                                                                              |                                  | brew beer or ale              |
| earthy       | 粗俗的，土气的          | rough, plain in taste                                                                        | earth                            |                               |
| miserly      | 吝啬的              | penurious                                                                                    | miser 吝啬鬼                        |                               |
| slay         | 杀戮，杀死            | kill violently in great numbers                                                              |                                  |                               |
| flay         | 剥皮；抢夺，掠夺；严厉指责    | strip off the skin, rob, pillage, criticize or scold mercilessly                             | fray 吵架，打架                       |                               |
| retch        | 作呕，恶心            | vomit                                                                                        |                                  |                               |
| occlude      | 使闭塞              | prevent the passage of                                                                       | oc + clud 关闭                     |                               |
| traipse      | 漫步，闲荡            | wander, ramble, roam, jog                                                                    |                                  |                               |
| squeamish    | 易受惊的；易恶心的        | easily shocked or sickened                                                                   |                                  | the squeamish                 |
| overhaul     | 彻底检查；大修          | check/repair thoroughly                                                                      |                                  |                               |
| dainty       | 美味，精致的食品；娇美的；挑剔的 | small tasty piece of food, especially a tiny cake, delicately pretty, fastidious, particular |                                  |                               |
| receipt      | 收到，接到；发票，收据      |                                                                                              | receive v.                       |                               |
| saddle       | 鞍，马鞍             | a seat of one who rides on horseback                                                         |                                  |                               |
| drub         | 重击，打败            | beat severely, defeat decisively                                                             |                                  |                               |
| obsolete     | 废弃的，过时的          | no longer in use, out of date, old-fashioned                                                 |                                  |                               |
| trim         | 修剪，井井有条的         | make neat by cutting and clipping                                                            |                                  |                               |
| complaisance | 彬彬有礼；殷勤；柔顺       |                                                                                              | com 一起 + plais 高兴 + ance         |                               |
| asinine      | 愚笨的              | of asses, stupid, silly                                                                      | as (ass) + in + in + e           |                               |
| abash        | 使羞愧；使尴尬          | make embarrassed                                                                             | a+bash                           |                               |
| subterfuge   | 诡计，托辞            | a deceptive device or stratagem                                                              | subter 私下+ fuge 逃跑               |                               |
| hardihood    | 大胆，刚毅；厚颜无耻       | boldness, fortitude, brazen                                                                  | hardy 强壮的，大胆的，勇敢的                |                               |
| dispense     | 分配，分法            | to distribute in portions                                                                    |                                  |                               |
| affected     | 不自然的；假装的         | behaving in an artificial way, assumed, factitious, fictitious, unnatural                    |                                  |                               |
| ulcerate     | 溃烂               |                                                                                              | ulcer 溃疡                         |                               |
| burlesque    | 讽刺或滑稽的戏剧         | derisive caricature, parody                                                                  | “不如乐死他”                          |                               |
| flimflam     | 欺骗；胡言乱语          | deception, deceptive nonsense, poppycock, swindle                                            | film 电影 + flam 说                 |                               |
| wince        | 畏缩，退缩            | flinch, cringe, quail, shrink back involuntarily                                             |                                  |                               |
| dint         | 击打出凹痕；力          | make a dent in                                                                               | by dint of … 凭借                  |                               |
| evacuate     | 撤离；疏散            | withdraw, remove inhabitants for protective purpose                                          |                                  |                               |
| plateau      | 高原；平稳时期          | tableland, relatively stable period                                                          |                                  |                               |
| feline       | 猫的，猫科的           |                                                                                              | fel 猫 + ine                      |                               |
| infatuate    | 使迷恋；使糊涂          | inspire with a foolish or extravagant love or admiration                                     |                                  |                               |
| execrable    | 可憎的，讨厌的          | deserving to be execrated, abominable, detestable                                            |                                  | execrable poetry              |
| 25           | 25                  | 25                                                                      | 25                   | 25                 |
| implore      | 哀求，恳求               | beg                                                                     | im 进 + plor 哭泣 + e   |                    |
| nifty        | 极好的，极妙的             | good and attractive, great                                              |                      |                    |
| furbish      | 磨光，刷新               | polish, renovate                                                        | furnish 装饰           |                    |
| persnickety  | 势利的；爱挑剔的            | snobbish, fuzzy, fastidious                                             |                      |                    |
| crass        | 愚钝的，粗糙的             | crude and unrefined                                                     |                      |                    |
| abrogate     | 废止，废除               | abolish, repeal by authority                                            | ab + rog + ate       |                    |
| dissimulate  | 隐藏，掩饰               | pretend, dissemble                                                      |                      |                    |
| duress       | 胁迫                  | force and threats, compulsion                                           |                      |                    |
| aplomb       | 沉着，镇静               | complete and confident composure                                        |                      |                    |
| myopia       | 近视，缺乏远见             | lack of foresight or discernment                                        |                      |                    |
| jab          | 猛刺                  |                                                                         |                      |                    |
| canopy       | 蚊帐；华盖               | drapery, awning, cloth covering                                         |                      |                    |
| compunction  | 懊悔，良心不安             | guilt, remorse, penitence                                               |                      |                    |
| blasé        | 厌倦享乐的，玩厌了的          | board with pleasure or dissipation                                      |                      |                    |
| enclosure    | 圈地，围场               |                                                                         | enclose v.           |                    |
| comatose     | 昏迷的                 | unconscious, torpid                                                     | coma n.              |                    |
| ravish       | 使着迷；强夺              | overcome, take away                                                     |                      |                    |
| spout        | 喷出；滔滔不绝的说           | eject in stream                                                         |                      |                    |
| exculpate    | 开脱；申明无罪，证明无罪        | free from blame, declare and prove guiltless                            |                      |                    |
| boulder      | 大石头                 |                                                                         | shoulder 肩膀          |                    |
| illustrious  | 著名的，显赫的             | very distinguished, outstanding                                         |                      |                    |
| depose       | 免职，宣誓作证             |                                                                         | de + pose 放          |                    |
| grotesque    | 怪诞的，古怪的；（艺术）风格怪异的   | bizarre, fantastic                                                      |                      |                    |
| jug          | 用陶罐等炖；水壶；关押         | jail, imprison, in an earthenware vessel                                |                      |                    |
| embolden     | 鼓励                  | to give confidence to                                                   | em + bold + en       |                    |
| buoy         | 浮标，救生圈；支持，鼓励        | floating object, encourage, animate, elate, exhilarate, flush, inspirit |                      |                    |
| rumple       | 弄皱，弄乱               | make disheveled or tousled                                              |                      |                    |
| whoop        | 高喊，欢呼               | eagerness, exuberance, jubilation                                       |                      |                    |
| enunciate    | 发音，（清楚的）表达          |                                                                         | e + nunci 发音 + ate   |                    |
| char         | 烧焦，把…烧成炭            |                                                                         | chair -> char        |                    |
| martyr       | 烈士，殉道者              |                                                                         | 词根：目击者               |                    |
| daub         | 涂抹；乱画               |                                                                         |                      |                    |
| intensify    | 使加剧                 |                                                                         | intense adj.         |                    |
| ennoble      | 授予爵位，使高贵            | make noble                                                              |                      |                    |
| wiry         | 瘦而结实的               | lean, supple, and vigorous                                              |                      |                    |
| consent      | 同意，允许               | give agreement                                                          |                      |                    |
| woo          | 求爱，求婚；恳求，争取         |                                                                         |                      |                    |
| sullen       | 忧郁的                 | gloomy, dismal, brooding, morose, sulky                                 |                      |                    |
| spite        | 怨恨，恶臭               | petty ill will or hatred                                                |                      |                    |
| enthralling  | 迷人的，吸引人的            |                                                                         | en + thrall 奴隶 + ing |                    |
| anneal       | 使（金属、玻璃等）退火；使变强；使变硬 | strengthen and harden                                                   |                      |                    |
| debark       | 下船，下飞机，下车；卸载        |                                                                         | de + bark 船          |                    |
| celerity     | 快速，迅速               | swiftness                                                               |                      |                    |
| forfeiture   | （名誉等）丧失             |                                                                         | forfeit v.           |                    |
| malfeasance  | 不法行为，渎职             | misconduct by a public official                                         |                      |                    |
| scurvy       | 卑鄙的，下流的             | despicable                                                              | scurry v. 疾行         |                    |
| ogle         | 送秋波，媚眼              |                                                                         |                      |                    |
| pharmacology | 药理学，药物学，药理          |                                                                         |                      |                    |
| vouch        | 担保，保证               | guarantee                                                               |                      |                    |
| cringing     | 谄媚的，奉承的             |                                                                         |                      |                    |
| wheedle      | 哄骗，诱骗               | cajole, coax, seduce                                                    |                      |                    |
| careen       | （船）倾斜；使倾斜           | lean sideways                                                           |                      |                    |
| peculate     | 挪用（公款）              | embezzle                                                                |                      |                    |
| vandalism    | （对公物等）恶意破坏          | willful, malicious                                                      |                      |                    |
| anguish      | 极大的痛苦               | great suffering, distress                                               |                      |                    |
| entrance     | 使出神，使入迷             |                                                                         |                      |                    |
| desert       | 遗弃，离弃               | abandon                                                                 |                      |                    |
| effulgent    | 灿烂的，光辉的             | of great brightness                                                     |                      |                    |
| compatible   | 能和谐共处的；相容的          |                                                                         |                      | be compatible with |
| encomiast    | 赞美者                 | a eulogist                                                              |                      |                    |
| carouse      | 狂饮寻乐                | noisy, merry drinking party                                             |                      |                    |
| cephalic     | 头的，头部的              | of head of skull                                                        | cephal 头             |                    |
| pellucid     | 清晰的，清澈的             | transparent, clear                                                      |                      |                    |
| wan          | 虚弱的，病态的             | feeble, sickly, pallid, ghastly, haggard, sallow                        |                      |                    |
| preponderate | 超过，胜过               | exceed                                                                  | pre + ponderate      |                    |
| snuggle      | 紧靠，依偎               | draw close                                                              |                      |                    |
| hubris       | 傲慢，目中无人             |                                                                         |                      |                    |
| shabby       | 破旧的，卑鄙的             | scruffy, shoddy, dilapidated, despicable, contemptible                  |                      |                    |
| moat         | 壕沟，护城河              |                                                                         |                      |                    |
| heckle       | 诘问，责问               |                                                                         | he + buckle          |                    |
| compulsion   | 强迫；（难以抗拒的）冲动        |                                                                         |                      |                    |
| musket       | 旧式步枪；毛瑟枪            |                                                                         |                      |                    |
| evasion      | 躲避，借口               |                                                                         |                      |                    |
| snarl        | 纠缠，混乱；咆哮，怒骂；怒吼声     | knot                                                                    |                      |                    |
| grumpy       | 脾气暴躁的               | grouchy, peevish                                                        | grump v.             |                    |
| compress     | 压缩，压紧               | contract                                                                |                      |                    |
| tangy        | 气味刺激的，扑鼻的           | pleasantly sharp flavor                                                 |                      |                    |
| impeach      | 控告；怀疑；弹劾            | accuse, charge, indict                                                  |                      |                    |
| snappy       | 生气勃勃的；漂亮的，时髦的       | stylish                                                                 |                      |                    |
| muster       | 召集，聚集               | summon, gather                                                          |                      |                    |
| 26            | 26                | 26                                                              | 26                          | 26              |
| cession       | 割让，转让             |                                                                 | cede v.                     |                 |
| maul          | 打伤；伤害             | lacerate                                                        | haul 用力拖                    |                 |
| leisureliness | 悠然，从容             |                                                                 | leisurely adj.              |                 |
| shattered     | 破碎的               |                                                                 | shatter v.                  |                 |
| perverse      | 刚愎自用的，固执的；反常的     | obstinate in opposing, wrongheaded                              |                             |                 |
| extrude       | 挤出，推出，逐出；伸出，突出    | thrust out, protrude                                            |                             |                 |
| abject        | 极可怜的；卑下的          | miserable, wretched, degraded, base                             | ab + ject                   |                 |
| shrewd        | 机灵的，精明的           | clever discerning awareness                                     | shrew 泼妇                    |                 |
| worship       | 崇拜，敬仰             |                                                                 |                             |                 |
| dawdle        | 闲荡，虚度光阴           | idle, loiter, trifle                                            |                             |                 |
| malaise       | 不适，不舒服            | illness                                                         | “没累死”                       |                 |
| protuberant   | 突出的，隆起的           | prominent, thrusting out                                        |                             |                 |
| twee          | 矫揉造作的，故作多情的       | dainty, delicate, cute or quaint                                |                             |                 |
| nettle        | 荨麻；烦扰，激怒          | irritate, provoke                                               |                             |                 |
| reminisce     | 追忆，回想             |                                                                 | re + mind + sce             |                 |
| assuage       | 缓和，减轻             | lessen, relieve                                                 |                             |                 |
| remit         | 免除，宽恕；汇款          | release                                                         |                             |                 |
| wallow        | 打滚；沉迷             |                                                                 |                             |                 |
| lumber        | 跌跌撞撞的走，笨拙的走；杂物；木材 | move with heavy clumsiness                                      |                             |                 |
| persecute     | 迫害                |                                                                 |                             |                 |
| stealth       | 秘密行动              |                                                                 | steal + th                  |                 |
| rumpus        | 喧闹，骚乱             | noisy commotion, clamor, tumult, uproar                         |                             |                 |
| fold          | 羊栏；折叠             |                                                                 |                             |                 |
| obeisance     | 鞠躬，敬礼             | gesture of respect or reverence                                 |                             |                 |
| mirage        | 海市蜃楼；幻影           |                                                                 | mir 惊奇的 + age               |                 |
| peremptory    | 不容反抗的；专横的         | masterful                                                       | per + empt + ory            |                 |
| spate         | 许多，大量；暴涨，发洪水      |                                                                 |                             | a spate of …    |
| choleric      | 易怒的，暴躁的           | of irascible nature, irritable                                  | choler 胆汁 + ic              |                 |
| incubation    | 孵卵期，潜伏期           |                                                                 |                             |                 |
| invoke        | 祈求，恳求；使生效         | implore, entreat                                                |                             |                 |
| predilection  | 偏爱，嗜好             |                                                                 | pre + dilection (direction) |                 |
| exiguous      | 太少的，不足的           | scanty, meager, scarce, skimpy, sparse                          |                             |                 |
| acumen        | 敏锐，精明             |                                                                 | acu + men                   |                 |
| redolent      | 芬芳的，芳香的           | scented, aromatic                                               |                             |                 |
| gourmand      | 贪食者               | glutton                                                         |                             |                 |
| dimple        | 酒窝                |                                                                 |                             |                 |
| hurdle        | 跨栏，障碍；克服          | barrier, block, obstruction, snag, obstacle, surmount, overcome |                             |                 |
| particularize | 详述，列举             |                                                                 |                             |                 |
| hinge         | 铰链；关键             | joint, determining factor                                       |                             |                 |
| delusion      | 欺骗，幻想             | illusion, hallucination                                         | delude v.                   |                 |
| gander        | 雄鹅；笨蛋，傻瓜；闲逛       |                                                                 | gender 性别                   |                 |
| putrid        | 腐臭的               | rotten, malodorous                                              |                             |                 |
| enchant       | 使陶醉；施法于           | rouse to ecstatic admiration, bewitch                           |                             |                 |
| plush         | 豪华的               | notably luxurious                                               |                             |                 |
| remonstrance  | 抗议，抱怨             |                                                                 |                             |                 |
| gruff         | 粗鲁的，板着脸的；粗哑的      | rough, hoarse                                                   |                             |                 |
| ensconce      | 安置，安坐             | shelter, establish, settle                                      | en + sconce 小堡垒             |                 |
| hoodoo        | 厄运；招来不幸的人         | bring bad luck                                                  |                             |                 |
| egoism        | 利己主义              |                                                                 |                             |                 |
| cloy          | （甜食）生腻，吃腻         | satiate                                                         |                             |                 |
| tusk          | （大象等）长牙           |                                                                 |                             |                 |
| trounce       | 痛击；严惩             | thrash or punish severely                                       |                             |                 |
| winkle        | 挑出，剔出；取出          |                                                                 |                             |                 |
| beacon        | 烽火，灯塔             |                                                                 | beach                       |                 |
| stoop         | 弯腰，俯身；屈尊          |                                                                 |                             |                 |
| accessory     | 附属的，次要的           | additional, supplementary, subsidiary                           |                             |                 |
| differentiate | 辨别，区别             |                                                                 |                             |                 |
| curdle        | 使凝结，变稠            | coagulate, congeal                                              | curd n.                     |                 |
| gaffe         | 失礼，失态             | social or diplomatic blunder                                    |                             |                 |
| wend          | 行，走，前进            | proceed on                                                      |                             | wend one's way  |
| traduce       | 中伤，诽谤             | slander or defame                                               |                             |                 |
| scud          | 疾行，飞奔             |                                                                 |                             |                 |
| gutless       | 没有勇气的；怯懦的         | cowardly, spineless                                             |                             |                 |
| potboiler     | 粗制滥造的文艺作品；锅炉      |                                                                 | potboil v. 为了混饭吃粗制滥造        |                 |
| boding        | 凶兆，前兆；先兆的         | omen especially for coming evil                                 |                             |                 |
| demarcate     | 划分，划界             |                                                                 |                             |                 |
| smudge        | 污迹；污点；弄脏          |                                                                 | s + mud 泥 + age             |                 |
| consecrate    | 奉献；使神圣            | dedicate, sanctify                                              | con + sacre + ate           |                 |
| flux          | 不断的变动；变迁，动荡不安     |                                                                 |                             |                 |
| jeopardy      | 危险                | peril                                                           |                             | in jeopardy     |
| resigned      | 顺从的，听从的           | acquiescent                                                     |                             | be resigned to… |
| 27           | 27                  | 27                                                   | 27                   | 27              |
| harshly      | 严酷的，无情的             | severely, toughly                                    |                      |                 |
| neurosis     | 神经官能症               |                                                      | neu + osis           |                 |
| stranded     | 搁浅的，处于困境的           |                                                      |                      | a stranded ship |
| beholden     | 因受恩惠而心存感激，感谢；欠人情    | grateful, owing                                      |                      |                 |
| obtrude      | 突出，强加               | thrust out, force or impose                          |                      |                 |
| thump        | 重击，锤击               | pound, punch, smack, whack                           |                      |                 |
| perforate    | 打洞                  |                                                      |                      |                 |
| dissertation | 专题论文                | long essay on a particular subject                   |                      |                 |
| niggling     | 琐碎的                 | petty, trival                                        | nig 小                |                 |
| surge        | 波涛汹涌；激增             |                                                      |                      |                 |
| fruition     | 实现，完成               | fulfillment of hopes, plans                          | fruit n.             |                 |
| fabulous     | 难以置信的；寓言的           | incredible, astounding, imaginary, fictitious        |                      |                 |
| gadget       | 小工具，小机械             | small mechanical contrivance or device               |                      |                 |
| archetype    | 原型；典型               | prototype, original pattern                          |                      |                 |
| pulpit       | 讲道坛                 |                                                      |                      |                 |
| pertain      | 属于；关于               | belong, reference                                    |                      |                 |
| liability    | 责任，义务               | the state of being liable, obligation, debt          |                      |                 |
| bungle       | 笨拙的做                | botch, bumble, fumble, muff, stumble                 |                      |                 |
| drivel       | 胡说，糊涂话              | nonsense                                             |                      |                 |
| sardonic     | 讽刺的；嘲笑的             | disdainfully sneering, ironic, sarcastic             |                      |                 |
| jot          | 草草记下                |                                                      | lot 多                |                 |
| complicity   | 合谋，串通               | participation, involvement in a crime                |                      |                 |
| snare        | 圈套，陷阱               | trap, gin                                            | ensnare 使进入圈套        |                 |
| languor      | 无精打采；衰弱无力           | lack of vigor or vitality, weakness                  | lang 松弛 + uor        |                 |
| mendacity    | 不诚实                 | untruthfulness                                       |                      |                 |
| mushy        | 糊状的；感伤的，多情的         | consistency of mush, excessively tender or emotional |                      |                 |
| depredation  | 劫掠；蹂躏               | plundering                                           |                      |                 |
| onslaught    | 猛攻，猛袭               | fierce attack                                        | on + slaught         |                 |
| caprice      | 奇思怪想；反复无常；任性        | sudden change                                        |                      |                 |
| jerk         | 猛拉                  |                                                      |                      | jerk off 结结巴巴的说 |
| punch        | 以拳猛击；打孔             | strike with the fist, make a hole, pierce            |                      |                 |
| dyspeptic    | 消化不良的               | indigestible, morose, grouchy                        |                      |                 |
| agnostic     | 不可知论的；不可知论者         |                                                      |                      |                 |
| bogus        | 伪造的；假的              | not genuine, spurious                                |                      |                 |
| plait        | 发辫；编成辫              | pigtail, braid                                       |                      |                 |
| skullduggery | 欺骗；使诈               | underhanded or unscrupulous behavior                 |                      |                 |
| brusque      | 唐突的；鲁莽的             | blunt, rough and abrupt                              |                      |                 |
| piddle       | 鬼混；浪费               | diddle, dawdle, putter                               |                      |                 |
| lunatic      | 疯子；极蠢的              | insane person, utterly foolish                       |                      |                 |
| gracious     | 大方的；和善的；奢华的；优美的；雅致的 | polite, generous, luxurious                          |                      |                 |
| rollicking   | 欢乐的，喧闹的             | noisy and jolly                                      |                      |                 |
| ecstatic     | 狂喜的，心花怒放的           | enraptured                                           | ecstasy              |                 |
| 28            | 28                  | 28                                                                       | 28                   | 28                         |
| trinket       | 小装饰品；不值钱的珠宝；琐事      | trivial                                                                  |                      |                            |
| encompass     | 包围，围绕               | enclose, envelop                                                         |                      |                            |
| ribald        | 下流的，粗俗的             | crude                                                                    |                      |                            |
| bristling     | 竖立的                 | stiffly erect                                                            |                      |                            |
| scamp         | 草率的做；流氓；顽皮的家伙       | rogue, rescal                                                            |                      |                            |
| slit          | 撕裂；裂缝               | sever, long narrow or opening                                            |                      |                            |
| pavid         | 害怕的；胆小的             | timid                                                                    |                      |                            |
| disheveled    | 使蓬乱；使（头发）凌乱         |                                                                          | dishevel v.          |                            |
| grisly        | 恐怖的，可怕的             | frightening, ghastly, horrible                                           |                      |                            |
| crib          | 抄袭，剽窃               | plagiarize                                                               | crab 发牢骚             |                            |
| ornery        | 顽固的，爱争吵的            | irritable disposition, cantankerous                                      |                      |                            |
| abhorrent     | 可恨的，讨厌的             | obscene, repugnant, repulsive, hateful, detestable                       |                      |                            |
| daredevil     | 大胆的，冒失的（人）          | bold and reckless                                                        |                      |                            |
| formative     | 形成的；影响发展的           |                                                                          | form                 |                            |
| pilfer        | 偷窃                  | filch, pinch                                                             |                      |                            |
| oversee       | 监督                  | supervise                                                                | over + see           |                            |
| virtual       | 实质上的，实际上的           | in essence                                                               |                      |                            |
| tempestuous   | 狂暴的                 | turbulent, stormy                                                        |                      | a tempestuous relationship |
| strife        | 纷争，冲突               | bitter conflict or dissension, fight, struggle                           |                      |                            |
| funk          | 怯懦；恐惧；懦夫            | paralyzing fear                                                          |                      |                            |
| atrocious     | 残忍的；凶恶的             | cruel, brute, outrageous                                                 |                      |                            |
| residue       | 剩余                  | remainder, left behind                                                   |                      |                            |
| correspondent | 符合的；一致的；记者          | agreeing, matching                                                       |                      |                            |
| despise       | 鄙视；轻视               | look down with contempt or aversion, contemn, disdain, scorn, scout      |                      |                            |
| augury        | 占卜术；预兆              |                                                                          | augur 占卜 v.          |                            |
| shamble       | 蹒跚而行；踉跄的走           | walk awkwardly with dragging feet, shuffle                               |                      |                            |
| plangent      | 轰鸣的，凄凉的；剽窃          | plaintive quality                                                        |                      |                            |
| sheen         | 光辉；光泽               | bright or shining condition                                              |                      |                            |
| muniments     | 契据                  |                                                                          |                      |                            |
| loutish       | 粗鲁的                 | rough and rude                                                           |                      |                            |
| preside       | 担任主席；负责；指挥          |                                                                          |                      |                            |
| steer         | 掌舵；驾驶；公牛；食用牛        | control the course                                                       |                      |                            |
| rampage       | 乱冲乱跑；狂暴行为           |                                                                          |                      |                            |
| offbeat       | 不规则的；不平常的           | unconventional                                                           |                      |                            |
| bid           | 命令；出价，投标            | command                                                                  |                      |                            |
| slue          | （使）旋转               | rotate, slew                                                             |                      |                            |
| analgesia     | 无痛觉；痛觉丧失            | insensibility to pain without loss of consciousness                      |                      |                            |
| sleight       | 巧妙手法；诡计，灵巧          |                                                                          |                      |                            |
| supine        | 仰卧的；懒散的             | prone, inactive, lying, slack                                            |                      |                            |
| badger        | 獾；烦扰，纠缠不安           | torment, nag, beleaguer, bug, pester, tease                              |                      |                            |
| smirch        | 弄脏；污点               | blemish, soil, sully, tarnish                                            |                      |                            |
| glow          | 发光，发热；(脸）发红；发光，兴高采烈 | blush, flush, incandescence                                              |                      |                            |
| languish      | 衰弱，憔悴               | lose vigor or vitality                                                   | lang + uish          |                            |
| wobble        | 摇晃，摇摆；犹豫            | hesitate, move with a staggering motion, falter, stumble, teeter, tooter |                      |                            |
| digress       | 离题                  |                                                                          |                      |                            |
| imbroglio     | 纠纷；纠葛；纠缠不清          | confusion                                                                |                      |                            |
| providential  | 幸运的；适时的             | fortunate, opportune                                                     |                      |                            |
| sartorial     | 裁缝的，缝制的             |                                                                          | tailor 裁缝 n.         |                            |
