---
layout: post
title:  "Exercise of Data Structure"
date:   2022-08-18 13:52:02 +0800
category: CoursesPosts
---

# 1. 绪论

# 1.1

- “抽象数据类型”描述了数据的逻辑结构和抽象运算，通常用数据对象、数据关系、基本操作集表示，从而构成一个完整的数据结构定义。
- 顺序表、哈希表、单链表是三种不同的数据结构，即描述逻辑结构，又描述存储结构、数据运算。属于完整的数据结构。而有序表，仅表示逻辑结构，可以链式存储、顺序存储等。
- 循环队列是用顺序表示的队列，是一种数据结构。而栈仅表示逻辑结构。
- 存储数据时，不仅要存储数据元素的值，还要存储数据元素之间的关系。
- 链式存储设计时，各个不同结点的存储空间可以不连续，但是节点内部的存储单元地址必须连续。
- 对于两种不同的数据结构，逻辑结构与物理结构一定不相同吗？
    - 错误。数据结构还包括数据的运算。
    - 比如二叉树和二叉排序树，具有相同的逻辑结构与存储结构。但是在查找运算时，二叉树的时间复杂度为$O(N)$，而二叉排序树为$O(\log_2N)$。

# 1.2

- 算法是对特定问题求解步骤的一种描述，他是指令的有限序列，其中每条指令表示一个或者多个操作。程序$\neq$算法。五大重要特征为必要条件而非充分条件。
- 在相同规模的$n$下，复杂度为$O(n)$的算法在时间上总是优于复杂度为$O(2^n)$的算法。
- 所谓时间复杂度，是指再最坏条件下估算算法执行时间的一个上界。
- 同一个算法，实现语言的级别越高，执行效率越低。
- 已知两个长度分别为$m$，$n$的升序链表，若将其合并为长度为$m+n$的降序链表。最坏时间复杂度为：$O(\max(m, n))$。
- 该算法的时间复杂度为$O(\frac{1}{6}\cdot N^3) = O(N^3)$
```C++
for(i = 1; i <= n; i++) {
    for(j = 1; j <= i; j++) {
        for(k = 1; k <= j; k++) {
            x++;
        }
    }
}
```
- 该算法的时间复杂度为$O(N)$
```C++
int fact(int n) {
    if(n <= 1) return 1;
    return n * fact(n - 1);
}
```
- 斐波那契递归的时间复杂度为$O(N^2)$
```C++
int Fibonacci(int n){
	if(n==1 || n==2)
		return 1;
	else
		return Fibonacci(n - 1) + Fibonacci(n - 2);
} 
```
- 斐波那契非递归的时间复杂度为$O(N)$
```C++
int Fibonacci(int n){
	if(n <= 2){
		return 1;
	}else{
		int num1 = 1;
		int num2 = 1;
		int i;
		for(i = 2; i < n; i++){
            num1 = num2;
			num2 = num1 + num2;
		}
		return num2;
	}
}
```

# 2. 线性表

# 2.2 顺序表

- 严格来说，交换线性表中两个元素的值，顺序表的效率高于链表。
- 已知升序序列中第$[L/2]$个元素为中位数，试求两个等长升序序列$A, B$的中位数。时间复杂度$O(\log_2N)$，空间复杂度$O(1)$。

```C++
int M_Search(int A[], int B[], int n) {
    int s1 = 0, d1 = n-1, m1;
    int s2 = 0, d2 = n-1, m2;
    while(s1 != d1 || s2 != d2) {
        m1 = (s1 + d1)/2;
        m2 = (s2 + d2)/2;
        if(A[m1] == B[m2]) {
            return A[m1];
        }
        else if(A[m1] > B[m2]) {
            if((s1 + d1) % 2 == 0) {
                d1 = m1;
                s2 = m2;
            }
            else {
                d1 = m1;
                s2 = m2 + 1;
            }
        }
        else {
            if((s1 + d1) % 2 == 0) {
                d2 = m2;
                s1 = m1;
            }
            else {
                d2 = m2;
                s1 = m1 + 1;
            }
        }
    }
    return A[s1] < B[s2] ? A[s1] : B[s2];
}
```

- 寻找主元素。时间复杂度为$O(N)$，空间复杂度为$O(1)$。

```C++
int Majority(int A[], int n) {
    int M = A[0], count = 0;
    for(int i = 0; i < n; i++) {
        if(M == A[i]) {
            count++;
        }
        else {
            if(count > 0) {
                count--;
            }
            else {
                M = A[i];
                count = 1;
            }
        }
    }
    count = 0;
    for(int i = 0; i < n; i++) {
        if(M == A[i]) {
            count++;
        }
    }
    return count > n/2 ? M : -1;
}
```

- 定义三元组$(a, b, c)$，$D = \lvert a - b\rvert + \lvert a - c\rvert +  \lvert b - c\rvert$。给定三个非空升序整数数组，求$D$，最小的三元组（一个数组贡献一个数）。时间复杂度为$O(N)$，空间复杂度为$O(1)$。
- 分析：$D = 2*\max(\lvert a - b\rvert , \lvert a - c\rvert , \lvert b - c\rvert) = 2*[\max(a, b, c) - \min(a, b, c)]$。于是问题转变为寻找范围的两端，中间的数相对不重要。

```C++
#define INT_MAX 0x7fffffff

int abs(int a) {
    return a < 0 ? (-a) : a;
}

int find_min(int a, int b, int c) {
    if(a <= b && a <= c) return 1;
    else if(b <= a && b <= c) return 2;
    else return 3;
}

int find_min_D(int A[], int B[], int C[], int n1, int n2, int n3) {
    int i1 = 0, i2 = 0, i3 = 0, D_min = INT_MAX, D = INT_MAX;
    while(i1 < n1 && i2 < n2 && i3 < n3 && D_min > 0) {
        D = abs(A[i1] - B[i2]) + abs(A[i1] - C[i3]) + abs(B[i2] - C[i3]);
        if(D < D_min) {
            D_min = D;
        }
        int idx = find_min(A[i1], B[i2], C[i3]);
        if(idx == 1) {
            i1++;
        }
        else if(idx == 2) {
            i2++;
        }
        else {
            i3++;
        }
    }
    return D_min;
}
```

# 2.3 链式表

- 链式存储使用指针表示逻辑结构，而指针的设置是任意的，故可以很方便的表示各种逻辑结构。而顺序表只能使用物理上的邻接关系表示逻辑结构。
- 顺序存储结构和链式存储结构都可以进行顺序存取。
- 顺序存储方式不仅适用于线性表，还可以应用在图和树结构中。
- 队列需要在表尾插入，表头删除，故适合用带**尾指针**的**循环**单链表表示。
- 给定有$n$个元素的一维数组，建立一个有序单链表的最低时间复杂度为：$O(N\log_2N)$。
- 将长度为$n$的单链表附加到长度为$m$的单链表之后，其算法的时间复杂度为：$O(m)$。
- 单链表增加的头结点，**不是首结点**，而是**指向**首结点。其作用是方便运算的实现。
- 带头结点的双循环链表为空的条件为：`L -> prior == L && L -> next == L`

| 时间复杂度       |  删除第一个结点 | 删除最后一个结点 | 在头结点前插入结点 | 在尾结点后插入结点 |
|:-----------:|:--------:|:--------:|:---------:|:---------:|
| 有尾无头的循环单链表  | $O(1)$   | $O(N)$   | $O(1)$    | $O(1)$    |
| 有尾无头的非循环双链表 | $O(N)$   | $O(1)$   | $O(N)$    | $O(1)$    |
| 有头无尾的循环双链表  | $O(1)$   | $O(1)$   | $O(1)$    | $O(1)$    |
| 有头有尾的循环单链表  | $O(1)$   | $O(N)$   | $O(1)$    | $O(1)$    |

- 某带头结点的循环单链表，若`head -> next -> next = head`成立，则该线性表的长度可能是：**0或者1**（0的时候就意味着`head -> next = head`，故可以无限套娃）。
- 头指针$h$指向一个带头结点的非空循环单链表，$p$是尾指针，$q$是临时指针。现要删除该链表的第一个元素，正确的语句为：

```C++
q = h -> next;  // to be deleted
h -> next = q -> next;
if (q == p) {   
    // if the node to be deleted is the last node (there is only one node in the list)
    // we have to guarantee pointer p is not empty
    p = h;
}
free(q);
```

- $L$为带头结点的单链表，实现从尾到头打印输出。

```C++
void R_print(LinkList L) {
    if (L == NULL) return;
    if (L -> next != NULL) {
        R_print(L -> next);
    }
    print(L -> data);
}

void R_Ignore_Head(LinkList L) {
    if (L -> next != NULL) {
        R_print(L -> next);
    }
}
```

- 将带头结点的单链表就地置逆（空间复杂度为$O(1)$）。

```C++
LinkList Reverse(LinkList L) {
    LNode *p, *r;
    p = L -> next;
    L -> next = NULL;
    while(p != NULL) {
        r = p -> next;          // store the p -> next
        p -> next = L -> next;  // cut p and r
        L -> next = p;          // p is the new first node
        p = r;                  // restore the original p -> next, move to the next node
    }
    return L;
}
```

- 给定两个单链表，找出他们的公共结点。
- 分析：因为是单链表，所以一旦出现公共结点，则所有后续结点一定一样，即只会有Y形，而绝不会有X形。因为公共部分长度$\leqslant$短的链表的长度。所以可以先将长链表的多出的部分遍历掉，然后和短链表依次比较，直到出现相同结点。

```C++
int Length(LinkList L) {
    int len = 0;
    while (L -> next != NULL) {
        len++;
        L = L -> next;
    }
    return len;
}

LinkList Search_Common(LinkList L1, LinkList L2) {
    int delta, len_1 = Length(L1), len_2 = Length(L2);
    LinkList longList = NULL, shortList = NULL;
    if (len_1 > len_2) {
        delta = len_1 - len_2;
        longList = L1;
        shortList = L2;
    }
    else {
        delta = len_2 - len_1;
        longList = L2;
        shortList = L1;
    }
    while (delta--) longList = longList -> next;
    while (longList != NULL) {
        if (longList == shortList) {
            return longList;
        }
        else {
            longList = longList -> next;
            shortList = shortList -> next;
        }
    }
    return NULL;
}
```

- 将两个增序排列的单链表合并为一个降序排列的单链表。
- 使用**头插法**即可实现。尾插法合并的单链表仍然是增序排列。
- 两个整数序列$A, B$已经存入两个单链表中，判断$B$是否为$A$的连续子序列。
- 我们不能放过每一个机会，针对A中每一个可能（一共$len(A) - len(B)$个）都要去尝试，时间复杂度为$O((len(A) - len(B))\times len(B) )$。
- 判断带头结点的循环双链表是否对称：从头结点开始，一个向左指，一个向右指，若它们最终指到同一个结点，则为对称。
- 判断单链表是否有“环”（尾结点指向中间的某结点）。设置`fast`与`slow`两个指针，若有环，则两个指针肯定会碰上。碰上后，`slow`和`head`作为相遇点与起点，一次各走一步，必然能在环起点相遇。

$$
Assume\quad

\begin{aligned}

a &: \text{the distance from ListStart to LoopStart}\\
r &: \text{the length of loop}\\
n &: \text{the number of loops `fast` pointer has gone through}\\
x &: \text{the distance from LoopStart to MeetPoint}

\end{aligned}

\\


\begin{aligned}

\Rightarrow&\quad 2\times(a + x) = a + n\times r + x\\
\Rightarrow&\quad a =  n\times r - x\\
\Rightarrow&\quad \text{`slow` and `head` will surly meet at LoopStart}

\end{aligned}

$$

- 而且由于`slow`指针移动的次数必然小于整个单链表的长度的两倍（一次相遇，一次确定环起点），故时间复杂度为$O(N)$，空间复杂度为$O(1)$。

```C++
LNode* FindLoopStart(LNode* head) {
    LNode* fast = head, slow = head;
    while (fast && fast -> next) {
        slow = slow -> next;
        fast = fast -> next -> next;
        if (slow == fast) break;
    }
    if (slow == NULL || fast -> next == NULL) {
        return NULL;
    }
    LNode* p1 = head, p2 = slow;
    while (p1 != p2) {
        p1 = p1 -> next;
        p2 = p2 -> next;
    }
    return p1;
}
```

- 寻找头指针为`list`的单链表的倒数第$k$个结点。
- 设置两个指针`p, q`，起初都指向`list -> next`，`p`不同，`q`移动，当`q`移动到第$k$个结点时，`p, q`同时移动，当`q`移动到尾结点时，`p`指向倒数第$k$个。
- 假设线性表$L= (a_1, a_2, \cdots, a_n)$采用带头结点的单链表保存。设计空间复杂度为$O(1)$的算法将线性表重拍列为$L= (a_1, a_{n}, a_2, a_{n-1}, \cdots)$。
- 分三步：
    - 设置`p, q`两个链表指针，`p`每次走一步，`q`走两步，`q`走完后`p`指向中间点。
    - 将中间点后的原地置逆。
    - 将前半部分与后半部分重排。

```C++
void ChangeList(NODE* h) {
    NODE *p = h, *q = h, *r;
    while (q -> next != NULL) {
        q = q -> next;
        if (q -> next != NULL) {
            q = q -> next;
        }
        p = p -> next;
    }
    q = p -> next;
    p -> next = NULL;
    NODE *mid = p;      // pointer p is not dummy head
    while (q != NULL) {
        r = q -> next;
        q -> next = p;
        p = q;
        q = r;
    }
    NODE *s = h -> next;
    while (s != mid) {
        r = s ->next;
        s -> next = p;
        Ss = r;
        if (p == mid) {
            break;
        }
        r = p -> next;
        p -> next = s;
        p = r;
    }
}
```

- 寻找整型顺序表（长度为$n$）中所有两两之和等于$X$的整数对。
- 首先排序，然后分别从数组两端开始寻找。若`A[i] + A[j] < X`则`i++`，若大于则`j--`，直到`i >= j`。

# 3

# 3.1

- 链栈通常不会出现栈满的情况。
- 无头结点的链栈进行出栈操作，并把出栈**元素**存在`x`中：`x = top -> data; top = top -> next`。若出栈的是**结点**：`x = top; top = top -> next`。
- 共享栈的好处：
    - 节省存储空间，存取其实没有区别。
    - 因为栈底在两端，所以下溢的概率和普通栈没有改变。但是由于左栈的上溢某种程度上说以右栈的下溢为先决条件，所以可以认为共享栈降低了上溢的可能。
- 采用非递归方式重写递归程序时**并非**必须要使用栈，比如斐波那契数列的非递归算法使用一个循环实现。
- 在函数调用时，系统采用**栈**保存传递参数，函数指针等必要信息。
- 设有一个顺序共享栈`Share[0:n-1]`，其中`top_1 = -1`，`top_2 = n`。则栈满的条件是：`top_1 == top_2 - 1`。注意两个指针不可能相等，除非在栈满的情况下又有新元素入栈，吃掉了另一个栈的栈顶元素。
- FIFO
    - 最早进，最早删。
    - 最晚进，最晚删。
    - 删对头，插队尾。
- A[`0...n`]一共有$n + 1$个元素。
- 带队首指针和队尾指针的**非循环**单链表比带队首指针和队尾指针的**循环**单链表更加适合作为链队，因为两者都可能存在新建结点，但是前者不需要维持循环状态，而循环状态的维持与否于队列本身的操作没有关系，可以去掉。
- 在使用单链表实现队列时，队头设置在链头的位置。
- 循环单链表长度为$n$，队头固定在链表尾，若仅设有头指针，则进队操作时间复杂度为：$O(N)$。

# 3.3

- 栈的应用：
    - 递归（非递归算法通常效率更高一点）
    - 迷宫求解
    - 进制转换
    - 函数调用
    - 中缀表达式转后缀表达式
    - 深度优先搜索
- 队列的应用：
    - 缓冲区
    - 广度优先搜索
    - 页面替换算法（总是选择在主存中停留时间最长的一页置换，即先进入内存的页，先退出内存）
- 中缀表达式转后缀表达式：
    - 若为操作数，直接进入后缀表达式。
    - 若遇到`(`，入栈。
    - 若遇到`)`，依次把栈中的运算符弹出到后缀表达式，并删除栈中的`(`。
    - 若遇到`+-*/`
        - 栈顶为`(`，入栈。
        - 栈顶为优先级更低的操作符，入栈（遇到`*/`时栈顶为`+-`）。
        - 依次弹出优先级$\geqslant$当前运算符的运算符，直到遇到了优先级更低的运算符或者`(`，之后再入栈该运算符。
- 中缀表达式转前缀or后缀的手动算法：
    - $a/b+(c*d-e*f)/g\quad \Rightarrow\quad ((a/b)+(((c*d)-(e*f))/g))$
    - 转前缀：把操作符移到相应的括号前面。
        - $+(/(ab)/(-(*(cd)*(ef))g))\quad \Rightarrow\quad +/ab/-*cd*efg$
    - 转后缀：把操作符移到相应的括号后面。
        - $((ab)/(((cd)*(ef)*)-g)/)+\quad \Rightarrow\quad ab/cd*ef*-g/+$
- 利用栈实现以下递归函数。

$$
P_n(x) = \left\{   

\begin{aligned}

&1,\quad\quad\quad\quad\quad  &n=0\\
&2x,\quad\quad\quad\quad \quad &n=1\\
&2x\cdot P_{n-1}(x) - 2(n-1)\cdot P_{n-2}(x)\quad\quad &n > 1

\end{aligned}

\right.
$$

```C++
struct stack {
    int num;
    double val;
};

double P(int n, double x) {
    if (n == 1) return 2 * x;
    if (n == 0) return 1;
    struct stack st[MaxSize];
    int top = -1;
    double fv1 = 1, fv2 = 2 * x;
    for (int i = n; i >= 2; i--) {
        top++;
        st[top].num = i;
    }
    while (top >= 0) {
        st[top].val = 2 * x * fv2 + 2 * (st[top].num - 1) * fv1;
        fv1 = fv2;
        fv2 = st[top].val;
        top--;
    }
    return fv2;
}
```

# 3.4

- 将$n\times n$的对称矩阵`A`的下三角部分按行存放在一维数组`B`中，`A[0][0]`存放在`B[0]`中，`A[i][i]`存放在`B[k]`中。

$$
k = \displaystyle\frac{(1 + i + 1)(i + 1)}{2} - 1 = \frac{(i+2)(i+1) - 2}{2} = \frac{i(i+3)}{2}
$$

- 有一个100阶的三对角矩阵`M`，其元素$m_{i, j},1\leqslant i, j\leqslant 100$按行优先存放在下标从0开始的数组中，则$m_{30, 30}$存放下标为：

$$
k = \displaystyle (29-1+1)\times 3 - 1 + 1 = 87
$$

# 4

# 4.2 KMP

- 为什么要取`max{k}`，`k`的最大值是多少？
    - 当主串的第`i`个字符与模式串的第`j`个字符比较不相等时，主串`i`不回溯，则假定模式串中的第`k`个字符与主串的第`i`个字符比较。为了不使右移丢失可能的匹配，右移距离应最小，由于`j - k`表示右移距离，故要取`max{k}`。
    - 最大的可能值为`j - 1`，而且`j -1 > 1`。

# 5

# 5.1

- 一棵度为$m$的树有$n$个结点，则高度至多为：$n - m + 1$。
- 树的路径长度 = 从树根到每个结点的路径长度的**总和**。
- 度为$n$的树至少存在$n + 1$个结点，即至少两层。

# 5.2

- 结点按照完全二叉树层序编号的二叉树中，第`i`个结点的左孩子编号为`2i`。**不一定对**，因为不确定其左孩子**是否存在**。
- 设二叉树存在$2n$个结点，而且$m < n$，则不可能存在：
    - $n$个度为2的结点。
        - $2n \geqslant n_2 + n_0 = n_2 + n_2 + 1 $
        - $\Rightarrow \quad n_2 < n$
    - $2m$个度为1的结点。
        - $2n = n_2 + n_1 + n_0 = n_2 + n_2 + 1 + n_1 $
        - $\Rightarrow \quad (1 + n_1 )\% 2 = 0$
        - $\Rightarrow \quad n_1\% 2 = 1$
- 一棵有$n$个结点的二叉树，采用二叉链存储结构，则空指针的个数为：

$$
P_{empty} = 2n - n_1 - 2n_2 = 2n - n_1 - n_2 - (n_0 - 1) = n + 1
$$

- 一棵有$124$个叶子结点的完全二叉树，最多有多少个结点：

$$
N = 2^7 - 1 + 120 + 1 = 248
$$

- **第`6`层的4个叶子结点中的最左边的，可以有一个左孩子，不会增加叶子结点的总个数。于是第`6`层存在`3`个叶子结点，第`7`层有`121`个。**
- 在默认情况下，二叉树中没有右孩子的结点，视为**度为1和度为0**的结点。
- 一个完全二叉树的第6层（根在第1层）有8个叶子结点，则树的总结点最多为：

$$
N = 2^6 - 1 + 2\times(2^5 - 8) = 64 - 1 + 2\times 24 = 63 + 48 = 111
$$

- 第6层可以只有8个结点（此时对应最少，即39个结点），也可以**满节点，但是有8个结点在第7层没有孩子**。

# 5.3

- 若有一个**叶子结点**是二叉树的中序遍历结果的最后一个结点。
    - 则该结点一定是整个树的右子树中**最右的**右孩子叶子结点。
    - 故该结点也一定是该树前序遍历的最后一个结点。
- 在二叉树中有两个结点`m, n`，其中`m`是`n`的祖先，若想找到`m => n`的路径，需要使用**后序遍历**。
    - 后序遍历退回时访问根结点，就可以从下向上把`n => m`的路径上的结点进行输出（递归算法），若采用非递归的算法，则搜索到`n`后在栈中把`n => m`的路径进行记录。
- 同一个二叉树的前序、中序、后序遍历的序列中，所有叶子结点的先后顺序**完全相同**。
    - 因为三种遍历的算法都是先左子树后右子树。
- 层次遍历和中序遍历可以唯一确定一棵树。
    - 前序和后序**不能**唯一确定一棵树。
    - 前+中、中+后都可以唯一确定一棵树。
    - 层次+前、层次+后**都不能**唯一确定一棵树。
- 非空二叉树的前序遍历和后序遍历刚好相反，
    - 则`Root-Left-Right`与`Left-Right-Root`正好相反，
    - 则对于该树的所有有孩子的结点，均不能同时具有左孩子、右孩子，
    - 树中的所有非叶子结点度均为1，也即整个二叉树只有根或者**只有一个叶子结点**，也即**高度等于结点数**。
- 二叉树属于**逻辑结构**。
    - 但是线索二叉树是加上线索的链表结构（优化的二叉链表），属于**存储结构**，也即**物理结构**。
- `n`个结点的二叉线索树还有的线索个数为：**`n + 1`**，其中**至多**`2`个`NULL`线索。
- **后序线索树**的遍历仍然需要栈的支持。
    - 后续搜索最后访问根结点，若从右孩子访问根结点时，其右子树不一定为空，故可能无法通过指针遍历整棵树。
    - 因此后序遍历仍然需要**栈**的支持。
- 二叉树线索化后，仍然不能解决**后续线索二叉树的后继求解问题**。
    - 后序搜索树查找结点的后继往往比较复杂
        - 根结点：没有后继。
        - 右孩子：则后继为双亲。
        - 左孩子，且双亲没有右孩子：后继为双亲。
        - 左孩子，且双亲有右孩子：后继为右孩子的后序遍历第一个点。
- 后序遍历二叉树的非递归算法。
    - A：沿着根的左孩子一路入栈，直到为空。
    - 若栈顶元素的右孩子非空且未被访问，则对于右孩子执行A，否则出栈，访问。

```C++
void PostOrder(BiTree T) {
    InitStack(S);
    BiTree p = T, r = NULL;
    while (p || !IsEmpty(S)) {
        if (p) {
            Push(S, p);
            p = p -> lchild;
        }
        else {
            GetTop(S, p);
            if (p -> rchlid && p -> rchlid != r) {
                // if p has right child and this child has not been visited
                p = p -> rchild;
            }
            else {
                Pop(S, p);
                visit(p);
                r = p;      // the last visited node
                p = NULL;   // set p NULL to avoid being pushed again
            }
        }
    }
}
```

- 给出二叉树的从下至上、从右至左的遍历算法。
    - 传统层次遍历 + 栈输出。
- 使用非递归算法，计算二叉链表存储的二叉树的高度。
    - 以层次遍历为基础。
    - 设置`last`为当前层的最右结点，若与访问结点相同，层数++，更新`last`为当前队列的最后结点。
    - 求某层的结点个数，每层的结点个数，树的最大宽度等都可应用此思想。

```C++
int Depth(BiTree T) {
    if (T == NULL) return 0;
    int front = -1, rear = -1;  // queue
    int last = 0, level = 0;    // last points to the rightest node of one layer
    BiTree Q[MaxSize];          // queue
    Q[++rear] = T;
    BiTree p;
    while (front < rear) {
        p = Q[++front];
        if (p -> lchild) {
            Q[++rear] = p -> lchild;
        }
        if (p -> rchild) {
            Q[++rear] = p -> rchild;
        }
        if (front == last) {
            last = rear;        // last point to next level
            level++;
        }
    }
    return level;
}
```

- 递归算法

```C++
int Depth(BiTree T) {
    if (T == NULL) return 0;
    int h1 = Depth(T -> lchild);
    int h2 = Depth(T -> rchild);
    return (h1 > h2) ? h1 + 1 : h2 + 1; 
}
```

- 判断二叉链表存储的二叉树是否为完全二叉树。
    - 采用层次遍历思想，将所有结点加入队列（包括空）。
    - 通过空结点后是否还有非空结点进行判断。

```C++
bool IsComplete(BiTree T) {
    if (T == NULL) return 1;
    InitQueue(Q);
    EnQueue(Q, T);
    BiTree p;
    while (!IsEmpty(Q)) {
        DeQueue(Q, p);
        if (p) {
            EnQueue(Q, p -> lchild);
            EnQueue(Q, p -> rchild);
        }
        else {
            while (!IsEmpty(Q)) {
                DeQueue(Q, p);
                if (p) return 0;
            }
        }
    }
    return 1;
}
```

- 计算二叉树中的双分支结点个数。
    - 递归实现比较方便。
        - `F(p) = 0, if p is NULL`
        - `F(p) = F(p->l) + F(p->r) + 1, if b has both children`
        - `F(p) = F(p->l) + F(p->r), if b has only one child`

- 将二叉树的所有左右子树交换。

```C++
void Swap(BiTree b) {
    if (b) {
        Swap(b -> lchild);
        Swap(b -> rchild);
        BiTree temp = b -> lchild;
        b -> lchild = b -> rchild;
        b -> rchild = temp;
    }
}
```

- 删去以二叉链表存储的二叉树中所有元素值为$x$的结点以及以其为根的子树，并释放空间。

```C++
void DeleteTree(BiTree &bt) {
    if (bt) {
        DeleteTree(bt -> lchild);
        DeleteTree(bt -> rchild);
        free(bt);
    }
}

void Search(BiTree bt, ElemType x) {
    if (!bt) return;
    if (bt -> data == x) {
        DeleteTree(pt);
        return;
    }
    BiTree Q[];
    InitQueue(Q);
    EnQueue(Q, bt);
    while (!IsEmpty(Q)) {
        DeQueue(Q, p);
        if (p -> lchild) {
            if (p -> lchild -> data == x) {
                DeleteTree(p -> lchild);
                p -> lchlid = NULL;
            }
            else {
                EnQueue(p -> lchild);
            }
        }
        if (p -> rchild) {
            if (p -> rchild -> data == x) {
                DeleteTree(p -> rchild);
                p -> rchild = NULL;
            }
            else {
                EnQueue(p -> rchild);
            }
        }
    }
}
```

- 查找二叉树中值为$x$的结点（不多于1个），并把其所有的祖先打印输出。
    - 采用非递归的后序遍历，当访问到值为$x$的结点后，栈里面的结点均为其祖先，依次输出。

```C++
typedef struct {
    BiTree t;
    int tag;        // 0 means the left child is visited, 1 for the right
} stack;

void Search(BiTree bt, ElemType x) {
    stack s[];
    top = 0;
    while (bt != NULL || top > 0) {
        whlie (bt != NULL && bt -> data != x) {
            s[++top].t = bt;
            s[top].tag = 0;
            bt = bt -> lchild;
        }
        if (bt != NULL && bt -> data == x) {
            for (int i = 0; i <= top; i++) {
                printf("%d ", s[i].t -> data);
            }
            return;
        }
        while (top != 0 && s[top].tag == 1) {
            top--;
        }
        if (top != 0) {
            s[top].tag = 1;
            bt = s[top].t -> rchild;
        }
    }
}
```

- 设一棵二叉树的结点结构为`[LLINK, INFO, RLINK]`，`ROOT`为根指针。`p, q`为指向树种任意两个结点的指针，试编写`ANCESTOR(ROOT, p, q, r)`寻找`p, q`的最近公共祖先结点`r`。

```C++
typedef struct {
    BiTree t;
    int tag;        // 0 means the left child is visited, 1 for the right
} stack;

stack s[], s1[];

BiTree ANCESTOR(BiTree ROOT, BiTree p, BiTree q, BiTree &r) {
    int top = 0, top1 = 0;
    BiTree bt = ROOT;
    while (bt != NULL || top > 0) {
        while (bt != NULL) {
            s[++top].t = bt;
            s[top].tag = 0;
            bt = bt -> LINK;
        }
        while (top != 0 && s[top].tag == 1) {
            if (s[top].t == p) {
                for (int i = 1; i <= top; i++) {
                    s1[i] = s[i];
                    top1 = top;
                }
            }
            if (s[top].t == q) {
                for (int i = top; i > 0; i--) {
                    for (int j = top1; j > 0; j--) {
                        if (s[i].t == s1[j].t) {
                            return s[i].t;
                        }
                    }
                }
            }
            top--;
        }
        if (top != 0) {
            s[top].tag = 1;
            bt = s[top].t -> RLINK;
        }
    }
    return NULL;
}
```

- 二叉树采用二叉链表存储结构，设计一个算法，求非空二叉树`b`的宽度。
    - 采用层次遍历求出所有结点的层次。
    - 之后通过扫描队列求出每一层的结点个数，最大值几位二叉树的宽度。

```C++
typedef struct {
    BiTree t;
    int level;
    int front, rear;
} Queue;

Queue Q[MaxSize];

void LevelOrder() {
    while (!IsEmpty(Q)) {
        DeQueue(Q, p);
        int level = p.level;        // not corret!!! just for illustrating!!!
        visit(p);
        if (p -> lchild != NULL) {
            p -> lchild.level = level + 1;
            EnQueue(Q, p -> lchild);
        }
        if (p -> rchild != NULL) {
            p -> rchild.level = level + 1;
            EnQueue(Q, p -> rchild);
        }
    }
}

- 满二叉树前序转后序。

```C++
void PreToPost(ElemType pre[], int l1, int h1, ElemType post[], int l2, int h2) {
    if (h1 >= l1) {
        post[h2] = pre[l1];
        int half = (h1 - l1)/2;
        PreToPost(pre, l1 + 1, l1 + half, post, l2, l2 + half - 1);
        PreToPost(pre, l1 + half + 1, h1, post, l2 + half, h2 - 1);
    }
}
```

- 判断两颗二叉树是否相似。

```C++
int similiar(BiTree T1, BiTree T2) {
    int leftS, rightS;
    if (T1 == NULL && T2 == NULL) {
        return 1;
    }
    else if (T1 == NULL || T2 == NULL) {
        return 0;
    }
    else {
        leftS = similiar(T1 -> lchild, T2 -> lchild);
        rightS = similiar(T1 -> rchild, T2 -> rchild);
        return leftS && rightS;
    }
}
```

- 给定中序搜索二叉树，寻找特定结点的在后序遍历中的前驱。
    - 若`p`有右孩子无左孩子，则右孩子为前驱。
    - 若`p`有左孩子无右孩子，则左孩子为前驱。
    - 若`p`无孩子，假设其中序左线索指向某祖先`f`，意味着`p`是`f`的右子树的中序遍历的第一个结点。
        - 若`f`有左孩子，则其左孩子是`p`的后序遍历的前驱。
        - 若`f`无左孩子，则顺其前驱直到找到有左孩子的结点，则该结点的左孩子为`p`的前驱。
    - 若`p`是中序遍历的第一个，则前驱为`NULL`。

```C++
BiThrTree InPostPre(BiThrTree t,  BiThrTree p) {
    BiThrTree q;
    if (p -> rtag == 0) {
        return p -> rchild;
    }
    else if (p -> ltag == 0) {
        return p -> lchild;
    }
    else if (p -> lchild == NULL) {
        return NULL;
    }
    else {
        while (p -> ltag == 1 && p -> lchild != NULL) {
            p = p -> lchild;
        }
        if (p -> ltag == 0) {
            return p -> lchild;
        }
        else {
            return NULL;
        }
    }
    return NULL;
}
```

- 求二叉树的带权路径长度`WPL`。
    - 基于先序遍历的递归算法。

```C++
int WPL(BiTree root) {
    return wpl_PreOrder(root, 0);
}

int wpl_PreOrder(BiTree root, int depth) {
    static int wpl = 0; // must be static
    if (root -> lchild == NULL && root -> rchild == NULL) {
        wpl += root -> weight * depth;
    }
    else if (root -> lchild != NULL) {
        wpl_PreOrder(root -> lchild, depth + 1);
    }
    else if (root -> rchild != NULL) {
        wpl_PreOrder(root -> rchild, depth + 1);
    }
    return wpl;
}
```

# 5.4

- 有森林`F`，`B`为其转化而来的二叉树，若`F`中存在`n`个非终端结点，则`B`中右指针为空的结点有`n + 1`个。
    - 右指针为空代表其没有右兄弟。
    - 每一个非终端结点在其所有孩子结点转化之后，最后一个孩子的右指针为空。
    - 最右边的树的根结点的右指针为空。
- 有森林`F`，`B`为其转化而来的二叉树。`F`中叶子结点的个数等于`B`中左指针为空的结点个数。
- 森林`F`中有15条边，25个结点，则有**10**棵树。
- 以孩子兄弟表示法存储的森林的叶子结点数。

```C++
typedef struct node {
    ElemType data;
    struct node *fch, *nsib;
} *Tree;

int Leaves(Tree t) {
    if (t == NULL) {
        return 0;
    }
    if (t -> tch == NULL) {
        return 1 + Leaves(t -> nsib);
    }
    else {
        return Leaves(t -> fch) + Leaves(t -> nsib);
    }
}
```

- 以孩子兄弟链表存储的树，递归求树的深度。

```C++
int Height(CSTree bt) {
    int hc, hs;
    if (bt == NULL) {
        return 0;
    }
    else {
        hc = Height(bt -> fch);
        hs = Height(bt -> nsib);
        if (hc + 1 > hs) {
            return hc + 1;
        }
        else {
            return hs;
        }
    }
}
```

- 已知树的层次遍历和每个节点的度，构造此树的孩子兄弟链表。

```C++
#define MaxNodes 99

void create_CSTree(CSTree &T, DataType e[], int degree[], int n) {
    CSNode *pointer = new CSNode[MaxNodes];
    int i, j, d, k = 0;
    for (i = 0; i < n; i++) {
        pointer[i].data = e[i];
        pointer[i] -> fch = pointer[i] -> nsib = NULL;
    }
    for (i = 0; i < n; i++) {
        d = degree[i];
        if (d > 0) {
            k++;
            pointer[i] -> fch = pointer[k];
            for (j = 2;  j <= d; j++) {
                k++;
                pointer[k - 1] -> nsib = pointer[k];
            }
        }
    }
    T = pointer[0];
    delete [] pointer;
}
```

# 5.5

- 哈夫曼树中不存在度为1的结点，故：`n1 + n2 = n2 = n0 - 1`
- 一棵哈夫曼树有215个结点，则能得到$\displaystyle\frac{215 + 1}{2} = 108$种不同的码字。
    - 哈夫曼树中有几个叶子结点就代表着有几个不同的码字，因为哈夫曼编码属于前缀编码。
- 度为`m`的哈夫曼树中，叶子结点个数为$n_0$，则非叶子结点个数为：

$$
\begin{aligned}

N &= n_0 + n_m \\

\Rightarrow\quad m\cdot n_m &= N - 1 \\&= n_m + n_0 - 1 \\

\Rightarrow\quad n_m &= \frac{n_0 - 1}{m - 1}

\end{aligned}
$$

- 并查集的结构是一种双亲表示法存储的树。
- 并查集可用于实现Krusual算法。
- 并查集可用于判断无向图的连通性。
- 并查集在未优化路径的情况下搜索的时间复杂度可以达到$O(N)$。
- 哈夫曼树中通常不存在`0`权值。
- 设存在6个有序表`A, B, C, D, E, F`，分别含有`10, 35, 40, 50, 60, 200`个元素，升序排列。
- 要求通过5次两两合并，合成一个总升序表。
- 合并两个有序表，最坏情况下是比较`m + n - 1`次。
    - `10 + 35 - 1 = 44`
    - `45 + 40 - 1 = 84`
    - `50 + 60 - 1 = 109`
    - `110 + 85 - 1 = 194`
    - `195 + 200 - 1 = 394`
    - `44 + 84 + 109 + 194 + 394 = 825`
- 判断二叉树编码是否具有前缀特性。
    - 依次读入每一个编码`C`，建立/寻找从根开始的路径，若遇到空指针就新建。
        - 若遇到了叶子结点，则不具备前缀特性。
        - 若没有创建任何结点，则不具备前缀特性。
        - 若在最后一个编码创建了新结点，则继续验证下一个编码。
