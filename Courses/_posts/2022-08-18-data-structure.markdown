---
layout: post
title:  "Data Structure"
date:   2022-08-18 13:52:01 +0800
category: CoursesPosts
---

# 1. 绪论

- 数据
- 数据元素
- 数据对象
- 数据类型：原子、结构、抽象数据
- 数据结构

# 1.1

- 数据结构是相互之间存在一种或多种特定关系的数据元素的集合。数据结构包含三方面的内容：逻辑结构、存储结构、数据的运算。一个算法的设计取决于所选定的逻辑结构，而算法的实现则依赖于所采用的存储结构。
- 逻辑结构是指数据元素之间的逻辑关系，即从逻辑关系上描述数据。它与数据的存储无关，是独立于计算机的。
    - 线性结构：线性表、栈、队列
    - 非线性结构：树、图、集合
- 存储结构是指数据结构在计算机中的表示，也称物理结构。它包括顺序存储、链式存储、索引存储、散列存储（Hash）

# 1.2

- 算法的5大重要特征：
    - 有穷性
    - 确定性
    - 可行性
    - 输入
    - 输出
- 一个好的算法需要达到的目标
    - 正确性
    - 可读性
    - 健壮性
    - 效率与低存储要求
- 时间复杂度
$$
T(N) = O(f(N))
$$
- 数学定义为：若$T(N)$与$f(N)$是定义在正整数集合上的两个函数，则存在正常数$C, N_0$，使得当$N > N_0$时，$0 \leqslant T(N) \leqslant C\cdot f(N)$
- 3种子类
    - 最坏
    - 平均
    - 最好
- 加法原则
$$
T(N) = T_1(N) + T_2(N) = O(max(f(N), g(N)))
$$
- 乘法原则
$$
T(N) = T_1(N) \times T_2(N) = O(f(N)\times g(N))
$$
- 阶
$$
1 < \log_2N < N < N\cdot \log_2N < N^2 < N^3 < 2^N < N! < N^N
$$
- 空间复杂度
$$
S(N) = O(g(N))
$$

# 2. 线性表（逻辑结构，表示数据元素之间的一对一的相邻关系）

# 2.1

- 线性表是具有相同数据类型（相同的存储空间）的$n \geqslant 0$个数据元素的有限序列。
- $a_1$为表头元素，$a_n$为表尾元素。
- 除表头元素外，每个元素有且只有一个直接前驱。除表尾元素外，每个元素有且只有一个直接后继。
- 具有逻辑上的顺序性。
- 基本操作：
    - `InitList(&L)`
    - `Length(L)`
    - `LocateElem(L, e)`
    - `GetElem(L, i)`
    - `ListInsert(&L, i, e)`
    - `ListDelete(&L, i, &e)`
    - `PrintList(L)`
    - `Empty(L)`
    - `DestoryList(&L)`

# 2.2

- 顺序表：逻辑上相邻的数据元素在物理位置上也相邻。
- 线性表中的任一数据元素都可以**随机存取**，所以线性表的顺序存储结构（数组）是一种随机存取的结构。
- 线性表的表头元素下表为1，而数组中为0。
- 一维数组
    - 静态分配：数组的大小与空间实现固定，一旦空间占满会产生异常。
    - 动态分配：分配空间的大小可以在运行时动态决定。但是需要指针。而且动态分配在分配过程中需要移动大量元素，效率较低。
- 顺序表最主要的特点是**随机访问**，在$O(1)$时间内找到指定元素。但是插入、删除（都是$O(N)$）比较费时。查找也是$O(N)$。

# 2.3 

- 链表头节点
    - 让链表第一个的操作与其他位置的操作没有区别。
    - 空表和非空表实现了统一。
- 静态链表中的指针是指结点的相对地址（数组下标）。

# 3 栈、队列和数组

# 3.1 栈

- 栈：只允许在**一端**插入和删除的线性表。Last In First Out。
- $n$个不同元素进出栈，出栈的不同排列数为$\displaystyle\frac{1}{n+1}\cdot C_{2n}^n$。
- **基本操作**（无特殊说明可以直接使用）
    - `InitStack(&S)`
    - `StackEmpty(S)`
    - `Push(&S, x)`
    - `Pop(&S, &x)`
    - `GetTop(S, &x)`
    - `DestoryStack(&S)`
- 共享栈：让两个顺序栈共享一个一维数组的空间，将两个栈底分别设置在一维数组的两端。
- 栈的链式存储：出栈入栈都在表头进行。而且通常不会出现栈满的情况。

# 3.2 队列

- 队列：只允许在表的一端（尾）插入，另一端（头）弹出。First In First Out。
- 基本操作：
    - `InitQueue(&Q)`
    - `QueueEmpty(Q)`
    - `EnQueue(&Q, x)`
    - `DeQueue(&Q, &x)`
    - `GetHead(Q, &x)`
- 队列的顺序存储可能存在“假溢出”的情况。
- 不带头结点的链式队列操作上比较麻烦（分配空间），于是常采用带头结点的链式队列，以统一插入与删除操作。
- 用单链表表示的链式队列适合于数据元素变动比较大的情况，而且基本不存在队满溢出的情况。
- 循环单链表表示队列，默认`front`指向对头元素，`rear`指向队尾元素的下一个（空的）元素。
    - `front == rear`表示对空。
    - `front == (rear + 1) % MaxLength`表示队满。
    - 初始化时`front`和`rear`均指向一个空的元素。当插入第一个元素时，`front`值向新插入的元素，`rear`指向新元素的`next`，即一个新建立的空元素。
- 双端队列：两端都可以出队和入队。
    - 输入受限：一端可插可删，另一端只可删。
    - 输出受限：一端可插可删，另一端只可插。

# 3.3

- 主机输出数据给打印机，可以使用队列存放待打印的数据（缓存）。
- 多个用户向同一个CPU发送请求，可以使用队列缓存未计时处理的请求。

# 3.4

- 数组是线性表的推广。2维数组可以视为元素是定长线性表的线性表，以此类推。
- 数组一旦定义，其维数和维界不能被改变。
- 因此除结构的初始化和销毁外，数组只会有元素的存取和修改。
- 压缩存储：指为多个值相同的元素分配一个存储空间，对零元素不分配存储空间。
- 特殊矩阵：指有许多相同的矩阵元素或者零元素，且相同的矩阵元素或者零元素的分布具有一定的规律性。
    - 对称矩阵
    - 上/下三角矩阵
    - 对角矩阵
- 将对称矩阵存放在一维数组中，$a_{i, j}$对应的数组下标为：

$$
k = \left\{
    \begin{aligned}
    &\displaystyle\frac{i(i-1)}{2} + j - 1,\quad \quad  &i\geqslant j\\\\
    &\displaystyle\frac{j(j-1)}{2} + i - 1,\quad \quad  &j> i
    \end{aligned}
\right.
$$

- 假设三角矩阵$A[1\cdots n][1\cdots n]$的上三角区域为同一常量（下三角矩阵）。则下标关系为：

$$
k = \left\{
    \begin{aligned}
    &\displaystyle\frac{i(i-1)}{2} + j - 1,\quad \quad  &i\geqslant j\\\\
    &\displaystyle\frac{n(n+1)}{2},\quad \quad  &j> i
    \end{aligned}
\right.
$$

- 上三角矩阵：

$$
k = \left\{
    \begin{aligned}
    &\displaystyle\frac{(i-1)(2n-i+2)}{2} + j - i,\quad \quad  &i\leqslant j\\\\
    &\displaystyle\frac{n(n+1)}{2},\quad \quad  &i>j
    \end{aligned}
\right.
$$

- 三对角矩阵，也称带状矩阵（$2, 3, 3, 3,\cdots, 3, 2$）。
- 稀疏矩阵的三元组既可以使用数组存储，也可以使用十字链表存储。

```C++
typedef struct OLNode {    
     int  LineNumber, ColumneNumber;    
     ElemType value; 
     struct OLNode *right, *down;               // pointer to next non-zero value
                                                // in same column or row   
}OLNode, *OList;
```

# 串

# 4.2 串的模式匹配

- KMP算法
    - 首先计算出模式串的PM（部分匹配值）表
    - 之后一旦出现不匹配的现象，则：**移动位数 = 已匹配字符数 - 对应字符的PM值**
- 优化：将PM表整体右移一个单位并且整体+1。
    - 于是字串指针的变化公式为：`j = next[j]`
    - `next[j]`：在字串的第`j`个字符与主串发生失配时，则跳到字串的`next[j]`位置重新与主串当前位置进行比较。
- 暴力匹配$O(mn)$，KMP为$O(m+n)$，其最主要的优点是**主串不回溯**。

```C++
void get_next(Sstring T, int next[]) {
    int i = 1, j = 0;
    next[i] = 0;
    while (i < T.length) {
        if (j == 0 || T.ch[i] == T.ch[j]) {
            i++;
            j++;
            next[i] = j;
        }
        else {
            j = next[j];
        }
    }
}
```

- `next[1] = 0`可以理解为将主串的第`i`个元素和模式串的第一个字符前面的空位置对齐，也即模式串后移`1`位，**主串也后移**`1`位，重新比较。

```C++
int KMP(String S, String T, int next[]) {
    int i = 1, j = 1;
    while(i <= S.length && j <= T.length) {
        if (j == 0 || S.ch[i] == T.ch[j]) {
            i++;
            j++;
        }
        else {
            j = next[j];
        }
    }
    if (j > T.length) {
        return i - T.length;
    }
    else {
        return -1;
    }
}
```

- 进一步优化：如果`p[j] == p[next[j]]`，则必然浪费一次递归调用的资源。

```C++
void get_nextval(Sstring T, int nextval[]) {
    int i = 1, j = 0;
    nextval[i] = 0;
    while (i < T.length) {
        if (j == 0 || T.ch[i] == T.ch[j]) {
            i++;
            j++;
            if (T.ch[i] != T.ch[j]) {
                nextval[i] = j;
            }
            else {
                nextval[i] = nextval[j];
            }
        }
        else {
            j = nextval[j];
        }
    }
}
```

- `next`是否`+1`**都正确**
    - 如果模式串的位序从`1`开始，则为了简洁，需要`+1`。
    - 如果模式串的位序从`0`开始，则不需要`+1`。

# 5. 树与二叉树

# 5.1

- 树的定义是递归的，在树的定义中又用到了其自身，树是一种递归的数据结构。是一种逻辑结构也是一种分层结构。
    - 树的根结点没有前驱，除了根结点外的所有结点有且仅有1个前驱。
    - 树中的所有结点可以有0个和多个后继。
- 度$>0$的结点称为分支结点，$=0$的被称为叶子节点。
- 树的结点数等于所有结点的度数之和$+1$。
- 度为$m$的树在第$i$层至少有$m^{i - 1}, \ i \geqslant 1$个结点。
- 高度为$h$的$m$叉树至多有$\displaystyle\frac{m^h - 1}{m - 1}$个结点。
- 具有$n$个结点的$m$叉树的最小高度为$\lceil\log_m[n(m - 1) + 1]  \rceil$。

# 5.2

- 二叉树：每个结点至多只有两棵子树。并且存在左右之分，顺序不能够颠倒。
- 二叉树是有序树，若左右颠倒，则会成为另一颗不同的二叉树。
- 度为$n$的树至少存在$n + 1$个结点（度为0则至少1个结点），但是二叉树可以为空。
- 满二叉树
- 完全二叉树
    - 对完全二叉树按从上到下，从左到右的顺序编号为$1, 2, 3, \cdots, n$（顺序存储）。
        - 当$i > 1$时，结点$i$的双亲结点编号为$\lfloor i/2 \rfloor$。
            - 若$i$为偶数，则双亲编号为$i/2$，他是左孩子。
            - 若$i$为奇数，则双亲编号为$(i-1)/2$，他是右孩子。
        - 若$2i \leqslant n$，则该结点存在左孩子（否则没有），而且左孩子编号为$2i$。
        - 若$(2i+1) \leqslant n$，则该结点存在右孩子（否则没有），而且右孩子编号为$(2i + 1)$。
        - 若存在$n$个结点，则树的高度为：$\lceil  \log_2(n + 1) \rceil\quad\text{or} \quad\lfloor \log_2(n) \rfloor + 1$
- 二叉排序树
- 平衡二叉树
- 二叉树的顺序存储结构建议从数组的下标`1`处开始存储结点，即`Array[1]`为树的根结点，方便计算。

# 5.3

- 中序遍历的非递归算法

```C++
void InOrder(BiTree T) {
    InitStack(S);
    BiTree p = T;
    while (p || !IsEmpty(S)) {
        if (p != NULL) {
            Push(S, p);
            p = p -> lchild;
        }
        else {
            Pop(S, p);
            visit(p);
            p = p->rchild;
        }
    }
}
```

- 前序遍历的非递归算法

```C++
void InOrder(BiTree T) {
    InitStack(S);
    BiTree p = T;
    while (p || !IsEmpty(S)) {
        if (p != NULL) {
            visit(p);
            Push(S, p);
            p = p -> lchild;
        }
        else {
            Pop(S, p);
            p = p->rchild;
        }
    }
}
```

- 层次遍历

```C++
void LevelOrder(BiTree T) {
    InitQueue(Q);
    BiTree p;
    EnQueue(Q, p);
    while (!IsEmpty(Q)) {
        DeQueue(Q, p);
        visit(p);
        if (p -> lchild != NULL) {
            EnQueue(Q, p -> lchild);
        }
        if (p -> rchild != NULL) {
            EnQueue(Q, p -> rchild);
        }
    }
}
```

- 搜索二叉树
    - 二叉链表存储的二叉树假设有$n$个结点，那必然有$n+1$个指针为空（因为只有$n-1$条边）。
    - 不妨用这些指针存放其遍历表中的前驱与后继。
    - 搜索二叉树正是为了加快查找结点前驱与后继的速度。
- 建立中序搜索树

```C++
void InThread(ThreadTree &p, ThreadTree &pre) {
    if (p != NULL) {
        InThread(p -> lchild, pre);
        if (p -> lchild == NULL) {                  // start visit
            p -> lchild = pre;
            p -> ltag = 1;
        }
        if (pre != NULL && pre -> rchild == NULL) {
            pre -> rchild = p;
            pre -> rtag = 1;
        }
        pre = p;                                    // end visit
        InThread(p -> rchild, pre);
    }
}

void CreateInThread(ThreadTree T) {
    ThreadTree pre = NULL;
    if (T != NULL) {
        InThread(T, pre);
        pre -> rchild = NULL;
        pre -> rtag = 1;
    }
}
```

- 中序搜索树的遍历

```C++
ThreadTree *FirstNode(ThreadTree *p) {
    while (p -> ltag == 0) {
        p = p -> lchild;
    }
    return p;
}

ThreadTree *NextNode(ThreadTree *p) {
    if (p -> rtag == 0) {
        return FirstNode(p -> rchild);
    }
    else {
        return p -> rchild;
    }
}

void InOrder(ThreadTree *T) {
    for(ThreadNode *p = FirstNode(T); p != NULL; p = NextNode(p)) {
        visit(p);
    }
}
```

- 后序搜索树查找结点的后继往往比较复杂
    - 根结点：没有后继。
    - 右孩子：则后继为双亲。
    - 左孩子，且双亲没有右孩子：后继为双亲。
    - 左孩子，且双亲有右孩子：后继为右孩子的后序遍历第一个点。

# 5.4

- 树的三种存储结构：
- 双亲表示法
    - 寻找双亲比较简单，但是找孩子时需要遍历整个结构。

```C++
typedef struct {
    ElemType data;
    int parent;
} PTNode;

typedef struct {
    PTNode nodes[MaxSize];
    int n;
} PTree;
```

- 孩子表示法
    - 将每个结点的孩子都按照单链表结构链接起来。
    - 寻找孩子比较简单，但是找双亲时需要遍历整个结构。
- 孩子兄弟表示法（二叉树表示法）

```C++
typedef struct CSNode{
    ElemType data;
    struct CSNode *firstchild, *nextsibling;
} CSNode, *CSTree;
```

- 树转化为二叉树
    - 每个结点的左指针指向其第一个孩子，右指针指向他在树中相邻的右兄弟。
    - 若没有有兄弟，则右指针指向`NULL`。
- 森林转化为二叉树
    - 将森林中的每棵树转化为二叉树。
    - 所有树的根视为兄弟关系，每个根的右指针指向相邻的右根，最左边的根为整个二叉树的根。
- 树的两种遍历
    - 先根遍历：若树非空，先访问根结点，在依次遍历所有子结点。与先序序列相同。
    - 后根遍历：若树非空，则先遍历访问每个子树后再访问根结点。与中序遍历相同。
- 森林的两种遍历
    - 先序遍历
        - 访问第一棵树的根结点。
        - 先序遍历第一棵树中根结点的子树森林。
        - 先序遍历除去第一棵树的森林。
    - 中序遍历
        - 中序遍历第一棵树的根结点的子树森林。
        - 访问第一颗树的根结点。
        - 中序遍历除去第一棵树的森林。

| 树  |森林  |对应的二叉树  |
|:-----:|:-----:|:-----:|
| 先根遍历|先序遍历 |先序遍历 |
| 后根遍历| 中序遍历|中序遍历 |

# 5.5

- 从树根到任意结点的路径长度与该结点的权值乘积，成为该结点的带权路径长度。
- 树的所有的带权路径长度之和为树的带权路径长度。
- 在含有`n`个结点的二叉树中，其中带权路径长度最小的二叉树成为哈夫曼树。
- 在数据通信中，若每一个字符使用相等长度的二进制位表示，则为固定长度编码。
- 若允许不同字符使用不等长二进制位表示，则为可变长度编码。
    - 可以对频率高的字符赋以短编码，频率低的高编码，以减小平均编码，压缩数据。
- 若没有一个编码是另一个编码的前缀，则称这样的编码为前缀编码，可唯一翻译。
- 哈夫曼编码
    - 权值代表频率
    - 向左为0，向右为1
- 并查集
    - `Initial(S)`：将集合`S`中每个元素都初始化为只有一个单元素的子集合。
    - `Union(S, Root1, Root2)`：将`S`中自集合`Root2`并入`Root1`中，要求两个集合互不交互，否则不执行合并。
    - `Find(S, x)`：查找`S`中`x`所在的子集，返回集合的根结点。

```C++
#define SIZE 100

int UFSets[SIZE];

void Initial(int S[]) {
    for (int i = 0; i < size; i++) {
        S[i] = -1;
    }
}

int Find(int S[], int x) {
    while (S[x] >= 0) {
        x = S[x]
    }
    return x;
}

void Union(int S[], int Root1, int Root2) {
    if (Find(S, Root1) == Find(S, Root2)) {
        return;
    }
    else {
        S[Root2] = Root1;
    }
}
```

# 6 图

# 6.1

- 简单图：
    - 不存在重复边。
    - 不存在顶点到自身的边。
- 多重图：
    - 某两个顶点之间的边数大于1条。
    - 同时**又**允许顶点通过一条边和自身关联。
    - 只讨论简单图。
- 完全图：
    - 对于无向图，若存在$\displaystyle\frac{n(n - 1)}{2}$条边，则为完全图。
    - 对于有向图，若存在$n(n - 1)$条边，则为完全图。
- 两个图$G_1 = (V_1, E_1), G_2 = (V_2, E_2)$，若$V_2\in V_1, E_2\in E_1$，则$G_2$是$G_1$的子图。
    - 若$V(G_2) = V(G_1)$，则$G_2$是$G_1$的子生成图。
    - 并非$V, E$的任何子集都可以构成子图，因为这样的子集有**可能不是图**。
- 图中任意两个顶点之间存在路径（即连通），则该图为连通图，否则为非连通图。
    - 无向图中的极大连通子图称为连通分量，
    - 有$n$个顶点的非连通图（无向），最多有$\displaystyle\frac{(n-2)(n - 1)}{2}$条边。
- 在有向图中，若一对顶点之间存在双线的路径，则这两个顶点为强连通。若图中任何一对顶点均在强连通，则为强连通图。
    - 有向图中的极大强连通子图为有向图的强连通分量。
    - 强连通图至少需要$n$条边，连接成一个环路。
- 连通图的生成树是包含图中全部顶点的极小连通子图。若图中含有$n$个顶点，则极小连通子图有$n-1$条边。
    - 在非连通图中，连通分量的生成树构成了非连通图的生成森林。
- 无向图中，顶点的度代表依附于顶点的边数。无向图中全部顶点的度数之和等于图中边数的两倍。
- 有向图中，顶点有入度和出度，顶点的度等于入度和出度之和。有向图中的全部入度之和=出度之和=边数。
